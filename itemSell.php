<?php
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
    $_SESSION['item_sell_success'] = "";
    $_SESSION['item_sell_error'] = "";
	if(count($_POST) > 0)
	{
		$customer =isset($_POST['customer']) ? $_POST['customer'] : "";
		$item =isset($_POST['item']) ? $_POST['item'] : "";
		$qty = isset($_POST['qty']) ? $_POST['qty'] : 0;
		$rate = isset($_POST['rate']) ? $_POST['rate'] : 0;
		$amount = isset($_POST['amt']) ? $_POST['amt'] : 0;
		$sellDate= $_POST['sellYear']."-".$_POST['sellMonth']."-".$_POST['sellDay'];
		$notes = isset($_POST['notes']) ? $_POST['notes'] : "";
		$insertItem  = "INSERT INTO itemsell (itemId,customerId,sellDate,qty,rate,amount,notes)
                                VALUES(".$item.",".$customer.",'".$sellDate."',".$qty.",".$rate.",".$amount.",'".addslashes($notes)."')";
		$insertItemRes = mysql_query($insertItem);
		
		//UPDATE ITEM
		$updateItem="UPDATE item SET qty=(qty-'".$qty."') WHERE itemId=".$item;
		mysql_query($updateItem);
		
		if(!$insertItemRes)
		{
		$_SESSION['item_sell_error'] = "Unable to add details.";
		}
		else
		{
		$_SESSION['item_sell_success'] = "Details successfully added.";
		}
	}
}

	$n=0;
	$itemArray=array();
	$item="SELECT itemId,name,unit FROM item where status='A' ORDER BY name";
	$itemRes=mysql_query($item);
	while($itemRow=mysql_fetch_array($itemRes))
	{
		$itemArray['itemId'][$n]=$itemRow['itemId'];
		$itemArray['name'][$n]=$itemRow['name'];
				
		//get first Item Unit
		if($n==0)
		{
			$unit=$itemRow['unit'];
		}
		$n++;
	}
	if(isset($unit))
	{
		$itemUnit="SELECT * FROM unit WHERE unitId=".$unit;
		$itemUnitRes=mysql_query($itemUnit);
		$itemUnitRow=mysql_fetch_array($itemUnitRes);
		$itemUnitVar=$itemUnitRow['unit'];
	}
	else
	{
		$itemUnitVar='';
	}
	
	$m=0;
	$customerArray=array();
	$customer="SELECT customerId,name FROM customer ORDER BY name";
	$customerRes=mysql_query($customer);
	while($customerRow=mysql_fetch_array($customerRes))
	{
		$customerArray['customerId'][$m]=$customerRow['customerId'];
		$customerArray['name'][$m]=$customerRow['name'];
		$m++;
	}
	
//GET ALL RECORDS TO DISPLAY IN DATATABLE
	$sellArrayTable=array();
	$selectSellItem="SELECT i.itemId,i.name as itemName,s.*,c.customerId,c.name as customerName FROM itemsell s 
						LEFT JOIN item i ON s.itemId=i.itemId
						LEFT JOIN customer c ON s.customerId=c.customerId
						ORDER BY i.name";
	$selectSellItemRes=mysql_query($selectSellItem);
	
	if(mysql_num_rows($selectSellItemRes)>0)
	{
		$t=0;
		while($selectSellItemRow=mysql_fetch_array($selectSellItemRes))
		{
		$sellArrayTable[$t]['customerName']      = $selectSellItemRow['customerName'];
		$sellArrayTable[$t]['itemName']      = $selectSellItemRow['itemName'];
		$sellArrayTable[$t]['sellDate']         = date('d-m-Y',strtotime($selectSellItemRow['sellDate']));
		$sellArrayTable[$t]['qty']    = $selectSellItemRow['qty'];
		$sellArrayTable[$t]['rate']    = $selectSellItemRow['rate'];
		$sellArrayTable[$t]['amount']    = $selectSellItemRow['amount'];
		$sellArrayTable[$t]['notes']   = $selectSellItemRow['notes'];
		$sellArrayTable[$t]['itemSellId']   = $selectSellItemRow['itemSellId'];
		$t++;             
		}
	
	}
	
include("bottom.php");
$smarty->assign("sellArrayTable",$sellArrayTable);
$smarty->assign("customerArray",$customerArray);
$smarty->assign("itemArray",$itemArray);
$smarty->assign("itemUnitVar",$itemUnitVar);
$smarty->assign("item_sell_error",$_SESSION['item_sell_error']);
$smarty->assign("item_sell_success",$_SESSION['item_sell_success']);

$smarty->display("itemSell.tpl");
?>