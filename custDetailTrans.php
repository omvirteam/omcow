<?php
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
	$custId = $_GET['cid'];
	$custArr = array();
	$custQry = "SELECT * FROM `transaction`
	             WHERE customerIdCr = ".$custId."
	                OR customerIdDr = ".$custId."
	             ORDER BY transDate";
	$custQryRes = mysql_query($custQry);
	
	$creditAmt = 0;
	$debitAmt = 0;
	$a = 0;
	if(mysql_num_rows($custQryRes)) {
		while($custRow = mysql_fetch_array($custQryRes))
		{
			if($custRow['customerIdCr'] == $custId){
				$creditAmt += $custRow['amount'];
			}else{
				$debitAmt += $custRow['amount'];
			}
			
			$custArr[$a]['customerNotes'] = $custRow['notes'];
			$custArr[$a]['customerIdCr'] = $custRow['customerIdCr'];
			$custArr[$a]['staffIdCr'] = $custRow['staffIdCr'];
			$custArr[$a]['accountIdCr'] = $custRow['accountIdCr'];
			$custArr[$a]['customerIdDr'] = $custRow['customerIdDr'];
			$custArr[$a]['staffIdDr'] = $custRow['staffIdDr'];
			$custArr[$a]['accountIdDr'] = $custRow['accountIdDr'];
			$custArr[$a]['amount'] = number_format($custRow['amount'],2);
			$custArr[$a]['transDate'] = date("d-m-Y",strtotime($custRow['transDate']));
			$a++;
		}
	}
	
	$customers = array();
	$customerQry = "SELECT * FROM customer where name <> '' order by name";
	$customerQryRes = mysql_query($customerQry);
	$b=0;
	while($customerRow = mysql_fetch_array($customerQryRes))
	{
		$customers[$b]['id'] = $customerRow['customerId'];
		$customers[$b]['name'] = $customerRow['name'];
		$b++;
	}
	
}
include("./bottom.php");
$smarty->assign("balance",number_format($creditAmt - $debitAmt,2));
$smarty->assign("custArr",$custArr);
$smarty->assign("customers",$customers);
$smarty->assign("custId",$custId);
$smarty->display("custDetailTrans.tpl");
?>