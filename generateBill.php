<?php
/*
 *	Generate Bill search options are Distributor wise, customer wise, month and year.
 */   
include("include/conn.inc");
include("include/function.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Om Cow</title>
    <!-- Core CSS - Include with every page -->
    <link type="text/css" href="./css/style.css" rel="stylesheet">
    <link type="text/css" href="./css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="./css/bootstrapValidator.min.css" rel="stylesheet">
    <link type="text/css" href="./font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link type="text/css" href="./css/sb-admin.css" rel="stylesheet">
    
	<!-- Core Scripts - Include with every page -->
    <script type="text/javascript" src="./js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js/bootstrapValidator.min.js"></script>
    <script type="text/javascript" src="./js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <!-- SB Admin Scripts - Include with every page -->
    <script type="text/javascript" src="./js/sb-admin.js"></script>

	<!-- Page-Level Plugin CSS - Tables -->
	<link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

	<!-- Page-Level Plugin Scripts - Tables -->
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$('#staffList').dataTable({
		"aoColumnDefs": [{ 'bSortable': false, 'aTargets': [ 4] }]
		//"aaSorting": [[ 1, "asc" ]]
		});
		
		$('#cmbDistributor').change(function(){
			var staffid = $(this).val();
			$('.loader').show();
			
			$.post('ajax/aj-getcustomerofstaff.php',{staffid:staffid},function(data, success){
				if(success == 'success' && data != ''){
					$('#cmbCustomer').html(data);
				}
				
				$('.loader').hide();
			});
		});
	});
	</script>
</head>
<body>
    <div id="wrapper">
	<?php include("include/header.php"); ?>
	
	<div id="page-wrapper">
<div class="row">
    <div class="col-lg-6 col-md-offset-3">
		
			<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-edit fa-fw"></i>Generate Bill
			</div>
			<div class="panel-body">
				<div class="row col-lg-13">
					<form id="frmGenerateBill" method="POST" action="generateBill.php" class="formBox form-horizontal" role="form">
						<div class="form-group">
							<label class="control-label col-sm-4">Distributor</label>
							<div class="col-sm-5">
								<select name="cmbDistributor" id="cmbDistributor" class="form-control input-sm">
									<option value="All">All</option>
									<?php
									$qrySelStaff = "SELECT * FROM staff WHERE name<>'' ORDER BY name ASC";
									$resSelStaff = mysql_query($qrySelStaff);
									if(mysql_num_rows($resSelStaff)>0)
									{
										while($qFetchStaff = mysql_fetch_array($resSelStaff))
										{
											$selectedStaff = ($_POST["cmbDistributor"]==$qFetchStaff["staffId"])?" selected=\"selected\"":"";
											?>
											<option value="<?php echo $qFetchStaff["staffId"]; ?>"<?php echo $selectedStaff; ?>><?php echo $qFetchStaff["name"]; ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>	
                            <div class="col-sm-1">
                            <img src="images/loader.gif" class="loader" style="display:none" />
                            </div>						
						</div>
						
						<div class="form-group">
							<label class="control-label col-sm-4">Customer</label>
							<div class="col-sm-5">
								<select name="cmbCustomer" id="cmbCustomer" class="form-control input-sm">
									<option value="All">All</option>
									<?php
									$qrySelCustomer = "SELECT * FROM customer WHERE name<>'' ORDER BY name ASC";
									$resSelCustomer = mysql_query($qrySelCustomer);
									if(mysql_num_rows($resSelCustomer)>0)
									{
										while($qFetchCustomer = mysql_fetch_array($resSelCustomer))
										{
											$selectedCustomer = ($_POST["cmbCustomer"]==$qFetchCustomer["customerId"])?" selected=\"selected\"":"";
											?>
											<option value="<?php echo $qFetchCustomer["customerId"]; ?>"<?php echo $selectedCustomer; ?>><?php echo $qFetchCustomer["name"]; ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						</div>
						
						<div class="form-group">
								<label class="control-label col-sm-4">Month</label>
								<div class="col-sm-8">
									<select class="form-control date-controls pull-left input-sm" name="cmbFromMonth">
										<?php
										for($fromMonth=1; $fromMonth<=12; $fromMonth++)
										{
											$monthName = date("F", mktime(0, 0, 0, $fromMonth, 10));
											$fromMonth = ($fromMonth<10)?"0".$fromMonth:$fromMonth;
											if(isset($_POST["cmbFromMonth"]))
											{
												$selectedFromMonth = ($fromMonth==$_POST["cmbFromMonth"])?" selected=\"selected\"":"";
											}
											else
											{
												$selectedFromMonth = ($fromMonth==date('m'))?" selected=\"selected\"":"";
											}
											
											?>
											<option value="<?php echo $fromMonth; ?>"<?php echo $selectedFromMonth; ?>><?php echo $monthName; ?></option>
											<?php
										}
										?>
									</select>
								</div>
						</div>						
						
						<div class="form-group">
								<label class="control-label col-sm-4">Year</label>
								<div class="col-sm-8">
									<select class="form-control date-controls pull-left input-sm" name="cmbFromYear">
										<?php
										for($fromYear=date('Y'); $fromYear>=2000; $fromYear--)
										{
											$selectedFromYear = ($_POST["cmbFromYear"]==$fromYear)?" selected=\"selected\"":"";
											?>
											<option value="<?php echo $fromYear; ?>"<?php echo $selectedFromYear; ?>><?php echo $fromYear; ?></option>
											<?php
										}
										?>
									</select>
								</div>
						</div>	
						<div class="form-group">
							<label class="control-label col-sm-4"></label>
								<div class="col-sm-7">
								<button name="btnGenerateBill" class="btn btn-success btn-lg" type="submit">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
	    <!-- /.panel-body -->
		</div>
	<!-- /.panel -->
	</div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<?php
if(isset($_POST["btnGenerateBill"]))
{
	$postDistributor = $_POST["cmbDistributor"];
	$postCustomer = $_POST["cmbCustomer"];
	$postMonth = $_POST["cmbFromMonth"];
	$postYear = $_POST["cmbFromYear"];
	$monthName = date("F", mktime(0, 0, 0, $postMonth, 10));
	?>
	<!-- Display Records-->
	<div class="row">
		<div class="col-lg-10 col-md-offset-1">
		<div class="panel panel-primary">
			<div class="panel-heading">
			<i class="fa fa-list-ul fa-fw"></i> Generate Bill (<?php echo $monthName; ?> <?php echo $postYear; ?>) 
			</div>
			<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="staffList">
				<thead>
					<tr>
					<th align="center">Customer Name</th>
					<th align="center">Bill Date</th>
					<th align="center">Bill Amount</th>
					<th align="center">Distributor Name</th>
					<th align="center">Action</th> 
					</tr>
				</thead>
				<tbody>
				<?php
					if($postDistributor=='All' && $postCustomer=='All')
					{
						$whereClause = "";
					}
					else if($postDistributor!='All' && $postCustomer=='All')
					{
						$whereClause = " AND customer.distStaffIdId = '".$postDistributor."'";
					}
					else if($postDistributor=='All' && $postCustomer!='All')
					{
						$whereClause = " AND customer.customerId = '".$postCustomer."'";
					}
					else if($postDistributor!='All' && $postCustomer!='All')
					{
						$whereClause = " AND customer.customerId = '".$postCustomer."'
						AND customer.distStaffIdId = '".$postDistributor."'";
					}
					
					$qrySelBill = "SELECT SUM( milksell.rate * milksell.milkGiven) as totalMilkAmount, cron_bill.created_date, cron_bill.cron_month,cron_bill.cron_year,cron_bill.customer_id, customer.name, customer.distStaffIdId
					FROM milksell, cron_bill, customer
					WHERE milksell.customerId = cron_bill.customer_id
					AND cron_bill.customer_id  = customer.customerId
					AND milksell.billCreated = 'y'
					 ".$whereClause."
					AND cron_bill.cron_month = '".$postMonth."' AND cron_bill.cron_year = '".$postYear."'
					GROUP BY milksell.customerId ASC";
					$resSelBill = mysql_query($qrySelBill) or die(" error in sql query ");
					
					if(mysql_num_rows($resSelBill)>0)
					{
						while($qFetchBill = mysql_fetch_array($resSelBill))
						{
							$totalMilkAmount = $qFetchBill["totalMilkAmount"];
							$countMilkSell = $totalMilkAmount;
							$billCreatedDate = $qFetchBill["created_date"];
							$customerName = $qFetchBill["name"];
							$staffId = $qFetchBill["distStaffIdId"];
							$customerId = $qFetchBill["customer_id"];
							$cronMonth = $qFetchBill["cron_month"];
							$cronYear = $qFetchBill["cron_year"];
							
							$distributorName = getFieldValue("staff","name","staffId='".$staffId."'");
							
							$itemWhereClause = "customerId='".$customerId."' AND MONTH(sellDate) = '".$cronMonth."' AND  YEAR(sellDate)='".$cronYear."'";
							$totalExtraAmount = getItemSellAmountTotal($itemWhereClause);
							
							$totalBillAmount = $countMilkSell+$totalExtraAmount;
							?>
							<tr>
								<td><?php echo $customerName; ?></td>
								<td><?php echo $billCreatedDate; ?></td>
								<td><?php echo $totalBillAmount; ?></td>
								<td><?php echo $distributorName; ?></td>
								<td><a href="generatePdf.php?cid=<?php echo $customerId; ?>&month=<?php echo $cronMonth; ?>&year=<?php echo $cronYear; ?>" target="_black"><img src="images/print.png" alt="Print" title="Print Bill" width="20"></a></td>
							</tr>
							<?php
						}
					}
				?>
				</tbody>
				</table>
			</div>
			</div>
		</div>
		</div>
	</div>
<?php
}
?>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
</body>
</html>