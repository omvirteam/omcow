<?php
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
    $_SESSION['staff_entry_success'] = "";
    $_SESSION['staff_entry_error'] = "";
    
  if(count($_POST) > 0)
  {
    $staffTypeId     = isset($_POST['staffTypeId']) ? $_POST['staffTypeId'] : 0;
    $placeId         = isset($_POST['placeId']) ? $_POST['placeId'] : 0;
    $userName        = isset($_POST['userName']) ? $_POST['userName'] : "";
    $pass            = isset($_POST['pass']) ? $_POST['pass'] : "";
    $parentStaffId   = isset($_POST['parentStaffId']) ? $_POST['parentStaffId'] : 0;
    $commission      = (isset($_POST['commission']) &&  $_POST['commission']) ? $_POST['commission'] : 0;
    $salary          = (isset($_POST['salary']) && $_POST['salary'] != '') ? $_POST['salary'] : 0;
    $name            = isset($_POST['name']) ? $_POST['name'] : "";
    $phone           = isset($_POST['phone']) ? $_POST['phone'] : "";
    $joinDate        = $_POST['joinDateYear']."-".$_POST['joinDateMonth']."-".$_POST['joinDateDay'];
  	
    $insertStaff  = "INSERT INTO staff (staffTypeId,placeId,userName,pass,parentStaffId,name,phone,joinDate,commission,salary)
                                VALUES(".$staffTypeId.",".$placeId.",'".$userName."','".$pass."',".$parentStaffId.",
                                       '".$name."','".$phone."','".$joinDate."',".$commission.",".$salary.")";
    $insertStaffRes = mysql_query($insertStaff);
    if(!$insertStaffRes)
    {
    	$_SESSION['staff_entry_error'] = "Unable to add details of <b>" . $name . '</b>.';
    }
    else
    {
	$_SESSION['staff_entry_success'] = "Details of <b>" . $name . '</b> successfully added.';
    }
  }
  
  $k=0;
  $staffArray = array();
  
  $staffType = "SELECT staffTypeId,staffType
                  FROM stafftype ORDER BY staffType";
  $staffTypeRes = mysql_query($staffType);
  while($staffRow = mysql_fetch_array($staffTypeRes))
  {
  	$staffArray['staffTypeId'][$k] = $staffRow['staffTypeId'];
  	$staffArray['staffType'][$k]   = $staffRow['staffType'];
  	$k++;
  }
  
  $l=0;
  $placeArray = array();
  
  $place = "SELECT placeId,place
                  FROM place ORDER BY place";
  $placeRes = mysql_query($place);
  while($placeRow = mysql_fetch_array($placeRes))
  {
  	$placeArray['placeId'][$l] = $placeRow['placeId'];
  	$placeArray['place'][$l]   = $placeRow['place'];
  	$l++;
  }
  
  $m=0;
  $parentStaff = array();
  
  $parent = "SELECT staffId,name
                  FROM staff ORDER BY name";
  $parentRes = mysql_query($parent);
  while($parentRow = mysql_fetch_array($parentRes))
  {
  	$parentStaff['staffId'][$m] = $parentRow['staffId'];
  	$parentStaff['name'][$m]    = $parentRow['name'];
  	$m++;
  }
}
include("bottom.php");
$smarty->assign("staffArray",$staffArray);
$smarty->assign("placeArray",$placeArray);
$smarty->assign("parentStaff",$parentStaff);
$smarty->assign("staff_entry_error",$_SESSION['staff_entry_error']);
$smarty->assign("staff_entry_success",$_SESSION['staff_entry_success']);
$smarty->display("staff.tpl");
?>