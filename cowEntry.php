<?php
include('include/config.inc.php');

if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
    $_SESSION['cow_entry_success'] = "";
    $_SESSION['cow_entry_error'] = "";
    
    
  if(count($_POST) > 0)
  {
  	$name          = isset($_POST['name']) ? $_POST['name'] : "";
  	$grn           = isset($_POST['grn']) ? $_POST['grn'] : "";
  	$cowTypeId     = isset($_POST['cowTypeId']) ? $_POST['cowTypeId'] : 0;
  	$motherId      = isset($_POST['motherId']) ? $_POST['motherId'] : 0;
  	$fatherId      = isset($_POST['fatherId']) ? $_POST['fatherId'] : 0;
  	$dhankhutId    = isset($_POST['dhankhutId']) ? $_POST['dhankhutId'] : 0;
  	$milkPerDay    = (isset($_POST['milkPerDay']) && $_POST['milkPerDay'] != "") ? $_POST['milkPerDay'] : 0;
  	$purchaseRate  = (isset($_POST['purchaseRate']) && $_POST['purchaseRate'] != "") ? $_POST['purchaseRate'] : 0;
  	$purchasedFrom = (isset($_POST['purchasedFrom']) && $_POST['purchasedFrom']) ? $_POST['purchasedFrom'] : NULL;
  	$isDied        = isset($_POST['isDied']) ? $_POST['isDied'] : 0;
  	$notes         = isset($_POST['notes']) ? $_POST['notes'] : "";
  	$staffId         = isset($_POST['staffId']) ? $_POST['staffId'] : 0;
  	$birthDate     = $_POST['birthDateYear']."-".$_POST['birthDateMonth']."-".$_POST['birthDateDay'];
	
	if($_POST['purchaseDateDay'] == '' || $_POST['purchaseDateMonth'] == '' || $_POST['purchaseDateYear'] == ''){
	    $purchaseDate  = "0000-00-00";
	} else {
	    $purchaseDate  = $_POST['purchaseDateYear']."-".$_POST['purchaseDateMonth']."-".$_POST['purchaseDateDay'];
	}
	
	if($_POST['dwarningDateDay'] == '' || $_POST['dwarningDateMonth'] == '' || $_POST['dwarningDateYear'] == ''){
	    $dwarningDate  = "0000-00-00";
	} else {
	    $dwarningDate  = $_POST['dwarningDateYear']."-".$_POST['dwarningDateMonth']."-".$_POST['dwarningDateDay'];
	}
	
	if($_POST['milkCloseDateDay'] == '' || $_POST['purchaseDateMonth'] == '' || $_POST['milkCloseDateYear'] == ''){
	    $milkCloseDate  = "0000-00-00";
	} else {
	    $milkCloseDate = $_POST['milkCloseDateYear']."-".$_POST['milkCloseDateMonth']."-".$_POST['milkCloseDateDay'];
	}
  	
	$insertCow  = "INSERT INTO cow(name,grn,cowTypeId,motherId,fatherId,dhankhutId,milkPerDay,purchaseRate,
                                       birthDate,purchasedfrom,purchaseDate,dwarningDate,milkCloseDate,staffId,
                                       isDied,notes)
                                VALUES('".$name."','".$grn."',".$cowTypeId.",".$motherId.",".$fatherId.",
                                       ".$dhankhutId.",".$milkPerDay.",".$purchaseRate.",'".$birthDate."',
                                       '".$purchasedFrom."','".$purchaseDate."','".$dwarningDate."',
                                       '".$milkCloseDate."',".$staffId.",".$isDied.",'".$notes."')";
    $insertCowRes = mysql_query($insertCow);
    if(!$insertCowRes)
    {
    	$_SESSION['cow_entry_error'] = "Unable to add details of <b>" . $name . '</b>.';
    }
    else
    {
	$_SESSION['cow_entry_success'] = "Details of <b>" . $name . '</b> successfully added.';
    }
  }
  
  $k=0;
  $cowArray = array();
  
  $cowType = "SELECT cowTypeId,type
                  FROM cowtype ORDER BY type";
  $cowTypeRes = mysql_query($cowType);
  while($usertTypeRow = mysql_fetch_array($cowTypeRes))
  {
  	$cowArray['cowTypeId'][$k]  = $usertTypeRow['cowTypeId'];
  	$cowArray['type'][$k]       = $usertTypeRow['type'];
  	$k++;
  }
  
  $l=0;
  $motherArray = array();
  $motherName = "SELECT cowId,name
                  FROM cow
                 WHERE cowTypeId = 1 ORDER BY name";
  $motherNameRes = mysql_query($motherName);
  while($motherRow = mysql_fetch_array($motherNameRes))
  {
  	$motherArray['cowId'][$l]  = $motherRow['cowId'];
  	$motherArray['name'][$l]   = $motherRow['name'];
  	$l++;
  }
  
  $j=0;
  $fatherArray = array();
  $FatherName = "SELECT cowId,name
                  FROM cow
                 WHERE cowTypeId = 2 ORDER BY name";
  $FatherNameRes = mysql_query($FatherName);
  while($fatherRow = mysql_fetch_array($FatherNameRes))
  {
  	$fatherArray['cowId'][$j]  = $fatherRow['cowId'];
  	$fatherArray['name'][$j]   = $fatherRow['name'];
  	$j++;
  }
  
  $o=0;
  $staffArray = array();
  $staffName = "SELECT staffId,name
                  FROM staff
                 WHERE staffTypeId = 3
              ORDER BY name";
  $staffNameRes = mysql_query($staffName);
  while($staffRow = mysql_fetch_array($staffNameRes))
  {
  	$staffArray['staffId'][$o]  = $staffRow['staffId'];
  	$staffArray['name'][$o]     = $staffRow['name'];
  	$o++;
  }
  
  
  $isDiedValues[0] = 0;
  $isDiedOutput[0] = "No";
  $isDiedValues[1] = 1;
  $isDiedOutput[1] = "Yes";
  
}
include("bottom.php");
$smarty->assign("isDiedValues",$isDiedValues);
$smarty->assign("isDiedOutput",$isDiedOutput);
$smarty->assign("fatherArray",$fatherArray);
$smarty->assign("staffArray",$staffArray);
$smarty->assign("motherArray",$motherArray);
$smarty->assign("cowArray",$cowArray);
$smarty->assign("cow_entry_error",$_SESSION['cow_entry_error']);
$smarty->assign("cow_entry_success",$_SESSION['cow_entry_success']);
$smarty->display("cowEntry.tpl");
?>