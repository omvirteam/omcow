<?php
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
  $milkTime = isset($_REQUEST['milkTime']) ? $_REQUEST['milkTime'] : 'M';
  $distStaffIdId = isset($_REQUEST['staffId']) ? $_REQUEST['staffId'] : 0;
  $distStaffIdName = isset($_REQUEST['staffName']) ? $_REQUEST['staffName'] : "";
  if(isset($_REQUEST['submitBtn']))
  {
    $milkDate = $_POST['milkDate'];
    $milktime = isset($_POST['milkTime']) ? $_POST['milkTime'] : '';
    $i = 0;
    while($i < count($_REQUEST['customerId']))
    {
      $customerId = $_POST['customerId'][$i];
      $rate = isset($_POST['rate'][$i]) && $_POST['rate'][$i] != '' ? $_POST['rate'][$i] : 0;
      $milkGiven = isset($_POST['milkGiven'][$i]) && $_POST['milkGiven'][$i] != '' ? $_POST['milkGiven'][$i] : 0;
      $customerMilkTime = isset($_POST['customerMilkTime'][$i]) && $_POST['customerMilkTime'][$i] != '' ? $_POST['customerMilkTime'][$i] : 0;
      $nextDate = ($_POST['nextDate'][$i] != '') ? $_POST['nextDate'][$i * 3 + 2]['Year']."-".$_POST['nextDate'][$i * 3 + 1]['Month']."-".$_POST['nextDate'][$i * 3]['Day'] : 0;
      $nextLitre = isset($_POST['nextLitre'][$i]) && $_POST['nextLitre'][$i] != '' ? $_POST['nextLitre'][$i] : 0;


      $updateQry = "update milksell set milkSellDate='".$milkDate."',milkTime='".$milktime."',
                   milkGiven=".$milkGiven.",customerMilkTime='".$customerMilkTime."',nextDate='".$nextDate."',nextLitre=".$nextLitre.",
                 rate=".$rate." where  customerId=".$customerId." and milkSellDate='".$milkDate."'";
      mysql_query($updateQry);

      $i++;
    }
    header("Location:milkSell.php?milkTime=".$milkTime."&staffId=".$distStaffIdId."&milkDate=".$milkDate);
    exit();
  }
  else
  {
    if(isset($_POST['topFormSubmitted']))
    {
      $milkDate = $_POST['milkDateYear']."-".$_POST['milkDateMonth']."-".$_POST['milkDateDay'];
      header("Location:milkSell.php?milkTime=".$milkTime."&staffId=".$distStaffIdId."&milkDate=".$milkDate);
      exit();
    }
    else
    {
      $milkDate = $_GET['milkDate'];
    }

    $j = 0;
    $custArrLoop = array();
    $custArr = array();
    $custMonthlyArr = array();
    $dateArray = array();
    $grandTotLitre = 0;

    $selcustomer = "SELECT customerId,name,nickName,grnNo,rate
                      FROM customer
                     WHERE (milkTime='".$milkTime."' OR milkTime='B')";
    if($distStaffIdId > 0)
    {
      $selStaffName = "SELECT name 
                         FROM staff 
                        WHERE staffId = ".$distStaffIdId;
      $selStaffNameRes = mysql_query($selStaffName);
      if($staffData = mysql_fetch_array($selStaffNameRes))
      {
        $distStaffIdName = $staffData['name'];
      }
      else
      {
        $distStaffIdName = "";
      }
      $selcustomer .= " AND distStaffIdId = ".$distStaffIdId;
    }

    if($milkTime == "M")
    {
      $selcustomer .= " ORDER BY distStaffIdId,morningSequence";
    }
    else
    {
      $selcustomer .= " ORDER BY distStaffIdId,eveningSequence";
    }

    $selcustomerRes = mysql_query($selcustomer);
    while($custRow = mysql_fetch_array($selcustomerRes))
    {
      //check for record exist or not:Start
      $sqlCheck = "SELECT * FROM milksell 
		            WHERE milkSellDate = '".$milkDate."' 
                  AND milkTime     = '".$milkTime."'
					        AND customerId   = ".$custRow['customerId'];
      $sqlCheck = mysql_query($sqlCheck);
      if(!mysql_num_rows($sqlCheck) > 0)
      {
        $insSellQry = "INSERT INTO milksell (milkSellDate,milkTime,customerId,milkGiven,customerMilkTime,nextDate,nextLitre)
    	               VALUES ('".$milkDate."','".$milkTime."',".$custRow['customerId'].",0,'".$milkTime."',
                             '".date('Y-m-d', strtotime($milkDate.'+1 day'))."','0')";
        $insSellQryRes = mysql_query($insSellQry);
      }
      //check for record exist or not:End

      $i = 1;
      $k = 0;
      $totLitre = 0;
      while($i <= date('d'))
      {
        if(!isset($dateArray[$k]))
        {
          $dateArray[$k] = 0; //only on 1st day we want to set 0, on other days, we want to increase that day's value
        }
        $selMonthly = "SELECT customer.name,customer.nickName,
                              milksell.milkSellDate,milksell.milkGiven,milksell.customerMilkTime
		                     FROM milksell
		                LEFT JOIN customer ON customer.customerId = milksell.customerId
		                    WHERE milksell.milkSellDate = '".date('Y-m-'.$i.'')."'
		                      AND milksell.milkTime     = '".$milkTime."'
		                      AND milksell.customerId   = ".$custRow['customerId']."
		                 ORDER BY customer.name";
        $selMonthlyRes = mysql_query($selMonthly);
        if($dataRow = mysql_fetch_array($selMonthlyRes))
        {
          $custMonthlyArr[$j][$k]['milkSellDate'] = $dataRow['milkSellDate'];
          $custMonthlyArr[$j][$k]['milkGiven'] = $dataRow['milkGiven'];
          $totLitre      += $dataRow['milkGiven'];
          $dateArray[$k] += $dataRow['milkGiven'];
          $grandTotLitre += $dataRow['milkGiven'];
        }
        else
        {
          $custMonthlyArr[$j][$k]['milkSellDate'] = date('Y-m-'.$i.'');
          $custMonthlyArr[$j][$k]['milkGiven'] = 0;
        }
        $i++;
        $k++;
      }

      $custArrLoop[$j]['totLitre'] = $totLitre;
      $custArrLoop[$j]['customerId'] = $custRow['customerId'];
      $custArrLoop[$j]['name'] = $custRow['name'] ." : ". $custRow['nickName'] ." : ".$custRow['grnNo'];
      $custArrLoop[$j]['rate'] = $custRow['rate'];
      $custArr['customerId'][$j]  = $custRow['customerId'];
      $custArr['name'][$j]     = $custRow['name'] .":". $custRow['nickName'];
      $sqlSelect = "SELECT * FROM milksell "
                 . " WHERE customerId   = ".$custRow['customerId']
                 . "   AND milkSellDate = '".$milkDate."'";
      $sqlSelectRes = mysql_query($sqlSelect);
      if(mysql_num_rows($sqlSelectRes) > 0)
      {
        $sqlSelectRow = mysql_fetch_array($sqlSelectRes);
        $custArrLoop[$j]['milkGiven'] = $sqlSelectRow['milkGiven'];
      }
      else
      {
        $custArrLoop[$j]['milkGiven'] = 0;
      }
      $j++;
    }

    include("./bottom.php");
    $smarty->assign('custArrLoop', $custArrLoop);
    $smarty->assign('distStaffIdId', $distStaffIdId);
    $smarty->assign('distStaffIdName', $distStaffIdName);
    $smarty->assign('custMonthlyArr', $custMonthlyArr);
    $smarty->assign('custArr', $custArr);
    $smarty->assign('dateArray', $dateArray);
    $smarty->assign('milkTime', $milkTime);
    $smarty->assign('tomorrow', strtotime('+1 day'));
    $smarty->assign('grandTotLitre', $grandTotLitre);
    $smarty->display('milkSell.tpl');
  }
}
?>