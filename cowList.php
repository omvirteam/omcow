<?php
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
	$cowArray = array();
	$i = 0;
  $selectCow = "SELECT master.cowId,master.name,master.grn,type,master.fatherId,master.dhankhutId,master.milkPerDay,
                       master.purchaseRate,master.purchasedFrom,master.isDied,master.notes,father.name AS fatherName,
                       mother.name AS motherName,dhankhut.name AS dhankhutName,master.cowTypeId
	                 FROM  cow AS master
	               LEFT JOIN cowtype ON cowtype.cowTypeId = master.cowTypeId
	               LEFT JOIN cow AS father ON father.cowId = master.fatherId
	               LEFT JOIN cow AS mother ON mother.cowId = master.motherId
	               LEFT JOIN cow AS dhankhut ON dhankhut.cowId = master.dhankhutId
	                ORDER BY name";
	$selectCowRes = mysql_query($selectCow);
	while($cowRow = mysql_fetch_array($selectCowRes))
	{
		$cowArray[$i]['cowId']         = $cowRow['cowId'];
		$cowArray[$i]['name']          = $cowRow['name'];
		$cowArray[$i]['grn']           = $cowRow['grn'];
		$cowArray[$i]['type']          = $cowRow['type'];
		$cowArray[$i]['fatherName']    = $cowRow['fatherName'];
		$cowArray[$i]['motherName']    = $cowRow['motherName'];
		$cowArray[$i]['dhankhutName']  = $cowRow['dhankhutName'];
		$cowArray[$i]['milkPerDay']    = $cowRow['milkPerDay'];
		$cowArray[$i]['purchaseRate']  = $cowRow['purchaseRate'];
		$cowArray[$i]['purchasedFrom'] = $cowRow['purchasedFrom'];
		$cowArray[$i]['isDied']        = $cowRow['isDied'];
		$cowArray[$i]['notes']         = $cowRow['notes'];
		$cowArray[$i]['cowTypeId']     = $cowRow['cowTypeId'];
		$i++;                                      
	}
  
}
include("./bottom.php");
$smarty->assign("cowArray",$cowArray);
$smarty->display("cowList.tpl");
?>