<?php
include("include/conn.inc");
include("include/function.php");

if((isset($_GET["cid"]) && $_GET["cid"]>0) && (isset($_GET["month"]) && $_GET["month"]>0) && (isset($_GET["year"]) && $_GET["year"]>0))
{
	$getCustomerId = $_GET["cid"];
	$getMonth = $_GET["month"];
	$getYear = $_GET["year"];
	$billFor = $_GET["month"]." ".$_GET["year"];
	$pdfFileName = "bill_".$getCustomerId."_".$getYear."_".$getMonth.".pdf";
	
	if(!file_exists("uploads/pdf/".$getYear."_".$getMonth))
	{
		mkdir("uploads/pdf/".$getYear."_".$getMonth,0777);
	}
		
	$qrySelCustomer = "SELECT * FROM customer WHERE customerId='".$getCustomerId."'";
	$resSelCustomer = mysql_query($qrySelCustomer);
	$qFetchCustomer = mysql_fetch_array($resSelCustomer);
	$customerName = $qFetchCustomer["name"];
	$customerNickName = $qFetchCustomer["nickName"];
	$customerGRN = $qFetchCustomer["grnNo"];
	$address = $qFetchCustomer['society'].",".$qFetchCustomer['appartment'].",".$qFetchCustomer['city'];
	
	$getDistributorName = getFieldValue("staff","name"," staffId='".$qFetchCustomer["distStaffIdId"]."'");
	$getDistributorPhone = getFieldValue("staff","phone"," staffId='".$qFetchCustomer["distStaffIdId"]."'");
	
	$qrySelCronBill = "SELECT * FROM cron_bill WHERE customer_id='".$getCustomerId."' AND cron_month='".$getMonth."' AND cron_year='".$getYear."'";
	$resSelCronBill = mysql_query($qrySelCronBill);
	$qFetchCronBill = mysql_fetch_array($resSelCronBill);
	$billCreatedDate = date('d/m/Y',strtotime($qFetchCronBill["created_date"]));
	
	$billHtml = '';
	
	$billHtml .= '<html>';
		$billHtml .= '<head>';
		$billHtml .= '<style type="text/css">';
		$billHtml .= '.billList td {margin-left: 50px;border: 1px solid #d7d7d7;font-size: 10px;margin: 10pt;}';
		$billHtml .= '.billList th {background-color: #F5F5F5;border: 1px solid #d7d7d7;}';
		$billHtml .= '</style>';
		$billHtml .= '</head>';
		$billHtml .= '<body>';
			$billHtml .= '<table border="0" width="100%" cellpadding="0" cellspacing="0">';
				$billHtml .= '<tr>';
				  $billHtml .= '<td></td><td style="font-size: 15px;font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gir Ganga Milk</td><td></td>';
				$billHtml .= '</tr>';
				$billHtml .= '<tr>';
				  $billHtml .= '<td></td><td style="font-size: 13px;">Gopi Gau Gurukul, Vodisang</td><td></td>';
				$billHtml .= '</tr>';
				$billHtml .= '<tr>';
					$billHtml .= '<td><strong>Name : </strong>'.$customerName.'</td>';
					$billHtml .= '<td></td>';
					$billHtml .= '<td><strong>GRN: </strong>'.$customerGRN.'</td>';
				$billHtml .= '</tr>';
				$billHtml .= '<tr>';
					$billHtml .= '<td><strong>Address: </strong>'.$address.'</td>';
					$billHtml .= '<td></td>';
					$billHtml .= '<td><strong>Bill Date: </strong>'.$billCreatedDate.'</td>';
				$billHtml .= '</tr>';
				$billHtml .= '<tr>';
					$billHtml .= '<td><strong>Nick name: </strong>'.$customerNickName.'</td>';
					$billHtml .= '<td></td>';
					$billHtml .= '<td><strong>Bill for: </strong>'.$billFor.'</td>';
				$billHtml .= '</tr>';
				$billHtml .= '<tr>';
				  $billHtml .= '<td><strong>Distributor: </strong> '.$getDistributorName.'</td>';
				  $billHtml .= '<td></td>';
				  $billHtml .= '<td><strong>Distributor Mob.: </strong>'.$getDistributorPhone.'</td>';
				$billHtml .= '</tr>';
				$billHtml .= '<tr>';
				  $billHtml .= '<td></td>';
				  $billHtml .= '<td>Customer Care : 94292 71369</td>';
				  $billHtml .= '<td></td>';
				$billHtml .= '</tr>';
				$billHtml .= '</table>';
				$billHtml .= '<br /><br />';
				$billHtml .= '<table border="0" width="100%" class="billList" cellpadding="5" cellspacing="0">';
				$billHtml .= '<tr>';
					$billHtml .= '<th width="15%" style="margin-left: 15px;">Date</th>';
					$billHtml .= '<th width="45%">Title</th>';
					$billHtml .= '<th width="15%"align="right">Rate</th>';
					$billHtml .= '<th width="15%" align="right">Total Pouch</th>';
					$billHtml .= '<th width="10%" align="right">Amount</th>';
				$billHtml .= '</tr>';
				
				$totalBillAmount = 0;
				
				$qrySelMilkSell = "SELECT SUM(milkGiven) as totalPouch, milkSellDate,rate FROM milksell WHERE customerId='".$getCustomerId."' AND MONTH(milkSellDate)='".$getMonth."' AND YEAR(milkSellDate)='".$getYear."' AND billCreated='y' GROUP BY rate ASC ORDER BY milkSellId ASC";
				$resSelMilkSell = mysql_query($qrySelMilkSell);
				
				if(mysql_num_rows($resSelMilkSell)>0)
				{
					while($qFetchMilkSell = mysql_fetch_array($resSelMilkSell))
					{
						$milkTotalAmount = $qFetchMilkSell["rate"]*$qFetchMilkSell["totalPouch"];
						$totalBillAmount += $milkTotalAmount;
						
						$billHtml .= '<tr>';
							$billHtml .= '<td>&nbsp;</td>';
							$billHtml .= '<td>Milk</td>';
							$billHtml .= '<td align="right">'.$qFetchMilkSell["rate"].'</td>';
							$billHtml .= '<td align="right">'.$qFetchMilkSell["totalPouch"].'</td>';
							$billHtml .= '<td align="right">'.$milkTotalAmount.'</td>';
						$billHtml .= '</tr>';
						
					}
				}
				
				$qrySelItemSell = "SELECT * FROM itemsell WHERE customerId='".$getCustomerId."' AND MONTH(sellDate)='".$getMonth."' AND YEAR(sellDate)='".$getYear."' AND billCreated='y' ORDER BY itemSellId ASC";
				$resSelItemSell = mysql_query($qrySelItemSell);
				if(mysql_num_rows($resSelItemSell)>0)
				{
					while($qFetchItemSell = mysql_fetch_array($resSelItemSell))
					{
						$getItemName = getFieldValue("item","name"," itemId='".$qFetchItemSell["itemId"]."'");
						$totalBillAmount += $qFetchItemSell["amount"];
						$billHtml .= '<tr>';
						$billHtml .= '<td>'.date('d/m/Y',strtotime($qFetchItemSell["sellDate"])).'</td>';
						$billHtml .= '<td>'.$getItemName.'</td>';
						$billHtml .= '<td align="right">'.$qFetchItemSell["rate"].'</td>';
						$billHtml .= '<td align="right">'.$qFetchItemSell["qty"].'</td>';
						$billHtml .= '<td align="right">'.$qFetchItemSell["amount"].'</td>';
						$billHtml .= '</tr>';
					}
				}
				
				if($totalBillAmount>0)
				{
					$billHtml .= '<tr>';
					$billHtml .= '<td>&nbsp;</td>';
					$billHtml .= '<td>&nbsp;</td>';
					$billHtml .= '<td>&nbsp;</td>';
					$billHtml .= '<td align="right">Total : </td>';
					$billHtml .= '<td align="right">'.$totalBillAmount.'</td>';
					$billHtml .= '</tr>';
				}
				
				$qrySelMilkSellRoute = "SELECT customerMilkTime FROM milksell WHERE customerId='".$getCustomerId."' AND MONTH(milkSellDate)='".$getMonth."' AND YEAR(milkSellDate)='".$getYear."' AND billCreated='y' GROUP BY  customerMilkTime ASC ORDER BY milkSellId ASC";
				$resSelMilkSellRoute = mysql_query($qrySelMilkSellRoute);
				if(mysql_num_rows($resSelMilkSellRoute)>0)
				{
					$milkRoute = "";
					while($qFetchSellRoute = mysql_fetch_array($resSelMilkSellRoute))
					{
						if($qFetchSellRoute["customerMilkTime"]=='E')
						{
							$milkRoute .= "Evening / ";
						}
						if($qFetchSellRoute["customerMilkTime"]=='A')
						{
							$milkRoute .= "Afternoon / ";
						}
						if($qFetchSellRoute["customerMilkTime"]=='M')
						{
							$milkRoute .= "Morning / ";
						}
						if($qFetchSellRoute["customerMilkTime"]=='B')
						{
							$milkRoute .= "Both / ";
						}
						$milkRoute = trim($milkRoute," / ");
					}
					$billHtml .= '<tr>';
					$billHtml .= '<td>&nbsp;</td>';
					$billHtml .= '<td><strong>Route: </strong> '.$milkRoute.'</td>';
					$billHtml .= '<td>&nbsp;</td>';
					//$billHtml .= '<td><strong>Distributor: </strong> '.$getDistributorName.'</td>';
					$billHtml .= '<td>&nbsp;</td>';
					$billHtml .= '<td>&nbsp;</td>';
					$billHtml .= '</tr>';
				}
				
					$billHtml .= '<tr>';
					$billHtml .= '<td>&nbsp;</td>';
					$billHtml .= '<td>&nbsp;</td>';
					$billHtml .= '<td>&nbsp;</td>';
					$billHtml .= '<td>&nbsp;</td>';
					$billHtml .= '<td>&nbsp;</td>';
					$billHtml .= '</tr>';
			$billHtml .= '</table>';
		$billHtml .= '</body>';
	$billHtml .= '</html>';
}
//echo $billHtml;
//exit;

 
require_once('tcpdf/config/tcpdf_config.php');
require_once('tcpdf/tcpdf.php');
/**** below code store pdf file in folder ***/
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->AddPage();
$pdf->writeHTML($billHtml, true, 0, true, 0);
$pdf->lastPage();
$pdf->Output("uploads/pdf/".$getYear."_".$getMonth."/".$pdfFileName, 'F');
//$pdf->Output($pdfFileName, 'F');
/******************************/

/**** below code open pdf file from folder ***/
$pdf1 = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf1->setPrintHeader(false);
$pdf1->setPrintFooter(false);
$pdf1->AddPage();
$pdf1->writeHTML($billHtml, true, 0, true, 0);
$pdf1->lastPage();
//$pdf->Output("/uploads/pdf/".$getYear."_".$getMonth."/".$pdfFileName, 'F');
$pdf1->Output($pdfFileName, 'I');

//============================================================+
// END OF FILE
//============================================================+

?>