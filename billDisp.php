<?php

include('include/config.inc.php');
require('include/html_table.php');
//fetch staff data
//=============================
//==================================
$month = "07";
$year = "2014";

$q1 = "SELECT * FROM transaction
        WHERE DATE_FORMAT( `transDate` , '%m-%Y' ) = '07-2014'";
$res1 = mysql_query($q1);
$data = array();
$i = 0;
while($row = mysql_fetch_assoc($res1))
{
  $data[$i]['data'] = $row['transDate'];
  $data[$i]['name'] = "test";
  $data[$i]['rate'] = $row['rate'];
  $data[$i]['qty'] = $row['totalQty'];
  $data[$i]['amt'] = $row['rate'] * $row['totalQty'];
  $i++;
}

$pdf = new PDF();
$pdf->AddPage();
$pdf->SetFont('Arial', '', 12);

$html = '<table border="1">
<tr>
							  <td width="100" height="30">Date</td>
								<td width="100" height="30">Name</td>
								<td width="100" height="30">Rate</td>
								<td width="100" height="30">Qty</td>
								<td width="100" height="30">Amount</td>
							</tr>';
$total = 0;
foreach($data as $d)
{
  $html .= '<tr>
								<td width="100" height="30">'.$d['data'].'</td>
								<td width="100" height="30">'.$d['name'].'</td>
								<td width="100" height="30">'.$d['rate'].'</td>
								<td width="100" height="30">'.$d['qty'].'</td>
								<td width="100" height="30">'.number_format($d['amt'], 2).'</td>
							</tr>';
  $total += $d['amt'];
}
$html .= '<tr>
								<td width="100" height="30">&nbsp;</td>
								<td width="100" height="30">&nbsp;</td>
								<td width="100" height="30">&nbsp;</td>
								<td width="100" height="30">Total</td>
								<td width="100" height="30">'.number_format($total, 2).'</td>
							</tr>';

$html .= '</table>';
$pdf->SetTitle('BILL');
$pdf->WriteHTML($html);
$pdf->Output();
?>