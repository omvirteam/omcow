-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 02, 2014 at 03:58 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `omcow`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
`accountId` int(11) NOT NULL,
  `accountTypeId` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `placeId` int(11) DEFAULT '0',
  `contactNo` varchar(25) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `accounttype`
--

CREATE TABLE IF NOT EXISTS `accounttype` (
`accountTypeId` int(11) NOT NULL,
  `accountType` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `accounttype`
--

INSERT INTO `accounttype` (`accountTypeId`, `accountType`) VALUES
(1, 'Expense'),
(2, 'Doctor'),
(3, 'Donor'),
(4, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `cow`
--

CREATE TABLE IF NOT EXISTS `cow` (
`cowId` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `grn` varchar(30) DEFAULT NULL,
  `cowTypeId` int(11) DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `motherId` int(11) DEFAULT NULL,
  `fatherId` int(11) DEFAULT NULL,
  `dhankhutId` int(11) DEFAULT NULL,
  `milkPerDay` int(11) DEFAULT NULL,
  `purchaseRate` double DEFAULT NULL,
  `purchasedFrom` varchar(255) DEFAULT NULL,
  `purchaseDate` date DEFAULT NULL,
  `dwarningDate` date DEFAULT NULL,
  `staffId` int(11) DEFAULT NULL,
  `milkCloseDate` date DEFAULT NULL,
  `isDied` tinyint(1) DEFAULT '0',
  `notes` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cowtype`
--

CREATE TABLE IF NOT EXISTS `cowtype` (
`cowTypeId` int(11) NOT NULL,
  `type` varchar(500) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `cowtype`
--

INSERT INTO `cowtype` (`cowTypeId`, `type`) VALUES
(1, 'Cow'),
(2, 'Dhankhut'),
(3, 'Balad'),
(4, 'Vaachhdi'),
(5, 'Vaachhdo');

-- --------------------------------------------------------

--
-- Table structure for table `cron_bill`
--

CREATE TABLE IF NOT EXISTS `cron_bill` (
`id` int(11) NOT NULL,
  `cron_month` int(11) NOT NULL,
  `cron_year` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `customer_id` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=98 ;

--
-- Dumping data for table `cron_bill`
--

INSERT INTO `cron_bill` (`id`, `cron_month`, `cron_year`, `created_date`, `customer_id`) VALUES
(1, 7, 2014, '2014-08-20 08:34:22', 11),
(2, 7, 2014, '2014-08-20 08:34:22', 12),
(3, 7, 2014, '2014-08-20 08:34:22', 13),
(4, 7, 2014, '2014-08-20 08:34:22', 14),
(5, 7, 2014, '2014-08-20 08:34:22', 15),
(6, 7, 2014, '2014-08-20 08:34:22', 16),
(7, 7, 2014, '2014-08-20 08:34:22', 17),
(8, 7, 2014, '2014-08-20 08:34:22', 22),
(9, 7, 2014, '2014-08-20 08:34:22', 23),
(10, 7, 2014, '2014-08-20 08:34:22', 24),
(11, 7, 2014, '2014-08-20 08:34:22', 26),
(12, 7, 2014, '2014-08-20 08:34:22', 27),
(13, 7, 2014, '2014-08-20 08:34:22', 28),
(14, 7, 2014, '2014-08-20 08:34:23', 29),
(15, 7, 2014, '2014-08-20 08:34:23', 30),
(16, 7, 2014, '2014-08-20 08:34:23', 31),
(17, 7, 2014, '2014-08-20 08:34:23', 33),
(18, 7, 2014, '2014-08-20 08:34:23', 34),
(19, 7, 2014, '2014-08-20 08:34:23', 35),
(20, 7, 2014, '2014-08-20 08:34:23', 36),
(21, 7, 2014, '2014-08-20 08:34:23', 37),
(22, 7, 2014, '2014-08-20 08:34:23', 38),
(23, 7, 2014, '2014-08-20 08:34:23', 39),
(24, 7, 2014, '2014-08-20 08:34:23', 40),
(25, 7, 2014, '2014-08-20 08:34:23', 41),
(26, 7, 2014, '2014-08-20 08:34:23', 42),
(27, 7, 2014, '2014-08-20 08:34:23', 43),
(28, 7, 2014, '2014-08-20 08:34:23', 44),
(29, 7, 2014, '2014-08-20 08:34:23', 45),
(30, 7, 2014, '2014-08-20 08:34:23', 46),
(31, 7, 2014, '2014-08-20 08:34:23', 47),
(32, 7, 2014, '2014-08-20 08:34:23', 48),
(33, 7, 2014, '2014-08-20 08:34:23', 49),
(34, 7, 2014, '2014-08-20 08:34:23', 50),
(35, 7, 2014, '2014-08-20 08:34:23', 51),
(36, 7, 2014, '2014-08-20 08:34:23', 52),
(37, 7, 2014, '2014-08-20 08:34:23', 53),
(38, 7, 2014, '2014-08-20 08:34:23', 54),
(39, 7, 2014, '2014-08-20 08:34:23', 55),
(40, 7, 2014, '2014-08-20 08:34:24', 57),
(41, 7, 2014, '2014-08-20 08:34:24', 58),
(42, 7, 2014, '2014-08-20 08:34:24', 59),
(43, 7, 2014, '2014-08-20 08:34:24', 60),
(44, 7, 2014, '2014-08-20 08:34:24', 61),
(45, 7, 2014, '2014-08-20 08:34:24', 62),
(46, 7, 2014, '2014-08-20 08:34:24', 63),
(47, 7, 2014, '2014-08-20 08:34:24', 64),
(48, 7, 2014, '2014-08-20 08:34:24', 65),
(49, 7, 2014, '2014-08-20 08:34:24', 66),
(50, 7, 2014, '2014-08-20 08:34:24', 67),
(51, 7, 2014, '2014-08-20 08:34:24', 68),
(52, 7, 2014, '2014-08-20 08:34:24', 69),
(53, 7, 2014, '2014-08-20 08:34:24', 70),
(54, 7, 2014, '2014-08-20 08:34:24', 71),
(55, 7, 2014, '2014-08-20 08:34:24', 72),
(56, 7, 2014, '2014-08-20 08:34:24', 73),
(57, 7, 2014, '2014-08-20 08:34:24', 74),
(58, 7, 2014, '2014-08-20 08:34:24', 75),
(59, 7, 2014, '2014-08-20 08:34:24', 76),
(60, 7, 2014, '2014-08-20 08:34:24', 77),
(61, 7, 2014, '2014-08-20 08:34:24', 78),
(62, 7, 2014, '2014-08-20 08:34:24', 79),
(63, 7, 2014, '2014-08-20 08:34:24', 80),
(64, 7, 2014, '2014-08-20 08:34:24', 81),
(65, 7, 2014, '2014-08-20 08:34:25', 82),
(66, 7, 2014, '2014-08-20 08:34:25', 83),
(67, 7, 2014, '2014-08-20 08:34:25', 84),
(68, 7, 2014, '2014-08-20 08:34:25', 85),
(69, 7, 2014, '2014-08-20 08:34:25', 86),
(70, 7, 2014, '2014-08-20 08:34:25', 87),
(71, 7, 2014, '2014-08-20 08:34:25', 88),
(72, 7, 2014, '2014-08-20 08:34:25', 89),
(73, 7, 2014, '2014-08-20 08:34:25', 90),
(74, 7, 2014, '2014-08-20 08:34:25', 91),
(75, 7, 2014, '2014-08-20 08:34:25', 92),
(76, 7, 2014, '2014-08-20 08:34:25', 93),
(77, 7, 2014, '2014-08-20 08:34:25', 94),
(78, 7, 2014, '2014-08-20 08:34:25', 95),
(79, 7, 2014, '2014-08-20 08:34:25', 96),
(80, 7, 2014, '2014-08-20 08:34:25', 97),
(81, 7, 2014, '2014-08-20 08:34:25', 98),
(82, 7, 2014, '2014-08-20 08:34:25', 99),
(83, 7, 2014, '2014-08-20 08:34:25', 100),
(84, 7, 2014, '2014-08-20 08:34:25', 101),
(85, 7, 2014, '2014-08-20 08:34:25', 105),
(86, 7, 2014, '2014-08-20 08:34:25', 106),
(87, 7, 2014, '2014-08-20 08:34:25', 107),
(88, 7, 2014, '2014-08-20 08:34:25', 108),
(89, 7, 2014, '2014-08-20 08:34:25', 109),
(90, 7, 2014, '2014-08-20 08:34:26', 110),
(91, 7, 2014, '2014-08-20 08:34:26', 111),
(92, 7, 2014, '2014-08-20 08:34:26', 112),
(93, 7, 2014, '2014-08-20 08:34:26', 113),
(94, 7, 2014, '2014-08-20 08:34:26', 114),
(95, 7, 2014, '2014-08-20 08:34:26', 115),
(96, 7, 2014, '2014-08-20 08:34:26', 116),
(97, 7, 2014, '2014-08-20 08:34:26', 117);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
`customerId` int(11) NOT NULL,
  `grnNo` int(11) DEFAULT NULL,
  `customerTypeId` int(11) DEFAULT NULL,
  `distStaffIdId` int(11) DEFAULT NULL,
  `milkTime` varchar(50) DEFAULT NULL,
  `morningSequence` double DEFAULT NULL,
  `eveningSequence` double DEFAULT NULL,
  `currentLitre` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `nickName` varchar(255) DEFAULT NULL,
  `society` varchar(255) DEFAULT NULL,
  `appartment` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `phone1` varchar(255) DEFAULT NULL,
  `phone2` varchar(255) DEFAULT NULL,
  `joinDate` date DEFAULT NULL,
  `currentAmt` double DEFAULT NULL,
  `currentDrCr` char(2) DEFAULT NULL,
  `collectionDate` date DEFAULT NULL,
  `notes` text
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=118 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customerId`, `grnNo`, `customerTypeId`, `distStaffIdId`, `milkTime`, `morningSequence`, `eveningSequence`, `currentLitre`, `rate`, `name`, `nickName`, `society`, `appartment`, `city`, `phone1`, `phone2`, `joinDate`, `currentAmt`, `currentDrCr`, `collectionDate`, `notes`) VALUES
(11, 1, 2, 10, 'E', 0, 1, 1, NULL, 'P.D.Jadeja', 'P.D.Jadeja', 'Kevlam, Puskardham main road', '', 'Rajkot', '9727244888', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(12, 2, 2, 10, 'E', 0, 2, 3, NULL, 'Nitinbhai Dudani', 'Nitinbhai', 'Ghanshyamnagar, Near swastik school, Puskardham main road', 'S.S.Star - 802', 'Rajkot', '9726195426', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(13, 3, 2, 10, 'E', 0, 3, 3, NULL, 'Kantibhai Dudani', 'Kantibhai Dudani', 'Ghanshyamnagar, Near Swastik School, Puskardham main road', 'SunStar Appartment- 502', 'Rajkot', '9427255923', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(14, 4, 2, 10, 'E', 0, 4, 1, NULL, 'Prabhudasbhai Kameriya', 'Prabhudasbhai', 'Ghanshyamnagar, Near Swastik School, Puskardham main road', 'S.S.App - 402', 'Rajkot', '', '', '2014-06-16', 60, 'dr', '2014-06-16', ''),
(15, 5, 2, 10, 'E', 0, 5, 0.5, NULL, 'Maulikbhai Patel', 'Maulikbhai', 'Ghanshyamnagar, Near Swastik School, Puskardham main road', 'S.S.App.- 203', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(16, 6, 2, 10, 'E', 0, 6, 0.5, NULL, 'Hardikbhai', 'Hemhard', '"Hemhard", shree Ram park-1', '', 'Rajkot', '9726230003', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(17, 7, 1, 10, 'E', 0, 7, 1, NULL, 'Pravinchandra N Maheta', 'Krishna park - 62', '62-Krishna Park, Street of Ram Mandir', '', 'Rajkot', '9929401123', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(22, 12, 2, 10, 'M', 2, NULL, 1, NULL, 'Rajnikant Vallbhbhai Kathrotiya', 'Rani Tower B - 702', 'Kalawad Road', 'B - 702 Rani Tower', 'Rajkot', '9909751515, 0281 - 2563600', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(23, 13, 2, 11, 'M', 35, NULL, 1, NULL, 'Kananben', 'KAnanben', 'Puskardham main Road', 'rajdhani app -1102', 'Rajkot', '9428003129', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(24, 14, 1, 11, 'M', 5, NULL, 2, NULL, 'Hetal Kalavadiya', 'Oskar City Tower A - 1101', 'Sadhuvasvani Road', 'Oskar City Tower A - 111', 'Rajkot', '', '', '2014-06-17', 0, 'dr', '2014-06-17', ''),
(26, 16, 2, 11, 'E', 0, 1, 2, NULL, 'Satishbhai', 'Satishbhai', 'Near Gol Resedence. nana mauva road', 'Hevlock Tower - 901', 'Rajkot', '9909221212', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(27, 17, 2, 11, 'E', 0, 2, 1, NULL, 'Dipen B. Maheta', 'Hevlok Tower B - 301', 'Near Gol Recedence, Nana mauva road', 'Havlock horayjans B - 301', 'Rajkot', '9929919794', '', '2014-01-01', 60, 'dr', '2014-05-01', ''),
(28, 18, 2, 11, 'E', 0, 3, 1.5, NULL, 'Dharmendrabahi', 'Pentagon tower - 802', 'near Speed well parti plot, nana mauva road', 'Pentagon tower - 802', 'Rajkot', '9909651515', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(29, 19, 2, 11, 'E', 0, 4, 2, NULL, 'Bharatbhai Khanpara', 'Haridvar  A - 902', 'Near Sastri nagar, Near Gandhi school, nana mava road ', 'Haridvar  A - 902', 'Rajkot', '9898139239', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(30, 20, 2, 11, 'E', 0, 5, 2, NULL, 'chandrakantbhai ', 'Alap - 1101', 'nana mava maqin road, near gandhi school ', 'Alap - 1101', 'Rajkot', '9879879487', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(31, 21, 2, 11, 'E', 0, 6, 2, NULL, 'Manishaben Khanpara ', 'Tulsi patra - 401 ', 'Behind sastri nagar, nana mava main road', 'Tulsi patra - 401 ', 'Rajkot', '9727725395', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(33, 23, 2, 11, 'E', 0, 7, 1, NULL, 'Bhavesh patel', 'Aasray - 302', 'Gandhi school, Nana mauva main road', 'Aasray - 302', 'Rajkot', '9638587716', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(34, 24, 2, 11, 'E', 0, 8, 1.5, NULL, 'Mahendrabhai', 'Aksardham - 101', 'Aksar vatica sosayti, Behind Sastri nagar, Nana mava main road', 'Aksardham - 101', 'Rajkot', '9925728200', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(35, 25, 2, 11, 'E', 0, 9, 1, NULL, 'Vijaybhai Ghetiya', 'Akhasardham App.201', 'Akhar vatika sosayti,Behind  Sastri nagar, nana mava main road ', 'Akhasardham App.201', 'Rajkot', '9879506663', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(36, 26, 2, 11, 'E', 0, 10, 1, 0, 'BHIMANI SIR', 'BHIMANI SIR, RAMESHWER PARK, OM APP. A - 58', 'mayani chock', 'Rameshwer park, om app. a - 58', 'Rajkot', '999958083', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(37, 27, 2, 11, 'E', 0, 11, 1.5, NULL, 'Jitubhai', 'Jitubhai Alay park B - 20', 'Bapasitaram chock, mavdi', 'Alay park B - 20', 'Rajkot', '9429981430', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(38, 28, 2, 11, 'E', 0, 12, 1.5, NULL, 'Achutbhai Vachhani', 'Alay vatica A - 12', 'Behind Tapovan School, 150 Feet ring road', 'Alay vatica A - 12', 'Rajkot', '9327606017,9376910197', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(39, 29, 2, 11, 'E', 0, 14, 1, NULL, 'VIjaybhai Trada', 'Vijaybhai Trada A - 17', 'Aastha Residensi, 150 feet ring road', '', 'Rajkot', '9426225881', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(40, 30, 2, 11, 'E', 0, 16, 0.5, NULL, 'Pradipbhai Sojitra', 'Alka Sosayti', 'Alka Sosayti 1/7, Malaviya police station vali street, Rameswar ni same ', '', 'Rajkot', '9428255256', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(41, 31, 2, 11, 'E', 0, 17, 1.5, NULL, 'Samirbhai', 'Samirbhai, Mayani chock', 'Mayani chock, "Matru Krupa", Near Sardar Restorant ', '', 'Rajkot', '9998262626, 9824500071', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(42, 32, 2, 11, 'E', 0, 18, 2, NULL, 'Prakashbhai  ', 'Bapasitaram chock near', 'Nesh nagar Street no 3, Maldhari chock', '', 'Rajkot', '9374101951', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(43, 33, 2, 11, 'E', 0, 19, 0.5, NULL, 'Bipinbhai Popat', 'Silvar ston, Bipinbhai Popat', 'Aasutos - 2, Silvar ston, 150 Feet Ring road', 'Silvar ston', 'Rajkot', '9898585367, 2585357', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(44, 34, 2, 11, 'E', 0, 20, 1, NULL, 'Oskar tower', 'Osakar Tower B - 102', 'Opp. Big bazzar, Silvar ston main Road, Near tanti park', 'Osakar Tower B - 102', 'Rajkot', '9825215538, 9925215534', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(45, 35, 2, 11, 'E', 0, 22, 0.5, NULL, 'Dilipbhai Jivani', 'Dilipbhai Jivani - rajmani', '102 - Rajmani complex, Opp Gspc Gas Office, Near Krishna Hospital, 150 ring road', 'Rajmani complex - 102', 'Rajkot', '8866227156, 9714125331', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(46, 36, 2, 11, 'E', 0, 26, 1, NULL, 'Prakashbhai Trivedi', '"Maa Harsiddhi"', 'D - 33, Manio nagar, Opp. Rameswar Hall, Raya Chokadi, Near Raya Gam', '', 'Rajkot', '9427200909, 0281-2926444', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(47, 37, 2, 11, 'E', 0, 27, 1, NULL, 'Navinchandra Ramanlal Seth', 'Greencity - 24', 'Alap Green City - A 42, Riya Road', '', 'Rajkot', '9099939417, 0281-2583322', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(48, 38, 2, 11, 'E', 0, 28, 0.5, NULL, 'Mukat Prasad Ladva', 'Topland B - 13', 'Street no - 1,Topland B - 13,  Sadhuvasvani Road', '', 'Rajkot', '9978951053', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(49, 39, 2, 11, 'E', 0, 29, 1, NULL, 'B.M.Purohit', 'Topland - C 2', 'Sadhuvasvani road', '', 'Rajkot', '9428004598', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(50, 40, 2, 11, 'E', 0, 30, 1, NULL, 'Dipakbhai R. Kotadiya', 'Nandvihar', 'Sadhuvasvani Road', 'Nandvihar App. - 3/3,', 'Rajkot', '9376769933', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(51, 41, 2, 11, 'E', 0, 33, 0.5, NULL, 'Ketanbhai Vachhani', 'Ketanbhai Vachhani', 'Opp. Oskar city, Sadhuvasvani Road', 'Rameshwer Complex - 205', 'Rajkot', '9824215990, 9662470700', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(52, 42, 2, 11, 'E', 0, 41, 1, NULL, 'Jagdishbhai Hansalpara', 'Puskardham', 'Puskardham, Street No. 4, ', '', 'Rajkot', '9408046464', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(53, 43, 2, 11, 'E', 0, 34, 1, NULL, 'Krusansinh jadeja', '"Jay Ashapura"', '"Jay Ashapura", Natraj nagar Street no.- 1, Uni. Road', '', 'Rajkot', '9624790001', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(54, 44, 2, 11, 'E', 0, 39, 1, NULL, 'Gunvantbhai Bhadani', '"Vraj"', '"Vraj - A 49", 3 - B, Panchayat nagar', '', 'Rajkot', '9638855824,2586258', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(55, 45, 2, 11, 'E', 0, 40, 1, NULL, 'Kalaben', 'Kalaben', '"Isawar krupa", Karmchari Sosayti - 12/2, Uni. Road,  ', '', 'Rajkot', '9825208080,9228252455', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(57, 47, 2, 11, 'E', 0, 42, 0.5, NULL, '', 'Alay Vatika - 23', 'Behind Tapovan School, 150 Ring Road', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(58, 48, 2, 11, 'E', 0, 21, 0.5, NULL, 'Dr. Payal', 'Dr. Payal, Oskar A - 774', '', 'Oskar A - 774', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(59, 49, 2, 11, 'E', 0, 23, 0.5, NULL, '', 'Rajmani - 303', '', 'Rajmani - 303', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(60, 50, 2, 11, 'E', 0, 24, 1, NULL, 'Harishbhai', 'Harishbhai - Nilkanth pan', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(61, 51, 2, 11, 'E', 0, 25, 1, NULL, 'Bhartbhai', 'Bharatbhai', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-07-01', ''),
(62, 52, 2, 11, 'E', 0, 35, 2.5, NULL, 'Piushbhai Kaneriya', 'Piushbhai Kaneriya', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(63, 53, 2, 11, 'E', 0, 36, 2, NULL, 'Dineshbhai Tilva', 'Dineshbhai Tilva', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(64, 54, 2, 11, 'E', 0, 37, 1, NULL, 'Himansubhai Rupavatiya', 'Himansubhai Rupavatiya', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(65, 55, 2, 11, 'E', 0, 38, 1, NULL, 'Kilasbhai Patel', 'Kilasbhai Patel', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(66, 56, 1, 11, 'M', 1, NULL, 1, NULL, 'Kananben', 'Kananben Rajdhani App. - 1102', 'Puskardham main Road', 'Rajdhani App. - 1102', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(67, 57, 2, 11, 'M', 2, NULL, 1.5, NULL, '', 'Oskar city Tower A - 111', '', 'Oskar city Tower A - 111', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(68, 58, 2, 11, 'M', 3, NULL, 0.5, NULL, '', 'Oskar city Tower A - 173', '', 'Oskar city Tower A - 173', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(69, 59, 2, 11, 'M', 4, NULL, 1, NULL, '', 'Oskar city Tower A - 184', '', 'Oskar city Tower A - 184', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(70, 60, 2, 11, 'M', 37, NULL, 1.5, NULL, '', 'Oskar city Tower A - 1101', '', 'Oskar city Tower A - 1101', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(71, 61, 2, 11, 'M', 6, NULL, 3, NULL, 'B.M.Vyas', 'B.M.Vyas', '"Radhesyam", 11 - Surastra kala kendra Sosayti', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(72, 62, 2, 11, 'M', 36, NULL, 0.5, NULL, 'Janibhai', 'Janibhai', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(73, 63, 2, 11, 'M', 7, NULL, 2, NULL, 'Rasminkant Trivedi', 'Rasminkant Trivedi', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(74, 64, 2, 11, 'M', 8, NULL, 1, NULL, '', 'Santkrupa', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(75, 65, 2, 11, 'M', 9, NULL, 1, NULL, 'Nanakbhai Purvani', 'Nanakbhai Purvani - Paras sosayti', '"Upasna", 5 - Paras sosayti, Nirmala konvent road', '', 'Rajkot', '9427431836, 2576098', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(76, 66, 2, 11, 'M', 10, NULL, 0.5, NULL, 'Himansubhai patel', 'Himansubhai - Reva app - 202', '', 'App. Reva - 202', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(77, 67, 2, 11, 'M', 11, NULL, 1, NULL, 'Devendrabhai', 'Devendrabhai - Balmukund plot', 'B - 102, Ruchita flet, Balmukund plot, Nirmal convent road, Fiar breged road', 'B - 102, Ruchita flet', 'Rajkot', '9978966621', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(78, 68, 2, 11, 'M', 12, NULL, 2, NULL, 'Manishbhai Maru', 'Manishbhai maru', '"Subham", 2 - Akasr vadi, Saurastra kala kendra main road, nirmal convent road, Rajkot', '', 'Rajkot', '9925042435, 2585741', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(79, 69, 2, 11, 'M', 13, NULL, 1, NULL, 'Sureshbhai', 'Sureshbhai - Raya Road', 'Raiya road', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(80, 70, 2, 11, 'M', 14, NULL, 2, NULL, 'Maheshbhai Goti ', 'Maheshbhai Goti', '"Ramkrishna", 50 - B Banglo, Mahavir Sosayti, Nirmala convent road, ', '', 'Rajkot', '9898592027, 02812453212', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(81, 71, 2, 11, 'M', 15, NULL, 1.5, NULL, 'Jigneshbhai', 'Jigneshbhai - Gunjan Point C - 102', 'Gunjan Point C - 102,Near  Kanya chhatralay, Kalavad road  ', '', 'Rajkot', '9099972225, 02812332206', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(82, 72, 2, 11, 'M', 16, NULL, 0.5, NULL, 'Dineshbhai Limbad', 'Dineshbhai Limbad', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(83, 73, 2, 11, 'M', 17, NULL, 1, NULL, 'Virenbhai', 'Virenbhai', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(84, 74, 2, 11, 'M', 18, NULL, 0.5, NULL, '', 'Chandan App. - 401', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(85, 75, 2, 11, 'M', 19, NULL, 1.5, NULL, 'Dr. Jay makadiya', 'Dr. Jay Makadiya, "Laxminivas"', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(86, 76, 2, 11, 'M', 20, NULL, 0.5, NULL, '', 'Jagnath - 25', 'Jagnath - 25', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(87, 77, 2, 11, 'M', 21, NULL, 2, NULL, '', 'New Jagnath - 41, "Giriraj"', 'New Jagnath - 41, "Giriraj"', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(88, 78, 2, 11, 'M', 22, NULL, 1, NULL, 'Vasuben Seth', 'Vasuben Seth - Narayan App.', '', 'Narayan App.', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(89, 79, 2, 11, 'M', 23, NULL, 1, NULL, 'Hemangbhai', 'Hemangbhai - Jagnath 3 / 11', 'Jagnath 3 / 11', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(90, 80, 2, 11, 'M', 24, NULL, 5.5, NULL, 'Dipakbhai', 'Dipakbhai - Fieldmarsal', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(91, 81, 2, 11, 'M', 25, NULL, 1, NULL, 'Pooja Tevedi', 'Pooja Tevedi', '"Balabhuvan", Karanshihji main road, Opp. Kapad mil, Opp. Dr. Suraj seth ', '', 'Rajkot', '9979729639', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(92, 82, 2, 11, 'M', 26, NULL, 2, NULL, 'Gopalbhai ', 'Gopalbhai Metalwood Farnicher', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(93, 83, 2, 11, 'M', 27, NULL, 1, NULL, 'Ajitshih Jadeja', 'Ajitshih Jadeja', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(94, 84, 2, 11, 'M', 28, NULL, 1, NULL, 'Dr.Bhavesh Joshi', 'Dr.Bhavesh Joshi', 'Virani Chock, "Rudram Narsing Home" Hospital, Vidyanagar main road ', '', 'Rajkot', '9427724531', '', '2014-01-01', 60, 'cr', '2014-05-01', '\r\n'),
(95, 85, 2, 11, 'M', 29, NULL, 0.5, NULL, 'M.J.Vasavda', 'M.J.Vasavda - Opp. Modi School', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(96, 86, 2, 11, 'M', 30, NULL, 0.5, NULL, '', 'Rajnagar - 1, Vijaymrut', 'Rajnagar - 1, Vijaymrut', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(97, 87, 2, 11, 'M', 31, NULL, 2, NULL, 'Pravinbhai Kalvadiya', 'Pravinbhai - Shiv Bhumi - 2', 'Shiv bhumi sosayti Street No. 2, Block No. 5, "shyamsundar" Near in Big Bazzar, Near Astron sosayti,Parnkuti sosayti main road', '', 'Rajkot', '9427561275', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(98, 88, 2, 11, 'M', 32, NULL, 2, NULL, 'Umaben', 'Umaben - "Satyam"', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(99, 89, 2, 11, 'M', 33, NULL, 1, NULL, 'Navinbhai Patel', 'Navinbhai patel', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(100, 90, 2, 11, 'M', 34, NULL, 2, NULL, 'Jaybhai Vasavda', 'Jaybhai Vasavda', '', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(101, 91, 2, 10, 'M', 1, NULL, 1, 0, 'KISHORBHAI ', 'KISHORBHAI - P. G. CITY - 802', 'Kalawad road', 'Pradhuman Green city - 802', 'Rajkot', '9909521212, 0281 - 2563637', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(105, 95, 2, 10, 'M', 3, NULL, 1, NULL, '', 'Rani Tower - b- 603', '', 'Rani Tower - b- 603', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(106, 96, 2, 10, 'M', 4, NULL, 1, NULL, '', 'Rani Tower b- 204', '', 'Rani Tower b- 204', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(107, 97, 2, 10, 'M', 5, NULL, 1, NULL, 'Trivedi Sir', 'trived sir-Ashopalav-Dhumketu', 'Dhumketu, Block No A - 69, Get No. 2, Aasopalav Banglo, Near Satyasia Hospital, Satysai Road, Rajkot', '', 'Rajkot', '9825989251', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(108, 98, 2, 10, 'M', 6, NULL, 1, NULL, 'Bholabhai', 'bhola bhai faldu--"dhruti"', '"Dhriti", Asopalv, Satyasai Road ', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(109, 99, 2, 10, 'M', 7, NULL, 1.5, NULL, '', '"Shree shree"', '"Shree shree" - Ashopalv Residenci - Satyasai Road', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(110, 100, 1, 10, 'M', 8, NULL, 1, NULL, '', 'Sivam Banglo', '"Sivam Banglo", Satysai Road, Kalawad Road', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(111, 101, 1, 10, 'M', 9, NULL, 0.5, NULL, 'Pravinbhai Modi ', 'Pravinbhai Modi - yogi park - 7', ' "parijat", Yogi Park - Street No. 7, Opp. Mahadev temple, Behind Rani tower', '', 'Rajkot', '0281 2561372', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(112, 102, 2, 10, 'M', 10, NULL, 2, NULL, 'Dr. amiben Trivedi', 'Dr.Ami, Chitra Lekha K.K.V', 'Near k.k.v holl, 150 feet ring road, Near Gelexi pan ', '404 - Chitralekha Buldig', 'Rajkot', '9624090907', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(113, 103, 2, 10, 'M', 11, NULL, 5, NULL, '', 'Aryvilla - Neelcity ', 'Uni.Road, Near Uni. Get', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(114, 104, 2, 11, 'E', 0, 13, 1, 60, '', 'ALAY VATIKA -14', '', 'ALAY VATIKA -14', 'Rajkot', '', '', '2014-01-02', 60, 'cr', '2014-05-01', ''),
(115, 105, 2, 11, 'E', 0, 15, 1, 60, 'MANISHBHAI MAYANI', 'MANISHBHAI', 'Astha Residenci - c127', '', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-05-01', ''),
(116, 106, 2, 11, 'E', 0, 31, 1, 60, 'SURESHBHIA HIRAPARA', 'SURESHBHAI', 'Sadhuvasvani road', 'Cristal Appartment - 602', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-06-01', ''),
(117, 107, 2, 11, 'E', 0, 32, 1, 60, 'PARESHBHAI VIRAMGAMA', 'PARESHBHAI', 'Opp. Gitanjali coolage', 'Panjari App - C 1', 'Rajkot', '', '', '2014-01-01', 60, 'cr', '2014-06-01', '');

-- --------------------------------------------------------

--
-- Table structure for table `customertype`
--

CREATE TABLE IF NOT EXISTS `customertype` (
`customerTypeId` int(11) NOT NULL,
  `customerType` varchar(50) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `customertype`
--

INSERT INTO `customertype` (`customerTypeId`, `customerType`) VALUES
(1, 'Waiting'),
(2, 'Current'),
(3, 'Closed'),
(4, 'Dairy'),
(5, 'Personal'),
(6, 'Donor');

-- --------------------------------------------------------

--
-- Table structure for table `fariya`
--

CREATE TABLE IF NOT EXISTS `fariya` (
`fariyaId` int(11) NOT NULL,
  `cowId` int(11) DEFAULT NULL,
  `fariyaDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fathermaster`
--

CREATE TABLE IF NOT EXISTS `fathermaster` (
`fatherMaster` int(11) NOT NULL,
  `fatherName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `fathermaster`
--

INSERT INTO `fathermaster` (`fatherMaster`, `fatherName`) VALUES
(1, 'Bhimji'),
(2, 'Samji');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
`itemId` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `qty` double DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  `status` varchar(3) DEFAULT NULL COMMENT 'D=Delete,A=Active'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`itemId`, `name`, `rate`, `qty`, `unit`, `status`) VALUES
(5, 'Cow Milk', 60, 1, NULL, NULL),
(7, 'Gau Mutra Ark', 50, 250, NULL, NULL),
(8, 'Finayl', 50, 1, NULL, NULL),
(13, 'Ghee', 1200, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `itemcreate`
--

CREATE TABLE IF NOT EXISTS `itemcreate` (
`itemCreateId` int(11) NOT NULL,
  `itemId` int(11) DEFAULT NULL,
  `createDate` date DEFAULT NULL,
  `qty` double DEFAULT NULL,
  `notes` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `itemdispatch`
--

CREATE TABLE IF NOT EXISTS `itemdispatch` (
`itemDispatchId` int(11) NOT NULL,
  `staffId` int(11) DEFAULT NULL,
  `itemId` int(11) DEFAULT NULL,
  `qty` double DEFAULT NULL,
  `dispDate` date DEFAULT NULL,
  `notes` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `itemexpense`
--

CREATE TABLE IF NOT EXISTS `itemexpense` (
`itemExpenseId` int(11) NOT NULL,
  `itemId` int(11) DEFAULT NULL,
  `expDate` date DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `notes` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `itemsell`
--

CREATE TABLE IF NOT EXISTS `itemsell` (
`itemSellId` int(11) NOT NULL,
  `itemId` int(11) DEFAULT NULL,
  `customerId` int(11) DEFAULT NULL,
  `sellDate` date DEFAULT NULL,
  `qty` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `notes` text,
  `billCreated` char(1) DEFAULT 'n'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `licence`
--

CREATE TABLE IF NOT EXISTS `licence` (
`licenceId` int(11) NOT NULL,
  `licenceName` varchar(255) DEFAULT NULL,
  `renewDate` date DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `licence`
--

INSERT INTO `licence` (`licenceId`, `licenceName`, `renewDate`) VALUES
(1, 'a', '2014-06-01');

-- --------------------------------------------------------

--
-- Table structure for table `meeting`
--

CREATE TABLE IF NOT EXISTS `meeting` (
`meetingId` int(11) NOT NULL,
  `cowId` int(11) DEFAULT NULL,
  `meetingDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `milkbycow`
--

CREATE TABLE IF NOT EXISTS `milkbycow` (
`milkByCowId` int(11) NOT NULL,
  `cowId` int(11) DEFAULT NULL,
  `milkDate` date DEFAULT NULL,
  `milkTime` char(1) DEFAULT NULL,
  `litre` double DEFAULT NULL,
  `staffId` int(11) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `milksell`
--

CREATE TABLE IF NOT EXISTS `milksell` (
`milkSellId` int(11) NOT NULL,
  `milkSellDate` date DEFAULT NULL,
  `milkTime` char(1) DEFAULT NULL,
  `customerId` int(11) DEFAULT NULL,
  `milkGiven` double DEFAULT NULL,
  `customerMilkTime` char(1) DEFAULT NULL,
  `nextDate` date DEFAULT NULL,
  `nextLitre` int(11) DEFAULT NULL,
  `billCreated` char(1) DEFAULT 'n',
  `rate` double DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2656 ;

--
-- Dumping data for table `milksell`
--

INSERT INTO `milksell` (`milkSellId`, `milkSellDate`, `milkTime`, `customerId`, `milkGiven`, `customerMilkTime`, `nextDate`, `nextLitre`, `billCreated`, `rate`) VALUES
(13, '2014-06-16', 'E', 16, 1, 'E', '2014-06-17', 1, 'n', 30),
(14, '2014-06-16', 'E', 13, 6, 'E', '2013-06-14', 6, 'n', 30),
(15, '2014-06-16', 'E', 15, 1, 'E', '2014-06-17', 1, 'n', 30),
(16, '2014-06-16', 'E', 12, 6, 'E', '2014-06-17', 6, 'n', 30),
(17, '2014-06-16', 'E', 11, 2, 'E', '2014-06-17', 2, 'n', 30),
(18, '2014-06-16', 'E', 14, 2, 'E', '2014-06-17', 2, 'n', 30),
(19, '2014-06-16', 'M', 17, 0, 'M', '2014-06-17', 0, 'n', 30),
(20, '2014-06-21', 'E', 16, 1, 'E', '2014-06-22', 0, 'n', 30),
(21, '2014-06-21', 'E', 13, 6, 'E', '2014-06-22', 0, 'n', 30),
(22, '2014-06-21', 'E', 15, 2, 'E', '2014-06-22', 0, 'n', 30),
(23, '2014-06-21', 'E', 12, 6, 'E', '2014-06-22', 0, 'n', 30),
(24, '2014-06-21', 'E', 11, 1, 'E', '2014-06-22', 0, 'n', 30),
(25, '2014-06-21', 'E', 14, 2, 'E', '2014-06-22', 0, 'n', 30),
(26, '2014-07-12', 'M', 84, 0, 'M', '2014-07-13', 0, 'y', 30),
(27, '2014-07-12', 'M', 86, 0, 'M', '2014-07-13', 0, 'y', 30),
(28, '2014-07-12', 'M', 87, 0, 'M', '2014-07-13', 0, 'y', 30),
(29, '2014-07-12', 'M', 74, 0, 'M', '2014-07-13', 0, 'y', 30),
(30, '2014-07-12', 'M', 70, 0, 'M', '2014-07-13', 0, 'y', 30),
(31, '2014-07-12', 'M', 69, 0, 'M', '2014-07-13', 0, 'y', 30),
(32, '2014-07-12', 'M', 68, 0, 'M', '2014-07-13', 0, 'y', 30),
(33, '2014-07-12', 'M', 67, 0, 'M', '2014-07-13', 0, 'y', 30),
(34, '2014-07-12', 'M', 93, 0, 'M', '2014-07-13', 0, 'y', 30),
(35, '2014-07-12', 'M', 71, 0, 'M', '2014-07-13', 0, 'y', 30),
(36, '2014-07-12', 'M', 77, 0, 'M', '2014-07-13', 0, 'y', 30),
(37, '2014-07-12', 'M', 82, 0, 'M', '2014-07-13', 0, 'y', 30),
(38, '2014-07-12', 'M', 90, 0, 'M', '2014-07-13', 0, 'y', 30),
(39, '2014-07-12', 'M', 85, 0, 'M', '2014-07-13', 0, 'y', 30),
(40, '2014-07-12', 'M', 94, 0, 'M', '2014-07-13', 0, 'y', 30),
(41, '2014-07-12', 'M', 92, 0, 'M', '2014-07-13', 0, 'y', 30),
(42, '2014-07-12', 'M', 89, 0, 'M', '2014-07-13', 0, 'y', 30),
(43, '2014-07-12', 'M', 24, 0, 'M', '2014-07-13', 0, 'y', 30),
(44, '2014-07-12', 'M', 76, 0, 'M', '2014-07-13', 0, 'y', 30),
(45, '2014-07-12', 'M', 72, 0, 'M', '2014-07-13', 0, 'y', 30),
(46, '2014-07-12', 'M', 81, 0, 'M', '2014-07-13', 0, 'y', 30),
(47, '2014-07-12', 'M', 23, 0, 'M', '2014-07-13', 0, 'y', 30),
(48, '2014-07-12', 'M', 66, 0, 'M', '2014-07-13', 0, 'y', 30),
(49, '2014-07-12', 'M', 80, 0, 'M', '2014-07-13', 0, 'y', 30),
(50, '2014-07-12', 'M', 78, 0, 'M', '2014-07-13', 0, 'y', 30),
(51, '2014-07-12', 'M', 75, 0, 'M', '2014-07-13', 0, 'y', 30),
(52, '2014-07-12', 'M', 91, 0, 'M', '2014-07-13', 0, 'y', 30),
(53, '2014-07-12', 'M', 73, 0, 'M', '2014-07-13', 0, 'y', 30),
(54, '2014-07-12', 'M', 79, 0, 'M', '2014-07-13', 0, 'y', 30),
(55, '2014-07-12', 'M', 88, 0, 'M', '2014-07-13', 0, 'y', 30),
(56, '2014-07-12', 'M', 83, 0, 'M', '2014-07-13', 0, 'y', 30),
(57, '2014-07-12', 'M', 84, 0, 'M', '2014-07-13', 0, 'y', 30),
(58, '2014-07-12', 'M', 86, 0, 'M', '2014-07-13', 0, 'y', 30),
(59, '2014-07-12', 'M', 87, 0, 'M', '2014-07-13', 0, 'y', 30),
(60, '2014-07-12', 'M', 74, 0, 'M', '2014-07-13', 0, 'y', 30),
(61, '2014-07-12', 'M', 70, 0, 'M', '2014-07-13', 0, 'y', 30),
(62, '2014-07-12', 'M', 69, 0, 'M', '2014-07-13', 0, 'y', 30),
(63, '2014-07-12', 'M', 68, 0, 'M', '2014-07-13', 0, 'y', 30),
(64, '2014-07-12', 'M', 67, 0, 'M', '2014-07-13', 0, 'y', 30),
(65, '2014-07-12', 'M', 93, 0, 'M', '2014-07-13', 0, 'y', 30),
(66, '2014-07-12', 'M', 71, 0, 'M', '2014-07-13', 0, 'y', 30),
(67, '2014-07-12', 'M', 77, 0, 'M', '2014-07-13', 0, 'y', 30),
(68, '2014-07-12', 'M', 82, 0, 'M', '2014-07-13', 0, 'y', 30),
(69, '2014-07-12', 'M', 90, 0, 'M', '2014-07-13', 0, 'y', 30),
(70, '2014-07-12', 'M', 85, 0, 'M', '2014-07-13', 0, 'y', 30),
(71, '2014-07-12', 'M', 94, 0, 'M', '2014-07-13', 0, 'y', 30),
(72, '2014-07-12', 'M', 92, 0, 'M', '2014-07-13', 0, 'y', 30),
(73, '2014-07-12', 'M', 89, 0, 'M', '2014-07-13', 0, 'y', 30),
(74, '2014-07-12', 'M', 24, 0, 'M', '2014-07-13', 0, 'y', 30),
(75, '2014-07-12', 'M', 76, 0, 'M', '2014-07-13', 0, 'y', 30),
(76, '2014-07-12', 'M', 72, 0, 'M', '2014-07-13', 0, 'y', 30),
(77, '2014-07-12', 'M', 81, 0, 'M', '2014-07-13', 0, 'y', 30),
(78, '2014-07-12', 'M', 23, 0, 'M', '2014-07-13', 0, 'y', 30),
(79, '2014-07-12', 'M', 66, 0, 'M', '2014-07-13', 0, 'y', 30),
(80, '2014-07-12', 'M', 80, 0, 'M', '2014-07-13', 0, 'y', 30),
(81, '2014-07-12', 'M', 78, 0, 'M', '2014-07-13', 0, 'y', 30),
(82, '2014-07-12', 'M', 75, 0, 'M', '2014-07-13', 0, 'y', 30),
(83, '2014-07-12', 'M', 91, 0, 'M', '2014-07-13', 0, 'y', 30),
(84, '2014-07-12', 'M', 73, 0, 'M', '2014-07-13', 0, 'y', 30),
(85, '2014-07-12', 'M', 79, 0, 'M', '2014-07-13', 0, 'y', 30),
(86, '2014-07-12', 'M', 88, 0, 'M', '2014-07-13', 0, 'y', 30),
(87, '2014-07-12', 'M', 83, 0, 'M', '2014-07-13', 0, 'y', 30),
(88, '2014-07-12', 'M', 84, 0, 'M', '2014-07-13', 0, 'y', 30),
(89, '2014-07-12', 'M', 86, 0, 'M', '2014-07-13', 0, 'y', 30),
(90, '2014-07-12', 'M', 87, 0, 'M', '2014-07-13', 0, 'y', 30),
(91, '2014-07-12', 'M', 74, 0, 'M', '2014-07-13', 0, 'y', 30),
(92, '2014-07-12', 'M', 70, 0, 'M', '2014-07-13', 0, 'y', 30),
(93, '2014-07-12', 'M', 69, 0, 'M', '2014-07-13', 0, 'y', 30),
(94, '2014-07-12', 'M', 68, 0, 'M', '2014-07-13', 0, 'y', 30),
(95, '2014-07-12', 'M', 67, 0, 'M', '2014-07-13', 0, 'y', 30),
(96, '2014-07-12', 'M', 93, 0, 'M', '2014-07-13', 0, 'y', 30),
(97, '2014-07-12', 'M', 71, 0, 'M', '2014-07-13', 0, 'y', 30),
(98, '2014-07-12', 'M', 77, 0, 'M', '2014-07-13', 0, 'y', 30),
(99, '2014-07-12', 'M', 82, 0, 'M', '2014-07-13', 0, 'y', 30),
(100, '2014-07-12', 'M', 90, 0, 'M', '2014-07-13', 0, 'y', 30),
(101, '2014-07-12', 'M', 85, 0, 'M', '2014-07-13', 0, 'y', 30),
(102, '2014-07-12', 'M', 94, 0, 'M', '2014-07-13', 0, 'y', 30),
(103, '2014-07-12', 'M', 92, 0, 'M', '2014-07-13', 0, 'y', 30),
(104, '2014-07-12', 'M', 89, 0, 'M', '2014-07-13', 0, 'y', 30),
(105, '2014-07-12', 'M', 24, 0, 'M', '2014-07-13', 0, 'y', 30),
(106, '2014-07-12', 'M', 76, 0, 'M', '2014-07-13', 0, 'y', 30),
(107, '2014-07-12', 'M', 72, 0, 'M', '2014-07-13', 0, 'y', 30),
(108, '2014-07-12', 'M', 81, 0, 'M', '2014-07-13', 0, 'y', 30),
(109, '2014-07-12', 'M', 23, 0, 'M', '2014-07-13', 0, 'y', 30),
(110, '2014-07-12', 'M', 66, 0, 'M', '2014-07-13', 0, 'y', 30),
(111, '2014-07-12', 'M', 80, 0, 'M', '2014-07-13', 0, 'y', 30),
(112, '2014-07-12', 'M', 78, 0, 'M', '2014-07-13', 0, 'y', 30),
(113, '2014-07-12', 'M', 75, 0, 'M', '2014-07-13', 0, 'y', 30),
(114, '2014-07-12', 'M', 91, 0, 'M', '2014-07-13', 0, 'y', 30),
(115, '2014-07-12', 'M', 73, 0, 'M', '2014-07-13', 0, 'y', 30),
(116, '2014-07-12', 'M', 79, 0, 'M', '2014-07-13', 0, 'y', 30),
(117, '2014-07-12', 'M', 88, 0, 'M', '2014-07-13', 0, 'y', 30),
(118, '2014-07-12', 'M', 83, 0, 'M', '2014-07-13', 0, 'y', 30),
(119, '2014-07-12', 'M', 84, 0, 'M', '2014-07-13', 0, 'y', 30),
(120, '2014-07-12', 'M', 86, 0, 'M', '2014-07-13', 0, 'y', 30),
(121, '2014-07-12', 'M', 87, 0, 'M', '2014-07-13', 0, 'y', 30),
(122, '2014-07-12', 'M', 74, 0, 'M', '2014-07-13', 0, 'y', 30),
(123, '2014-07-12', 'M', 70, 0, 'M', '2014-07-13', 0, 'y', 30),
(124, '2014-07-12', 'M', 69, 0, 'M', '2014-07-13', 0, 'y', 30),
(125, '2014-07-12', 'M', 68, 0, 'M', '2014-07-13', 0, 'y', 30),
(126, '2014-07-12', 'M', 67, 0, 'M', '2014-07-13', 0, 'y', 30),
(127, '2014-07-12', 'M', 93, 0, 'M', '2014-07-13', 0, 'y', 30),
(128, '2014-07-12', 'M', 71, 0, 'M', '2014-07-13', 0, 'y', 30),
(129, '2014-07-12', 'M', 77, 0, 'M', '2014-07-13', 0, 'y', 30),
(130, '2014-07-12', 'M', 82, 0, 'M', '2014-07-13', 0, 'y', 30),
(131, '2014-07-12', 'M', 90, 0, 'M', '2014-07-13', 0, 'y', 30),
(132, '2014-07-12', 'M', 85, 0, 'M', '2014-07-13', 0, 'y', 30),
(133, '2014-07-12', 'M', 94, 0, 'M', '2014-07-13', 0, 'y', 30),
(134, '2014-07-12', 'M', 92, 0, 'M', '2014-07-13', 0, 'y', 30),
(135, '2014-07-12', 'M', 89, 0, 'M', '2014-07-13', 0, 'y', 30),
(136, '2014-07-12', 'M', 24, 0, 'M', '2014-07-13', 0, 'y', 30),
(137, '2014-07-12', 'M', 76, 0, 'M', '2014-07-13', 0, 'y', 30),
(138, '2014-07-12', 'M', 72, 0, 'M', '2014-07-13', 0, 'y', 30),
(139, '2014-07-12', 'M', 81, 0, 'M', '2014-07-13', 0, 'y', 30),
(140, '2014-07-12', 'M', 23, 0, 'M', '2014-07-13', 0, 'y', 30),
(141, '2014-07-12', 'M', 66, 0, 'M', '2014-07-13', 0, 'y', 30),
(142, '2014-07-12', 'M', 80, 0, 'M', '2014-07-13', 0, 'y', 30),
(143, '2014-07-12', 'M', 78, 0, 'M', '2014-07-13', 0, 'y', 30),
(144, '2014-07-12', 'M', 75, 0, 'M', '2014-07-13', 0, 'y', 30),
(145, '2014-07-12', 'M', 91, 0, 'M', '2014-07-13', 0, 'y', 30),
(146, '2014-07-12', 'M', 73, 0, 'M', '2014-07-13', 0, 'y', 30),
(147, '2014-07-12', 'M', 79, 0, 'M', '2014-07-13', 0, 'y', 30),
(148, '2014-07-12', 'M', 88, 0, 'M', '2014-07-13', 0, 'y', 30),
(149, '2014-07-12', 'M', 83, 0, 'M', '2014-07-13', 0, 'y', 30),
(150, '2014-07-01', 'E', 11, 0, 'E', '2014-07-16', 0, 'y', 30),
(151, '2014-07-01', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(152, '2014-07-01', 'E', 13, 6, 'E', '2014-07-16', 0, 'y', 30),
(153, '2014-07-01', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(154, '2014-07-01', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(155, '2014-07-01', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(156, '2014-07-01', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(157, '2014-07-01', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(158, '2014-07-15', 'E', 11, 0, 'E', '2014-07-16', 0, 'y', 30),
(159, '2014-07-15', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(160, '2014-07-15', 'E', 13, 6, 'E', '2014-07-16', 0, 'y', 30),
(161, '2014-07-15', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(162, '2014-07-15', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(163, '2014-07-15', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(164, '2014-07-15', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(165, '2014-07-15', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(166, '2014-07-02', 'E', 11, 0, 'E', '2014-07-16', 0, 'y', 30),
(167, '2014-07-02', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(168, '2014-07-02', 'E', 13, 6, 'E', '2014-07-16', 0, 'y', 30),
(169, '2014-07-02', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(170, '2014-07-02', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(171, '2014-07-02', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(172, '2014-07-02', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(173, '2014-07-02', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(174, '2014-07-15', 'E', 11, 0, 'E', '2014-07-16', 0, 'y', 30),
(175, '2014-07-15', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(176, '2014-07-15', 'E', 13, 6, 'E', '2014-07-16', 0, 'y', 30),
(177, '2014-07-15', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(178, '2014-07-15', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(179, '2014-07-15', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(180, '2014-07-15', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(181, '2014-07-15', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(182, '2014-07-15', 'E', 11, 0, 'E', '2014-07-16', 0, 'y', 30),
(183, '2014-07-15', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(184, '2014-07-15', 'E', 13, 6, 'E', '2014-07-16', 0, 'y', 30),
(185, '2014-07-15', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(186, '2014-07-15', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(187, '2014-07-15', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(188, '2014-07-15', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(189, '2014-07-15', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(190, '2014-07-15', 'E', 11, 0, 'E', '2014-07-16', 0, 'y', 30),
(191, '2014-07-15', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(192, '2014-07-15', 'E', 13, 6, 'E', '2014-07-16', 0, 'y', 30),
(193, '2014-07-15', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(194, '2014-07-15', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(195, '2014-07-15', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(196, '2014-07-15', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(197, '2014-07-15', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(198, '2014-07-03', 'E', 11, 0, 'E', '2014-07-16', 0, 'y', 30),
(199, '2014-07-03', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(200, '2014-07-03', 'E', 13, 6, 'E', '2014-07-16', 0, 'y', 30),
(201, '2014-07-03', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(202, '2014-07-03', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(203, '2014-07-03', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(204, '2014-07-03', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(205, '2014-07-03', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(206, '2014-07-04', 'E', 11, 0, 'E', '2014-07-16', 0, 'y', 30),
(207, '2014-07-04', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(208, '2014-07-04', 'E', 13, 6, 'E', '2014-07-16', 0, 'y', 30),
(209, '2014-07-04', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(210, '2014-07-04', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(211, '2014-07-04', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(212, '2014-07-04', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(213, '2014-07-04', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(214, '2014-07-15', 'E', 11, 0, 'E', '2014-07-16', 0, 'y', 30),
(215, '2014-07-15', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(216, '2014-07-15', 'E', 13, 6, 'E', '2014-07-16', 0, 'y', 30),
(217, '2014-07-15', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(218, '2014-07-15', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(219, '2014-07-15', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(220, '2014-07-15', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(221, '2014-07-15', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(222, '2014-07-05', 'E', 11, 0, 'E', '2014-07-16', 0, 'y', 30),
(223, '2014-07-05', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(224, '2014-07-05', 'E', 13, 6, 'E', '2014-07-16', 0, 'y', 30),
(225, '2014-07-05', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(226, '2014-07-05', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(227, '2014-07-05', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(228, '2014-07-05', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(229, '2014-07-05', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(230, '2014-07-06', 'E', 11, 0, 'E', '2014-07-16', 0, 'y', 30),
(231, '2014-07-06', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(232, '2014-07-06', 'E', 13, 6, 'E', '2014-07-16', 0, 'y', 30),
(233, '2014-07-06', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(234, '2014-07-06', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(235, '2014-07-06', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(236, '2014-07-06', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(237, '2014-07-06', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(238, '2014-07-06', 'E', 11, 0, 'E', '2014-07-16', 0, 'y', 30),
(239, '2014-07-06', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(240, '2014-07-06', 'E', 13, 6, 'E', '2014-07-16', 0, 'y', 30),
(241, '2014-07-06', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(242, '2014-07-06', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(243, '2014-07-06', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(244, '2014-07-06', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(245, '2014-07-06', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(246, '2014-07-08', 'E', 11, 0, 'E', '2014-07-16', 0, 'y', 30),
(247, '2014-07-08', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(248, '2014-07-08', 'E', 13, 6, 'E', '2014-07-16', 0, 'y', 30),
(249, '2014-07-08', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(250, '2014-07-08', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(251, '2014-07-08', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(252, '2014-07-08', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(253, '2014-07-08', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(254, '2014-07-09', 'E', 11, 0, 'E', '2014-07-16', 0, 'y', 30),
(255, '2014-07-09', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(256, '2014-07-09', 'E', 13, 6, 'E', '2014-07-16', 0, 'y', 30),
(257, '2014-07-09', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(258, '2014-07-09', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(259, '2014-07-09', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(260, '2014-07-09', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(261, '2014-07-09', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(262, '2014-07-04', 'E', 11, 1, 'E', '2014-07-16', 0, 'y', 30),
(263, '2014-07-04', 'E', 12, 0, 'E', '2014-07-16', 0, 'y', 30),
(264, '2014-07-04', 'E', 13, 0, 'E', '2014-07-16', 0, 'y', 30),
(265, '2014-07-04', 'E', 14, 0, 'E', '2014-07-16', 0, 'y', 30),
(266, '2014-07-04', 'E', 15, 0, 'E', '2014-07-16', 0, 'y', 30),
(267, '2014-07-04', 'E', 16, 0, 'E', '2014-07-16', 0, 'y', 30),
(268, '2014-07-04', 'E', 17, 0, 'E', '2014-07-16', 0, 'y', 30),
(269, '2014-07-04', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(270, '2014-07-07', 'E', 11, 2, 'E', '2014-07-16', 0, 'y', 30),
(271, '2014-07-07', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(272, '2014-07-07', 'E', 13, 6, 'E', '2014-07-16', 0, 'y', 30),
(273, '2014-07-07', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(274, '2014-07-07', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(275, '2014-07-07', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(276, '2014-07-07', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(277, '2014-07-07', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(278, '2014-07-10', 'E', 11, 1, 'E', '2014-07-16', 0, 'y', 30),
(279, '2014-07-10', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(280, '2014-07-10', 'E', 13, 0, 'E', '2014-07-16', 0, 'y', 30),
(281, '2014-07-10', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(282, '2014-07-10', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(283, '2014-07-10', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(284, '2014-07-10', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(285, '2014-07-10', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(286, '2014-07-11', 'E', 11, 1, 'E', '2014-07-16', 0, 'y', 30),
(287, '2014-07-11', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(288, '2014-07-11', 'E', 13, 0, 'E', '2014-07-16', 0, 'y', 30),
(289, '2014-07-11', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(290, '2014-07-11', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(291, '2014-07-11', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(292, '2014-07-11', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(293, '2014-07-11', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(294, '2014-07-12', 'E', 11, 1, 'E', '2014-07-16', 0, 'y', 30),
(295, '2014-07-12', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(296, '2014-07-12', 'E', 13, 0, 'E', '2014-07-16', 0, 'y', 30),
(297, '2014-07-12', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(298, '2014-07-12', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(299, '2014-07-12', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(300, '2014-07-12', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(301, '2014-07-12', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(302, '2014-07-13', 'E', 11, 1, 'E', '2014-07-16', 0, 'y', 30),
(303, '2014-07-13', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(304, '2014-07-13', 'E', 13, 6, 'E', '2014-07-16', 0, 'y', 30),
(305, '2014-07-13', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(306, '2014-07-13', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(307, '2014-07-13', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(308, '2014-07-13', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(309, '2014-07-13', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(310, '2014-07-14', 'E', 11, 0, 'E', '2014-07-16', 0, 'y', 30),
(311, '2014-07-14', 'E', 12, 5, 'E', '2014-07-16', 0, 'y', 30),
(312, '2014-07-14', 'E', 13, 6, 'E', '2014-07-16', 0, 'y', 30),
(313, '2014-07-14', 'E', 14, 2, 'E', '2014-07-16', 0, 'y', 30),
(314, '2014-07-14', 'E', 15, 1, 'E', '2014-07-16', 0, 'y', 30),
(315, '2014-07-14', 'E', 16, 1, 'E', '2014-07-16', 0, 'y', 30),
(316, '2014-07-14', 'E', 17, 2, 'E', '2014-07-16', 0, 'y', 30),
(317, '2014-07-14', 'E', 36, 0, 'E', '2014-07-16', 0, 'y', 30),
(318, '2014-07-01', 'M', 66, 2, 'M', '2014-07-18', 0, 'y', 30),
(319, '2014-07-01', 'M', 67, 3, 'M', '2014-07-18', 0, 'y', 30),
(320, '2014-07-01', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(321, '2014-07-01', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(322, '2014-07-01', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(323, '2014-07-01', 'M', 71, 5, 'M', '2014-07-18', 0, 'y', 30),
(324, '2014-07-01', 'M', 73, 4, 'M', '2014-07-18', 0, 'y', 30),
(325, '2014-07-01', 'M', 74, 1, 'M', '2014-07-18', 0, 'y', 30),
(326, '2014-07-01', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(327, '2014-07-01', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(328, '2014-07-01', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(329, '2014-07-01', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(330, '2014-07-01', 'M', 79, 1, 'M', '2014-07-18', 0, 'y', 30),
(331, '2014-07-01', 'M', 80, 3, 'M', '2014-07-18', 0, 'y', 30),
(332, '2014-07-01', 'M', 81, 2, 'M', '2014-07-18', 0, 'y', 30),
(333, '2014-07-01', 'M', 82, 1, 'M', '2014-07-18', 0, 'y', 30),
(334, '2014-07-01', 'M', 83, 0, 'M', '2014-07-18', 0, 'y', 30),
(335, '2014-07-01', 'M', 84, 0, 'M', '2014-07-18', 0, 'y', 30),
(336, '2014-07-01', 'M', 85, 0, 'M', '2014-07-18', 0, 'y', 30),
(337, '2014-07-01', 'M', 86, 0, 'M', '2014-07-18', 0, 'y', 30),
(338, '2014-07-01', 'M', 87, 0, 'M', '2014-07-18', 0, 'y', 30),
(339, '2014-07-01', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(340, '2014-07-01', 'M', 89, 0, 'M', '2014-07-18', 0, 'y', 30),
(341, '2014-07-01', 'M', 90, 9, 'M', '2014-07-18', 0, 'y', 30),
(342, '2014-07-01', 'M', 91, 0, 'M', '2014-07-18', 0, 'y', 30),
(343, '2014-07-01', 'M', 92, 0, 'M', '2014-07-18', 0, 'y', 30),
(344, '2014-07-01', 'M', 93, 0, 'M', '2014-07-18', 0, 'y', 30),
(345, '2014-07-01', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(346, '2014-07-01', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(347, '2014-07-01', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(348, '2014-07-01', 'M', 97, 3, 'M', '2014-07-18', 0, 'y', 30),
(349, '2014-07-01', 'M', 98, 3, 'M', '2014-07-18', 0, 'y', 30),
(350, '2014-07-01', 'M', 99, 0, 'M', '2014-07-18', 0, 'y', 30),
(351, '2014-07-01', 'M', 100, 4, 'M', '2014-07-18', 0, 'y', 30),
(352, '2014-07-01', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(353, '2014-07-01', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(354, '2014-07-01', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(355, '2014-07-02', 'M', 66, 2, 'M', '2014-07-18', 0, 'y', 30),
(356, '2014-07-02', 'M', 67, 3, 'M', '2014-07-18', 0, 'y', 30),
(357, '2014-07-02', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(358, '2014-07-02', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(359, '2014-07-02', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(360, '2014-07-02', 'M', 71, 4, 'M', '2014-07-18', 0, 'y', 30),
(361, '2014-07-02', 'M', 73, 3, 'M', '2014-07-18', 0, 'y', 30),
(362, '2014-07-02', 'M', 74, 2, 'M', '2014-07-18', 0, 'y', 30),
(363, '2014-07-02', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(364, '2014-07-02', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(365, '2014-07-02', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(366, '2014-07-02', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(367, '2014-07-02', 'M', 79, 2, 'M', '2014-07-18', 0, 'y', 30),
(368, '2014-07-02', 'M', 80, 3, 'M', '2014-07-18', 0, 'y', 30),
(369, '2014-07-02', 'M', 81, 3, 'M', '2014-07-18', 0, 'y', 30),
(370, '2014-07-02', 'M', 82, 1, 'M', '2014-07-18', 0, 'y', 30),
(371, '2014-07-02', 'M', 83, 2, 'M', '2014-07-18', 0, 'y', 30),
(372, '2014-07-02', 'M', 84, 1, 'M', '2014-07-18', 0, 'y', 30),
(373, '2014-07-02', 'M', 85, 3, 'M', '2014-07-18', 0, 'y', 30),
(374, '2014-07-02', 'M', 86, 1, 'M', '2014-07-18', 0, 'y', 30),
(375, '2014-07-02', 'M', 87, 3, 'M', '2014-07-18', 0, 'y', 30),
(376, '2014-07-02', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(377, '2014-07-02', 'M', 89, 0, 'M', '2014-07-18', 0, 'y', 30),
(378, '2014-07-02', 'M', 90, 8, 'M', '2014-07-18', 0, 'y', 30),
(379, '2014-07-02', 'M', 91, 2, 'M', '2014-07-18', 0, 'y', 30),
(380, '2014-07-02', 'M', 92, 2, 'M', '2014-07-18', 0, 'y', 30),
(381, '2014-07-02', 'M', 93, 2, 'M', '2014-07-18', 0, 'y', 30),
(382, '2014-07-02', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(383, '2014-07-02', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(384, '2014-07-02', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(385, '2014-07-02', 'M', 97, 3, 'M', '2014-07-18', 0, 'y', 30),
(386, '2014-07-02', 'M', 98, 3, 'M', '2014-07-18', 0, 'y', 30),
(387, '2014-07-02', 'M', 99, 2, 'M', '2014-07-18', 0, 'y', 30),
(388, '2014-07-02', 'M', 100, 3, 'M', '2014-07-18', 0, 'y', 30),
(389, '2014-07-02', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(390, '2014-07-02', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(391, '2014-07-02', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(392, '2014-07-03', 'M', 66, 3, 'M', '2014-07-18', 0, 'y', 30),
(393, '2014-07-03', 'M', 67, 3, 'M', '2014-07-18', 0, 'y', 30),
(394, '2014-07-03', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(395, '2014-07-03', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(396, '2014-07-03', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(397, '2014-07-03', 'M', 71, 5, 'M', '2014-07-18', 0, 'y', 30),
(398, '2014-07-03', 'M', 73, 4, 'M', '2014-07-18', 0, 'y', 30),
(399, '2014-07-03', 'M', 74, 2, 'M', '2014-07-18', 0, 'y', 30),
(400, '2014-07-03', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(401, '2014-07-03', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(402, '2014-07-03', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(403, '2014-07-03', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(404, '2014-07-03', 'M', 79, 2, 'M', '2014-07-18', 0, 'y', 30),
(405, '2014-07-03', 'M', 80, 3, 'M', '2014-07-18', 0, 'y', 30),
(406, '2014-07-03', 'M', 81, 3, 'M', '2014-07-18', 0, 'y', 30),
(407, '2014-07-03', 'M', 82, 1, 'M', '2014-07-18', 0, 'y', 30),
(408, '2014-07-03', 'M', 83, 2, 'M', '2014-07-18', 0, 'y', 30),
(409, '2014-07-03', 'M', 84, 1, 'M', '2014-07-18', 0, 'y', 30),
(410, '2014-07-03', 'M', 85, 3, 'M', '2014-07-18', 0, 'y', 30),
(411, '2014-07-03', 'M', 86, 1, 'M', '2014-07-18', 0, 'y', 30),
(412, '2014-07-03', 'M', 87, 3, 'M', '2014-07-18', 0, 'y', 30),
(413, '2014-07-03', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(414, '2014-07-03', 'M', 89, 0, 'M', '2014-07-18', 0, 'y', 30),
(415, '2014-07-03', 'M', 90, 9, 'M', '2014-07-18', 0, 'y', 30),
(416, '2014-07-03', 'M', 91, 2, 'M', '2014-07-18', 0, 'y', 30),
(417, '2014-07-03', 'M', 92, 2, 'M', '2014-07-18', 0, 'y', 30),
(418, '2014-07-03', 'M', 93, 2, 'M', '2014-07-18', 0, 'y', 30),
(419, '2014-07-03', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(420, '2014-07-03', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(421, '2014-07-03', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(422, '2014-07-03', 'M', 97, 4, 'M', '2014-07-18', 0, 'y', 30),
(423, '2014-07-03', 'M', 98, 3, 'M', '2014-07-18', 0, 'y', 30),
(424, '2014-07-03', 'M', 99, 2, 'M', '2014-07-18', 0, 'y', 30),
(425, '2014-07-03', 'M', 100, 4, 'M', '2014-07-18', 0, 'y', 30),
(426, '2014-07-03', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(427, '2014-07-03', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(428, '2014-07-03', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(429, '2014-07-17', 'M', 66, 3, 'M', '2014-07-18', 0, 'y', 30),
(430, '2014-07-17', 'M', 67, 3, 'M', '2014-07-18', 0, 'y', 30),
(431, '2014-07-17', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(432, '2014-07-17', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(433, '2014-07-17', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(434, '2014-07-17', 'M', 71, 5, 'M', '2014-07-18', 0, 'y', 30),
(435, '2014-07-17', 'M', 73, 4, 'M', '2014-07-18', 0, 'y', 30),
(436, '2014-07-17', 'M', 74, 2, 'M', '2014-07-18', 0, 'y', 30),
(437, '2014-07-17', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(438, '2014-07-17', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(439, '2014-07-17', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(440, '2014-07-17', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(441, '2014-07-17', 'M', 79, 2, 'M', '2014-07-18', 0, 'y', 30),
(442, '2014-07-17', 'M', 80, 3, 'M', '2014-07-18', 0, 'y', 30),
(443, '2014-07-17', 'M', 81, 2, 'M', '2014-07-18', 0, 'y', 30),
(444, '2014-07-17', 'M', 82, 1, 'M', '2014-07-18', 0, 'y', 30),
(445, '2014-07-17', 'M', 83, 2, 'M', '2014-07-18', 0, 'y', 30),
(446, '2014-07-17', 'M', 84, 1, 'M', '2014-07-18', 0, 'y', 30),
(447, '2014-07-17', 'M', 85, 2, 'M', '2014-07-18', 0, 'y', 30),
(448, '2014-07-17', 'M', 86, 0, 'M', '2014-07-18', 0, 'y', 30),
(449, '2014-07-17', 'M', 87, 3, 'M', '2014-07-18', 0, 'y', 30),
(450, '2014-07-17', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(451, '2014-07-17', 'M', 89, 0, 'M', '2014-07-18', 0, 'y', 30),
(452, '2014-07-17', 'M', 90, 9, 'M', '2014-07-18', 0, 'y', 30),
(453, '2014-07-17', 'M', 91, 2, 'M', '2014-07-18', 0, 'y', 30),
(454, '2014-07-17', 'M', 92, 2, 'M', '2014-07-18', 0, 'y', 30),
(455, '2014-07-17', 'M', 93, 0, 'M', '2014-07-18', 0, 'y', 30),
(456, '2014-07-17', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(457, '2014-07-17', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(458, '2014-07-17', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(459, '2014-07-17', 'M', 97, 3, 'M', '2014-07-18', 0, 'y', 30),
(460, '2014-07-17', 'M', 98, 3, 'M', '2014-07-18', 0, 'y', 30),
(461, '2014-07-17', 'M', 99, 2, 'M', '2014-07-18', 0, 'y', 30),
(462, '2014-07-17', 'M', 100, 4, 'M', '2014-07-18', 0, 'y', 30),
(463, '2014-07-17', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(464, '2014-07-17', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(465, '2014-07-17', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(466, '2014-07-05', 'M', 66, 3, 'M', '2014-07-18', 0, 'y', 30),
(467, '2014-07-05', 'M', 67, 3, 'M', '2014-07-18', 0, 'y', 30),
(468, '2014-07-05', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(469, '2014-07-05', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(470, '2014-07-05', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(471, '2014-07-05', 'M', 71, 5, 'M', '2014-07-18', 0, 'y', 30),
(472, '2014-07-05', 'M', 73, 3, 'M', '2014-07-18', 0, 'y', 30),
(473, '2014-07-05', 'M', 74, 1, 'M', '2014-07-18', 0, 'y', 30),
(474, '2014-07-05', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(475, '2014-07-05', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(476, '2014-07-05', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(477, '2014-07-05', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(478, '2014-07-05', 'M', 79, 0, 'M', '2014-07-18', 0, 'y', 30),
(479, '2014-07-05', 'M', 80, 3, 'M', '2014-07-18', 0, 'y', 30),
(480, '2014-07-05', 'M', 81, 2, 'M', '2014-07-18', 0, 'y', 30),
(481, '2014-07-05', 'M', 82, 0, 'M', '2014-07-18', 0, 'y', 30),
(482, '2014-07-05', 'M', 83, 0, 'M', '2014-07-18', 0, 'y', 30),
(483, '2014-07-05', 'M', 84, 1, 'M', '2014-07-18', 0, 'y', 30),
(484, '2014-07-05', 'M', 85, 3, 'M', '2014-07-18', 0, 'y', 30),
(485, '2014-07-05', 'M', 86, 1, 'M', '2014-07-18', 0, 'y', 30),
(486, '2014-07-05', 'M', 87, 3, 'M', '2014-07-18', 0, 'y', 30),
(487, '2014-07-05', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(488, '2014-07-05', 'M', 89, 0, 'M', '2014-07-18', 0, 'y', 30),
(489, '2014-07-05', 'M', 90, 9, 'M', '2014-07-18', 0, 'y', 30),
(490, '2014-07-05', 'M', 91, 0, 'M', '2014-07-18', 0, 'y', 30),
(491, '2014-07-05', 'M', 92, 2, 'M', '2014-07-18', 0, 'y', 30),
(492, '2014-07-05', 'M', 93, 0, 'M', '2014-07-18', 0, 'y', 30),
(493, '2014-07-05', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(494, '2014-07-05', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(495, '2014-07-05', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(496, '2014-07-05', 'M', 97, 3, 'M', '2014-07-18', 0, 'y', 30),
(497, '2014-07-05', 'M', 98, 3, 'M', '2014-07-18', 0, 'y', 30),
(498, '2014-07-05', 'M', 99, 2, 'M', '2014-07-18', 0, 'y', 30),
(499, '2014-07-05', 'M', 100, 4, 'M', '2014-07-18', 0, 'y', 30),
(500, '2014-07-05', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(501, '2014-07-05', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(502, '2014-07-05', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(503, '2014-07-06', 'M', 66, 3, 'M', '2014-07-18', 0, 'y', 30),
(504, '2014-07-06', 'M', 67, 3, 'M', '2014-07-18', 0, 'y', 30),
(505, '2014-07-06', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(506, '2014-07-06', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(507, '2014-07-06', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(508, '2014-07-06', 'M', 71, 5, 'M', '2014-07-18', 0, 'y', 30),
(509, '2014-07-06', 'M', 73, 4, 'M', '2014-07-18', 0, 'y', 30),
(510, '2014-07-06', 'M', 74, 2, 'M', '2014-07-18', 0, 'y', 30),
(511, '2014-07-06', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(512, '2014-07-06', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(513, '2014-07-06', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(514, '2014-07-06', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(515, '2014-07-06', 'M', 79, 2, 'M', '2014-07-18', 0, 'y', 30),
(516, '2014-07-06', 'M', 80, 3, 'M', '2014-07-18', 0, 'y', 30),
(517, '2014-07-06', 'M', 81, 3, 'M', '2014-07-18', 0, 'y', 30),
(518, '2014-07-06', 'M', 82, 1, 'M', '2014-07-18', 0, 'y', 30),
(519, '2014-07-06', 'M', 83, 2, 'M', '2014-07-18', 0, 'y', 30),
(520, '2014-07-06', 'M', 84, 1, 'M', '2014-07-18', 0, 'y', 30),
(521, '2014-07-06', 'M', 85, 3, 'M', '2014-07-18', 0, 'y', 30),
(522, '2014-07-06', 'M', 86, 1, 'M', '2014-07-18', 0, 'y', 30),
(523, '2014-07-06', 'M', 87, 3, 'M', '2014-07-18', 0, 'y', 30),
(524, '2014-07-06', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(525, '2014-07-06', 'M', 89, 0, 'M', '2014-07-18', 0, 'y', 30),
(526, '2014-07-06', 'M', 90, 9, 'M', '2014-07-18', 0, 'y', 30),
(527, '2014-07-06', 'M', 91, 2, 'M', '2014-07-18', 0, 'y', 30),
(528, '2014-07-06', 'M', 92, 2, 'M', '2014-07-18', 0, 'y', 30),
(529, '2014-07-06', 'M', 93, 2, 'M', '2014-07-18', 0, 'y', 30),
(530, '2014-07-06', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(531, '2014-07-06', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(532, '2014-07-06', 'M', 96, 0, 'M', '2014-07-18', 0, 'y', 30),
(533, '2014-07-06', 'M', 97, 3, 'M', '2014-07-18', 0, 'y', 30),
(534, '2014-07-06', 'M', 98, 3, 'M', '2014-07-18', 0, 'y', 30),
(535, '2014-07-06', 'M', 99, 2, 'M', '2014-07-18', 0, 'y', 30),
(536, '2014-07-06', 'M', 100, 4, 'M', '2014-07-18', 0, 'y', 30),
(537, '2014-07-06', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(538, '2014-07-06', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(539, '2014-07-06', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(540, '2014-07-04', 'M', 66, 3, 'M', '2014-07-18', 0, 'y', 30),
(541, '2014-07-04', 'M', 67, 3, 'M', '2014-07-18', 0, 'y', 30),
(542, '2014-07-04', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(543, '2014-07-04', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(544, '2014-07-04', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(545, '2014-07-04', 'M', 71, 5, 'M', '2014-07-18', 0, 'y', 30),
(546, '2014-07-04', 'M', 73, 4, 'M', '2014-07-18', 0, 'y', 30),
(547, '2014-07-04', 'M', 74, 2, 'M', '2014-07-18', 0, 'y', 30),
(548, '2014-07-04', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(549, '2014-07-04', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(550, '2014-07-04', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(551, '2014-07-04', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(552, '2014-07-04', 'M', 79, 2, 'M', '2014-07-18', 0, 'y', 30),
(553, '2014-07-04', 'M', 80, 3, 'M', '2014-07-18', 0, 'y', 30),
(554, '2014-07-04', 'M', 81, 2, 'M', '2014-07-18', 0, 'y', 30),
(555, '2014-07-04', 'M', 82, 1, 'M', '2014-07-18', 0, 'y', 30),
(556, '2014-07-04', 'M', 83, 2, 'M', '2014-07-18', 0, 'y', 30),
(557, '2014-07-04', 'M', 84, 1, 'M', '2014-07-18', 0, 'y', 30),
(558, '2014-07-04', 'M', 85, 2, 'M', '2014-07-18', 0, 'y', 30),
(559, '2014-07-04', 'M', 86, 0, 'M', '2014-07-18', 0, 'y', 30),
(560, '2014-07-04', 'M', 87, 3, 'M', '2014-07-18', 0, 'y', 30),
(561, '2014-07-04', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(562, '2014-07-04', 'M', 89, 0, 'M', '2014-07-18', 0, 'y', 30),
(563, '2014-07-04', 'M', 90, 9, 'M', '2014-07-18', 0, 'y', 30),
(564, '2014-07-04', 'M', 91, 2, 'M', '2014-07-18', 0, 'y', 30),
(565, '2014-07-04', 'M', 92, 2, 'M', '2014-07-18', 0, 'y', 30),
(566, '2014-07-04', 'M', 93, 0, 'M', '2014-07-18', 0, 'y', 30),
(567, '2014-07-04', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(568, '2014-07-04', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(569, '2014-07-04', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(570, '2014-07-04', 'M', 97, 3, 'M', '2014-07-18', 0, 'y', 30),
(571, '2014-07-04', 'M', 98, 3, 'M', '2014-07-18', 0, 'y', 30),
(572, '2014-07-04', 'M', 99, 2, 'M', '2014-07-18', 0, 'y', 30),
(573, '2014-07-04', 'M', 100, 4, 'M', '2014-07-18', 0, 'y', 30),
(574, '2014-07-04', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(575, '2014-07-04', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(576, '2014-07-04', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(577, '2014-07-07', 'M', 66, 2, 'M', '2014-07-18', 0, 'y', 30),
(578, '2014-07-07', 'M', 67, 3, 'M', '2014-07-18', 0, 'y', 30),
(579, '2014-07-07', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(580, '2014-07-07', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(581, '2014-07-07', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(582, '2014-07-07', 'M', 71, 5, 'M', '2014-07-18', 0, 'y', 30),
(583, '2014-07-07', 'M', 73, 4, 'M', '2014-07-18', 0, 'y', 30),
(584, '2014-07-07', 'M', 74, 0, 'M', '2014-07-18', 0, 'y', 30),
(585, '2014-07-07', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(586, '2014-07-07', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(587, '2014-07-07', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(588, '2014-07-07', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(589, '2014-07-07', 'M', 79, 2, 'M', '2014-07-18', 0, 'y', 30),
(590, '2014-07-07', 'M', 80, 0, 'M', '2014-07-18', 0, 'y', 30),
(591, '2014-07-07', 'M', 81, 3, 'M', '2014-07-18', 0, 'y', 30),
(592, '2014-07-07', 'M', 82, 1, 'M', '2014-07-18', 0, 'y', 30),
(593, '2014-07-07', 'M', 83, 2, 'M', '2014-07-18', 0, 'y', 30),
(594, '2014-07-07', 'M', 84, 1, 'M', '2014-07-18', 0, 'y', 30),
(595, '2014-07-07', 'M', 85, 2, 'M', '2014-07-18', 0, 'y', 30),
(596, '2014-07-07', 'M', 86, 1, 'M', '2014-07-18', 0, 'y', 30),
(597, '2014-07-07', 'M', 87, 2, 'M', '2014-07-18', 0, 'y', 30),
(598, '2014-07-07', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(599, '2014-07-07', 'M', 89, 0, 'M', '2014-07-18', 0, 'y', 30),
(600, '2014-07-07', 'M', 90, 9, 'M', '2014-07-18', 0, 'y', 30),
(601, '2014-07-07', 'M', 91, 2, 'M', '2014-07-18', 0, 'y', 30),
(602, '2014-07-07', 'M', 92, 3, 'M', '2014-07-18', 0, 'y', 30),
(603, '2014-07-07', 'M', 93, 0, 'M', '2014-07-18', 0, 'y', 30),
(604, '2014-07-07', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(605, '2014-07-07', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(606, '2014-07-07', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(607, '2014-07-07', 'M', 97, 3, 'M', '2014-07-18', 0, 'y', 30),
(608, '2014-07-07', 'M', 98, 3, 'M', '2014-07-18', 0, 'y', 30),
(609, '2014-07-07', 'M', 99, 2, 'M', '2014-07-18', 0, 'y', 30),
(610, '2014-07-07', 'M', 100, 4, 'M', '2014-07-18', 0, 'y', 30),
(611, '2014-07-07', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(612, '2014-07-07', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(613, '2014-07-07', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(614, '2014-07-08', 'M', 66, 2, 'M', '2014-07-18', 0, 'y', 30),
(615, '2014-07-08', 'M', 67, 3, 'M', '2014-07-18', 0, 'y', 30),
(616, '2014-07-08', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(617, '2014-07-08', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(618, '2014-07-08', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(619, '2014-07-08', 'M', 71, 5, 'M', '2014-07-18', 0, 'y', 30),
(620, '2014-07-08', 'M', 73, 4, 'M', '2014-07-18', 0, 'y', 30),
(621, '2014-07-08', 'M', 74, 0, 'M', '2014-07-18', 0, 'y', 30),
(622, '2014-07-08', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(623, '2014-07-08', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(624, '2014-07-08', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(625, '2014-07-08', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(626, '2014-07-08', 'M', 79, 2, 'M', '2014-07-18', 0, 'y', 30),
(627, '2014-07-08', 'M', 80, 0, 'M', '2014-07-18', 0, 'y', 30),
(628, '2014-07-08', 'M', 81, 3, 'M', '2014-07-18', 0, 'y', 30),
(629, '2014-07-08', 'M', 82, 1, 'M', '2014-07-18', 0, 'y', 30),
(630, '2014-07-08', 'M', 83, 2, 'M', '2014-07-18', 0, 'y', 30),
(631, '2014-07-08', 'M', 84, 1, 'M', '2014-07-18', 0, 'y', 30),
(632, '2014-07-08', 'M', 85, 3, 'M', '2014-07-18', 0, 'y', 30),
(633, '2014-07-08', 'M', 86, 1, 'M', '2014-07-18', 0, 'y', 30),
(634, '2014-07-08', 'M', 87, 3, 'M', '2014-07-18', 0, 'y', 30),
(635, '2014-07-08', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(636, '2014-07-08', 'M', 89, 0, 'M', '2014-07-18', 0, 'y', 30),
(637, '2014-07-08', 'M', 90, 9, 'M', '2014-07-18', 0, 'y', 30),
(638, '2014-07-08', 'M', 91, 2, 'M', '2014-07-18', 0, 'y', 30),
(639, '2014-07-08', 'M', 92, 2, 'M', '2014-07-18', 0, 'y', 30),
(640, '2014-07-08', 'M', 93, 0, 'M', '2014-07-18', 0, 'y', 30),
(641, '2014-07-08', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(642, '2014-07-08', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(643, '2014-07-08', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(644, '2014-07-08', 'M', 97, 3, 'M', '2014-07-18', 0, 'y', 30),
(645, '2014-07-08', 'M', 98, 3, 'M', '2014-07-18', 0, 'y', 30),
(646, '2014-07-08', 'M', 99, 2, 'M', '2014-07-18', 0, 'y', 30),
(647, '2014-07-08', 'M', 100, 4, 'M', '2014-07-18', 0, 'y', 30),
(648, '2014-07-08', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(649, '2014-07-08', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(650, '2014-07-08', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(651, '2014-07-10', 'M', 66, 2, 'M', '2014-07-18', 0, 'y', 30),
(652, '2014-07-10', 'M', 67, 3, 'M', '2014-07-18', 0, 'y', 30),
(653, '2014-07-10', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(654, '2014-07-10', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(655, '2014-07-10', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(656, '2014-07-10', 'M', 71, 5, 'M', '2014-07-18', 0, 'y', 30),
(657, '2014-07-10', 'M', 73, 4, 'M', '2014-07-18', 0, 'y', 30),
(658, '2014-07-10', 'M', 74, 0, 'M', '2014-07-18', 0, 'y', 30),
(659, '2014-07-10', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(660, '2014-07-10', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(661, '2014-07-10', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(662, '2014-07-10', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(663, '2014-07-10', 'M', 79, 2, 'M', '2014-07-18', 0, 'y', 30),
(664, '2014-07-10', 'M', 80, 3, 'M', '2014-07-18', 0, 'y', 30),
(665, '2014-07-10', 'M', 81, 3, 'M', '2014-07-18', 0, 'y', 30),
(666, '2014-07-10', 'M', 82, 1, 'M', '2014-07-18', 0, 'y', 30),
(667, '2014-07-10', 'M', 83, 2, 'M', '2014-07-18', 0, 'y', 30),
(668, '2014-07-10', 'M', 84, 1, 'M', '2014-07-18', 0, 'y', 30),
(669, '2014-07-10', 'M', 85, 3, 'M', '2014-07-18', 0, 'y', 30),
(670, '2014-07-10', 'M', 86, 1, 'M', '2014-07-18', 0, 'y', 30),
(671, '2014-07-10', 'M', 87, 2, 'M', '2014-07-18', 0, 'y', 30),
(672, '2014-07-10', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(673, '2014-07-10', 'M', 89, 0, 'M', '2014-07-18', 0, 'y', 30),
(674, '2014-07-10', 'M', 90, 9, 'M', '2014-07-18', 0, 'y', 30),
(675, '2014-07-10', 'M', 91, 3, 'M', '2014-07-18', 0, 'y', 30),
(676, '2014-07-10', 'M', 92, 3, 'M', '2014-07-18', 0, 'y', 30),
(677, '2014-07-10', 'M', 93, 0, 'M', '2014-07-18', 0, 'y', 30),
(678, '2014-07-10', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(679, '2014-07-10', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(680, '2014-07-10', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(681, '2014-07-10', 'M', 97, 3, 'M', '2014-07-18', 0, 'y', 30),
(682, '2014-07-10', 'M', 98, 3, 'M', '2014-07-18', 0, 'y', 30),
(683, '2014-07-10', 'M', 99, 2, 'M', '2014-07-18', 0, 'y', 30),
(684, '2014-07-10', 'M', 100, 4, 'M', '2014-07-18', 0, 'y', 30),
(685, '2014-07-10', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(686, '2014-07-10', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(687, '2014-07-10', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(688, '2014-07-11', 'M', 66, 3, 'M', '2014-07-18', 0, 'y', 30),
(689, '2014-07-11', 'M', 67, 3, 'M', '2014-07-18', 0, 'y', 30),
(690, '2014-07-11', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(691, '2014-07-11', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(692, '2014-07-11', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(693, '2014-07-11', 'M', 71, 5, 'M', '2014-07-18', 0, 'y', 30),
(694, '2014-07-11', 'M', 73, 3, 'M', '2014-07-18', 0, 'y', 30),
(695, '2014-07-11', 'M', 74, 0, 'M', '2014-07-18', 0, 'y', 30),
(696, '2014-07-11', 'M', 75, 0, 'M', '2014-07-18', 0, 'y', 30),
(697, '2014-07-11', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(698, '2014-07-11', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(699, '2014-07-11', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(700, '2014-07-11', 'M', 79, 2, 'M', '2014-07-18', 0, 'y', 30),
(701, '2014-07-11', 'M', 80, 3, 'M', '2014-07-18', 0, 'y', 30),
(702, '2014-07-11', 'M', 81, 0, 'M', '2014-07-18', 0, 'y', 30),
(703, '2014-07-11', 'M', 82, 1, 'M', '2014-07-18', 0, 'y', 30),
(704, '2014-07-11', 'M', 83, 2, 'M', '2014-07-18', 0, 'y', 30),
(705, '2014-07-11', 'M', 84, 1, 'M', '2014-07-18', 0, 'y', 30),
(706, '2014-07-11', 'M', 85, 2, 'M', '2014-07-18', 0, 'y', 30),
(707, '2014-07-11', 'M', 86, 1, 'M', '2014-07-18', 0, 'y', 30),
(708, '2014-07-11', 'M', 87, 2, 'M', '2014-07-18', 0, 'y', 30),
(709, '2014-07-11', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(710, '2014-07-11', 'M', 89, 0, 'M', '2014-07-18', 0, 'y', 30),
(711, '2014-07-11', 'M', 90, 9, 'M', '2014-07-18', 0, 'y', 30),
(712, '2014-07-11', 'M', 91, 2, 'M', '2014-07-18', 0, 'y', 30),
(713, '2014-07-11', 'M', 92, 2, 'M', '2014-07-18', 0, 'y', 30),
(714, '2014-07-11', 'M', 93, 0, 'M', '2014-07-18', 0, 'y', 30),
(715, '2014-07-11', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(716, '2014-07-11', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(717, '2014-07-11', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(718, '2014-07-11', 'M', 97, 3, 'M', '2014-07-18', 0, 'y', 30),
(719, '2014-07-11', 'M', 98, 3, 'M', '2014-07-18', 0, 'y', 30),
(720, '2014-07-11', 'M', 99, 2, 'M', '2014-07-18', 0, 'y', 30),
(721, '2014-07-11', 'M', 100, 4, 'M', '2014-07-18', 0, 'y', 30),
(722, '2014-07-11', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(723, '2014-07-11', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(724, '2014-07-11', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(725, '2014-07-12', 'M', 66, 3, 'M', '2014-07-18', 0, 'y', 30),
(726, '2014-07-12', 'M', 67, 4, 'M', '2014-07-18', 0, 'y', 30),
(727, '2014-07-12', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(728, '2014-07-12', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(729, '2014-07-12', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(730, '2014-07-12', 'M', 71, 5, 'M', '2014-07-18', 0, 'y', 30),
(731, '2014-07-12', 'M', 73, 4, 'M', '2014-07-18', 0, 'y', 30),
(732, '2014-07-12', 'M', 74, 0, 'M', '2014-07-18', 0, 'y', 30),
(733, '2014-07-12', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(734, '2014-07-12', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(735, '2014-07-12', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(736, '2014-07-12', 'M', 78, 1, 'M', '2014-07-18', 0, 'y', 30),
(737, '2014-07-12', 'M', 79, 4, 'M', '2014-07-18', 0, 'y', 30),
(738, '2014-07-12', 'M', 80, 1, 'M', '2014-07-18', 0, 'y', 30),
(739, '2014-07-12', 'M', 81, 5, 'M', '2014-07-18', 0, 'y', 30),
(740, '2014-07-12', 'M', 82, 0, 'M', '2014-07-18', 0, 'y', 30),
(741, '2014-07-12', 'M', 83, 1, 'M', '2014-07-18', 0, 'y', 30),
(742, '2014-07-12', 'M', 84, 2, 'M', '2014-07-18', 0, 'y', 30),
(743, '2014-07-12', 'M', 85, 1, 'M', '2014-07-18', 0, 'y', 30),
(744, '2014-07-12', 'M', 86, 3, 'M', '2014-07-18', 0, 'y', 30),
(745, '2014-07-12', 'M', 87, 1, 'M', '2014-07-18', 0, 'y', 30),
(746, '2014-07-12', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(747, '2014-07-12', 'M', 89, 3, 'M', '2014-07-18', 0, 'y', 30),
(748, '2014-07-12', 'M', 90, 10, 'M', '2014-07-18', 0, 'y', 30),
(749, '2014-07-12', 'M', 91, 4, 'M', '2014-07-18', 0, 'y', 30),
(750, '2014-07-12', 'M', 92, 4, 'M', '2014-07-18', 0, 'y', 30),
(751, '2014-07-12', 'M', 93, 2, 'M', '2014-07-18', 0, 'y', 30),
(752, '2014-07-12', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(753, '2014-07-12', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(754, '2014-07-12', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(755, '2014-07-12', 'M', 97, 4, 'M', '2014-07-18', 0, 'y', 30),
(756, '2014-07-12', 'M', 98, 4, 'M', '2014-07-18', 0, 'y', 30),
(757, '2014-07-12', 'M', 99, 2, 'M', '2014-07-18', 0, 'y', 30),
(758, '2014-07-12', 'M', 100, 4, 'M', '2014-07-18', 0, 'y', 30),
(759, '2014-07-12', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(760, '2014-07-12', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(761, '2014-07-12', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(762, '2014-07-13', 'M', 66, 2, 'M', '2014-07-18', 0, 'y', 30),
(763, '2014-07-13', 'M', 67, 3, 'M', '2014-07-18', 0, 'y', 30),
(764, '2014-07-13', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(765, '2014-07-13', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(766, '2014-07-13', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(767, '2014-07-13', 'M', 71, 5, 'M', '2014-07-18', 0, 'y', 30),
(768, '2014-07-13', 'M', 73, 4, 'M', '2014-07-18', 0, 'y', 30),
(769, '2014-07-13', 'M', 74, 0, 'M', '2014-07-18', 0, 'y', 30),
(770, '2014-07-13', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(771, '2014-07-13', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(772, '2014-07-13', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(773, '2014-07-13', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(774, '2014-07-13', 'M', 79, 1, 'M', '2014-07-18', 0, 'y', 30),
(775, '2014-07-13', 'M', 80, 3, 'M', '2014-07-18', 0, 'y', 30),
(776, '2014-07-13', 'M', 81, 3, 'M', '2014-07-18', 0, 'y', 30),
(777, '2014-07-13', 'M', 82, 1, 'M', '2014-07-18', 0, 'y', 30),
(778, '2014-07-13', 'M', 83, 2, 'M', '2014-07-18', 0, 'y', 30),
(779, '2014-07-13', 'M', 84, 1, 'M', '2014-07-18', 0, 'y', 30),
(780, '2014-07-13', 'M', 85, 3, 'M', '2014-07-18', 0, 'y', 30),
(781, '2014-07-13', 'M', 86, 1, 'M', '2014-07-18', 0, 'y', 30),
(782, '2014-07-13', 'M', 87, 2, 'M', '2014-07-18', 0, 'y', 30),
(783, '2014-07-13', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(784, '2014-07-13', 'M', 89, 2, 'M', '2014-07-18', 0, 'y', 30),
(785, '2014-07-13', 'M', 90, 10, 'M', '2014-07-18', 0, 'y', 30),
(786, '2014-07-13', 'M', 91, 2, 'M', '2014-07-18', 0, 'y', 30),
(787, '2014-07-13', 'M', 92, 2, 'M', '2014-07-18', 0, 'y', 30),
(788, '2014-07-13', 'M', 93, 0, 'M', '2014-07-18', 0, 'y', 30),
(789, '2014-07-13', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(790, '2014-07-13', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(791, '2014-07-13', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(792, '2014-07-13', 'M', 97, 3, 'M', '2014-07-18', 0, 'y', 30),
(793, '2014-07-13', 'M', 98, 0, 'M', '2014-07-18', 0, 'y', 30),
(794, '2014-07-13', 'M', 99, 0, 'M', '2014-07-18', 0, 'y', 30),
(795, '2014-07-13', 'M', 100, 0, 'M', '2014-07-18', 0, 'y', 30),
(796, '2014-07-13', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(797, '2014-07-13', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(798, '2014-07-13', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(799, '2014-07-13', 'M', 66, 2, 'M', '2014-07-18', 0, 'y', 30),
(800, '2014-07-13', 'M', 67, 3, 'M', '2014-07-18', 0, 'y', 30),
(801, '2014-07-13', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(802, '2014-07-13', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(803, '2014-07-13', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(804, '2014-07-13', 'M', 71, 5, 'M', '2014-07-18', 0, 'y', 30),
(805, '2014-07-13', 'M', 73, 4, 'M', '2014-07-18', 0, 'y', 30),
(806, '2014-07-13', 'M', 74, 0, 'M', '2014-07-18', 0, 'y', 30),
(807, '2014-07-13', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(808, '2014-07-13', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(809, '2014-07-13', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(810, '2014-07-13', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(811, '2014-07-13', 'M', 79, 1, 'M', '2014-07-18', 0, 'y', 30),
(812, '2014-07-13', 'M', 80, 3, 'M', '2014-07-18', 0, 'y', 30),
(813, '2014-07-13', 'M', 81, 3, 'M', '2014-07-18', 0, 'y', 30),
(814, '2014-07-13', 'M', 82, 1, 'M', '2014-07-18', 0, 'y', 30),
(815, '2014-07-13', 'M', 83, 2, 'M', '2014-07-18', 0, 'y', 30),
(816, '2014-07-13', 'M', 84, 1, 'M', '2014-07-18', 0, 'y', 30);
INSERT INTO `milksell` (`milkSellId`, `milkSellDate`, `milkTime`, `customerId`, `milkGiven`, `customerMilkTime`, `nextDate`, `nextLitre`, `billCreated`, `rate`) VALUES
(817, '2014-07-13', 'M', 85, 3, 'M', '2014-07-18', 0, 'y', 30),
(818, '2014-07-13', 'M', 86, 4, 'M', '2014-07-18', 0, 'y', 30),
(819, '2014-07-13', 'M', 87, 2, 'M', '2014-07-18', 0, 'y', 30),
(820, '2014-07-13', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(821, '2014-07-13', 'M', 89, 2, 'M', '2014-07-18', 0, 'y', 30),
(822, '2014-07-13', 'M', 90, 10, 'M', '2014-07-18', 0, 'y', 30),
(823, '2014-07-13', 'M', 91, 2, 'M', '2014-07-18', 0, 'y', 30),
(824, '2014-07-13', 'M', 92, 2, 'M', '2014-07-18', 0, 'y', 30),
(825, '2014-07-13', 'M', 93, 0, 'M', '2014-07-18', 0, 'y', 30),
(826, '2014-07-13', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(827, '2014-07-13', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(828, '2014-07-13', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(829, '2014-07-13', 'M', 97, 3, 'M', '2014-07-18', 0, 'y', 30),
(830, '2014-07-13', 'M', 98, 3, 'M', '2014-07-18', 0, 'y', 30),
(831, '2014-07-13', 'M', 99, 2, 'M', '2014-07-18', 0, 'y', 30),
(832, '2014-07-13', 'M', 100, 4, 'M', '2014-07-18', 0, 'y', 30),
(833, '2014-07-13', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(834, '2014-07-13', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(835, '2014-07-13', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(836, '2014-07-14', 'M', 66, 2, 'M', '2014-07-18', 0, 'y', 30),
(837, '2014-07-14', 'M', 67, 3, 'M', '2014-07-18', 0, 'y', 30),
(838, '2014-07-14', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(839, '2014-07-14', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(840, '2014-07-14', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(841, '2014-07-14', 'M', 71, 5, 'M', '2014-07-18', 0, 'y', 30),
(842, '2014-07-14', 'M', 73, 3, 'M', '2014-07-18', 0, 'y', 30),
(843, '2014-07-14', 'M', 74, 0, 'M', '2014-07-18', 0, 'y', 30),
(844, '2014-07-14', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(845, '2014-07-14', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(846, '2014-07-14', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(847, '2014-07-14', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(848, '2014-07-14', 'M', 79, 0, 'M', '2014-07-18', 0, 'y', 30),
(849, '2014-07-14', 'M', 80, 3, 'M', '2014-07-18', 0, 'y', 30),
(850, '2014-07-14', 'M', 81, 3, 'M', '2014-07-18', 0, 'y', 30),
(851, '2014-07-14', 'M', 82, 1, 'M', '2014-07-18', 0, 'y', 30),
(852, '2014-07-14', 'M', 83, 2, 'M', '2014-07-18', 0, 'y', 30),
(853, '2014-07-14', 'M', 84, 1, 'M', '2014-07-18', 0, 'y', 30),
(854, '2014-07-14', 'M', 85, 2, 'M', '2014-07-18', 0, 'y', 30),
(855, '2014-07-14', 'M', 86, 1, 'M', '2014-07-18', 0, 'y', 30),
(856, '2014-07-14', 'M', 87, 2, 'M', '2014-07-18', 0, 'y', 30),
(857, '2014-07-14', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(858, '2014-07-14', 'M', 89, 2, 'M', '2014-07-18', 0, 'y', 30),
(859, '2014-07-14', 'M', 90, 6, 'M', '2014-07-18', 0, 'y', 30),
(860, '2014-07-14', 'M', 91, 3, 'M', '2014-07-18', 0, 'y', 30),
(861, '2014-07-14', 'M', 92, 3, 'M', '2014-07-18', 0, 'y', 30),
(862, '2014-07-14', 'M', 93, 2, 'M', '2014-07-18', 0, 'y', 30),
(863, '2014-07-14', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(864, '2014-07-14', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(865, '2014-07-14', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(866, '2014-07-14', 'M', 97, 4, 'M', '2014-07-18', 0, 'y', 30),
(867, '2014-07-14', 'M', 98, 0, 'M', '2014-07-18', 0, 'y', 30),
(868, '2014-07-14', 'M', 99, 0, 'M', '2014-07-18', 0, 'y', 30),
(869, '2014-07-14', 'M', 100, 4, 'M', '2014-07-18', 0, 'y', 30),
(870, '2014-07-14', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(871, '2014-07-14', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(872, '2014-07-14', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(873, '2014-07-15', 'M', 66, 2, 'M', '2014-07-18', 0, 'y', 30),
(874, '2014-07-15', 'M', 67, 0, 'M', '2014-07-18', 0, 'y', 30),
(875, '2014-07-15', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(876, '2014-07-15', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(877, '2014-07-15', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(878, '2014-07-15', 'M', 71, 5, 'M', '2014-07-18', 0, 'y', 30),
(879, '2014-07-15', 'M', 73, 4, 'M', '2014-07-18', 0, 'y', 30),
(880, '2014-07-15', 'M', 74, 0, 'M', '2014-07-18', 0, 'y', 30),
(881, '2014-07-15', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(882, '2014-07-15', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(883, '2014-07-15', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(884, '2014-07-15', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(885, '2014-07-15', 'M', 79, 1, 'M', '2014-07-18', 0, 'y', 30),
(886, '2014-07-15', 'M', 80, 3, 'M', '2014-07-18', 0, 'y', 30),
(887, '2014-07-15', 'M', 81, 3, 'M', '2014-07-18', 0, 'y', 30),
(888, '2014-07-15', 'M', 82, 1, 'M', '2014-07-18', 0, 'y', 30),
(889, '2014-07-15', 'M', 83, 2, 'M', '2014-07-18', 0, 'y', 30),
(890, '2014-07-15', 'M', 84, 1, 'M', '2014-07-18', 0, 'y', 30),
(891, '2014-07-15', 'M', 85, 3, 'M', '2014-07-18', 0, 'y', 30),
(892, '2014-07-15', 'M', 86, 1, 'M', '2014-07-18', 0, 'y', 30),
(893, '2014-07-15', 'M', 87, 3, 'M', '2014-07-18', 0, 'y', 30),
(894, '2014-07-15', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(895, '2014-07-15', 'M', 89, 2, 'M', '2014-07-18', 0, 'y', 30),
(896, '2014-07-15', 'M', 90, 6, 'M', '2014-07-18', 0, 'y', 30),
(897, '2014-07-15', 'M', 91, 2, 'M', '2014-07-18', 0, 'y', 30),
(898, '2014-07-15', 'M', 92, 2, 'M', '2014-07-18', 0, 'y', 30),
(899, '2014-07-15', 'M', 93, 0, 'M', '2014-07-18', 0, 'y', 30),
(900, '2014-07-15', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(901, '2014-07-15', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(902, '2014-07-15', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(903, '2014-07-15', 'M', 97, 3, 'M', '2014-07-18', 0, 'y', 30),
(904, '2014-07-15', 'M', 98, 3, 'M', '2014-07-18', 0, 'y', 30),
(905, '2014-07-15', 'M', 99, 2, 'M', '2014-07-18', 0, 'y', 30),
(906, '2014-07-15', 'M', 100, 4, 'M', '2014-07-18', 0, 'y', 30),
(907, '2014-07-15', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(908, '2014-07-15', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(909, '2014-07-15', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(910, '2014-07-16', 'M', 66, 3, 'M', '2014-07-18', 0, 'y', 30),
(911, '2014-07-16', 'M', 67, 0, 'M', '2014-07-18', 0, 'y', 30),
(912, '2014-07-16', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(913, '2014-07-16', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(914, '2014-07-16', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(915, '2014-07-16', 'M', 71, 5, 'M', '2014-07-18', 0, 'y', 30),
(916, '2014-07-16', 'M', 73, 4, 'M', '2014-07-18', 0, 'y', 30),
(917, '2014-07-16', 'M', 74, 0, 'M', '2014-07-18', 0, 'y', 30),
(918, '2014-07-16', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(919, '2014-07-16', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(920, '2014-07-16', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(921, '2014-07-16', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(922, '2014-07-16', 'M', 79, 1, 'M', '2014-07-18', 0, 'y', 30),
(923, '2014-07-16', 'M', 80, 3, 'M', '2014-07-18', 0, 'y', 30),
(924, '2014-07-16', 'M', 81, 3, 'M', '2014-07-18', 0, 'y', 30),
(925, '2014-07-16', 'M', 82, 1, 'M', '2014-07-18', 0, 'y', 30),
(926, '2014-07-16', 'M', 83, 0, 'M', '2014-07-18', 0, 'y', 30),
(927, '2014-07-16', 'M', 84, 1, 'M', '2014-07-18', 0, 'y', 30),
(928, '2014-07-16', 'M', 85, 2, 'M', '2014-07-18', 0, 'y', 30),
(929, '2014-07-16', 'M', 86, 0, 'M', '2014-07-18', 0, 'y', 30),
(930, '2014-07-16', 'M', 87, 2, 'M', '2014-07-18', 0, 'y', 30),
(931, '2014-07-16', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(932, '2014-07-16', 'M', 89, 0, 'M', '2014-07-18', 0, 'y', 30),
(933, '2014-07-16', 'M', 90, 6, 'M', '2014-07-18', 0, 'y', 30),
(934, '2014-07-16', 'M', 91, 0, 'M', '2014-07-18', 0, 'y', 30),
(935, '2014-07-16', 'M', 92, 0, 'M', '2014-07-18', 0, 'y', 30),
(936, '2014-07-16', 'M', 93, 0, 'M', '2014-07-18', 0, 'y', 30),
(937, '2014-07-16', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(938, '2014-07-16', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(939, '2014-07-16', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(940, '2014-07-16', 'M', 97, 3, 'M', '2014-07-18', 0, 'y', 30),
(941, '2014-07-16', 'M', 98, 3, 'M', '2014-07-18', 0, 'y', 30),
(942, '2014-07-16', 'M', 99, 2, 'M', '2014-07-18', 0, 'y', 30),
(943, '2014-07-16', 'M', 100, 4, 'M', '2014-07-18', 0, 'y', 30),
(944, '2014-07-16', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(945, '2014-07-16', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(946, '2014-07-16', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(947, '2014-07-17', 'M', 66, 3, 'M', '2014-07-18', 0, 'y', 30),
(948, '2014-07-17', 'M', 67, 3, 'M', '2014-07-18', 0, 'y', 30),
(949, '2014-07-17', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(950, '2014-07-17', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(951, '2014-07-17', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(952, '2014-07-17', 'M', 71, 4, 'M', '2014-07-18', 0, 'y', 30),
(953, '2014-07-17', 'M', 73, 3, 'M', '2014-07-18', 0, 'y', 30),
(954, '2014-07-17', 'M', 74, 0, 'M', '2014-07-18', 0, 'y', 30),
(955, '2014-07-17', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(956, '2014-07-17', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(957, '2014-07-17', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(958, '2014-07-17', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(959, '2014-07-17', 'M', 79, 0, 'M', '2014-07-18', 0, 'y', 30),
(960, '2014-07-17', 'M', 80, 3, 'M', '2014-07-18', 0, 'y', 30),
(961, '2014-07-17', 'M', 81, 2, 'M', '2014-07-18', 0, 'y', 30),
(962, '2014-07-17', 'M', 82, 0, 'M', '2014-07-18', 0, 'y', 30),
(963, '2014-07-17', 'M', 83, 0, 'M', '2014-07-18', 0, 'y', 30),
(964, '2014-07-17', 'M', 84, 1, 'M', '2014-07-18', 0, 'y', 30),
(965, '2014-07-17', 'M', 85, 2, 'M', '2014-07-18', 0, 'y', 30),
(966, '2014-07-17', 'M', 86, 1, 'M', '2014-07-18', 0, 'y', 30),
(967, '2014-07-17', 'M', 87, 2, 'M', '2014-07-18', 0, 'y', 30),
(968, '2014-07-17', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(969, '2014-07-17', 'M', 89, 0, 'M', '2014-07-18', 0, 'y', 30),
(970, '2014-07-17', 'M', 90, 10, 'M', '2014-07-18', 0, 'y', 30),
(971, '2014-07-17', 'M', 91, 0, 'M', '2014-07-18', 0, 'y', 30),
(972, '2014-07-17', 'M', 92, 0, 'M', '2014-07-18', 0, 'y', 30),
(973, '2014-07-17', 'M', 93, 0, 'M', '2014-07-18', 0, 'y', 30),
(974, '2014-07-17', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(975, '2014-07-17', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(976, '2014-07-17', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(977, '2014-07-17', 'M', 97, 3, 'M', '2014-07-18', 0, 'y', 30),
(978, '2014-07-17', 'M', 98, 3, 'M', '2014-07-18', 0, 'y', 30),
(979, '2014-07-17', 'M', 99, 2, 'M', '2014-07-18', 0, 'y', 30),
(980, '2014-07-17', 'M', 100, 4, 'M', '2014-07-18', 0, 'y', 30),
(981, '2014-07-17', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(982, '2014-07-17', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(983, '2014-07-17', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(984, '2014-07-09', 'M', 66, 3, 'M', '2014-07-18', 0, 'y', 30),
(985, '2014-07-09', 'M', 67, 3, 'M', '2014-07-18', 0, 'y', 30),
(986, '2014-07-09', 'M', 68, 1, 'M', '2014-07-18', 0, 'y', 30),
(987, '2014-07-09', 'M', 69, 2, 'M', '2014-07-18', 0, 'y', 30),
(988, '2014-07-09', 'M', 24, 3, 'M', '2014-07-18', 0, 'y', 30),
(989, '2014-07-09', 'M', 71, 5, 'M', '2014-07-18', 0, 'y', 30),
(990, '2014-07-09', 'M', 73, 4, 'M', '2014-07-18', 0, 'y', 30),
(991, '2014-07-09', 'M', 74, 0, 'M', '2014-07-18', 0, 'y', 30),
(992, '2014-07-09', 'M', 75, 2, 'M', '2014-07-18', 0, 'y', 30),
(993, '2014-07-09', 'M', 76, 1, 'M', '2014-07-18', 0, 'y', 30),
(994, '2014-07-09', 'M', 77, 2, 'M', '2014-07-18', 0, 'y', 30),
(995, '2014-07-09', 'M', 78, 4, 'M', '2014-07-18', 0, 'y', 30),
(996, '2014-07-09', 'M', 79, 2, 'M', '2014-07-18', 0, 'y', 30),
(997, '2014-07-09', 'M', 80, 3, 'M', '2014-07-18', 0, 'y', 30),
(998, '2014-07-09', 'M', 81, 2, 'M', '2014-07-18', 0, 'y', 30),
(999, '2014-07-09', 'M', 82, 1, 'M', '2014-07-18', 0, 'y', 30),
(1000, '2014-07-09', 'M', 83, 2, 'M', '2014-07-18', 0, 'y', 30),
(1001, '2014-07-09', 'M', 84, 1, 'M', '2014-07-18', 0, 'y', 30),
(1002, '2014-07-09', 'M', 85, 2, 'M', '2014-07-18', 0, 'y', 30),
(1003, '2014-07-09', 'M', 86, 0, 'M', '2014-07-18', 0, 'y', 30),
(1004, '2014-07-09', 'M', 87, 3, 'M', '2014-07-18', 0, 'y', 30),
(1005, '2014-07-09', 'M', 88, 0, 'M', '2014-07-18', 0, 'y', 30),
(1006, '2014-07-09', 'M', 89, 0, 'M', '2014-07-18', 0, 'y', 30),
(1007, '2014-07-09', 'M', 90, 9, 'M', '2014-07-18', 0, 'y', 30),
(1008, '2014-07-09', 'M', 91, 2, 'M', '2014-07-18', 0, 'y', 30),
(1009, '2014-07-09', 'M', 92, 3, 'M', '2014-07-18', 0, 'y', 30),
(1010, '2014-07-09', 'M', 93, 0, 'M', '2014-07-18', 0, 'y', 30),
(1011, '2014-07-09', 'M', 94, 2, 'M', '2014-07-18', 0, 'y', 30),
(1012, '2014-07-09', 'M', 95, 1, 'M', '2014-07-18', 0, 'y', 30),
(1013, '2014-07-09', 'M', 96, 1, 'M', '2014-07-18', 0, 'y', 30),
(1014, '2014-07-09', 'M', 97, 3, 'M', '2014-07-18', 0, 'y', 30),
(1015, '2014-07-09', 'M', 98, 3, 'M', '2014-07-18', 0, 'y', 30),
(1016, '2014-07-09', 'M', 99, 2, 'M', '2014-07-18', 0, 'y', 30),
(1017, '2014-07-09', 'M', 100, 4, 'M', '2014-07-18', 0, 'y', 30),
(1018, '2014-07-09', 'M', 23, 0, 'M', '2014-07-18', 0, 'y', 30),
(1019, '2014-07-09', 'M', 72, 0, 'M', '2014-07-18', 0, 'y', 30),
(1020, '2014-07-09', 'M', 70, 0, 'M', '2014-07-18', 0, 'y', 30),
(1021, '2014-07-01', 'E', 26, 3, 'E', '2014-07-19', 0, 'y', 30),
(1022, '2014-07-01', 'E', 27, 1, 'E', '2014-07-19', 0, 'y', 30),
(1023, '2014-07-01', 'E', 28, 3, 'E', '2014-07-19', 0, 'y', 30),
(1024, '2014-07-01', 'E', 29, 4, 'E', '2014-07-19', 0, 'y', 30),
(1025, '2014-07-01', 'E', 30, 4, 'E', '2014-07-19', 0, 'y', 30),
(1026, '2014-07-01', 'E', 31, 2, 'E', '2014-07-19', 0, 'y', 30),
(1027, '2014-07-01', 'E', 33, 2, 'E', '2014-07-19', 0, 'y', 30),
(1028, '2014-07-01', 'E', 34, 3, 'E', '2014-07-19', 0, 'y', 30),
(1029, '2014-07-01', 'E', 35, 2, 'E', '2014-07-19', 0, 'y', 30),
(1030, '2014-07-01', 'E', 36, 2, 'E', '2014-07-19', 0, 'y', 30),
(1031, '2014-07-01', 'E', 37, 3, 'E', '2014-07-19', 0, 'y', 30),
(1032, '2014-07-01', 'E', 38, 3, 'E', '2014-07-19', 0, 'y', 30),
(1033, '2014-07-01', 'E', 114, 1, 'E', '2014-07-19', 0, 'y', 30),
(1034, '2014-07-01', 'E', 39, 2, 'E', '2014-07-19', 0, 'y', 30),
(1035, '2014-07-01', 'E', 115, 2, 'E', '2014-07-19', 0, 'y', 30),
(1036, '2014-07-01', 'E', 40, 1, 'E', '2014-07-19', 0, 'y', 30),
(1037, '2014-07-01', 'E', 41, 3, 'E', '2014-07-19', 0, 'y', 30),
(1038, '2014-07-01', 'E', 42, 4, 'E', '2014-07-19', 0, 'y', 30),
(1039, '2014-07-01', 'E', 43, 1, 'E', '2014-07-19', 0, 'y', 30),
(1040, '2014-07-01', 'E', 44, 2, 'E', '2014-07-19', 0, 'y', 30),
(1041, '2014-07-01', 'E', 58, 1, 'E', '2014-07-19', 0, 'y', 30),
(1042, '2014-07-01', 'E', 45, 1, 'E', '2014-07-19', 0, 'y', 30),
(1043, '2014-07-01', 'E', 59, 1, 'E', '2014-07-19', 0, 'y', 30),
(1044, '2014-07-01', 'E', 60, 2, 'E', '2014-07-19', 0, 'y', 30),
(1045, '2014-07-01', 'E', 61, 2, 'E', '2014-07-19', 0, 'y', 30),
(1046, '2014-07-01', 'E', 46, 2, 'E', '2014-07-19', 0, 'y', 30),
(1047, '2014-07-01', 'E', 47, 0, 'E', '2014-07-19', 0, 'y', 30),
(1048, '2014-07-01', 'E', 48, 1, 'E', '2014-07-19', 0, 'y', 30),
(1049, '2014-07-01', 'E', 49, 2, 'E', '2014-07-19', 0, 'y', 30),
(1050, '2014-07-01', 'E', 50, 4, 'E', '2014-07-19', 0, 'y', 30),
(1051, '2014-07-01', 'E', 116, 4, 'E', '2014-07-19', 0, 'y', 30),
(1052, '2014-07-01', 'E', 117, 0, 'E', '2014-07-19', 0, 'y', 30),
(1053, '2014-07-01', 'E', 51, 1, 'E', '2014-07-19', 0, 'y', 30),
(1054, '2014-07-01', 'E', 53, 2, 'E', '2014-07-19', 0, 'y', 30),
(1055, '2014-07-01', 'E', 62, 6, 'E', '2014-07-19', 0, 'y', 30),
(1056, '2014-07-01', 'E', 63, 4, 'E', '2014-07-19', 0, 'y', 30),
(1057, '2014-07-01', 'E', 64, 3, 'E', '2014-07-19', 0, 'y', 30),
(1058, '2014-07-01', 'E', 65, 0, 'E', '2014-07-19', 0, 'y', 30),
(1059, '2014-07-01', 'E', 54, 2, 'E', '2014-07-19', 0, 'y', 30),
(1060, '2014-07-01', 'E', 55, 2, 'E', '2014-07-19', 0, 'y', 30),
(1061, '2014-07-01', 'E', 52, 0, 'E', '2014-07-19', 0, 'y', 30),
(1062, '2014-07-01', 'E', 57, 0, 'E', '2014-07-19', 0, 'y', 30),
(1063, '2014-07-02', 'E', 26, 3, 'E', '2014-07-19', 0, 'y', 30),
(1064, '2014-07-02', 'E', 27, 2, 'E', '2014-07-19', 0, 'y', 30),
(1065, '2014-07-02', 'E', 28, 3, 'E', '2014-07-19', 0, 'y', 30),
(1066, '2014-07-02', 'E', 29, 4, 'E', '2014-07-19', 0, 'y', 30),
(1067, '2014-07-02', 'E', 30, 4, 'E', '2014-07-19', 0, 'y', 30),
(1068, '2014-07-02', 'E', 31, 4, 'E', '2014-07-19', 0, 'y', 30),
(1069, '2014-07-02', 'E', 33, 0, 'E', '2014-07-19', 0, 'y', 30),
(1070, '2014-07-02', 'E', 34, 3, 'E', '2014-07-19', 0, 'y', 30),
(1071, '2014-07-02', 'E', 35, 2, 'E', '2014-07-19', 0, 'y', 30),
(1072, '2014-07-02', 'E', 36, 2, 'E', '2014-07-19', 0, 'y', 30),
(1073, '2014-07-02', 'E', 37, 3, 'E', '2014-07-19', 0, 'y', 30),
(1074, '2014-07-02', 'E', 38, 2, 'E', '2014-07-19', 0, 'y', 30),
(1075, '2014-07-02', 'E', 114, 1, 'E', '2014-07-19', 0, 'y', 30),
(1076, '2014-07-02', 'E', 39, 2, 'E', '2014-07-19', 0, 'y', 30),
(1077, '2014-07-02', 'E', 115, 3, 'E', '2014-07-19', 0, 'y', 30),
(1078, '2014-07-02', 'E', 40, 1, 'E', '2014-07-19', 0, 'y', 30),
(1079, '2014-07-02', 'E', 41, 3, 'E', '2014-07-19', 0, 'y', 30),
(1080, '2014-07-02', 'E', 42, 4, 'E', '2014-07-19', 0, 'y', 30),
(1081, '2014-07-02', 'E', 43, 1, 'E', '2014-07-19', 0, 'y', 30),
(1082, '2014-07-02', 'E', 44, 2, 'E', '2014-07-19', 0, 'y', 30),
(1083, '2014-07-02', 'E', 58, 1, 'E', '2014-07-19', 0, 'y', 30),
(1084, '2014-07-02', 'E', 45, 1, 'E', '2014-07-19', 0, 'y', 30),
(1085, '2014-07-02', 'E', 59, 1, 'E', '2014-07-19', 0, 'y', 30),
(1086, '2014-07-02', 'E', 60, 2, 'E', '2014-07-19', 0, 'y', 30),
(1087, '2014-07-02', 'E', 61, 2, 'E', '2014-07-19', 0, 'y', 30),
(1088, '2014-07-02', 'E', 46, 2, 'E', '2014-07-19', 0, 'y', 30),
(1089, '2014-07-02', 'E', 47, 0, 'E', '2014-07-19', 0, 'y', 30),
(1090, '2014-07-02', 'E', 48, 1, 'E', '2014-07-19', 0, 'y', 30),
(1091, '2014-07-02', 'E', 49, 2, 'E', '2014-07-19', 0, 'y', 30),
(1092, '2014-07-02', 'E', 50, 2, 'E', '2014-07-19', 0, 'y', 30),
(1093, '2014-07-02', 'E', 116, 4, 'E', '2014-07-19', 0, 'y', 30),
(1094, '2014-07-02', 'E', 117, 0, 'E', '2014-07-19', 0, 'y', 30),
(1095, '2014-07-02', 'E', 51, 1, 'E', '2014-07-19', 0, 'y', 30),
(1096, '2014-07-02', 'E', 53, 2, 'E', '2014-07-19', 0, 'y', 30),
(1097, '2014-07-02', 'E', 62, 5, 'E', '2014-07-19', 0, 'y', 30),
(1098, '2014-07-02', 'E', 63, 4, 'E', '2014-07-19', 0, 'y', 30),
(1099, '2014-07-02', 'E', 64, 3, 'E', '2014-07-19', 0, 'y', 30),
(1100, '2014-07-02', 'E', 65, 0, 'E', '2014-07-19', 0, 'y', 30),
(1101, '2014-07-02', 'E', 54, 2, 'E', '2014-07-19', 0, 'y', 30),
(1102, '2014-07-02', 'E', 55, 0, 'E', '2014-07-19', 0, 'y', 30),
(1103, '2014-07-02', 'E', 52, 2, 'E', '2014-07-19', 0, 'y', 30),
(1104, '2014-07-02', 'E', 57, 0, 'E', '2014-07-19', 0, 'y', 30),
(1105, '2014-07-03', 'E', 26, 3, 'E', '2014-07-19', 0, 'y', 30),
(1106, '2014-07-03', 'E', 27, 1, 'E', '2014-07-19', 0, 'y', 30),
(1107, '2014-07-03', 'E', 28, 3, 'E', '2014-07-19', 0, 'y', 30),
(1108, '2014-07-03', 'E', 29, 4, 'E', '2014-07-19', 0, 'y', 30),
(1109, '2014-07-03', 'E', 30, 4, 'E', '2014-07-19', 0, 'y', 30),
(1110, '2014-07-03', 'E', 31, 3, 'E', '2014-07-19', 0, 'y', 30),
(1111, '2014-07-03', 'E', 33, 2, 'E', '2014-07-19', 0, 'y', 30),
(1112, '2014-07-03', 'E', 34, 3, 'E', '2014-07-19', 0, 'y', 30),
(1113, '2014-07-03', 'E', 35, 2, 'E', '2014-07-19', 0, 'y', 30),
(1114, '2014-07-03', 'E', 36, 2, 'E', '2014-07-19', 0, 'y', 30),
(1115, '2014-07-03', 'E', 37, 2, 'E', '2014-07-19', 0, 'y', 30),
(1116, '2014-07-03', 'E', 38, 0, 'E', '2014-07-19', 0, 'y', 30),
(1117, '2014-07-03', 'E', 114, 1, 'E', '2014-07-19', 0, 'y', 30),
(1118, '2014-07-03', 'E', 39, 2, 'E', '2014-07-19', 0, 'y', 30),
(1119, '2014-07-03', 'E', 115, 3, 'E', '2014-07-19', 0, 'y', 30),
(1120, '2014-07-03', 'E', 40, 1, 'E', '2014-07-19', 0, 'y', 30),
(1121, '2014-07-03', 'E', 41, 0, 'E', '2014-07-19', 0, 'y', 30),
(1122, '2014-07-03', 'E', 42, 4, 'E', '2014-07-19', 0, 'y', 30),
(1123, '2014-07-03', 'E', 43, 1, 'E', '2014-07-19', 0, 'y', 30),
(1124, '2014-07-03', 'E', 44, 2, 'E', '2014-07-19', 0, 'y', 30),
(1125, '2014-07-03', 'E', 58, 0, 'E', '2014-07-19', 0, 'y', 30),
(1126, '2014-07-03', 'E', 45, 1, 'E', '2014-07-19', 0, 'y', 30),
(1127, '2014-07-03', 'E', 59, 1, 'E', '2014-07-19', 0, 'y', 30),
(1128, '2014-07-03', 'E', 60, 2, 'E', '2014-07-19', 0, 'y', 30),
(1129, '2014-07-03', 'E', 61, 2, 'E', '2014-07-19', 0, 'y', 30),
(1130, '2014-07-03', 'E', 46, 2, 'E', '2014-07-19', 0, 'y', 30),
(1131, '2014-07-03', 'E', 47, 0, 'E', '2014-07-19', 0, 'y', 30),
(1132, '2014-07-03', 'E', 48, 1, 'E', '2014-07-19', 0, 'y', 30),
(1133, '2014-07-03', 'E', 49, 2, 'E', '2014-07-19', 0, 'y', 30),
(1134, '2014-07-03', 'E', 50, 2, 'E', '2014-07-19', 0, 'y', 30),
(1135, '2014-07-03', 'E', 116, 4, 'E', '2014-07-19', 0, 'y', 30),
(1136, '2014-07-03', 'E', 117, 0, 'E', '2014-07-19', 0, 'y', 30),
(1137, '2014-07-03', 'E', 51, 1, 'E', '2014-07-19', 0, 'y', 30),
(1138, '2014-07-03', 'E', 53, 2, 'E', '2014-07-19', 0, 'y', 30),
(1139, '2014-07-03', 'E', 62, 5, 'E', '2014-07-19', 0, 'y', 30),
(1140, '2014-07-03', 'E', 63, 4, 'E', '2014-07-19', 0, 'y', 30),
(1141, '2014-07-03', 'E', 64, 2, 'E', '2014-07-19', 0, 'y', 30),
(1142, '2014-07-03', 'E', 65, 2, 'E', '2014-07-19', 0, 'y', 30),
(1143, '2014-07-03', 'E', 54, 2, 'E', '2014-07-19', 0, 'y', 30),
(1144, '2014-07-03', 'E', 55, 2, 'E', '2014-07-19', 0, 'y', 30),
(1145, '2014-07-03', 'E', 52, 2, 'E', '2014-07-19', 0, 'y', 30),
(1146, '2014-07-03', 'E', 57, 0, 'E', '2014-07-19', 0, 'y', 30),
(1147, '2014-07-04', 'E', 26, 3, 'E', '2014-07-19', 0, 'y', 30),
(1148, '2014-07-04', 'E', 27, 2, 'E', '2014-07-19', 0, 'y', 30),
(1149, '2014-07-04', 'E', 28, 3, 'E', '2014-07-19', 0, 'y', 30),
(1150, '2014-07-04', 'E', 29, 4, 'E', '2014-07-19', 0, 'y', 30),
(1151, '2014-07-04', 'E', 30, 4, 'E', '2014-07-19', 0, 'y', 30),
(1152, '2014-07-04', 'E', 31, 4, 'E', '2014-07-19', 0, 'y', 30),
(1153, '2014-07-04', 'E', 33, 2, 'E', '2014-07-19', 0, 'y', 30),
(1154, '2014-07-04', 'E', 34, 2, 'E', '2014-07-19', 0, 'y', 30),
(1155, '2014-07-04', 'E', 35, 1, 'E', '2014-07-19', 0, 'y', 30),
(1156, '2014-07-04', 'E', 36, 2, 'E', '2014-07-19', 0, 'y', 30),
(1157, '2014-07-04', 'E', 37, 3, 'E', '2014-07-19', 0, 'y', 30),
(1158, '2014-07-04', 'E', 38, 0, 'E', '2014-07-19', 0, 'y', 30),
(1159, '2014-07-04', 'E', 114, 1, 'E', '2014-07-19', 0, 'y', 30),
(1160, '2014-07-04', 'E', 39, 2, 'E', '2014-07-19', 0, 'y', 30),
(1161, '2014-07-04', 'E', 115, 3, 'E', '2014-07-19', 0, 'y', 30),
(1162, '2014-07-04', 'E', 40, 1, 'E', '2014-07-19', 0, 'y', 30),
(1163, '2014-07-04', 'E', 41, 0, 'E', '2014-07-19', 0, 'y', 30),
(1164, '2014-07-04', 'E', 42, 4, 'E', '2014-07-19', 0, 'y', 30),
(1165, '2014-07-04', 'E', 43, 1, 'E', '2014-07-19', 0, 'y', 30),
(1166, '2014-07-04', 'E', 44, 2, 'E', '2014-07-19', 0, 'y', 30),
(1167, '2014-07-04', 'E', 58, 0, 'E', '2014-07-19', 0, 'y', 30),
(1168, '2014-07-04', 'E', 45, 1, 'E', '2014-07-19', 0, 'y', 30),
(1169, '2014-07-04', 'E', 59, 1, 'E', '2014-07-19', 0, 'y', 30),
(1170, '2014-07-04', 'E', 60, 2, 'E', '2014-07-19', 0, 'y', 30),
(1171, '2014-07-04', 'E', 61, 2, 'E', '2014-07-19', 0, 'y', 30),
(1172, '2014-07-04', 'E', 46, 2, 'E', '2014-07-19', 0, 'y', 30),
(1173, '2014-07-04', 'E', 47, 0, 'E', '2014-07-19', 0, 'y', 30),
(1174, '2014-07-04', 'E', 48, 1, 'E', '2014-07-19', 0, 'y', 30),
(1175, '2014-07-04', 'E', 49, 2, 'E', '2014-07-19', 0, 'y', 30),
(1176, '2014-07-04', 'E', 50, 2, 'E', '2014-07-19', 0, 'y', 30),
(1177, '2014-07-04', 'E', 116, 4, 'E', '2014-07-19', 0, 'y', 30),
(1178, '2014-07-04', 'E', 117, 0, 'E', '2014-07-19', 0, 'y', 30),
(1179, '2014-07-04', 'E', 51, 1, 'E', '2014-07-19', 0, 'y', 30),
(1180, '2014-07-04', 'E', 53, 2, 'E', '2014-07-19', 0, 'y', 30),
(1181, '2014-07-04', 'E', 62, 3, 'E', '2014-07-19', 0, 'y', 30),
(1182, '2014-07-04', 'E', 63, 4, 'E', '2014-07-19', 0, 'y', 30),
(1183, '2014-07-04', 'E', 64, 3, 'E', '2014-07-19', 0, 'y', 30),
(1184, '2014-07-04', 'E', 65, 3, 'E', '2014-07-19', 0, 'y', 30),
(1185, '2014-07-04', 'E', 54, 2, 'E', '2014-07-19', 0, 'y', 30),
(1186, '2014-07-04', 'E', 55, 0, 'E', '2014-07-19', 0, 'y', 30),
(1187, '2014-07-04', 'E', 52, 2, 'E', '2014-07-19', 0, 'y', 30),
(1188, '2014-07-04', 'E', 57, 0, 'E', '2014-07-19', 0, 'y', 30),
(1189, '2014-07-05', 'E', 26, 3, 'E', '2014-07-19', 0, 'y', 30),
(1190, '2014-07-05', 'E', 27, 1, 'E', '2014-07-19', 0, 'y', 30),
(1191, '2014-07-05', 'E', 28, 3, 'E', '2014-07-19', 0, 'y', 30),
(1192, '2014-07-05', 'E', 29, 4, 'E', '2014-07-19', 0, 'y', 30),
(1193, '2014-07-05', 'E', 30, 3, 'E', '2014-07-19', 0, 'y', 30),
(1194, '2014-07-05', 'E', 31, 3, 'E', '2014-07-19', 0, 'y', 30),
(1195, '2014-07-05', 'E', 33, 2, 'E', '2014-07-19', 0, 'y', 30),
(1196, '2014-07-05', 'E', 34, 3, 'E', '2014-07-19', 0, 'y', 30),
(1197, '2014-07-05', 'E', 35, 2, 'E', '2014-07-19', 0, 'y', 30),
(1198, '2014-07-05', 'E', 36, 2, 'E', '2014-07-19', 0, 'y', 30),
(1199, '2014-07-05', 'E', 37, 2, 'E', '2014-07-19', 0, 'y', 30),
(1200, '2014-07-05', 'E', 38, 3, 'E', '2014-07-19', 0, 'y', 30),
(1201, '2014-07-05', 'E', 114, 1, 'E', '2014-07-19', 0, 'y', 30),
(1202, '2014-07-05', 'E', 39, 2, 'E', '2014-07-19', 0, 'y', 30),
(1203, '2014-07-05', 'E', 115, 3, 'E', '2014-07-19', 0, 'y', 30),
(1204, '2014-07-05', 'E', 40, 1, 'E', '2014-07-19', 0, 'y', 30),
(1205, '2014-07-05', 'E', 41, 0, 'E', '2014-07-19', 0, 'y', 30),
(1206, '2014-07-05', 'E', 42, 4, 'E', '2014-07-19', 0, 'y', 30),
(1207, '2014-07-05', 'E', 43, 1, 'E', '2014-07-19', 0, 'y', 30),
(1208, '2014-07-05', 'E', 44, 2, 'E', '2014-07-19', 0, 'y', 30),
(1209, '2014-07-05', 'E', 58, 0, 'E', '2014-07-19', 0, 'y', 30),
(1210, '2014-07-05', 'E', 45, 0, 'E', '2014-07-19', 0, 'y', 30),
(1211, '2014-07-05', 'E', 59, 0, 'E', '2014-07-19', 0, 'y', 30),
(1212, '2014-07-05', 'E', 60, 2, 'E', '2014-07-19', 0, 'y', 30),
(1213, '2014-07-05', 'E', 61, 2, 'E', '2014-07-19', 0, 'y', 30),
(1214, '2014-07-05', 'E', 46, 2, 'E', '2014-07-19', 0, 'y', 30),
(1215, '2014-07-05', 'E', 47, 0, 'E', '2014-07-19', 0, 'y', 30),
(1216, '2014-07-05', 'E', 48, 1, 'E', '2014-07-19', 0, 'y', 30),
(1217, '2014-07-05', 'E', 49, 2, 'E', '2014-07-19', 0, 'y', 30),
(1218, '2014-07-05', 'E', 50, 2, 'E', '2014-07-19', 0, 'y', 30),
(1219, '2014-07-05', 'E', 116, 4, 'E', '2014-07-19', 0, 'y', 30),
(1220, '2014-07-05', 'E', 117, 0, 'E', '2014-07-19', 0, 'y', 30),
(1221, '2014-07-05', 'E', 51, 1, 'E', '2014-07-19', 0, 'y', 30),
(1222, '2014-07-05', 'E', 53, 2, 'E', '2014-07-19', 0, 'y', 30),
(1223, '2014-07-05', 'E', 62, 4, 'E', '2014-07-19', 0, 'y', 30),
(1224, '2014-07-05', 'E', 63, 4, 'E', '2014-07-19', 0, 'y', 30),
(1225, '2014-07-05', 'E', 64, 2, 'E', '2014-07-19', 0, 'y', 30),
(1226, '2014-07-05', 'E', 65, 3, 'E', '2014-07-19', 0, 'y', 30),
(1227, '2014-07-05', 'E', 54, 2, 'E', '2014-07-19', 0, 'y', 30),
(1228, '2014-07-05', 'E', 55, 2, 'E', '2014-07-19', 0, 'y', 30),
(1229, '2014-07-05', 'E', 52, 2, 'E', '2014-07-19', 0, 'y', 30),
(1230, '2014-07-05', 'E', 57, 0, 'E', '2014-07-19', 0, 'y', 30),
(1231, '2014-07-06', 'E', 26, 4, 'E', '2014-07-19', 0, 'y', 30),
(1232, '2014-07-06', 'E', 27, 2, 'E', '2014-07-19', 0, 'y', 30),
(1233, '2014-07-06', 'E', 28, 3, 'E', '2014-07-19', 0, 'y', 30),
(1234, '2014-07-06', 'E', 29, 4, 'E', '2014-07-19', 0, 'y', 30),
(1235, '2014-07-06', 'E', 30, 4, 'E', '2014-07-19', 0, 'y', 30),
(1236, '2014-07-06', 'E', 31, 4, 'E', '2014-07-19', 0, 'y', 30),
(1237, '2014-07-06', 'E', 33, 2, 'E', '2014-07-19', 0, 'y', 30),
(1238, '2014-07-06', 'E', 34, 3, 'E', '2014-07-19', 0, 'y', 30),
(1239, '2014-07-06', 'E', 35, 2, 'E', '2014-07-19', 0, 'y', 30),
(1240, '2014-07-06', 'E', 36, 2, 'E', '2014-07-19', 0, 'y', 30),
(1241, '2014-07-06', 'E', 37, 3, 'E', '2014-07-19', 0, 'y', 30),
(1242, '2014-07-06', 'E', 38, 4, 'E', '2014-07-19', 0, 'y', 30),
(1243, '2014-07-06', 'E', 114, 1, 'E', '2014-07-19', 0, 'y', 30),
(1244, '2014-07-06', 'E', 39, 2, 'E', '2014-07-19', 0, 'y', 30),
(1245, '2014-07-06', 'E', 115, 0, 'E', '2014-07-19', 0, 'y', 30),
(1246, '2014-07-06', 'E', 40, 0, 'E', '2014-07-19', 0, 'y', 30),
(1247, '2014-07-06', 'E', 41, 0, 'E', '2014-07-19', 0, 'y', 30),
(1248, '2014-07-06', 'E', 42, 4, 'E', '2014-07-19', 0, 'y', 30),
(1249, '2014-07-06', 'E', 43, 1, 'E', '2014-07-19', 0, 'y', 30),
(1250, '2014-07-06', 'E', 44, 2, 'E', '2014-07-19', 0, 'y', 30),
(1251, '2014-07-06', 'E', 58, 0, 'E', '2014-07-19', 0, 'y', 30),
(1252, '2014-07-06', 'E', 45, 1, 'E', '2014-07-19', 0, 'y', 30),
(1253, '2014-07-06', 'E', 59, 1, 'E', '2014-07-19', 0, 'y', 30),
(1254, '2014-07-06', 'E', 60, 2, 'E', '2014-07-19', 0, 'y', 30),
(1255, '2014-07-06', 'E', 61, 2, 'E', '2014-07-19', 0, 'y', 30),
(1256, '2014-07-06', 'E', 46, 2, 'E', '2014-07-19', 0, 'y', 30),
(1257, '2014-07-06', 'E', 47, 0, 'E', '2014-07-19', 0, 'y', 30),
(1258, '2014-07-06', 'E', 48, 1, 'E', '2014-07-19', 0, 'y', 30),
(1259, '2014-07-06', 'E', 49, 2, 'E', '2014-07-19', 0, 'y', 30),
(1260, '2014-07-06', 'E', 50, 2, 'E', '2014-07-19', 0, 'y', 30),
(1261, '2014-07-06', 'E', 116, 4, 'E', '2014-07-19', 0, 'y', 30),
(1262, '2014-07-06', 'E', 117, 0, 'E', '2014-07-19', 0, 'y', 30),
(1263, '2014-07-06', 'E', 51, 1, 'E', '2014-07-19', 0, 'y', 30),
(1264, '2014-07-06', 'E', 53, 2, 'E', '2014-07-19', 0, 'y', 30),
(1265, '2014-07-06', 'E', 62, 4, 'E', '2014-07-19', 0, 'y', 30),
(1266, '2014-07-06', 'E', 63, 4, 'E', '2014-07-19', 0, 'y', 30),
(1267, '2014-07-06', 'E', 64, 3, 'E', '2014-07-19', 0, 'y', 30),
(1268, '2014-07-06', 'E', 65, 3, 'E', '2014-07-19', 0, 'y', 30),
(1269, '2014-07-06', 'E', 54, 2, 'E', '2014-07-19', 0, 'y', 30),
(1270, '2014-07-06', 'E', 55, 0, 'E', '2014-07-19', 0, 'y', 30),
(1271, '2014-07-06', 'E', 52, 2, 'E', '2014-07-19', 0, 'y', 30),
(1272, '2014-07-06', 'E', 57, 0, 'E', '2014-07-19', 0, 'y', 30),
(1273, '2014-07-07', 'E', 26, 3, 'E', '2014-07-19', 0, 'y', 30),
(1274, '2014-07-07', 'E', 27, 1, 'E', '2014-07-19', 0, 'y', 30),
(1275, '2014-07-07', 'E', 28, 3, 'E', '2014-07-19', 0, 'y', 30),
(1276, '2014-07-07', 'E', 29, 4, 'E', '2014-07-19', 0, 'y', 30),
(1277, '2014-07-07', 'E', 30, 4, 'E', '2014-07-19', 0, 'y', 30),
(1278, '2014-07-07', 'E', 31, 4, 'E', '2014-07-19', 0, 'y', 30),
(1279, '2014-07-07', 'E', 33, 2, 'E', '2014-07-19', 0, 'y', 30),
(1280, '2014-07-07', 'E', 34, 1, 'E', '2014-07-19', 0, 'y', 30),
(1281, '2014-07-07', 'E', 35, 2, 'E', '2014-07-19', 0, 'y', 30),
(1282, '2014-07-07', 'E', 36, 2, 'E', '2014-07-19', 0, 'y', 30),
(1283, '2014-07-07', 'E', 37, 3, 'E', '2014-07-19', 0, 'y', 30),
(1284, '2014-07-07', 'E', 38, 3, 'E', '2014-07-19', 0, 'y', 30),
(1285, '2014-07-07', 'E', 114, 1, 'E', '2014-07-19', 0, 'y', 30),
(1286, '2014-07-07', 'E', 39, 2, 'E', '2014-07-19', 0, 'y', 30),
(1287, '2014-07-07', 'E', 115, 0, 'E', '2014-07-19', 0, 'y', 30),
(1288, '2014-07-07', 'E', 40, 0, 'E', '2014-07-19', 0, 'y', 30),
(1289, '2014-07-07', 'E', 41, 0, 'E', '2014-07-19', 0, 'y', 30),
(1290, '2014-07-07', 'E', 42, 3, 'E', '2014-07-19', 0, 'y', 30),
(1291, '2014-07-07', 'E', 43, 0, 'E', '2014-07-19', 0, 'y', 30),
(1292, '2014-07-07', 'E', 44, 0, 'E', '2014-07-19', 0, 'y', 30),
(1293, '2014-07-07', 'E', 58, 0, 'E', '2014-07-19', 0, 'y', 30),
(1294, '2014-07-07', 'E', 45, 1, 'E', '2014-07-19', 0, 'y', 30),
(1295, '2014-07-07', 'E', 59, 1, 'E', '2014-07-19', 0, 'y', 30),
(1296, '2014-07-07', 'E', 60, 2, 'E', '2014-07-19', 0, 'y', 30),
(1297, '2014-07-07', 'E', 61, 2, 'E', '2014-07-19', 0, 'y', 30),
(1298, '2014-07-07', 'E', 46, 2, 'E', '2014-07-19', 0, 'y', 30),
(1299, '2014-07-07', 'E', 47, 0, 'E', '2014-07-19', 0, 'y', 30),
(1300, '2014-07-07', 'E', 48, 1, 'E', '2014-07-19', 0, 'y', 30),
(1301, '2014-07-07', 'E', 49, 2, 'E', '2014-07-19', 0, 'y', 30),
(1302, '2014-07-07', 'E', 50, 2, 'E', '2014-07-19', 0, 'y', 30),
(1303, '2014-07-07', 'E', 116, 4, 'E', '2014-07-19', 0, 'y', 30),
(1304, '2014-07-07', 'E', 117, 0, 'E', '2014-07-19', 0, 'y', 30),
(1305, '2014-07-07', 'E', 51, 1, 'E', '2014-07-19', 0, 'y', 30),
(1306, '2014-07-07', 'E', 53, 2, 'E', '2014-07-19', 0, 'y', 30),
(1307, '2014-07-07', 'E', 62, 4, 'E', '2014-07-19', 0, 'y', 30),
(1308, '2014-07-07', 'E', 63, 4, 'E', '2014-07-19', 0, 'y', 30),
(1309, '2014-07-07', 'E', 64, 3, 'E', '2014-07-19', 0, 'y', 30),
(1310, '2014-07-07', 'E', 65, 3, 'E', '2014-07-19', 0, 'y', 30),
(1311, '2014-07-07', 'E', 54, 2, 'E', '2014-07-19', 0, 'y', 30),
(1312, '2014-07-07', 'E', 55, 2, 'E', '2014-07-19', 0, 'y', 30),
(1313, '2014-07-07', 'E', 52, 2, 'E', '2014-07-19', 0, 'y', 30),
(1314, '2014-07-07', 'E', 57, 0, 'E', '2014-07-19', 0, 'y', 30),
(1315, '2014-07-08', 'E', 26, 3, 'E', '2014-07-19', 0, 'y', 30),
(1316, '2014-07-08', 'E', 27, 2, 'E', '2014-07-19', 0, 'y', 30),
(1317, '2014-07-08', 'E', 28, 2, 'E', '2014-07-19', 0, 'y', 30),
(1318, '2014-07-08', 'E', 29, 4, 'E', '2014-07-19', 0, 'y', 30),
(1319, '2014-07-08', 'E', 30, 4, 'E', '2014-07-19', 0, 'y', 30),
(1320, '2014-07-08', 'E', 31, 3, 'E', '2014-07-19', 0, 'y', 30),
(1321, '2014-07-08', 'E', 33, 2, 'E', '2014-07-19', 0, 'y', 30),
(1322, '2014-07-08', 'E', 34, 3, 'E', '2014-07-19', 0, 'y', 30),
(1323, '2014-07-08', 'E', 35, 2, 'E', '2014-07-19', 0, 'y', 30),
(1324, '2014-07-08', 'E', 36, 2, 'E', '2014-07-19', 0, 'y', 30),
(1325, '2014-07-08', 'E', 37, 3, 'E', '2014-07-19', 0, 'y', 30),
(1326, '2014-07-08', 'E', 38, 3, 'E', '2014-07-19', 0, 'y', 30),
(1327, '2014-07-08', 'E', 114, 1, 'E', '2014-07-19', 0, 'y', 30),
(1328, '2014-07-08', 'E', 39, 2, 'E', '2014-07-19', 0, 'y', 30),
(1329, '2014-07-08', 'E', 115, 0, 'E', '2014-07-19', 0, 'y', 30),
(1330, '2014-07-08', 'E', 40, 1, 'E', '2014-07-19', 0, 'y', 30),
(1331, '2014-07-08', 'E', 41, 0, 'E', '2014-07-19', 0, 'y', 30),
(1332, '2014-07-08', 'E', 42, 4, 'E', '2014-07-19', 0, 'y', 30),
(1333, '2014-07-08', 'E', 43, 0, 'E', '2014-07-19', 0, 'y', 30),
(1334, '2014-07-08', 'E', 44, 0, 'E', '2014-07-19', 0, 'y', 30),
(1335, '2014-07-08', 'E', 58, 0, 'E', '2014-07-19', 0, 'y', 30),
(1336, '2014-07-08', 'E', 45, 1, 'E', '2014-07-19', 0, 'y', 30),
(1337, '2014-07-08', 'E', 59, 1, 'E', '2014-07-19', 0, 'y', 30),
(1338, '2014-07-08', 'E', 60, 2, 'E', '2014-07-19', 0, 'y', 30),
(1339, '2014-07-08', 'E', 61, 2, 'E', '2014-07-19', 0, 'y', 30),
(1340, '2014-07-08', 'E', 46, 2, 'E', '2014-07-19', 0, 'y', 30),
(1341, '2014-07-08', 'E', 47, 0, 'E', '2014-07-19', 0, 'y', 30),
(1342, '2014-07-08', 'E', 48, 0, 'E', '2014-07-19', 0, 'y', 30),
(1343, '2014-07-08', 'E', 49, 0, 'E', '2014-07-19', 0, 'y', 30),
(1344, '2014-07-08', 'E', 50, 2, 'E', '2014-07-19', 0, 'y', 30),
(1345, '2014-07-08', 'E', 116, 4, 'E', '2014-07-19', 0, 'y', 30),
(1346, '2014-07-08', 'E', 117, 0, 'E', '2014-07-19', 0, 'y', 30),
(1347, '2014-07-08', 'E', 51, 1, 'E', '2014-07-19', 0, 'y', 30),
(1348, '2014-07-08', 'E', 53, 2, 'E', '2014-07-19', 0, 'y', 30),
(1349, '2014-07-08', 'E', 62, 4, 'E', '2014-07-19', 0, 'y', 30),
(1350, '2014-07-08', 'E', 63, 4, 'E', '2014-07-19', 0, 'y', 30),
(1351, '2014-07-08', 'E', 64, 3, 'E', '2014-07-19', 0, 'y', 30),
(1352, '2014-07-08', 'E', 65, 3, 'E', '2014-07-19', 0, 'y', 30),
(1353, '2014-07-08', 'E', 54, 2, 'E', '2014-07-19', 0, 'y', 30),
(1354, '2014-07-08', 'E', 55, 0, 'E', '2014-07-19', 0, 'y', 30),
(1355, '2014-07-08', 'E', 52, 2, 'E', '2014-07-19', 0, 'y', 30),
(1356, '2014-07-08', 'E', 57, 0, 'E', '2014-07-19', 0, 'y', 30),
(1357, '2014-07-09', 'E', 26, 4, 'E', '2014-07-19', 0, 'y', 30),
(1358, '2014-07-09', 'E', 27, 1, 'E', '2014-07-19', 0, 'y', 30),
(1359, '2014-07-09', 'E', 28, 0, 'E', '2014-07-19', 0, 'y', 30),
(1360, '2014-07-09', 'E', 29, 4, 'E', '2014-07-19', 0, 'y', 30),
(1361, '2014-07-09', 'E', 30, 4, 'E', '2014-07-19', 0, 'y', 30),
(1362, '2014-07-09', 'E', 31, 3, 'E', '2014-07-19', 0, 'y', 30),
(1363, '2014-07-09', 'E', 33, 2, 'E', '2014-07-19', 0, 'y', 30),
(1364, '2014-07-09', 'E', 34, 3, 'E', '2014-07-19', 0, 'y', 30),
(1365, '2014-07-09', 'E', 35, 1, 'E', '2014-07-19', 0, 'y', 30),
(1366, '2014-07-09', 'E', 36, 2, 'E', '2014-07-19', 0, 'y', 30),
(1367, '2014-07-09', 'E', 37, 3, 'E', '2014-07-19', 0, 'y', 30),
(1368, '2014-07-09', 'E', 38, 4, 'E', '2014-07-19', 0, 'y', 30),
(1369, '2014-07-09', 'E', 114, 1, 'E', '2014-07-19', 0, 'y', 30),
(1370, '2014-07-09', 'E', 39, 2, 'E', '2014-07-19', 0, 'y', 30),
(1371, '2014-07-09', 'E', 115, 4, 'E', '2014-07-19', 0, 'y', 30),
(1372, '2014-07-09', 'E', 40, 1, 'E', '2014-07-19', 0, 'y', 30),
(1373, '2014-07-09', 'E', 41, 0, 'E', '2014-07-19', 0, 'y', 30),
(1374, '2014-07-09', 'E', 42, 4, 'E', '2014-07-19', 0, 'y', 30),
(1375, '2014-07-09', 'E', 43, 1, 'E', '2014-07-19', 0, 'y', 30),
(1376, '2014-07-09', 'E', 44, 0, 'E', '2014-07-19', 0, 'y', 30),
(1377, '2014-07-09', 'E', 58, 0, 'E', '2014-07-19', 0, 'y', 30),
(1378, '2014-07-09', 'E', 45, 1, 'E', '2014-07-19', 0, 'y', 30),
(1379, '2014-07-09', 'E', 59, 1, 'E', '2014-07-19', 0, 'y', 30),
(1380, '2014-07-09', 'E', 60, 2, 'E', '2014-07-19', 0, 'y', 30),
(1381, '2014-07-09', 'E', 61, 2, 'E', '2014-07-19', 0, 'y', 30),
(1382, '2014-07-09', 'E', 46, 2, 'E', '2014-07-19', 0, 'y', 30),
(1383, '2014-07-09', 'E', 47, 0, 'E', '2014-07-19', 0, 'y', 30),
(1384, '2014-07-09', 'E', 48, 1, 'E', '2014-07-19', 0, 'y', 30),
(1385, '2014-07-09', 'E', 49, 2, 'E', '2014-07-19', 0, 'y', 30),
(1386, '2014-07-09', 'E', 50, 2, 'E', '2014-07-19', 0, 'y', 30),
(1387, '2014-07-09', 'E', 116, 4, 'E', '2014-07-19', 0, 'y', 30),
(1388, '2014-07-09', 'E', 117, 0, 'E', '2014-07-19', 0, 'y', 30),
(1389, '2014-07-09', 'E', 51, 1, 'E', '2014-07-19', 0, 'y', 30),
(1390, '2014-07-09', 'E', 53, 2, 'E', '2014-07-19', 0, 'y', 30),
(1391, '2014-07-09', 'E', 62, 4, 'E', '2014-07-19', 0, 'y', 30),
(1392, '2014-07-09', 'E', 63, 4, 'E', '2014-07-19', 0, 'y', 30),
(1393, '2014-07-09', 'E', 64, 2, 'E', '2014-07-19', 0, 'y', 30),
(1394, '2014-07-09', 'E', 65, 3, 'E', '2014-07-19', 0, 'y', 30),
(1395, '2014-07-09', 'E', 54, 2, 'E', '2014-07-19', 0, 'y', 30),
(1396, '2014-07-09', 'E', 55, 2, 'E', '2014-07-19', 0, 'y', 30),
(1397, '2014-07-09', 'E', 52, 2, 'E', '2014-07-19', 0, 'y', 30),
(1398, '2014-07-09', 'E', 57, 0, 'E', '2014-07-19', 0, 'y', 30),
(1399, '2014-07-10', 'E', 26, 3, 'E', '2014-07-19', 0, 'y', 30),
(1400, '2014-07-10', 'E', 27, 2, 'E', '2014-07-19', 0, 'y', 30),
(1401, '2014-07-10', 'E', 28, 3, 'E', '2014-07-19', 0, 'y', 30),
(1402, '2014-07-10', 'E', 29, 4, 'E', '2014-07-19', 0, 'y', 30),
(1403, '2014-07-10', 'E', 30, 4, 'E', '2014-07-19', 0, 'y', 30),
(1404, '2014-07-10', 'E', 31, 3, 'E', '2014-07-19', 0, 'y', 30),
(1405, '2014-07-10', 'E', 33, 2, 'E', '2014-07-19', 0, 'y', 30),
(1406, '2014-07-10', 'E', 34, 2, 'E', '2014-07-19', 0, 'y', 30),
(1407, '2014-07-10', 'E', 35, 1, 'E', '2014-07-19', 0, 'y', 30),
(1408, '2014-07-10', 'E', 36, 2, 'E', '2014-07-19', 0, 'y', 30),
(1409, '2014-07-10', 'E', 37, 3, 'E', '2014-07-19', 0, 'y', 30),
(1410, '2014-07-10', 'E', 38, 4, 'E', '2014-07-19', 0, 'y', 30),
(1411, '2014-07-10', 'E', 114, 4, 'E', '2014-07-19', 0, 'y', 30),
(1412, '2014-07-10', 'E', 39, 2, 'E', '2014-07-19', 0, 'y', 30),
(1413, '2014-07-10', 'E', 115, 4, 'E', '2014-07-19', 0, 'y', 30),
(1414, '2014-07-10', 'E', 40, 1, 'E', '2014-07-19', 0, 'y', 30),
(1415, '2014-07-10', 'E', 41, 0, 'E', '2014-07-19', 0, 'y', 30),
(1416, '2014-07-10', 'E', 42, 4, 'E', '2014-07-19', 0, 'y', 30),
(1417, '2014-07-10', 'E', 43, 2, 'E', '2014-07-19', 0, 'y', 30),
(1418, '2014-07-10', 'E', 44, 0, 'E', '2014-07-19', 0, 'y', 30),
(1419, '2014-07-10', 'E', 58, 0, 'E', '2014-07-19', 0, 'y', 30),
(1420, '2014-07-10', 'E', 45, 1, 'E', '2014-07-19', 0, 'y', 30),
(1421, '2014-07-10', 'E', 59, 1, 'E', '2014-07-19', 0, 'y', 30),
(1422, '2014-07-10', 'E', 60, 2, 'E', '2014-07-19', 0, 'y', 30),
(1423, '2014-07-10', 'E', 61, 2, 'E', '2014-07-19', 0, 'y', 30),
(1424, '2014-07-10', 'E', 46, 2, 'E', '2014-07-19', 0, 'y', 30),
(1425, '2014-07-10', 'E', 47, 0, 'E', '2014-07-19', 0, 'y', 30),
(1426, '2014-07-10', 'E', 48, 2, 'E', '2014-07-19', 0, 'y', 30),
(1427, '2014-07-10', 'E', 49, 2, 'E', '2014-07-19', 0, 'y', 30),
(1428, '2014-07-10', 'E', 50, 2, 'E', '2014-07-19', 0, 'y', 30),
(1429, '2014-07-10', 'E', 116, 4, 'E', '2014-07-19', 0, 'y', 30),
(1430, '2014-07-10', 'E', 117, 0, 'E', '2014-07-19', 0, 'y', 30),
(1431, '2014-07-10', 'E', 51, 1, 'E', '2014-07-19', 0, 'y', 30),
(1432, '2014-07-10', 'E', 53, 2, 'E', '2014-07-19', 0, 'y', 30),
(1433, '2014-07-10', 'E', 62, 4, 'E', '2014-07-19', 0, 'y', 30),
(1434, '2014-07-10', 'E', 63, 4, 'E', '2014-07-19', 0, 'y', 30),
(1435, '2014-07-10', 'E', 64, 3, 'E', '2014-07-19', 0, 'y', 30),
(1436, '2014-07-10', 'E', 65, 3, 'E', '2014-07-19', 0, 'y', 30),
(1437, '2014-07-10', 'E', 54, 2, 'E', '2014-07-19', 0, 'y', 30),
(1438, '2014-07-10', 'E', 55, 0, 'E', '2014-07-19', 0, 'y', 30),
(1439, '2014-07-10', 'E', 52, 2, 'E', '2014-07-19', 0, 'y', 30),
(1440, '2014-07-10', 'E', 57, 0, 'E', '2014-07-19', 0, 'y', 30),
(1441, '2014-07-11', 'E', 26, 3, 'E', '2014-07-19', 0, 'y', 30),
(1442, '2014-07-11', 'E', 27, 1, 'E', '2014-07-19', 0, 'y', 30),
(1443, '2014-07-11', 'E', 28, 3, 'E', '2014-07-19', 0, 'y', 30),
(1444, '2014-07-11', 'E', 29, 4, 'E', '2014-07-19', 0, 'y', 30),
(1445, '2014-07-11', 'E', 30, 4, 'E', '2014-07-19', 0, 'y', 30),
(1446, '2014-07-11', 'E', 31, 4, 'E', '2014-07-19', 0, 'y', 30),
(1447, '2014-07-11', 'E', 33, 2, 'E', '2014-07-19', 0, 'y', 30),
(1448, '2014-07-11', 'E', 34, 3, 'E', '2014-07-19', 0, 'y', 30),
(1449, '2014-07-11', 'E', 35, 2, 'E', '2014-07-19', 0, 'y', 30),
(1450, '2014-07-11', 'E', 36, 2, 'E', '2014-07-19', 0, 'y', 30),
(1451, '2014-07-11', 'E', 37, 3, 'E', '2014-07-19', 0, 'y', 30),
(1452, '2014-07-11', 'E', 38, 3, 'E', '2014-07-19', 0, 'y', 30),
(1453, '2014-07-11', 'E', 114, 1, 'E', '2014-07-19', 0, 'y', 30),
(1454, '2014-07-11', 'E', 39, 2, 'E', '2014-07-19', 0, 'y', 30),
(1455, '2014-07-11', 'E', 115, 3, 'E', '2014-07-19', 0, 'y', 30),
(1456, '2014-07-11', 'E', 40, 1, 'E', '2014-07-19', 0, 'y', 30),
(1457, '2014-07-11', 'E', 41, 0, 'E', '2014-07-19', 0, 'y', 30),
(1458, '2014-07-11', 'E', 42, 4, 'E', '2014-07-19', 0, 'y', 30),
(1459, '2014-07-11', 'E', 43, 1, 'E', '2014-07-19', 0, 'y', 30),
(1460, '2014-07-11', 'E', 44, 2, 'E', '2014-07-19', 0, 'y', 30),
(1461, '2014-07-11', 'E', 58, 1, 'E', '2014-07-19', 0, 'y', 30),
(1462, '2014-07-11', 'E', 45, 1, 'E', '2014-07-19', 0, 'y', 30),
(1463, '2014-07-11', 'E', 59, 1, 'E', '2014-07-19', 0, 'y', 30),
(1464, '2014-07-11', 'E', 60, 2, 'E', '2014-07-19', 0, 'y', 30),
(1465, '2014-07-11', 'E', 61, 2, 'E', '2014-07-19', 0, 'y', 30),
(1466, '2014-07-11', 'E', 46, 2, 'E', '2014-07-19', 0, 'y', 30),
(1467, '2014-07-11', 'E', 47, 0, 'E', '2014-07-19', 0, 'y', 30),
(1468, '2014-07-11', 'E', 48, 2, 'E', '2014-07-19', 0, 'y', 30),
(1469, '2014-07-11', 'E', 49, 2, 'E', '2014-07-19', 0, 'y', 30),
(1470, '2014-07-11', 'E', 50, 2, 'E', '2014-07-19', 0, 'y', 30),
(1471, '2014-07-11', 'E', 116, 4, 'E', '2014-07-19', 0, 'y', 30),
(1472, '2014-07-11', 'E', 117, 4, 'E', '2014-07-19', 0, 'y', 30),
(1473, '2014-07-11', 'E', 51, 1, 'E', '2014-07-19', 0, 'y', 30),
(1474, '2014-07-11', 'E', 53, 2, 'E', '2014-07-19', 0, 'y', 30),
(1475, '2014-07-11', 'E', 62, 4, 'E', '2014-07-19', 0, 'y', 30),
(1476, '2014-07-11', 'E', 63, 4, 'E', '2014-07-19', 0, 'y', 30),
(1477, '2014-07-11', 'E', 64, 3, 'E', '2014-07-19', 0, 'y', 30),
(1478, '2014-07-11', 'E', 65, 3, 'E', '2014-07-19', 0, 'y', 30),
(1479, '2014-07-11', 'E', 54, 2, 'E', '2014-07-19', 0, 'y', 30),
(1480, '2014-07-11', 'E', 55, 2, 'E', '2014-07-19', 0, 'y', 30),
(1481, '2014-07-11', 'E', 52, 2, 'E', '2014-07-19', 0, 'y', 30),
(1482, '2014-07-11', 'E', 57, 0, 'E', '2014-07-19', 0, 'y', 30),
(1483, '2014-07-12', 'E', 26, 4, 'E', '2014-07-19', 0, 'y', 30),
(1484, '2014-07-12', 'E', 27, 2, 'E', '2014-07-19', 0, 'y', 30),
(1485, '2014-07-12', 'E', 28, 3, 'E', '2014-07-19', 0, 'y', 30),
(1486, '2014-07-12', 'E', 29, 4, 'E', '2014-07-19', 0, 'y', 30),
(1487, '2014-07-12', 'E', 30, 4, 'E', '2014-07-19', 0, 'y', 30),
(1488, '2014-07-12', 'E', 31, 4, 'E', '2014-07-19', 0, 'y', 30),
(1489, '2014-07-12', 'E', 33, 2, 'E', '2014-07-19', 0, 'y', 30),
(1490, '2014-07-12', 'E', 34, 2, 'E', '2014-07-19', 0, 'y', 30),
(1491, '2014-07-12', 'E', 35, 2, 'E', '2014-07-19', 0, 'y', 30),
(1492, '2014-07-12', 'E', 36, 2, 'E', '2014-07-19', 0, 'y', 30),
(1493, '2014-07-12', 'E', 37, 3, 'E', '2014-07-19', 0, 'y', 30),
(1494, '2014-07-12', 'E', 38, 4, 'E', '2014-07-19', 0, 'y', 30),
(1495, '2014-07-12', 'E', 114, 0, 'E', '2014-07-19', 0, 'y', 30),
(1496, '2014-07-12', 'E', 39, 1, 'E', '2014-07-19', 0, 'y', 30),
(1497, '2014-07-12', 'E', 115, 3, 'E', '2014-07-19', 0, 'y', 30),
(1498, '2014-07-12', 'E', 40, 1, 'E', '2014-07-19', 0, 'y', 30),
(1499, '2014-07-12', 'E', 41, 0, 'E', '2014-07-19', 0, 'y', 30),
(1500, '2014-07-12', 'E', 42, 4, 'E', '2014-07-19', 0, 'y', 30),
(1501, '2014-07-12', 'E', 43, 1, 'E', '2014-07-19', 0, 'y', 30),
(1502, '2014-07-12', 'E', 44, 2, 'E', '2014-07-19', 0, 'y', 30),
(1503, '2014-07-12', 'E', 58, 1, 'E', '2014-07-19', 0, 'y', 30),
(1504, '2014-07-12', 'E', 45, 1, 'E', '2014-07-19', 0, 'y', 30),
(1505, '2014-07-12', 'E', 59, 1, 'E', '2014-07-19', 0, 'y', 30),
(1506, '2014-07-12', 'E', 60, 2, 'E', '2014-07-19', 0, 'y', 30),
(1507, '2014-07-12', 'E', 61, 2, 'E', '2014-07-19', 0, 'y', 30),
(1508, '2014-07-12', 'E', 46, 2, 'E', '2014-07-19', 0, 'y', 30),
(1509, '2014-07-12', 'E', 47, 0, 'E', '2014-07-19', 0, 'y', 30),
(1510, '2014-07-12', 'E', 48, 2, 'E', '2014-07-19', 0, 'y', 30),
(1511, '2014-07-12', 'E', 49, 2, 'E', '2014-07-19', 0, 'y', 30),
(1512, '2014-07-12', 'E', 50, 2, 'E', '2014-07-19', 0, 'y', 30),
(1513, '2014-07-12', 'E', 116, 0, 'E', '2014-07-19', 0, 'y', 30),
(1514, '2014-07-12', 'E', 117, 4, 'E', '2014-07-19', 0, 'y', 30),
(1515, '2014-07-12', 'E', 51, 1, 'E', '2014-07-19', 0, 'y', 30),
(1516, '2014-07-12', 'E', 53, 2, 'E', '2014-07-19', 0, 'y', 30),
(1517, '2014-07-12', 'E', 62, 4, 'E', '2014-07-19', 0, 'y', 30),
(1518, '2014-07-12', 'E', 63, 4, 'E', '2014-07-19', 0, 'y', 30),
(1519, '2014-07-12', 'E', 64, 2, 'E', '2014-07-19', 0, 'y', 30),
(1520, '2014-07-12', 'E', 65, 3, 'E', '2014-07-19', 0, 'y', 30),
(1521, '2014-07-12', 'E', 54, 2, 'E', '2014-07-19', 0, 'y', 30),
(1522, '2014-07-12', 'E', 55, 0, 'E', '2014-07-19', 0, 'y', 30),
(1523, '2014-07-12', 'E', 52, 2, 'E', '2014-07-19', 0, 'y', 30),
(1524, '2014-07-12', 'E', 57, 0, 'E', '2014-07-19', 0, 'y', 30),
(1525, '2014-07-13', 'E', 26, 1, 'E', '2014-07-19', 0, 'y', 30),
(1526, '2014-07-13', 'E', 27, 1, 'E', '2014-07-19', 0, 'y', 30),
(1527, '2014-07-13', 'E', 28, 3, 'E', '2014-07-19', 0, 'y', 30),
(1528, '2014-07-13', 'E', 29, 4, 'E', '2014-07-19', 0, 'y', 30),
(1529, '2014-07-13', 'E', 30, 0, 'E', '2014-07-19', 0, 'y', 30),
(1530, '2014-07-13', 'E', 31, 4, 'E', '2014-07-19', 0, 'y', 30),
(1531, '2014-07-13', 'E', 33, 2, 'E', '2014-07-19', 0, 'y', 30),
(1532, '2014-07-13', 'E', 34, 3, 'E', '2014-07-19', 0, 'y', 30),
(1533, '2014-07-13', 'E', 35, 2, 'E', '2014-07-19', 0, 'y', 30),
(1534, '2014-07-13', 'E', 36, 2, 'E', '2014-07-19', 0, 'y', 30),
(1535, '2014-07-13', 'E', 37, 3, 'E', '2014-07-19', 0, 'y', 30),
(1536, '2014-07-13', 'E', 38, 3, 'E', '2014-07-19', 0, 'y', 30),
(1537, '2014-07-13', 'E', 114, 1, 'E', '2014-07-19', 0, 'y', 30),
(1538, '2014-07-13', 'E', 39, 0, 'E', '2014-07-19', 0, 'y', 30),
(1539, '2014-07-13', 'E', 115, 3, 'E', '2014-07-19', 0, 'y', 30),
(1540, '2014-07-13', 'E', 40, 1, 'E', '2014-07-19', 0, 'y', 30),
(1541, '2014-07-13', 'E', 41, 0, 'E', '2014-07-19', 0, 'y', 30),
(1542, '2014-07-13', 'E', 42, 4, 'E', '2014-07-19', 0, 'y', 30),
(1543, '2014-07-13', 'E', 43, 1, 'E', '2014-07-19', 0, 'y', 30),
(1544, '2014-07-13', 'E', 44, 0, 'E', '2014-07-19', 0, 'y', 30),
(1545, '2014-07-13', 'E', 58, 0, 'E', '2014-07-19', 0, 'y', 30),
(1546, '2014-07-13', 'E', 45, 1, 'E', '2014-07-19', 0, 'y', 30),
(1547, '2014-07-13', 'E', 59, 1, 'E', '2014-07-19', 0, 'y', 30),
(1548, '2014-07-13', 'E', 60, 2, 'E', '2014-07-19', 0, 'y', 30),
(1549, '2014-07-13', 'E', 61, 2, 'E', '2014-07-19', 0, 'y', 30),
(1550, '2014-07-13', 'E', 46, 2, 'E', '2014-07-19', 0, 'y', 30),
(1551, '2014-07-13', 'E', 47, 0, 'E', '2014-07-19', 0, 'y', 30),
(1552, '2014-07-13', 'E', 48, 2, 'E', '2014-07-19', 0, 'y', 30),
(1553, '2014-07-13', 'E', 49, 2, 'E', '2014-07-19', 0, 'y', 30),
(1554, '2014-07-13', 'E', 50, 2, 'E', '2014-07-19', 0, 'y', 30),
(1555, '2014-07-13', 'E', 116, 0, 'E', '2014-07-19', 0, 'y', 30),
(1556, '2014-07-13', 'E', 117, 4, 'E', '2014-07-19', 0, 'y', 30),
(1557, '2014-07-13', 'E', 51, 1, 'E', '2014-07-19', 0, 'y', 30),
(1558, '2014-07-13', 'E', 53, 2, 'E', '2014-07-19', 0, 'y', 30),
(1559, '2014-07-13', 'E', 62, 4, 'E', '2014-07-19', 0, 'y', 30),
(1560, '2014-07-13', 'E', 63, 4, 'E', '2014-07-19', 0, 'y', 30),
(1561, '2014-07-13', 'E', 64, 3, 'E', '2014-07-19', 0, 'y', 30),
(1562, '2014-07-13', 'E', 65, 3, 'E', '2014-07-19', 0, 'y', 30),
(1563, '2014-07-13', 'E', 54, 2, 'E', '2014-07-19', 0, 'y', 30),
(1564, '2014-07-13', 'E', 55, 2, 'E', '2014-07-19', 0, 'y', 30),
(1565, '2014-07-13', 'E', 52, 2, 'E', '2014-07-19', 0, 'y', 30),
(1566, '2014-07-13', 'E', 57, 0, 'E', '2014-07-19', 0, 'y', 30),
(1567, '2014-07-14', 'E', 26, 3, 'E', '2014-07-19', 0, 'y', 30),
(1568, '2014-07-14', 'E', 27, 2, 'E', '2014-07-19', 0, 'y', 30),
(1569, '2014-07-14', 'E', 28, 2, 'E', '2014-07-19', 0, 'y', 30),
(1570, '2014-07-14', 'E', 29, 4, 'E', '2014-07-19', 0, 'y', 30),
(1571, '2014-07-14', 'E', 30, 3, 'E', '2014-07-19', 0, 'y', 30),
(1572, '2014-07-14', 'E', 31, 4, 'E', '2014-07-19', 0, 'y', 30),
(1573, '2014-07-14', 'E', 33, 0, 'E', '2014-07-19', 0, 'y', 30),
(1574, '2014-07-14', 'E', 34, 0, 'E', '2014-07-19', 0, 'y', 30),
(1575, '2014-07-14', 'E', 35, 0, 'E', '2014-07-19', 0, 'y', 30),
(1576, '2014-07-14', 'E', 36, 0, 'E', '2014-07-19', 0, 'y', 30),
(1577, '2014-07-14', 'E', 37, 2, 'E', '2014-07-19', 0, 'y', 30),
(1578, '2014-07-14', 'E', 38, 3, 'E', '2014-07-19', 0, 'y', 30),
(1579, '2014-07-14', 'E', 114, 1, 'E', '2014-07-19', 0, 'y', 30),
(1580, '2014-07-14', 'E', 39, 2, 'E', '2014-07-19', 0, 'y', 30),
(1581, '2014-07-14', 'E', 115, 3, 'E', '2014-07-19', 0, 'y', 30),
(1582, '2014-07-14', 'E', 40, 0, 'E', '2014-07-19', 0, 'y', 30),
(1583, '2014-07-14', 'E', 41, 0, 'E', '2014-07-19', 0, 'y', 30),
(1584, '2014-07-14', 'E', 42, 0, 'E', '2014-07-19', 0, 'y', 30),
(1585, '2014-07-14', 'E', 43, 0, 'E', '2014-07-19', 0, 'y', 30),
(1586, '2014-07-14', 'E', 44, 0, 'E', '2014-07-19', 0, 'y', 30),
(1587, '2014-07-14', 'E', 58, 0, 'E', '2014-07-19', 0, 'y', 30),
(1588, '2014-07-14', 'E', 45, 1, 'E', '2014-07-19', 0, 'y', 30),
(1589, '2014-07-14', 'E', 59, 1, 'E', '2014-07-19', 0, 'y', 30),
(1590, '2014-07-14', 'E', 60, 2, 'E', '2014-07-19', 0, 'y', 30),
(1591, '2014-07-14', 'E', 61, 2, 'E', '2014-07-19', 0, 'y', 30),
(1592, '2014-07-14', 'E', 46, 2, 'E', '2014-07-19', 0, 'y', 30),
(1593, '2014-07-14', 'E', 47, 0, 'E', '2014-07-19', 0, 'y', 30),
(1594, '2014-07-14', 'E', 48, 2, 'E', '2014-07-19', 0, 'y', 30),
(1595, '2014-07-14', 'E', 49, 2, 'E', '2014-07-19', 0, 'y', 30),
(1596, '2014-07-14', 'E', 50, 2, 'E', '2014-07-19', 0, 'y', 30),
(1597, '2014-07-14', 'E', 116, 0, 'E', '2014-07-19', 0, 'y', 30),
(1598, '2014-07-14', 'E', 117, 4, 'E', '2014-07-19', 0, 'y', 30),
(1599, '2014-07-14', 'E', 51, 0, 'E', '2014-07-19', 0, 'y', 30),
(1600, '2014-07-14', 'E', 53, 2, 'E', '2014-07-19', 0, 'y', 30),
(1601, '2014-07-14', 'E', 62, 4, 'E', '2014-07-19', 0, 'y', 30),
(1602, '2014-07-14', 'E', 63, 4, 'E', '2014-07-19', 0, 'y', 30),
(1603, '2014-07-14', 'E', 64, 2, 'E', '2014-07-19', 0, 'y', 30),
(1604, '2014-07-14', 'E', 65, 3, 'E', '2014-07-19', 0, 'y', 30),
(1605, '2014-07-14', 'E', 54, 2, 'E', '2014-07-19', 0, 'y', 30),
(1606, '2014-07-14', 'E', 55, 0, 'E', '2014-07-19', 0, 'y', 30),
(1607, '2014-07-14', 'E', 52, 2, 'E', '2014-07-19', 0, 'y', 30),
(1608, '2014-07-14', 'E', 57, 0, 'E', '2014-07-19', 0, 'y', 30);
INSERT INTO `milksell` (`milkSellId`, `milkSellDate`, `milkTime`, `customerId`, `milkGiven`, `customerMilkTime`, `nextDate`, `nextLitre`, `billCreated`, `rate`) VALUES
(1609, '2014-07-15', 'E', 26, 3, 'E', '2014-07-19', 0, 'y', 30),
(1610, '2014-07-15', 'E', 27, 1, 'E', '2014-07-19', 0, 'y', 30),
(1611, '2014-07-15', 'E', 28, 3, 'E', '2014-07-19', 0, 'y', 30),
(1612, '2014-07-15', 'E', 29, 4, 'E', '2014-07-19', 0, 'y', 30),
(1613, '2014-07-15', 'E', 30, 4, 'E', '2014-07-19', 0, 'y', 30),
(1614, '2014-07-15', 'E', 31, 4, 'E', '2014-07-19', 0, 'y', 30),
(1615, '2014-07-15', 'E', 33, 0, 'E', '2014-07-19', 0, 'y', 30),
(1616, '2014-07-15', 'E', 34, 3, 'E', '2014-07-19', 0, 'y', 30),
(1617, '2014-07-15', 'E', 35, 2, 'E', '2014-07-19', 0, 'y', 30),
(1618, '2014-07-15', 'E', 36, 2, 'E', '2014-07-19', 0, 'y', 30),
(1619, '2014-07-15', 'E', 37, 2, 'E', '2014-07-19', 0, 'y', 30),
(1620, '2014-07-15', 'E', 38, 3, 'E', '2014-07-19', 0, 'y', 30),
(1621, '2014-07-15', 'E', 114, 1, 'E', '2014-07-19', 0, 'y', 30),
(1622, '2014-07-15', 'E', 39, 2, 'E', '2014-07-19', 0, 'y', 30),
(1623, '2014-07-15', 'E', 115, 3, 'E', '2014-07-19', 0, 'y', 30),
(1624, '2014-07-15', 'E', 40, 0, 'E', '2014-07-19', 0, 'y', 30),
(1625, '2014-07-15', 'E', 41, 0, 'E', '2014-07-19', 0, 'y', 30),
(1626, '2014-07-15', 'E', 42, 0, 'E', '2014-07-19', 0, 'y', 30),
(1627, '2014-07-15', 'E', 43, 0, 'E', '2014-07-19', 0, 'y', 30),
(1628, '2014-07-15', 'E', 44, 0, 'E', '2014-07-19', 0, 'y', 30),
(1629, '2014-07-15', 'E', 58, 0, 'E', '2014-07-19', 0, 'y', 30),
(1630, '2014-07-15', 'E', 45, 0, 'E', '2014-07-19', 0, 'y', 30),
(1631, '2014-07-15', 'E', 59, 0, 'E', '2014-07-19', 0, 'y', 30),
(1632, '2014-07-15', 'E', 60, 2, 'E', '2014-07-19', 0, 'y', 30),
(1633, '2014-07-15', 'E', 61, 2, 'E', '2014-07-19', 0, 'y', 30),
(1634, '2014-07-15', 'E', 46, 2, 'E', '2014-07-19', 0, 'y', 30),
(1635, '2014-07-15', 'E', 47, 0, 'E', '2014-07-19', 0, 'y', 30),
(1636, '2014-07-15', 'E', 48, 2, 'E', '2014-07-19', 0, 'y', 30),
(1637, '2014-07-15', 'E', 49, 2, 'E', '2014-07-19', 0, 'y', 30),
(1638, '2014-07-15', 'E', 50, 2, 'E', '2014-07-19', 0, 'y', 30),
(1639, '2014-07-15', 'E', 116, 0, 'E', '2014-07-19', 0, 'y', 30),
(1640, '2014-07-15', 'E', 117, 3, 'E', '2014-07-19', 0, 'y', 30),
(1641, '2014-07-15', 'E', 51, 1, 'E', '2014-07-19', 0, 'y', 30),
(1642, '2014-07-15', 'E', 53, 2, 'E', '2014-07-19', 0, 'y', 30),
(1643, '2014-07-15', 'E', 62, 4, 'E', '2014-07-19', 0, 'y', 30),
(1644, '2014-07-15', 'E', 63, 4, 'E', '2014-07-19', 0, 'y', 30),
(1645, '2014-07-15', 'E', 64, 3, 'E', '2014-07-19', 0, 'y', 30),
(1646, '2014-07-15', 'E', 65, 3, 'E', '2014-07-19', 0, 'y', 30),
(1647, '2014-07-15', 'E', 54, 2, 'E', '2014-07-19', 0, 'y', 30),
(1648, '2014-07-15', 'E', 55, 2, 'E', '2014-07-19', 0, 'y', 30),
(1649, '2014-07-15', 'E', 52, 2, 'E', '2014-07-19', 0, 'y', 30),
(1650, '2014-07-15', 'E', 57, 0, 'E', '2014-07-19', 0, 'y', 30),
(1651, '2014-07-16', 'E', 26, 3, 'E', '2014-07-19', 0, 'y', 30),
(1652, '2014-07-16', 'E', 27, 2, 'E', '2014-07-19', 0, 'y', 30),
(1653, '2014-07-16', 'E', 28, 3, 'E', '2014-07-19', 0, 'y', 30),
(1654, '2014-07-16', 'E', 29, 4, 'E', '2014-07-19', 0, 'y', 30),
(1655, '2014-07-16', 'E', 30, 4, 'E', '2014-07-19', 0, 'y', 30),
(1656, '2014-07-16', 'E', 31, 4, 'E', '2014-07-19', 0, 'y', 30),
(1657, '2014-07-16', 'E', 33, 2, 'E', '2014-07-19', 0, 'y', 30),
(1658, '2014-07-16', 'E', 34, 2, 'E', '2014-07-19', 0, 'y', 30),
(1659, '2014-07-16', 'E', 35, 1, 'E', '2014-07-19', 0, 'y', 30),
(1660, '2014-07-16', 'E', 36, 2, 'E', '2014-07-19', 0, 'y', 30),
(1661, '2014-07-16', 'E', 37, 2, 'E', '2014-07-19', 0, 'y', 30),
(1662, '2014-07-16', 'E', 38, 3, 'E', '2014-07-19', 0, 'y', 30),
(1663, '2014-07-16', 'E', 114, 1, 'E', '2014-07-19', 0, 'y', 30),
(1664, '2014-07-16', 'E', 39, 2, 'E', '2014-07-19', 0, 'y', 30),
(1665, '2014-07-16', 'E', 115, 3, 'E', '2014-07-19', 0, 'y', 30),
(1666, '2014-07-16', 'E', 40, 0, 'E', '2014-07-19', 0, 'y', 30),
(1667, '2014-07-16', 'E', 41, 0, 'E', '2014-07-19', 0, 'y', 30),
(1668, '2014-07-16', 'E', 42, 0, 'E', '2014-07-19', 0, 'y', 30),
(1669, '2014-07-16', 'E', 43, 0, 'E', '2014-07-19', 0, 'y', 30),
(1670, '2014-07-16', 'E', 44, 0, 'E', '2014-07-19', 0, 'y', 30),
(1671, '2014-07-16', 'E', 58, 0, 'E', '2014-07-19', 0, 'y', 30),
(1672, '2014-07-16', 'E', 45, 1, 'E', '2014-07-19', 0, 'y', 30),
(1673, '2014-07-16', 'E', 59, 1, 'E', '2014-07-19', 0, 'y', 30),
(1674, '2014-07-16', 'E', 60, 2, 'E', '2014-07-19', 0, 'y', 30),
(1675, '2014-07-16', 'E', 61, 2, 'E', '2014-07-19', 0, 'y', 30),
(1676, '2014-07-16', 'E', 46, 0, 'E', '2014-07-19', 0, 'y', 30),
(1677, '2014-07-16', 'E', 47, 0, 'E', '2014-07-19', 0, 'y', 30),
(1678, '2014-07-16', 'E', 48, 0, 'E', '2014-07-19', 0, 'y', 30),
(1679, '2014-07-16', 'E', 49, 0, 'E', '2014-07-19', 0, 'y', 30),
(1680, '2014-07-16', 'E', 50, 2, 'E', '2014-07-19', 0, 'y', 30),
(1681, '2014-07-16', 'E', 116, 0, 'E', '2014-07-19', 0, 'y', 30),
(1682, '2014-07-16', 'E', 117, 0, 'E', '2014-07-19', 0, 'y', 30),
(1683, '2014-07-16', 'E', 51, 0, 'E', '2014-07-19', 0, 'y', 30),
(1684, '2014-07-16', 'E', 53, 2, 'E', '2014-07-19', 0, 'y', 30),
(1685, '2014-07-16', 'E', 62, 4, 'E', '2014-07-19', 0, 'y', 30),
(1686, '2014-07-16', 'E', 63, 4, 'E', '2014-07-19', 0, 'y', 30),
(1687, '2014-07-16', 'E', 64, 2, 'E', '2014-07-19', 0, 'y', 30),
(1688, '2014-07-16', 'E', 65, 3, 'E', '2014-07-19', 0, 'y', 30),
(1689, '2014-07-16', 'E', 54, 2, 'E', '2014-07-19', 0, 'y', 30),
(1690, '2014-07-16', 'E', 55, 0, 'E', '2014-07-19', 0, 'y', 30),
(1691, '2014-07-16', 'E', 52, 2, 'E', '2014-07-19', 0, 'y', 30),
(1692, '2014-07-16', 'E', 57, 0, 'E', '2014-07-19', 0, 'y', 30),
(1693, '2014-07-17', 'E', 26, 3, 'E', '2014-07-19', 0, 'y', 30),
(1694, '2014-07-17', 'E', 27, 1, 'E', '2014-07-19', 0, 'y', 30),
(1695, '2014-07-17', 'E', 28, 3, 'E', '2014-07-19', 0, 'y', 30),
(1696, '2014-07-17', 'E', 29, 3, 'E', '2014-07-19', 0, 'y', 30),
(1697, '2014-07-17', 'E', 30, 0, 'E', '2014-07-19', 0, 'y', 30),
(1698, '2014-07-17', 'E', 31, 3, 'E', '2014-07-19', 0, 'y', 30),
(1699, '2014-07-17', 'E', 33, 2, 'E', '2014-07-19', 0, 'y', 30),
(1700, '2014-07-17', 'E', 34, 2, 'E', '2014-07-19', 0, 'y', 30),
(1701, '2014-07-17', 'E', 35, 2, 'E', '2014-07-19', 0, 'y', 30),
(1702, '2014-07-17', 'E', 36, 2, 'E', '2014-07-19', 0, 'y', 30),
(1703, '2014-07-17', 'E', 37, 3, 'E', '2014-07-19', 0, 'y', 30),
(1704, '2014-07-17', 'E', 38, 0, 'E', '2014-07-19', 0, 'y', 30),
(1705, '2014-07-17', 'E', 114, 1, 'E', '2014-07-19', 0, 'y', 30),
(1706, '2014-07-17', 'E', 39, 2, 'E', '2014-07-19', 0, 'y', 30),
(1707, '2014-07-17', 'E', 115, 3, 'E', '2014-07-19', 0, 'y', 30),
(1708, '2014-07-17', 'E', 40, 0, 'E', '2014-07-19', 0, 'y', 30),
(1709, '2014-07-17', 'E', 41, 0, 'E', '2014-07-19', 0, 'y', 30),
(1710, '2014-07-17', 'E', 42, 0, 'E', '2014-07-19', 0, 'y', 30),
(1711, '2014-07-17', 'E', 43, 0, 'E', '2014-07-19', 0, 'y', 30),
(1712, '2014-07-17', 'E', 44, 0, 'E', '2014-07-19', 0, 'y', 30),
(1713, '2014-07-17', 'E', 58, 0, 'E', '2014-07-19', 0, 'y', 30),
(1714, '2014-07-17', 'E', 45, 1, 'E', '2014-07-19', 0, 'y', 30),
(1715, '2014-07-17', 'E', 59, 1, 'E', '2014-07-19', 0, 'y', 30),
(1716, '2014-07-17', 'E', 60, 0, 'E', '2014-07-19', 0, 'y', 30),
(1717, '2014-07-17', 'E', 61, 0, 'E', '2014-07-19', 0, 'y', 30),
(1718, '2014-07-17', 'E', 46, 0, 'E', '2014-07-19', 0, 'y', 30),
(1719, '2014-07-17', 'E', 47, 0, 'E', '2014-07-19', 0, 'y', 30),
(1720, '2014-07-17', 'E', 48, 2, 'E', '2014-07-19', 0, 'y', 30),
(1721, '2014-07-17', 'E', 49, 2, 'E', '2014-07-19', 0, 'y', 30),
(1722, '2014-07-17', 'E', 50, 2, 'E', '2014-07-19', 0, 'y', 30),
(1723, '2014-07-17', 'E', 116, 0, 'E', '2014-07-19', 0, 'y', 30),
(1724, '2014-07-17', 'E', 117, 3, 'E', '2014-07-19', 0, 'y', 30),
(1725, '2014-07-17', 'E', 51, 1, 'E', '2014-07-19', 0, 'y', 30),
(1726, '2014-07-17', 'E', 53, 2, 'E', '2014-07-19', 0, 'y', 30),
(1727, '2014-07-17', 'E', 62, 3, 'E', '2014-07-19', 0, 'y', 30),
(1728, '2014-07-17', 'E', 63, 3, 'E', '2014-07-19', 0, 'y', 30),
(1729, '2014-07-17', 'E', 64, 3, 'E', '2014-07-19', 0, 'y', 30),
(1730, '2014-07-17', 'E', 65, 2, 'E', '2014-07-19', 0, 'y', 30),
(1731, '2014-07-17', 'E', 54, 2, 'E', '2014-07-19', 0, 'y', 30),
(1732, '2014-07-17', 'E', 55, 2, 'E', '2014-07-19', 0, 'y', 30),
(1733, '2014-07-17', 'E', 52, 2, 'E', '2014-07-19', 0, 'y', 30),
(1734, '2014-07-17', 'E', 57, 0, 'E', '2014-07-19', 0, 'y', 30),
(1735, '2014-07-01', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(1736, '2014-07-01', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(1737, '2014-07-01', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(1738, '2014-07-01', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(1739, '2014-07-01', 'M', 107, 2, 'M', '2014-07-20', 0, 'y', 30),
(1740, '2014-07-01', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(1741, '2014-07-01', 'M', 109, 3, 'M', '2014-07-20', 0, 'y', 30),
(1742, '2014-07-01', 'M', 110, 1, 'M', '2014-07-20', 0, 'y', 30),
(1743, '2014-07-01', 'M', 111, 1, 'M', '2014-07-20', 0, 'y', 30),
(1744, '2014-07-01', 'M', 112, 3, 'M', '2014-07-20', 0, 'y', 30),
(1745, '2014-07-01', 'M', 113, 0, 'M', '2014-07-20', 0, 'y', 30),
(1746, '2014-07-19', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(1747, '2014-07-19', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(1748, '2014-07-19', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(1749, '2014-07-19', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(1750, '2014-07-19', 'M', 107, 2, 'M', '2014-07-20', 0, 'y', 30),
(1751, '2014-07-19', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(1752, '2014-07-19', 'M', 109, 4, 'M', '2014-07-20', 0, 'y', 30),
(1753, '2014-07-19', 'M', 110, 1, 'M', '2014-07-20', 0, 'y', 30),
(1754, '2014-07-19', 'M', 111, 2, 'M', '2014-07-20', 0, 'y', 30),
(1755, '2014-07-19', 'M', 112, 4, 'M', '2014-07-20', 0, 'y', 30),
(1756, '2014-07-19', 'M', 113, 0, 'M', '2014-07-20', 0, 'y', 30),
(1757, '2014-07-03', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(1758, '2014-07-03', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(1759, '2014-07-03', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(1760, '2014-07-03', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(1761, '2014-07-03', 'M', 107, 2, 'M', '2014-07-20', 0, 'y', 30),
(1762, '2014-07-03', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(1763, '2014-07-03', 'M', 109, 4, 'M', '2014-07-20', 0, 'y', 30),
(1764, '2014-07-03', 'M', 110, 1, 'M', '2014-07-20', 0, 'y', 30),
(1765, '2014-07-03', 'M', 111, 0, 'M', '2014-07-20', 0, 'y', 30),
(1766, '2014-07-03', 'M', 112, 4, 'M', '2014-07-20', 0, 'y', 30),
(1767, '2014-07-03', 'M', 113, 0, 'M', '2014-07-20', 0, 'y', 30),
(1768, '2014-07-02', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(1769, '2014-07-02', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(1770, '2014-07-02', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(1771, '2014-07-02', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(1772, '2014-07-02', 'M', 107, 2, 'M', '2014-07-20', 0, 'y', 30),
(1773, '2014-07-02', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(1774, '2014-07-02', 'M', 109, 4, 'M', '2014-07-20', 0, 'y', 30),
(1775, '2014-07-02', 'M', 110, 1, 'M', '2014-07-20', 0, 'y', 30),
(1776, '2014-07-02', 'M', 111, 1, 'M', '2014-07-20', 0, 'y', 30),
(1777, '2014-07-02', 'M', 112, 3, 'M', '2014-07-20', 0, 'y', 30),
(1778, '2014-07-02', 'M', 113, 0, 'M', '2014-07-20', 0, 'y', 30),
(1779, '2014-07-04', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(1780, '2014-07-04', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(1781, '2014-07-04', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(1782, '2014-07-04', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(1783, '2014-07-04', 'M', 107, 2, 'M', '2014-07-20', 0, 'y', 30),
(1784, '2014-07-04', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(1785, '2014-07-04', 'M', 109, 4, 'M', '2014-07-20', 0, 'y', 30),
(1786, '2014-07-04', 'M', 110, 1, 'M', '2014-07-20', 0, 'y', 30),
(1787, '2014-07-04', 'M', 111, 1, 'M', '2014-07-20', 0, 'y', 30),
(1788, '2014-07-04', 'M', 112, 4, 'M', '2014-07-20', 0, 'y', 30),
(1789, '2014-07-04', 'M', 113, 0, 'M', '2014-07-20', 0, 'y', 30),
(1790, '2014-07-19', 'M', 66, 2, 'M', '2014-07-20', 0, 'y', 30),
(1791, '2014-07-19', 'M', 67, 3, 'M', '2014-07-20', 0, 'y', 30),
(1792, '2014-07-19', 'M', 68, 1, 'M', '2014-07-20', 0, 'y', 30),
(1793, '2014-07-19', 'M', 69, 2, 'M', '2014-07-20', 0, 'y', 30),
(1794, '2014-07-19', 'M', 24, 3, 'M', '2014-07-20', 0, 'y', 30),
(1795, '2014-07-19', 'M', 71, 5, 'M', '2014-07-20', 0, 'y', 30),
(1796, '2014-07-19', 'M', 73, 3, 'M', '2014-07-20', 0, 'y', 30),
(1797, '2014-07-19', 'M', 74, 0, 'M', '2014-07-20', 0, 'y', 30),
(1798, '2014-07-19', 'M', 75, 2, 'M', '2014-07-20', 0, 'y', 30),
(1799, '2014-07-19', 'M', 76, 1, 'M', '2014-07-20', 0, 'y', 30),
(1800, '2014-07-19', 'M', 77, 2, 'M', '2014-07-20', 0, 'y', 30),
(1801, '2014-07-19', 'M', 78, 4, 'M', '2014-07-20', 0, 'y', 30),
(1802, '2014-07-19', 'M', 79, 1, 'M', '2014-07-20', 0, 'y', 30),
(1803, '2014-07-19', 'M', 80, 3, 'M', '2014-07-20', 0, 'y', 30),
(1804, '2014-07-19', 'M', 81, 3, 'M', '2014-07-20', 0, 'y', 30),
(1805, '2014-07-19', 'M', 82, 1, 'M', '2014-07-20', 0, 'y', 30),
(1806, '2014-07-19', 'M', 83, 0, 'M', '2014-07-20', 0, 'y', 30),
(1807, '2014-07-19', 'M', 84, 1, 'M', '2014-07-20', 0, 'y', 30),
(1808, '2014-07-19', 'M', 85, 0, 'M', '2014-07-20', 0, 'y', 30),
(1809, '2014-07-19', 'M', 86, 0, 'M', '2014-07-20', 0, 'y', 30),
(1810, '2014-07-19', 'M', 87, 2, 'M', '2014-07-20', 0, 'y', 30),
(1811, '2014-07-19', 'M', 88, 0, 'M', '2014-07-20', 0, 'y', 30),
(1812, '2014-07-19', 'M', 89, 0, 'M', '2014-07-20', 0, 'y', 30),
(1813, '2014-07-19', 'M', 90, 9, 'M', '2014-07-20', 0, 'y', 30),
(1814, '2014-07-19', 'M', 91, 0, 'M', '2014-07-20', 0, 'y', 30),
(1815, '2014-07-19', 'M', 92, 0, 'M', '2014-07-20', 0, 'y', 30),
(1816, '2014-07-19', 'M', 93, 0, 'M', '2014-07-20', 0, 'y', 30),
(1817, '2014-07-19', 'M', 94, 1, 'M', '2014-07-20', 0, 'y', 30),
(1818, '2014-07-19', 'M', 95, 0, 'M', '2014-07-20', 0, 'y', 30),
(1819, '2014-07-19', 'M', 96, 0, 'M', '2014-07-20', 0, 'y', 30),
(1820, '2014-07-19', 'M', 97, 3, 'M', '2014-07-20', 0, 'y', 30),
(1821, '2014-07-19', 'M', 98, 3, 'M', '2014-07-20', 0, 'y', 30),
(1822, '2014-07-19', 'M', 99, 0, 'M', '2014-07-20', 0, 'y', 30),
(1823, '2014-07-19', 'M', 100, 4, 'M', '2014-07-20', 0, 'y', 30),
(1824, '2014-07-19', 'M', 23, 0, 'M', '2014-07-20', 0, 'y', 30),
(1825, '2014-07-19', 'M', 72, 0, 'M', '2014-07-20', 0, 'y', 30),
(1826, '2014-07-19', 'M', 70, 0, 'M', '2014-07-20', 0, 'y', 30),
(1827, '2014-07-18', 'M', 66, 3, 'M', '2014-07-20', 0, 'y', 30),
(1828, '2014-07-18', 'M', 67, 3, 'M', '2014-07-20', 0, 'y', 30),
(1829, '2014-07-18', 'M', 68, 1, 'M', '2014-07-20', 0, 'y', 30),
(1830, '2014-07-18', 'M', 69, 2, 'M', '2014-07-20', 0, 'y', 30),
(1831, '2014-07-18', 'M', 24, 3, 'M', '2014-07-20', 0, 'y', 30),
(1832, '2014-07-18', 'M', 71, 5, 'M', '2014-07-20', 0, 'y', 30),
(1833, '2014-07-18', 'M', 73, 4, 'M', '2014-07-20', 0, 'y', 30),
(1834, '2014-07-18', 'M', 74, 0, 'M', '2014-07-20', 0, 'y', 30),
(1835, '2014-07-18', 'M', 75, 2, 'M', '2014-07-20', 0, 'y', 30),
(1836, '2014-07-18', 'M', 76, 1, 'M', '2014-07-20', 0, 'y', 30),
(1837, '2014-07-18', 'M', 77, 2, 'M', '2014-07-20', 0, 'y', 30),
(1838, '2014-07-18', 'M', 78, 4, 'M', '2014-07-20', 0, 'y', 30),
(1839, '2014-07-18', 'M', 79, 1, 'M', '2014-07-20', 0, 'y', 30),
(1840, '2014-07-18', 'M', 80, 3, 'M', '2014-07-20', 0, 'y', 30),
(1841, '2014-07-18', 'M', 81, 3, 'M', '2014-07-20', 0, 'y', 30),
(1842, '2014-07-18', 'M', 82, 1, 'M', '2014-07-20', 0, 'y', 30),
(1843, '2014-07-18', 'M', 83, 0, 'M', '2014-07-20', 0, 'y', 30),
(1844, '2014-07-18', 'M', 84, 1, 'M', '2014-07-20', 0, 'y', 30),
(1845, '2014-07-18', 'M', 85, 3, 'M', '2014-07-20', 0, 'y', 30),
(1846, '2014-07-18', 'M', 86, 1, 'M', '2014-07-20', 0, 'y', 30),
(1847, '2014-07-18', 'M', 87, 3, 'M', '2014-07-20', 0, 'y', 30),
(1848, '2014-07-18', 'M', 88, 0, 'M', '2014-07-20', 0, 'y', 30),
(1849, '2014-07-18', 'M', 89, 0, 'M', '2014-07-20', 0, 'y', 30),
(1850, '2014-07-18', 'M', 90, 10, 'M', '2014-07-20', 0, 'y', 30),
(1851, '2014-07-18', 'M', 91, 0, 'M', '2014-07-20', 0, 'y', 30),
(1852, '2014-07-18', 'M', 92, 0, 'M', '2014-07-20', 0, 'y', 30),
(1853, '2014-07-18', 'M', 93, 0, 'M', '2014-07-20', 0, 'y', 30),
(1854, '2014-07-18', 'M', 94, 2, 'M', '2014-07-20', 0, 'y', 30),
(1855, '2014-07-18', 'M', 95, 1, 'M', '2014-07-20', 0, 'y', 30),
(1856, '2014-07-18', 'M', 96, 1, 'M', '2014-07-20', 0, 'y', 30),
(1857, '2014-07-18', 'M', 97, 4, 'M', '2014-07-20', 0, 'y', 30),
(1858, '2014-07-18', 'M', 98, 3, 'M', '2014-07-20', 0, 'y', 30),
(1859, '2014-07-18', 'M', 99, 0, 'M', '2014-07-20', 0, 'y', 30),
(1860, '2014-07-18', 'M', 100, 4, 'M', '2014-07-20', 0, 'y', 30),
(1861, '2014-07-18', 'M', 23, 0, 'M', '2014-07-20', 0, 'y', 30),
(1862, '2014-07-18', 'M', 72, 0, 'M', '2014-07-20', 0, 'y', 30),
(1863, '2014-07-18', 'M', 70, 0, 'M', '2014-07-20', 0, 'y', 30),
(1864, '2014-07-18', 'E', 26, 3, 'E', '2014-07-20', 0, 'y', 30),
(1865, '2014-07-18', 'E', 27, 2, 'E', '2014-07-20', 0, 'y', 30),
(1866, '2014-07-18', 'E', 28, 3, 'E', '2014-07-20', 0, 'y', 30),
(1867, '2014-07-18', 'E', 29, 4, 'E', '2014-07-20', 0, 'y', 30),
(1868, '2014-07-18', 'E', 30, 4, 'E', '2014-07-20', 0, 'y', 30),
(1869, '2014-07-18', 'E', 31, 4, 'E', '2014-07-20', 0, 'y', 30),
(1870, '2014-07-18', 'E', 33, 2, 'E', '2014-07-20', 0, 'y', 30),
(1871, '2014-07-18', 'E', 34, 2, 'E', '2014-07-20', 0, 'y', 30),
(1872, '2014-07-18', 'E', 35, 1, 'E', '2014-07-20', 0, 'y', 30),
(1873, '2014-07-18', 'E', 36, 2, 'E', '2014-07-20', 0, 'y', 30),
(1874, '2014-07-18', 'E', 37, 3, 'E', '2014-07-20', 0, 'y', 30),
(1875, '2014-07-18', 'E', 38, 3, 'E', '2014-07-20', 0, 'y', 30),
(1876, '2014-07-18', 'E', 114, 1, 'E', '2014-07-20', 0, 'y', 30),
(1877, '2014-07-18', 'E', 39, 2, 'E', '2014-07-20', 0, 'y', 30),
(1878, '2014-07-18', 'E', 115, 3, 'E', '2014-07-20', 0, 'y', 30),
(1879, '2014-07-18', 'E', 40, 0, 'E', '2014-07-20', 0, 'y', 30),
(1880, '2014-07-18', 'E', 41, 0, 'E', '2014-07-20', 0, 'y', 30),
(1881, '2014-07-18', 'E', 42, 0, 'E', '2014-07-20', 0, 'y', 30),
(1882, '2014-07-18', 'E', 43, 0, 'E', '2014-07-20', 0, 'y', 30),
(1883, '2014-07-18', 'E', 44, 0, 'E', '2014-07-20', 0, 'y', 30),
(1884, '2014-07-18', 'E', 58, 0, 'E', '2014-07-20', 0, 'y', 30),
(1885, '2014-07-18', 'E', 45, 1, 'E', '2014-07-20', 0, 'y', 30),
(1886, '2014-07-18', 'E', 59, 1, 'E', '2014-07-20', 0, 'y', 30),
(1887, '2014-07-18', 'E', 60, 2, 'E', '2014-07-20', 0, 'y', 30),
(1888, '2014-07-18', 'E', 61, 2, 'E', '2014-07-20', 0, 'y', 30),
(1889, '2014-07-18', 'E', 46, 0, 'E', '2014-07-20', 0, 'y', 30),
(1890, '2014-07-18', 'E', 47, 0, 'E', '2014-07-20', 0, 'y', 30),
(1891, '2014-07-18', 'E', 48, 2, 'E', '2014-07-20', 0, 'y', 30),
(1892, '2014-07-18', 'E', 49, 2, 'E', '2014-07-20', 0, 'y', 30),
(1893, '2014-07-18', 'E', 50, 2, 'E', '2014-07-20', 0, 'y', 30),
(1894, '2014-07-18', 'E', 116, 0, 'E', '2014-07-20', 0, 'y', 30),
(1895, '2014-07-18', 'E', 117, 2, 'E', '2014-07-20', 0, 'y', 30),
(1896, '2014-07-18', 'E', 51, 1, 'E', '2014-07-20', 0, 'y', 30),
(1897, '2014-07-18', 'E', 53, 2, 'E', '2014-07-20', 0, 'y', 30),
(1898, '2014-07-18', 'E', 62, 4, 'E', '2014-07-20', 0, 'y', 30),
(1899, '2014-07-18', 'E', 63, 3, 'E', '2014-07-20', 0, 'y', 30),
(1900, '2014-07-18', 'E', 64, 0, 'E', '2014-07-20', 0, 'y', 30),
(1901, '2014-07-18', 'E', 65, 0, 'E', '2014-07-20', 0, 'y', 30),
(1902, '2014-07-18', 'E', 54, 0, 'E', '2014-07-20', 0, 'y', 30),
(1903, '2014-07-18', 'E', 55, 0, 'E', '2014-07-20', 0, 'y', 30),
(1904, '2014-07-18', 'E', 52, 1, 'E', '2014-07-20', 0, 'y', 30),
(1905, '2014-07-18', 'E', 57, 0, 'E', '2014-07-20', 0, 'y', 30),
(1906, '2014-07-05', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(1907, '2014-07-05', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(1908, '2014-07-05', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(1909, '2014-07-05', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(1910, '2014-07-05', 'M', 107, 2, 'M', '2014-07-20', 0, 'y', 30),
(1911, '2014-07-05', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(1912, '2014-07-05', 'M', 109, 4, 'M', '2014-07-20', 0, 'y', 30),
(1913, '2014-07-05', 'M', 110, 1, 'M', '2014-07-20', 0, 'y', 30),
(1914, '2014-07-05', 'M', 111, 2, 'M', '2014-07-20', 0, 'y', 30),
(1915, '2014-07-05', 'M', 112, 4, 'M', '2014-07-20', 0, 'y', 30),
(1916, '2014-07-05', 'M', 113, 0, 'M', '2014-07-20', 0, 'y', 30),
(1917, '2014-07-06', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(1918, '2014-07-06', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(1919, '2014-07-06', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(1920, '2014-07-06', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(1921, '2014-07-06', 'M', 107, 2, 'M', '2014-07-20', 0, 'y', 30),
(1922, '2014-07-06', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(1923, '2014-07-06', 'M', 109, 4, 'M', '2014-07-20', 0, 'y', 30),
(1924, '2014-07-06', 'M', 110, 1, 'M', '2014-07-20', 0, 'y', 30),
(1925, '2014-07-06', 'M', 111, 0, 'M', '2014-07-20', 0, 'y', 30),
(1926, '2014-07-06', 'M', 112, 4, 'M', '2014-07-20', 0, 'y', 30),
(1927, '2014-07-06', 'M', 113, 0, 'M', '2014-07-20', 0, 'y', 30),
(1928, '2014-07-07', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(1929, '2014-07-07', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(1930, '2014-07-07', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(1931, '2014-07-07', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(1932, '2014-07-07', 'M', 107, 2, 'M', '2014-07-20', 0, 'y', 30),
(1933, '2014-07-07', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(1934, '2014-07-07', 'M', 109, 0, 'M', '2014-07-20', 0, 'y', 30),
(1935, '2014-07-07', 'M', 110, 1, 'M', '2014-07-20', 0, 'y', 30),
(1936, '2014-07-07', 'M', 111, 2, 'M', '2014-07-20', 0, 'y', 30),
(1937, '2014-07-07', 'M', 112, 4, 'M', '2014-07-20', 0, 'y', 30),
(1938, '2014-07-07', 'M', 113, 0, 'M', '2014-07-20', 0, 'y', 30),
(1939, '2014-07-08', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(1940, '2014-07-08', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(1941, '2014-07-08', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(1942, '2014-07-08', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(1943, '2014-07-08', 'M', 107, 2, 'M', '2014-07-20', 0, 'y', 30),
(1944, '2014-07-08', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(1945, '2014-07-08', 'M', 109, 4, 'M', '2014-07-20', 0, 'y', 30),
(1946, '2014-07-08', 'M', 110, 1, 'M', '2014-07-20', 0, 'y', 30),
(1947, '2014-07-08', 'M', 111, 0, 'M', '2014-07-20', 0, 'y', 30),
(1948, '2014-07-08', 'M', 112, 4, 'M', '2014-07-20', 0, 'y', 30),
(1949, '2014-07-08', 'M', 113, 0, 'M', '2014-07-20', 0, 'y', 30),
(1950, '2014-07-09', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(1951, '2014-07-09', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(1952, '2014-07-09', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(1953, '2014-07-09', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(1954, '2014-07-09', 'M', 107, 2, 'M', '2014-07-20', 0, 'y', 30),
(1955, '2014-07-09', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(1956, '2014-07-09', 'M', 109, 4, 'M', '2014-07-20', 0, 'y', 30),
(1957, '2014-07-09', 'M', 110, 2, 'M', '2014-07-20', 0, 'y', 30),
(1958, '2014-07-09', 'M', 111, 1, 'M', '2014-07-20', 0, 'y', 30),
(1959, '2014-07-09', 'M', 112, 4, 'M', '2014-07-20', 0, 'y', 30),
(1960, '2014-07-09', 'M', 113, 10, 'M', '2014-07-20', 0, 'y', 30),
(1961, '2014-07-10', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(1962, '2014-07-10', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(1963, '2014-07-10', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(1964, '2014-07-10', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(1965, '2014-07-10', 'M', 107, 2, 'M', '2014-07-20', 0, 'y', 30),
(1966, '2014-07-10', 'M', 108, 1, 'M', '2014-07-20', 0, 'y', 30),
(1967, '2014-07-10', 'M', 109, 4, 'M', '2014-07-20', 0, 'y', 30),
(1968, '2014-07-10', 'M', 110, 1, 'M', '2014-07-20', 0, 'y', 30),
(1969, '2014-07-10', 'M', 111, 0, 'M', '2014-07-20', 0, 'y', 30),
(1970, '2014-07-10', 'M', 112, 4, 'M', '2014-07-20', 0, 'y', 30),
(1971, '2014-07-10', 'M', 113, 10, 'M', '2014-07-20', 0, 'y', 30),
(1972, '2014-07-11', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(1973, '2014-07-11', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(1974, '2014-07-11', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(1975, '2014-07-11', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(1976, '2014-07-11', 'M', 107, 2, 'M', '2014-07-20', 0, 'y', 30),
(1977, '2014-07-11', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(1978, '2014-07-11', 'M', 109, 3, 'M', '2014-07-20', 0, 'y', 30),
(1979, '2014-07-11', 'M', 110, 0, 'M', '2014-07-20', 0, 'y', 30),
(1980, '2014-07-11', 'M', 111, 1, 'M', '2014-07-20', 0, 'y', 30),
(1981, '2014-07-11', 'M', 112, 4, 'M', '2014-07-20', 0, 'y', 30),
(1982, '2014-07-11', 'M', 113, 9, 'M', '2014-07-20', 0, 'y', 30),
(1983, '2014-07-12', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(1984, '2014-07-12', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(1985, '2014-07-12', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(1986, '2014-07-12', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(1987, '2014-07-12', 'M', 107, 0, 'M', '2014-07-20', 0, 'y', 30),
(1988, '2014-07-12', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(1989, '2014-07-12', 'M', 109, 4, 'M', '2014-07-20', 0, 'y', 30),
(1990, '2014-07-12', 'M', 110, 2, 'M', '2014-07-20', 0, 'y', 30),
(1991, '2014-07-12', 'M', 111, 2, 'M', '2014-07-20', 0, 'y', 30),
(1992, '2014-07-12', 'M', 112, 4, 'M', '2014-07-20', 0, 'y', 30),
(1993, '2014-07-12', 'M', 113, 11, 'M', '2014-07-20', 0, 'y', 30),
(1994, '2014-07-13', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(1995, '2014-07-13', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(1996, '2014-07-13', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(1997, '2014-07-13', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(1998, '2014-07-13', 'M', 107, 0, 'M', '2014-07-20', 0, 'y', 30),
(1999, '2014-07-13', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(2000, '2014-07-13', 'M', 109, 4, 'M', '2014-07-20', 0, 'y', 30),
(2001, '2014-07-13', 'M', 110, 1, 'M', '2014-07-20', 0, 'y', 30),
(2002, '2014-07-13', 'M', 111, 0, 'M', '2014-07-20', 0, 'y', 30),
(2003, '2014-07-13', 'M', 112, 4, 'M', '2014-07-20', 0, 'y', 30),
(2004, '2014-07-13', 'M', 113, 11, 'M', '2014-07-20', 0, 'y', 30),
(2005, '2014-07-14', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(2006, '2014-07-14', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(2007, '2014-07-14', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(2008, '2014-07-14', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(2009, '2014-07-14', 'M', 107, 0, 'M', '2014-07-20', 0, 'y', 30),
(2010, '2014-07-14', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(2011, '2014-07-14', 'M', 109, 5, 'M', '2014-07-20', 0, 'y', 30),
(2012, '2014-07-14', 'M', 110, 1, 'M', '2014-07-20', 0, 'y', 30),
(2013, '2014-07-14', 'M', 111, 0, 'M', '2014-07-20', 0, 'y', 30),
(2014, '2014-07-14', 'M', 112, 0, 'M', '2014-07-20', 0, 'y', 30),
(2015, '2014-07-14', 'M', 113, 10, 'M', '2014-07-20', 0, 'y', 30),
(2016, '2014-07-15', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(2017, '2014-07-15', 'M', 22, 3, 'M', '2014-07-20', 0, 'y', 30),
(2018, '2014-07-15', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(2019, '2014-07-15', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(2020, '2014-07-15', 'M', 107, 0, 'M', '2014-07-20', 0, 'y', 30),
(2021, '2014-07-15', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(2022, '2014-07-15', 'M', 109, 5, 'M', '2014-07-20', 0, 'y', 30),
(2023, '2014-07-15', 'M', 110, 1, 'M', '2014-07-20', 0, 'y', 30),
(2024, '2014-07-15', 'M', 111, 2, 'M', '2014-07-20', 0, 'y', 30),
(2025, '2014-07-15', 'M', 112, 0, 'M', '2014-07-20', 0, 'y', 30),
(2026, '2014-07-15', 'M', 113, 9, 'M', '2014-07-20', 0, 'y', 30),
(2027, '2014-07-16', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(2028, '2014-07-16', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(2029, '2014-07-16', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(2030, '2014-07-16', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(2031, '2014-07-16', 'M', 107, 0, 'M', '2014-07-20', 0, 'y', 30),
(2032, '2014-07-16', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(2033, '2014-07-16', 'M', 109, 5, 'M', '2014-07-20', 0, 'y', 30),
(2034, '2014-07-16', 'M', 110, 1, 'M', '2014-07-20', 0, 'y', 30),
(2035, '2014-07-16', 'M', 111, 0, 'M', '2014-07-20', 0, 'y', 30),
(2036, '2014-07-16', 'M', 112, 0, 'M', '2014-07-20', 0, 'y', 30),
(2037, '2014-07-16', 'M', 113, 9, 'M', '2014-07-20', 0, 'y', 30),
(2038, '2014-07-17', 'M', 101, 2, 'M', '2014-07-20', 0, 'y', 30),
(2039, '2014-07-17', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(2040, '2014-07-17', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(2041, '2014-07-17', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(2042, '2014-07-17', 'M', 107, 0, 'M', '2014-07-20', 0, 'y', 30),
(2043, '2014-07-17', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(2044, '2014-07-17', 'M', 109, 5, 'M', '2014-07-20', 0, 'y', 30),
(2045, '2014-07-17', 'M', 110, 0, 'M', '2014-07-20', 0, 'y', 30),
(2046, '2014-07-17', 'M', 111, 1, 'M', '2014-07-20', 0, 'y', 30),
(2047, '2014-07-17', 'M', 112, 0, 'M', '2014-07-20', 0, 'y', 30),
(2048, '2014-07-17', 'M', 113, 9, 'M', '2014-07-20', 0, 'y', 30),
(2049, '2014-07-18', 'M', 101, 3, 'M', '2014-07-20', 0, 'y', 30),
(2050, '2014-07-18', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(2051, '2014-07-18', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(2052, '2014-07-18', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(2053, '2014-07-18', 'M', 107, 0, 'M', '2014-07-20', 0, 'y', 30),
(2054, '2014-07-18', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(2055, '2014-07-18', 'M', 109, 0, 'M', '2014-07-20', 0, 'y', 30),
(2056, '2014-07-18', 'M', 110, 1, 'M', '2014-07-20', 0, 'y', 30),
(2057, '2014-07-18', 'M', 111, 2, 'M', '2014-07-20', 0, 'y', 30),
(2058, '2014-07-18', 'M', 112, 0, 'M', '2014-07-20', 0, 'y', 30),
(2059, '2014-07-18', 'M', 113, 7, 'M', '2014-07-20', 0, 'y', 30),
(2060, '2014-07-19', 'M', 101, 3, 'M', '2014-07-20', 0, 'y', 30),
(2061, '2014-07-19', 'M', 22, 2, 'M', '2014-07-20', 0, 'y', 30),
(2062, '2014-07-19', 'M', 105, 2, 'M', '2014-07-20', 0, 'y', 30),
(2063, '2014-07-19', 'M', 106, 2, 'M', '2014-07-20', 0, 'y', 30),
(2064, '2014-07-19', 'M', 107, 0, 'M', '2014-07-20', 0, 'y', 30),
(2065, '2014-07-19', 'M', 108, 2, 'M', '2014-07-20', 0, 'y', 30),
(2066, '2014-07-19', 'M', 109, 4, 'M', '2014-07-20', 0, 'y', 30),
(2067, '2014-07-19', 'M', 110, 1, 'M', '2014-07-20', 0, 'y', 30),
(2068, '2014-07-19', 'M', 111, 0, 'M', '2014-07-20', 0, 'y', 30),
(2069, '2014-07-19', 'M', 112, 0, 'M', '2014-07-20', 0, 'y', 30),
(2070, '2014-07-19', 'M', 113, 9, 'M', '2014-07-20', 0, 'y', 30),
(2071, '2014-07-19', 'E', 11, 0, 'E', '2014-07-25', 0, 'y', 30),
(2072, '2014-07-19', 'E', 12, 6, 'E', '2014-07-25', 0, 'y', 30),
(2073, '2014-07-19', 'E', 13, 6, 'E', '2014-07-25', 0, 'y', 30),
(2074, '2014-07-19', 'E', 14, 2, 'E', '2014-07-25', 0, 'y', 30),
(2075, '2014-07-19', 'E', 15, 1, 'E', '2014-07-25', 0, 'y', 30),
(2076, '2014-07-19', 'E', 16, 1, 'E', '2014-07-25', 0, 'y', 30),
(2077, '2014-07-19', 'E', 17, 2, 'E', '2014-07-25', 0, 'y', 30),
(2078, '2014-07-16', 'E', 11, 0, 'E', '2014-07-20', 0, 'y', 30),
(2079, '2014-07-16', 'E', 12, 5, 'E', '2014-07-20', 0, 'y', 30),
(2080, '2014-07-16', 'E', 13, 6, 'E', '2014-07-20', 0, 'y', 30),
(2081, '2014-07-16', 'E', 14, 2, 'E', '2014-07-20', 0, 'y', 30),
(2082, '2014-07-16', 'E', 15, 1, 'E', '2014-07-20', 0, 'y', 30),
(2083, '2014-07-16', 'E', 16, 1, 'E', '2014-07-20', 0, 'y', 30),
(2084, '2014-07-16', 'E', 17, 2, 'E', '2014-07-20', 0, 'y', 30),
(2085, '2014-07-17', 'E', 11, 0, 'E', '2014-07-20', 0, 'y', 30),
(2086, '2014-07-17', 'E', 12, 5, 'E', '2014-07-20', 0, 'y', 30),
(2087, '2014-07-17', 'E', 13, 6, 'E', '2014-07-20', 0, 'y', 30),
(2088, '2014-07-17', 'E', 14, 2, 'E', '2014-07-20', 0, 'y', 30),
(2089, '2014-07-17', 'E', 15, 1, 'E', '2014-07-20', 0, 'y', 30),
(2090, '2014-07-17', 'E', 16, 1, 'E', '2014-07-20', 0, 'y', 30),
(2091, '2014-07-17', 'E', 17, 2, 'E', '2014-07-20', 0, 'y', 30),
(2092, '2014-07-18', 'E', 11, 0, 'E', '2014-07-20', 0, 'y', 30),
(2093, '2014-07-18', 'E', 12, 6, 'E', '2014-07-20', 0, 'y', 30),
(2094, '2014-07-18', 'E', 13, 6, 'E', '2014-07-20', 0, 'y', 30),
(2095, '2014-07-18', 'E', 14, 2, 'E', '2014-07-20', 0, 'y', 30),
(2096, '2014-07-18', 'E', 15, 1, 'E', '2014-07-20', 0, 'y', 30),
(2097, '2014-07-18', 'E', 16, 1, 'E', '2014-07-20', 0, 'y', 30),
(2098, '2014-07-18', 'E', 17, 2, 'E', '2014-07-20', 0, 'y', 30),
(2099, '2014-07-19', 'E', 11, 0, 'E', '2014-07-25', 0, 'y', 30),
(2100, '2014-07-19', 'E', 12, 6, 'E', '2014-07-25', 0, 'y', 30),
(2101, '2014-07-19', 'E', 13, 6, 'E', '2014-07-25', 0, 'y', 30),
(2102, '2014-07-19', 'E', 14, 2, 'E', '2014-07-25', 0, 'y', 30),
(2103, '2014-07-19', 'E', 15, 1, 'E', '2014-07-25', 0, 'y', 30),
(2104, '2014-07-19', 'E', 16, 1, 'E', '2014-07-25', 0, 'y', 30),
(2105, '2014-07-19', 'E', 17, 2, 'E', '2014-07-25', 0, 'y', 30),
(2106, '2014-07-19', 'E', 11, 0, 'E', '2014-07-25', 0, 'y', 30),
(2107, '2014-07-19', 'E', 12, 6, 'E', '2014-07-25', 0, 'y', 30),
(2108, '2014-07-19', 'E', 13, 6, 'E', '2014-07-25', 0, 'y', 30),
(2109, '2014-07-19', 'E', 14, 2, 'E', '2014-07-25', 0, 'y', 30),
(2110, '2014-07-19', 'E', 15, 1, 'E', '2014-07-25', 0, 'y', 30),
(2111, '2014-07-19', 'E', 16, 1, 'E', '2014-07-25', 0, 'y', 30),
(2112, '2014-07-19', 'E', 17, 2, 'E', '2014-07-25', 0, 'y', 30),
(2113, '2014-07-21', 'M', 66, 3, 'M', '2014-07-22', 0, 'y', 30),
(2114, '2014-07-21', 'M', 67, 3, 'M', '2014-07-22', 0, 'y', 30),
(2115, '2014-07-21', 'M', 68, 1, 'M', '2014-07-22', 0, 'y', 30),
(2116, '2014-07-21', 'M', 69, 2, 'M', '2014-07-22', 0, 'y', 30),
(2117, '2014-07-21', 'M', 24, 3, 'M', '2014-07-22', 0, 'y', 30),
(2118, '2014-07-21', 'M', 71, 5, 'M', '2014-07-22', 0, 'y', 30),
(2119, '2014-07-21', 'M', 73, 4, 'M', '2014-07-22', 0, 'y', 30),
(2120, '2014-07-21', 'M', 74, 0, 'M', '2014-07-22', 0, 'y', 30),
(2121, '2014-07-21', 'M', 75, 2, 'M', '2014-07-22', 0, 'y', 30),
(2122, '2014-07-21', 'M', 76, 1, 'M', '2014-07-22', 0, 'y', 30),
(2123, '2014-07-21', 'M', 77, 2, 'M', '2014-07-22', 0, 'y', 30),
(2124, '2014-07-21', 'M', 78, 4, 'M', '2014-07-22', 0, 'y', 30),
(2125, '2014-07-21', 'M', 79, 1, 'M', '2014-07-22', 0, 'y', 30),
(2126, '2014-07-21', 'M', 80, 3, 'M', '2014-07-22', 0, 'y', 30),
(2127, '2014-07-21', 'M', 81, 3, 'M', '2014-07-22', 0, 'y', 30),
(2128, '2014-07-21', 'M', 82, 1, 'M', '2014-07-22', 0, 'y', 30),
(2129, '2014-07-21', 'M', 83, 0, 'M', '2014-07-22', 0, 'y', 30),
(2130, '2014-07-21', 'M', 84, 1, 'M', '2014-07-22', 0, 'y', 30),
(2131, '2014-07-21', 'M', 85, 0, 'M', '2014-07-22', 0, 'y', 30),
(2132, '2014-07-21', 'M', 86, 0, 'M', '2014-07-22', 0, 'y', 30),
(2133, '2014-07-21', 'M', 87, 0, 'M', '2014-07-22', 0, 'y', 30),
(2134, '2014-07-21', 'M', 88, 0, 'M', '2014-07-22', 0, 'y', 30),
(2135, '2014-07-21', 'M', 89, 0, 'M', '2014-07-22', 0, 'y', 30),
(2136, '2014-07-21', 'M', 90, 8, 'M', '2014-07-22', 0, 'y', 30),
(2137, '2014-07-21', 'M', 91, 0, 'M', '2014-07-22', 0, 'y', 30),
(2138, '2014-07-21', 'M', 92, 0, 'M', '2014-07-22', 0, 'y', 30),
(2139, '2014-07-21', 'M', 93, 0, 'M', '2014-07-22', 0, 'y', 30),
(2140, '2014-07-21', 'M', 94, 2, 'M', '2014-07-22', 0, 'y', 30),
(2141, '2014-07-21', 'M', 95, 1, 'M', '2014-07-22', 0, 'y', 30),
(2142, '2014-07-21', 'M', 96, 1, 'M', '2014-07-22', 0, 'y', 30),
(2143, '2014-07-21', 'M', 97, 4, 'M', '2014-07-22', 0, 'y', 30),
(2144, '2014-07-21', 'M', 98, 3, 'M', '2014-07-22', 0, 'y', 30),
(2145, '2014-07-21', 'M', 99, 0, 'M', '2014-07-22', 0, 'y', 30),
(2146, '2014-07-21', 'M', 100, 4, 'M', '2014-07-22', 0, 'y', 30),
(2147, '2014-07-21', 'M', 23, 0, 'M', '2014-07-22', 0, 'y', 30),
(2148, '2014-07-21', 'M', 72, 0, 'M', '2014-07-22', 0, 'y', 30),
(2149, '2014-07-21', 'M', 70, 0, 'M', '2014-07-22', 0, 'y', 30),
(2150, '2014-07-21', 'M', 66, 2, 'M', '2014-07-22', 0, 'y', 30),
(2151, '2014-07-21', 'M', 67, 3, 'M', '2014-07-22', 0, 'y', 30),
(2152, '2014-07-21', 'M', 68, 1, 'M', '2014-07-22', 0, 'y', 30),
(2153, '2014-07-21', 'M', 69, 2, 'M', '2014-07-22', 0, 'y', 30),
(2154, '2014-07-21', 'M', 24, 3, 'M', '2014-07-22', 0, 'y', 30),
(2155, '2014-07-21', 'M', 71, 5, 'M', '2014-07-22', 0, 'y', 30),
(2156, '2014-07-21', 'M', 73, 4, 'M', '2014-07-22', 0, 'y', 30),
(2157, '2014-07-21', 'M', 74, 0, 'M', '2014-07-22', 0, 'y', 30),
(2158, '2014-07-21', 'M', 75, 2, 'M', '2014-07-22', 0, 'y', 30),
(2159, '2014-07-21', 'M', 76, 1, 'M', '2014-07-22', 0, 'y', 30),
(2160, '2014-07-21', 'M', 77, 2, 'M', '2014-07-22', 0, 'y', 30),
(2161, '2014-07-21', 'M', 78, 4, 'M', '2014-07-22', 0, 'y', 30),
(2162, '2014-07-21', 'M', 79, 1, 'M', '2014-07-22', 0, 'y', 30),
(2163, '2014-07-21', 'M', 80, 3, 'M', '2014-07-22', 0, 'y', 30),
(2164, '2014-07-21', 'M', 81, 3, 'M', '2014-07-22', 0, 'y', 30),
(2165, '2014-07-21', 'M', 82, 1, 'M', '2014-07-22', 0, 'y', 30),
(2166, '2014-07-21', 'M', 83, 0, 'M', '2014-07-22', 0, 'y', 30),
(2167, '2014-07-21', 'M', 84, 0, 'M', '2014-07-22', 0, 'y', 30),
(2168, '2014-07-21', 'M', 85, 0, 'M', '2014-07-22', 0, 'y', 30),
(2169, '2014-07-21', 'M', 86, 0, 'M', '2014-07-22', 0, 'y', 30),
(2170, '2014-07-21', 'M', 87, 0, 'M', '2014-07-22', 0, 'y', 30),
(2171, '2014-07-21', 'M', 88, 0, 'M', '2014-07-22', 0, 'y', 30),
(2172, '2014-07-21', 'M', 89, 0, 'M', '2014-07-22', 0, 'y', 30),
(2173, '2014-07-21', 'M', 90, 8, 'M', '2014-07-22', 0, 'y', 30),
(2174, '2014-07-21', 'M', 91, 0, 'M', '2014-07-22', 0, 'y', 30),
(2175, '2014-07-21', 'M', 92, 0, 'M', '2014-07-22', 0, 'y', 30),
(2176, '2014-07-21', 'M', 93, 0, 'M', '2014-07-22', 0, 'y', 30),
(2177, '2014-07-21', 'M', 94, 2, 'M', '2014-07-22', 0, 'y', 30),
(2178, '2014-07-21', 'M', 95, 1, 'M', '2014-07-22', 0, 'y', 30),
(2179, '2014-07-21', 'M', 96, 1, 'M', '2014-07-22', 0, 'y', 30),
(2180, '2014-07-21', 'M', 97, 0, 'M', '2014-07-22', 0, 'y', 30),
(2181, '2014-07-21', 'M', 98, 0, 'M', '2014-07-22', 0, 'y', 30),
(2182, '2014-07-21', 'M', 99, 0, 'M', '2014-07-22', 0, 'y', 30),
(2183, '2014-07-21', 'M', 100, 5, 'M', '2014-07-22', 0, 'y', 30),
(2184, '2014-07-21', 'M', 23, 0, 'M', '2014-07-22', 0, 'y', 30),
(2185, '2014-07-21', 'M', 72, 0, 'M', '2014-07-22', 0, 'y', 30),
(2186, '2014-07-21', 'M', 70, 0, 'M', '2014-07-22', 0, 'y', 30),
(2187, '2014-07-20', 'M', 66, 3, 'M', '2014-07-22', 0, 'y', 30),
(2188, '2014-07-20', 'M', 67, 3, 'M', '2014-07-22', 0, 'y', 30),
(2189, '2014-07-20', 'M', 68, 1, 'M', '2014-07-22', 0, 'y', 30),
(2190, '2014-07-20', 'M', 69, 2, 'M', '2014-07-22', 0, 'y', 30),
(2191, '2014-07-20', 'M', 24, 3, 'M', '2014-07-22', 0, 'y', 30),
(2192, '2014-07-20', 'M', 71, 5, 'M', '2014-07-22', 0, 'y', 30),
(2193, '2014-07-20', 'M', 73, 4, 'M', '2014-07-22', 0, 'y', 30),
(2194, '2014-07-20', 'M', 74, 0, 'M', '2014-07-22', 0, 'y', 30),
(2195, '2014-07-20', 'M', 75, 2, 'M', '2014-07-22', 0, 'y', 30),
(2196, '2014-07-20', 'M', 76, 1, 'M', '2014-07-22', 0, 'y', 30),
(2197, '2014-07-20', 'M', 77, 2, 'M', '2014-07-22', 0, 'y', 30),
(2198, '2014-07-20', 'M', 78, 4, 'M', '2014-07-22', 0, 'y', 30),
(2199, '2014-07-20', 'M', 79, 1, 'M', '2014-07-22', 0, 'y', 30),
(2200, '2014-07-20', 'M', 80, 3, 'M', '2014-07-22', 0, 'y', 30),
(2201, '2014-07-20', 'M', 81, 3, 'M', '2014-07-22', 0, 'y', 30),
(2202, '2014-07-20', 'M', 82, 1, 'M', '2014-07-22', 0, 'y', 30),
(2203, '2014-07-20', 'M', 83, 0, 'M', '2014-07-22', 0, 'y', 30),
(2204, '2014-07-20', 'M', 84, 1, 'M', '2014-07-22', 0, 'y', 30),
(2205, '2014-07-20', 'M', 85, 0, 'M', '2014-07-22', 0, 'y', 30),
(2206, '2014-07-20', 'M', 86, 0, 'M', '2014-07-22', 0, 'y', 30),
(2207, '2014-07-20', 'M', 87, 0, 'M', '2014-07-22', 0, 'y', 30),
(2208, '2014-07-20', 'M', 88, 0, 'M', '2014-07-22', 0, 'y', 30),
(2209, '2014-07-20', 'M', 89, 0, 'M', '2014-07-22', 0, 'y', 30),
(2210, '2014-07-20', 'M', 90, 8, 'M', '2014-07-22', 0, 'y', 30),
(2211, '2014-07-20', 'M', 91, 0, 'M', '2014-07-22', 0, 'y', 30),
(2212, '2014-07-20', 'M', 92, 0, 'M', '2014-07-22', 0, 'y', 30),
(2213, '2014-07-20', 'M', 93, 0, 'M', '2014-07-22', 0, 'y', 30),
(2214, '2014-07-20', 'M', 94, 2, 'M', '2014-07-22', 0, 'y', 30),
(2215, '2014-07-20', 'M', 95, 1, 'M', '2014-07-22', 0, 'y', 30),
(2216, '2014-07-20', 'M', 96, 1, 'M', '2014-07-22', 0, 'y', 30),
(2217, '2014-07-20', 'M', 97, 4, 'M', '2014-07-22', 0, 'y', 30),
(2218, '2014-07-20', 'M', 98, 3, 'M', '2014-07-22', 0, 'y', 30),
(2219, '2014-07-20', 'M', 99, 0, 'M', '2014-07-22', 0, 'y', 30),
(2220, '2014-07-20', 'M', 100, 4, 'M', '2014-07-22', 0, 'y', 30),
(2221, '2014-07-20', 'M', 23, 0, 'M', '2014-07-22', 0, 'y', 30),
(2222, '2014-07-20', 'M', 72, 0, 'M', '2014-07-22', 0, 'y', 30),
(2223, '2014-07-20', 'M', 70, 0, 'M', '2014-07-22', 0, 'y', 30),
(2224, '2014-07-21', 'M', 66, 2, 'M', '2014-07-22', 0, 'y', 30),
(2225, '2014-07-21', 'M', 67, 3, 'M', '2014-07-22', 0, 'y', 30),
(2226, '2014-07-21', 'M', 68, 1, 'M', '2014-07-22', 0, 'y', 30),
(2227, '2014-07-21', 'M', 69, 2, 'M', '2014-07-22', 0, 'y', 30),
(2228, '2014-07-21', 'M', 24, 3, 'M', '2014-07-22', 0, 'y', 30),
(2229, '2014-07-21', 'M', 71, 5, 'M', '2014-07-22', 0, 'y', 30),
(2230, '2014-07-21', 'M', 73, 4, 'M', '2014-07-22', 0, 'y', 30),
(2231, '2014-07-21', 'M', 74, 0, 'M', '2014-07-22', 0, 'y', 30),
(2232, '2014-07-21', 'M', 75, 2, 'M', '2014-07-22', 0, 'y', 30),
(2233, '2014-07-21', 'M', 76, 1, 'M', '2014-07-22', 0, 'y', 30),
(2234, '2014-07-21', 'M', 77, 2, 'M', '2014-07-22', 0, 'y', 30),
(2235, '2014-07-21', 'M', 78, 4, 'M', '2014-07-22', 0, 'y', 30),
(2236, '2014-07-21', 'M', 79, 1, 'M', '2014-07-22', 0, 'y', 30),
(2237, '2014-07-21', 'M', 80, 3, 'M', '2014-07-22', 0, 'y', 30),
(2238, '2014-07-21', 'M', 81, 3, 'M', '2014-07-22', 0, 'y', 30),
(2239, '2014-07-21', 'M', 82, 1, 'M', '2014-07-22', 0, 'y', 30),
(2240, '2014-07-21', 'M', 83, 0, 'M', '2014-07-22', 0, 'y', 30),
(2241, '2014-07-21', 'M', 84, 1, 'M', '2014-07-22', 0, 'y', 30),
(2242, '2014-07-21', 'M', 85, 0, 'M', '2014-07-22', 0, 'y', 30),
(2243, '2014-07-21', 'M', 86, 0, 'M', '2014-07-22', 0, 'y', 30),
(2244, '2014-07-21', 'M', 87, 0, 'M', '2014-07-22', 0, 'y', 30),
(2245, '2014-07-21', 'M', 88, 0, 'M', '2014-07-22', 0, 'y', 30),
(2246, '2014-07-21', 'M', 89, 0, 'M', '2014-07-22', 0, 'y', 30),
(2247, '2014-07-21', 'M', 90, 8, 'M', '2014-07-22', 0, 'y', 30),
(2248, '2014-07-21', 'M', 91, 0, 'M', '2014-07-22', 0, 'y', 30),
(2249, '2014-07-21', 'M', 92, 0, 'M', '2014-07-22', 0, 'y', 30),
(2250, '2014-07-21', 'M', 93, 0, 'M', '2014-07-22', 0, 'y', 30),
(2251, '2014-07-21', 'M', 94, 2, 'M', '2014-07-22', 0, 'y', 30),
(2252, '2014-07-21', 'M', 95, 1, 'M', '2014-07-22', 0, 'y', 30),
(2253, '2014-07-21', 'M', 96, 1, 'M', '2014-07-22', 0, 'y', 30),
(2254, '2014-07-21', 'M', 97, 0, 'M', '2014-07-22', 0, 'y', 30),
(2255, '2014-07-21', 'M', 98, 0, 'M', '2014-07-22', 0, 'y', 30),
(2256, '2014-07-21', 'M', 99, 0, 'M', '2014-07-22', 0, 'y', 30),
(2257, '2014-07-21', 'M', 100, 5, 'M', '2014-07-22', 0, 'y', 30),
(2258, '2014-07-21', 'M', 23, 0, 'M', '2014-07-22', 0, 'y', 30),
(2259, '2014-07-21', 'M', 72, 0, 'M', '2014-07-22', 0, 'y', 30),
(2260, '2014-07-21', 'M', 70, 0, 'M', '2014-07-22', 0, 'y', 30),
(2261, '2014-07-19', 'E', 26, 3, 'E', '2014-07-22', 0, 'y', 30),
(2262, '2014-07-19', 'E', 27, 1, 'E', '2014-07-22', 0, 'y', 30),
(2263, '2014-07-19', 'E', 28, 3, 'E', '2014-07-22', 0, 'y', 30),
(2264, '2014-07-19', 'E', 29, 4, 'E', '2014-07-22', 0, 'y', 30),
(2265, '2014-07-19', 'E', 30, 3, 'E', '2014-07-22', 0, 'y', 30),
(2266, '2014-07-19', 'E', 31, 3, 'E', '2014-07-22', 0, 'y', 30),
(2267, '2014-07-19', 'E', 33, 2, 'E', '2014-07-22', 0, 'y', 30),
(2268, '2014-07-19', 'E', 34, 0, 'E', '2014-07-22', 0, 'y', 30),
(2269, '2014-07-19', 'E', 35, 0, 'E', '2014-07-22', 0, 'y', 30),
(2270, '2014-07-19', 'E', 36, 2, 'E', '2014-07-22', 0, 'y', 30),
(2271, '2014-07-19', 'E', 37, 3, 'E', '2014-07-22', 0, 'y', 30),
(2272, '2014-07-19', 'E', 38, 3, 'E', '2014-07-22', 0, 'y', 30),
(2273, '2014-07-19', 'E', 114, 1, 'E', '2014-07-22', 0, 'y', 30),
(2274, '2014-07-19', 'E', 39, 0, 'E', '2014-07-22', 0, 'y', 30),
(2275, '2014-07-19', 'E', 115, 3, 'E', '2014-07-22', 0, 'y', 30),
(2276, '2014-07-19', 'E', 40, 0, 'E', '2014-07-22', 0, 'y', 30),
(2277, '2014-07-19', 'E', 41, 0, 'E', '2014-07-22', 0, 'y', 30),
(2278, '2014-07-19', 'E', 42, 0, 'E', '2014-07-22', 0, 'y', 30),
(2279, '2014-07-19', 'E', 43, 0, 'E', '2014-07-22', 0, 'y', 30),
(2280, '2014-07-19', 'E', 44, 0, 'E', '2014-07-22', 0, 'y', 30),
(2281, '2014-07-19', 'E', 58, 0, 'E', '2014-07-22', 0, 'y', 30),
(2282, '2014-07-19', 'E', 45, 1, 'E', '2014-07-22', 0, 'y', 30),
(2283, '2014-07-19', 'E', 59, 1, 'E', '2014-07-22', 0, 'y', 30),
(2284, '2014-07-19', 'E', 60, 2, 'E', '2014-07-22', 0, 'y', 30),
(2285, '2014-07-19', 'E', 61, 2, 'E', '2014-07-22', 0, 'y', 30),
(2286, '2014-07-19', 'E', 46, 0, 'E', '2014-07-22', 0, 'y', 30),
(2287, '2014-07-19', 'E', 47, 0, 'E', '2014-07-22', 0, 'y', 30),
(2288, '2014-07-19', 'E', 48, 1, 'E', '2014-07-22', 0, 'y', 30),
(2289, '2014-07-19', 'E', 49, 2, 'E', '2014-07-22', 0, 'y', 30),
(2290, '2014-07-19', 'E', 50, 2, 'E', '2014-07-22', 0, 'y', 30),
(2291, '2014-07-19', 'E', 116, 0, 'E', '2014-07-22', 0, 'y', 30),
(2292, '2014-07-19', 'E', 117, 3, 'E', '2014-07-22', 0, 'y', 30),
(2293, '2014-07-19', 'E', 51, 1, 'E', '2014-07-22', 0, 'y', 30),
(2294, '2014-07-19', 'E', 53, 2, 'E', '2014-07-22', 0, 'y', 30),
(2295, '2014-07-19', 'E', 62, 4, 'E', '2014-07-22', 0, 'y', 30),
(2296, '2014-07-19', 'E', 63, 3, 'E', '2014-07-22', 0, 'y', 30),
(2297, '2014-07-19', 'E', 64, 0, 'E', '2014-07-22', 0, 'y', 30),
(2298, '2014-07-19', 'E', 65, 0, 'E', '2014-07-22', 0, 'y', 30),
(2299, '2014-07-19', 'E', 54, 0, 'E', '2014-07-22', 0, 'y', 30),
(2300, '2014-07-19', 'E', 55, 0, 'E', '2014-07-22', 0, 'y', 30),
(2301, '2014-07-19', 'E', 52, 2, 'E', '2014-07-22', 0, 'y', 30),
(2302, '2014-07-19', 'E', 57, 0, 'E', '2014-07-22', 0, 'y', 30),
(2303, '2014-07-20', 'E', 26, 3, 'E', '2014-07-22', 0, 'y', 30),
(2304, '2014-07-20', 'E', 27, 2, 'E', '2014-07-22', 0, 'y', 30),
(2305, '2014-07-20', 'E', 28, 3, 'E', '2014-07-22', 0, 'y', 30),
(2306, '2014-07-20', 'E', 29, 4, 'E', '2014-07-22', 0, 'y', 30),
(2307, '2014-07-20', 'E', 30, 3, 'E', '2014-07-22', 0, 'y', 30),
(2308, '2014-07-20', 'E', 31, 3, 'E', '2014-07-22', 0, 'y', 30),
(2309, '2014-07-20', 'E', 33, 2, 'E', '2014-07-22', 0, 'y', 30),
(2310, '2014-07-20', 'E', 34, 0, 'E', '2014-07-22', 0, 'y', 30),
(2311, '2014-07-20', 'E', 35, 0, 'E', '2014-07-22', 0, 'y', 30),
(2312, '2014-07-20', 'E', 36, 2, 'E', '2014-07-22', 0, 'y', 30),
(2313, '2014-07-20', 'E', 37, 3, 'E', '2014-07-22', 0, 'y', 30),
(2314, '2014-07-20', 'E', 38, 3, 'E', '2014-07-22', 0, 'y', 30),
(2315, '2014-07-20', 'E', 114, 1, 'E', '2014-07-22', 0, 'y', 30),
(2316, '2014-07-20', 'E', 39, 0, 'E', '2014-07-22', 0, 'y', 30),
(2317, '2014-07-20', 'E', 115, 3, 'E', '2014-07-22', 0, 'y', 30),
(2318, '2014-07-20', 'E', 40, 0, 'E', '2014-07-22', 0, 'y', 30),
(2319, '2014-07-20', 'E', 41, 0, 'E', '2014-07-22', 0, 'y', 30),
(2320, '2014-07-20', 'E', 42, 0, 'E', '2014-07-22', 0, 'y', 30),
(2321, '2014-07-20', 'E', 43, 0, 'E', '2014-07-22', 0, 'y', 30),
(2322, '2014-07-20', 'E', 44, 0, 'E', '2014-07-22', 0, 'y', 30),
(2323, '2014-07-20', 'E', 58, 0, 'E', '2014-07-22', 0, 'y', 30),
(2324, '2014-07-20', 'E', 45, 1, 'E', '2014-07-22', 0, 'y', 30),
(2325, '2014-07-20', 'E', 59, 1, 'E', '2014-07-22', 0, 'y', 30),
(2326, '2014-07-20', 'E', 60, 2, 'E', '2014-07-22', 0, 'y', 30),
(2327, '2014-07-20', 'E', 61, 2, 'E', '2014-07-22', 0, 'y', 30),
(2328, '2014-07-20', 'E', 46, 0, 'E', '2014-07-22', 0, 'y', 30),
(2329, '2014-07-20', 'E', 47, 0, 'E', '2014-07-22', 0, 'y', 30),
(2330, '2014-07-20', 'E', 48, 1, 'E', '2014-07-22', 0, 'y', 30),
(2331, '2014-07-20', 'E', 49, 2, 'E', '2014-07-22', 0, 'y', 30),
(2332, '2014-07-20', 'E', 50, 2, 'E', '2014-07-22', 0, 'y', 30),
(2333, '2014-07-20', 'E', 116, 0, 'E', '2014-07-22', 0, 'y', 30),
(2334, '2014-07-20', 'E', 117, 3, 'E', '2014-07-22', 0, 'y', 30),
(2335, '2014-07-20', 'E', 51, 1, 'E', '2014-07-22', 0, 'y', 30),
(2336, '2014-07-20', 'E', 53, 2, 'E', '2014-07-22', 0, 'y', 30),
(2337, '2014-07-20', 'E', 62, 4, 'E', '2014-07-22', 0, 'y', 30),
(2338, '2014-07-20', 'E', 63, 3, 'E', '2014-07-22', 0, 'y', 30),
(2339, '2014-07-20', 'E', 64, 0, 'E', '2014-07-22', 0, 'y', 30),
(2340, '2014-07-20', 'E', 65, 0, 'E', '2014-07-22', 0, 'y', 30),
(2341, '2014-07-20', 'E', 54, 0, 'E', '2014-07-22', 0, 'y', 30),
(2342, '2014-07-20', 'E', 55, 0, 'E', '2014-07-22', 0, 'y', 30),
(2343, '2014-07-20', 'E', 52, 2, 'E', '2014-07-22', 0, 'y', 30),
(2344, '2014-07-20', 'E', 57, 0, 'E', '2014-07-22', 0, 'y', 30),
(2345, '2014-07-21', 'E', 26, 3, 'E', '2014-07-22', 0, 'y', 30),
(2346, '2014-07-21', 'E', 27, 1, 'E', '2014-07-22', 0, 'y', 30),
(2347, '2014-07-21', 'E', 28, 3, 'E', '2014-07-22', 0, 'y', 30),
(2348, '2014-07-21', 'E', 29, 4, 'E', '2014-07-22', 0, 'y', 30),
(2349, '2014-07-21', 'E', 30, 4, 'E', '2014-07-22', 0, 'y', 30),
(2350, '2014-07-21', 'E', 31, 4, 'E', '2014-07-22', 0, 'y', 30),
(2351, '2014-07-21', 'E', 33, 2, 'E', '2014-07-22', 0, 'y', 30),
(2352, '2014-07-21', 'E', 34, 0, 'E', '2014-07-22', 0, 'y', 30),
(2353, '2014-07-21', 'E', 35, 0, 'E', '2014-07-22', 0, 'y', 30),
(2354, '2014-07-21', 'E', 36, 2, 'E', '2014-07-22', 0, 'y', 30),
(2355, '2014-07-21', 'E', 37, 3, 'E', '2014-07-22', 0, 'y', 30),
(2356, '2014-07-21', 'E', 38, 3, 'E', '2014-07-22', 0, 'y', 30),
(2357, '2014-07-21', 'E', 114, 1, 'E', '2014-07-22', 0, 'y', 30),
(2358, '2014-07-21', 'E', 39, 2, 'E', '2014-07-22', 0, 'y', 30),
(2359, '2014-07-21', 'E', 115, 3, 'E', '2014-07-22', 0, 'y', 30),
(2360, '2014-07-21', 'E', 40, 0, 'E', '2014-07-22', 0, 'y', 30),
(2361, '2014-07-21', 'E', 41, 0, 'E', '2014-07-22', 0, 'y', 30),
(2362, '2014-07-21', 'E', 42, 0, 'E', '2014-07-22', 0, 'y', 30),
(2363, '2014-07-21', 'E', 43, 0, 'E', '2014-07-22', 0, 'y', 30),
(2364, '2014-07-21', 'E', 44, 0, 'E', '2014-07-22', 0, 'y', 30),
(2365, '2014-07-21', 'E', 58, 0, 'E', '2014-07-22', 0, 'y', 30),
(2366, '2014-07-21', 'E', 45, 1, 'E', '2014-07-22', 0, 'y', 30),
(2367, '2014-07-21', 'E', 59, 1, 'E', '2014-07-22', 0, 'y', 30),
(2368, '2014-07-21', 'E', 60, 0, 'E', '2014-07-22', 0, 'y', 30),
(2369, '2014-07-21', 'E', 61, 2, 'E', '2014-07-22', 0, 'y', 30),
(2370, '2014-07-21', 'E', 46, 0, 'E', '2014-07-22', 0, 'y', 30),
(2371, '2014-07-21', 'E', 47, 0, 'E', '2014-07-22', 0, 'y', 30),
(2372, '2014-07-21', 'E', 48, 1, 'E', '2014-07-22', 0, 'y', 30),
(2373, '2014-07-21', 'E', 49, 2, 'E', '2014-07-22', 0, 'y', 30),
(2374, '2014-07-21', 'E', 50, 2, 'E', '2014-07-22', 0, 'y', 30),
(2375, '2014-07-21', 'E', 116, 3, 'E', '2014-07-22', 0, 'y', 30),
(2376, '2014-07-21', 'E', 117, 3, 'E', '2014-07-22', 0, 'y', 30),
(2377, '2014-07-21', 'E', 51, 1, 'E', '2014-07-22', 0, 'y', 30),
(2378, '2014-07-21', 'E', 53, 2, 'E', '2014-07-22', 0, 'y', 30),
(2379, '2014-07-21', 'E', 62, 5, 'E', '2014-07-22', 0, 'y', 30),
(2380, '2014-07-21', 'E', 63, 3, 'E', '2014-07-22', 0, 'y', 30),
(2381, '2014-07-21', 'E', 64, 0, 'E', '2014-07-22', 0, 'y', 30),
(2382, '2014-07-21', 'E', 65, 0, 'E', '2014-07-22', 0, 'y', 30),
(2383, '2014-07-21', 'E', 54, 0, 'E', '2014-07-22', 0, 'y', 30),
(2384, '2014-07-21', 'E', 55, 2, 'E', '2014-07-22', 0, 'y', 30),
(2385, '2014-07-21', 'E', 52, 2, 'E', '2014-07-22', 0, 'y', 30),
(2386, '2014-07-21', 'E', 57, 0, 'E', '2014-07-22', 0, 'y', 30),
(2387, '2014-07-22', 'M', 66, 3, 'M', '2014-07-23', 0, 'y', 30),
(2388, '2014-07-22', 'M', 67, 3, 'M', '2014-07-23', 0, 'y', 30),
(2389, '2014-07-22', 'M', 68, 1, 'M', '2014-07-23', 0, 'y', 30),
(2390, '2014-07-22', 'M', 69, 2, 'M', '2014-07-23', 0, 'y', 30),
(2391, '2014-07-22', 'M', 24, 3, 'M', '2014-07-23', 0, 'y', 30),
(2392, '2014-07-22', 'M', 71, 5, 'M', '2014-07-23', 0, 'y', 30),
(2393, '2014-07-22', 'M', 73, 4, 'M', '2014-07-23', 0, 'y', 30),
(2394, '2014-07-22', 'M', 74, 0, 'M', '2014-07-23', 0, 'y', 30),
(2395, '2014-07-22', 'M', 75, 2, 'M', '2014-07-23', 0, 'y', 30);
INSERT INTO `milksell` (`milkSellId`, `milkSellDate`, `milkTime`, `customerId`, `milkGiven`, `customerMilkTime`, `nextDate`, `nextLitre`, `billCreated`, `rate`) VALUES
(2396, '2014-07-22', 'M', 76, 1, 'M', '2014-07-23', 0, 'y', 30),
(2397, '2014-07-22', 'M', 77, 0, 'M', '2014-07-23', 0, 'y', 30),
(2398, '2014-07-22', 'M', 78, 4, 'M', '2014-07-23', 0, 'y', 30),
(2399, '2014-07-22', 'M', 79, 1, 'M', '2014-07-23', 0, 'y', 30),
(2400, '2014-07-22', 'M', 80, 0, 'M', '2014-07-23', 0, 'y', 30),
(2401, '2014-07-22', 'M', 81, 2, 'M', '2014-07-23', 0, 'y', 30),
(2402, '2014-07-22', 'M', 82, 1, 'M', '2014-07-23', 0, 'y', 30),
(2403, '2014-07-22', 'M', 83, 0, 'M', '2014-07-23', 0, 'y', 30),
(2404, '2014-07-22', 'M', 84, 1, 'M', '2014-07-23', 0, 'y', 30),
(2405, '2014-07-22', 'M', 85, 0, 'M', '2014-07-23', 0, 'y', 30),
(2406, '2014-07-22', 'M', 86, 0, 'M', '2014-07-23', 0, 'y', 30),
(2407, '2014-07-22', 'M', 87, 0, 'M', '2014-07-23', 0, 'y', 30),
(2408, '2014-07-22', 'M', 88, 0, 'M', '2014-07-23', 0, 'y', 30),
(2409, '2014-07-22', 'M', 89, 0, 'M', '2014-07-23', 0, 'y', 30),
(2410, '2014-07-22', 'M', 90, 10, 'M', '2014-07-23', 0, 'y', 30),
(2411, '2014-07-22', 'M', 91, 0, 'M', '2014-07-23', 0, 'y', 30),
(2412, '2014-07-22', 'M', 92, 0, 'M', '2014-07-23', 0, 'y', 30),
(2413, '2014-07-22', 'M', 93, 0, 'M', '2014-07-23', 0, 'y', 30),
(2414, '2014-07-22', 'M', 94, 2, 'M', '2014-07-23', 0, 'y', 30),
(2415, '2014-07-22', 'M', 95, 1, 'M', '2014-07-23', 0, 'y', 30),
(2416, '2014-07-22', 'M', 96, 1, 'M', '2014-07-23', 0, 'y', 30),
(2417, '2014-07-22', 'M', 97, 0, 'M', '2014-07-23', 0, 'y', 30),
(2418, '2014-07-22', 'M', 98, 0, 'M', '2014-07-23', 0, 'y', 30),
(2419, '2014-07-22', 'M', 99, 0, 'M', '2014-07-23', 0, 'y', 30),
(2420, '2014-07-22', 'M', 100, 4, 'M', '2014-07-23', 0, 'y', 30),
(2421, '2014-07-22', 'M', 23, 0, 'M', '2014-07-23', 0, 'y', 30),
(2422, '2014-07-22', 'M', 72, 0, 'M', '2014-07-23', 0, 'y', 30),
(2423, '2014-07-22', 'M', 70, 0, 'M', '2014-07-23', 0, 'y', 30),
(2424, '2014-07-22', 'E', 26, 3, 'E', '2014-07-23', 0, 'y', 30),
(2425, '2014-07-22', 'E', 27, 2, 'E', '2014-07-23', 0, 'y', 30),
(2426, '2014-07-22', 'E', 28, 3, 'E', '2014-07-23', 0, 'y', 30),
(2427, '2014-07-22', 'E', 29, 4, 'E', '2014-07-23', 0, 'y', 30),
(2428, '2014-07-22', 'E', 30, 4, 'E', '2014-07-23', 0, 'y', 30),
(2429, '2014-07-22', 'E', 31, 4, 'E', '2014-07-23', 0, 'y', 30),
(2430, '2014-07-22', 'E', 33, 2, 'E', '2014-07-23', 0, 'y', 30),
(2431, '2014-07-22', 'E', 34, 0, 'E', '2014-07-23', 0, 'y', 30),
(2432, '2014-07-22', 'E', 35, 0, 'E', '2014-07-23', 0, 'y', 30),
(2433, '2014-07-22', 'E', 36, 2, 'E', '2014-07-23', 0, 'y', 30),
(2434, '2014-07-22', 'E', 37, 3, 'E', '2014-07-23', 0, 'y', 30),
(2435, '2014-07-22', 'E', 38, 3, 'E', '2014-07-23', 0, 'y', 30),
(2436, '2014-07-22', 'E', 114, 1, 'E', '2014-07-23', 0, 'y', 30),
(2437, '2014-07-22', 'E', 39, 2, 'E', '2014-07-23', 0, 'y', 30),
(2438, '2014-07-22', 'E', 115, 3, 'E', '2014-07-23', 0, 'y', 30),
(2439, '2014-07-22', 'E', 40, 0, 'E', '2014-07-23', 0, 'y', 30),
(2440, '2014-07-22', 'E', 41, 0, 'E', '2014-07-23', 0, 'y', 30),
(2441, '2014-07-22', 'E', 42, 0, 'E', '2014-07-23', 0, 'y', 30),
(2442, '2014-07-22', 'E', 43, 0, 'E', '2014-07-23', 0, 'y', 30),
(2443, '2014-07-22', 'E', 44, 0, 'E', '2014-07-23', 0, 'y', 30),
(2444, '2014-07-22', 'E', 58, 0, 'E', '2014-07-23', 0, 'y', 30),
(2445, '2014-07-22', 'E', 45, 1, 'E', '2014-07-23', 0, 'y', 30),
(2446, '2014-07-22', 'E', 59, 1, 'E', '2014-07-23', 0, 'y', 30),
(2447, '2014-07-22', 'E', 60, 2, 'E', '2014-07-23', 0, 'y', 30),
(2448, '2014-07-22', 'E', 61, 2, 'E', '2014-07-23', 0, 'y', 30),
(2449, '2014-07-22', 'E', 46, 0, 'E', '2014-07-23', 0, 'y', 30),
(2450, '2014-07-22', 'E', 47, 0, 'E', '2014-07-23', 0, 'y', 30),
(2451, '2014-07-22', 'E', 48, 1, 'E', '2014-07-23', 0, 'y', 30),
(2452, '2014-07-22', 'E', 49, 2, 'E', '2014-07-23', 0, 'y', 30),
(2453, '2014-07-22', 'E', 50, 0, 'E', '2014-07-23', 0, 'y', 30),
(2454, '2014-07-22', 'E', 116, 3, 'E', '2014-07-23', 0, 'y', 30),
(2455, '2014-07-22', 'E', 117, 0, 'E', '2014-07-23', 0, 'y', 30),
(2456, '2014-07-22', 'E', 51, 1, 'E', '2014-07-23', 0, 'y', 30),
(2457, '2014-07-22', 'E', 53, 2, 'E', '2014-07-23', 0, 'y', 30),
(2458, '2014-07-22', 'E', 62, 4, 'E', '2014-07-23', 0, 'y', 30),
(2459, '2014-07-22', 'E', 63, 3, 'E', '2014-07-23', 0, 'y', 30),
(2460, '2014-07-22', 'E', 64, 0, 'E', '2014-07-23', 0, 'y', 30),
(2461, '2014-07-22', 'E', 65, 0, 'E', '2014-07-23', 0, 'y', 30),
(2462, '2014-07-22', 'E', 54, 0, 'E', '2014-07-23', 0, 'y', 30),
(2463, '2014-07-22', 'E', 55, 0, 'E', '2014-07-23', 0, 'y', 30),
(2464, '2014-07-22', 'E', 52, 2, 'E', '2014-07-23', 0, 'y', 30),
(2465, '2014-07-22', 'E', 57, 0, 'E', '2014-07-23', 0, 'y', 30),
(2466, '2014-07-23', 'M', 66, 3, 'M', '2014-07-24', 0, 'y', 30),
(2467, '2014-07-23', 'M', 67, 3, 'M', '2014-07-24', 0, 'y', 30),
(2468, '2014-07-23', 'M', 68, 1, 'M', '2014-07-24', 0, 'y', 30),
(2469, '2014-07-23', 'M', 69, 2, 'M', '2014-07-24', 0, 'y', 30),
(2470, '2014-07-23', 'M', 24, 3, 'M', '2014-07-24', 0, 'y', 30),
(2471, '2014-07-23', 'M', 71, 4, 'M', '2014-07-24', 0, 'y', 30),
(2472, '2014-07-23', 'M', 73, 3, 'M', '2014-07-24', 0, 'y', 30),
(2473, '2014-07-23', 'M', 74, 0, 'M', '2014-07-24', 0, 'y', 30),
(2474, '2014-07-23', 'M', 75, 2, 'M', '2014-07-24', 0, 'y', 30),
(2475, '2014-07-23', 'M', 76, 1, 'M', '2014-07-24', 0, 'y', 30),
(2476, '2014-07-23', 'M', 77, 0, 'M', '2014-07-24', 0, 'y', 30),
(2477, '2014-07-23', 'M', 78, 3, 'M', '2014-07-24', 0, 'y', 30),
(2478, '2014-07-23', 'M', 79, 0, 'M', '2014-07-24', 0, 'y', 30),
(2479, '2014-07-23', 'M', 80, 0, 'M', '2014-07-24', 0, 'y', 30),
(2480, '2014-07-23', 'M', 81, 2, 'M', '2014-07-24', 0, 'y', 30),
(2481, '2014-07-23', 'M', 82, 1, 'M', '2014-07-24', 0, 'y', 30),
(2482, '2014-07-23', 'M', 83, 0, 'M', '2014-07-24', 0, 'y', 30),
(2483, '2014-07-23', 'M', 84, 1, 'M', '2014-07-24', 0, 'y', 30),
(2484, '2014-07-23', 'M', 85, 0, 'M', '2014-07-24', 0, 'y', 30),
(2485, '2014-07-23', 'M', 86, 0, 'M', '2014-07-24', 0, 'y', 30),
(2486, '2014-07-23', 'M', 87, 0, 'M', '2014-07-24', 0, 'y', 30),
(2487, '2014-07-23', 'M', 88, 0, 'M', '2014-07-24', 0, 'y', 30),
(2488, '2014-07-23', 'M', 89, 0, 'M', '2014-07-24', 0, 'y', 30),
(2489, '2014-07-23', 'M', 90, 10, 'M', '2014-07-24', 0, 'y', 30),
(2490, '2014-07-23', 'M', 91, 0, 'M', '2014-07-24', 0, 'y', 30),
(2491, '2014-07-23', 'M', 92, 0, 'M', '2014-07-24', 0, 'y', 30),
(2492, '2014-07-23', 'M', 93, 0, 'M', '2014-07-24', 0, 'y', 30),
(2493, '2014-07-23', 'M', 94, 2, 'M', '2014-07-24', 0, 'y', 30),
(2494, '2014-07-23', 'M', 95, 1, 'M', '2014-07-24', 0, 'y', 30),
(2495, '2014-07-23', 'M', 96, 0, 'M', '2014-07-24', 0, 'y', 30),
(2496, '2014-07-23', 'M', 97, 0, 'M', '2014-07-24', 0, 'y', 30),
(2497, '2014-07-23', 'M', 98, 0, 'M', '2014-07-24', 0, 'y', 30),
(2498, '2014-07-23', 'M', 99, 0, 'M', '2014-07-24', 0, 'y', 30),
(2499, '2014-07-23', 'M', 100, 4, 'M', '2014-07-24', 0, 'y', 30),
(2500, '2014-07-23', 'M', 23, 0, 'M', '2014-07-24', 0, 'y', 30),
(2501, '2014-07-23', 'M', 72, 0, 'M', '2014-07-24', 0, 'y', 30),
(2502, '2014-07-23', 'M', 70, 0, 'M', '2014-07-24', 0, 'y', 30),
(2503, '2014-07-23', 'E', 11, 0, 'E', '2014-07-25', 0, 'y', 30),
(2504, '2014-07-23', 'E', 12, 6, 'E', '2014-07-25', 0, 'y', 30),
(2505, '2014-07-23', 'E', 13, 6, 'E', '2014-07-25', 0, 'y', 30),
(2506, '2014-07-23', 'E', 14, 2, 'E', '2014-07-25', 0, 'y', 30),
(2507, '2014-07-23', 'E', 15, 1, 'E', '2014-07-25', 0, 'y', 30),
(2508, '2014-07-23', 'E', 16, 0, 'E', '2014-07-25', 0, 'y', 30),
(2509, '2014-07-23', 'E', 17, 2, 'E', '2014-07-25', 0, 'y', 30),
(2510, '2014-07-23', 'E', 26, 3, 'E', '2014-07-24', 0, 'y', 30),
(2511, '2014-07-23', 'E', 27, 1, 'E', '2014-07-24', 0, 'y', 30),
(2512, '2014-07-23', 'E', 28, 3, 'E', '2014-07-24', 0, 'y', 30),
(2513, '2014-07-23', 'E', 29, 4, 'E', '2014-07-24', 0, 'y', 30),
(2514, '2014-07-23', 'E', 30, 4, 'E', '2014-07-24', 0, 'y', 30),
(2515, '2014-07-23', 'E', 31, 4, 'E', '2014-07-24', 0, 'y', 30),
(2516, '2014-07-23', 'E', 33, 3, 'E', '2014-07-24', 0, 'y', 30),
(2517, '2014-07-23', 'E', 34, 0, 'E', '2014-07-24', 0, 'y', 30),
(2518, '2014-07-23', 'E', 35, 0, 'E', '2014-07-24', 0, 'y', 30),
(2519, '2014-07-23', 'E', 36, 2, 'E', '2014-07-24', 0, 'y', 30),
(2520, '2014-07-23', 'E', 37, 3, 'E', '2014-07-24', 0, 'y', 30),
(2521, '2014-07-23', 'E', 38, 3, 'E', '2014-07-24', 0, 'y', 30),
(2522, '2014-07-23', 'E', 114, 1, 'E', '2014-07-24', 0, 'y', 30),
(2523, '2014-07-23', 'E', 39, 2, 'E', '2014-07-24', 0, 'y', 30),
(2524, '2014-07-23', 'E', 115, 3, 'E', '2014-07-24', 0, 'y', 30),
(2525, '2014-07-23', 'E', 40, 0, 'E', '2014-07-24', 0, 'y', 30),
(2526, '2014-07-23', 'E', 41, 0, 'E', '2014-07-24', 0, 'y', 30),
(2527, '2014-07-23', 'E', 42, 0, 'E', '2014-07-24', 0, 'y', 30),
(2528, '2014-07-23', 'E', 43, 0, 'E', '2014-07-24', 0, 'y', 30),
(2529, '2014-07-23', 'E', 44, 0, 'E', '2014-07-24', 0, 'y', 30),
(2530, '2014-07-23', 'E', 58, 0, 'E', '2014-07-24', 0, 'y', 30),
(2531, '2014-07-23', 'E', 45, 1, 'E', '2014-07-24', 0, 'y', 30),
(2532, '2014-07-23', 'E', 59, 1, 'E', '2014-07-24', 0, 'y', 30),
(2533, '2014-07-23', 'E', 60, 2, 'E', '2014-07-24', 0, 'y', 30),
(2534, '2014-07-23', 'E', 61, 2, 'E', '2014-07-24', 0, 'y', 30),
(2535, '2014-07-23', 'E', 46, 0, 'E', '2014-07-24', 0, 'y', 30),
(2536, '2014-07-23', 'E', 47, 0, 'E', '2014-07-24', 0, 'y', 30),
(2537, '2014-07-23', 'E', 48, 1, 'E', '2014-07-24', 0, 'y', 30),
(2538, '2014-07-23', 'E', 49, 2, 'E', '2014-07-24', 0, 'y', 30),
(2539, '2014-07-23', 'E', 50, 2, 'E', '2014-07-24', 0, 'y', 30),
(2540, '2014-07-23', 'E', 116, 3, 'E', '2014-07-24', 0, 'y', 30),
(2541, '2014-07-23', 'E', 117, 2, 'E', '2014-07-24', 0, 'y', 30),
(2542, '2014-07-23', 'E', 51, 0, 'E', '2014-07-24', 0, 'y', 30),
(2543, '2014-07-23', 'E', 53, 2, 'E', '2014-07-24', 0, 'y', 30),
(2544, '2014-07-23', 'E', 62, 3, 'E', '2014-07-24', 0, 'y', 30),
(2545, '2014-07-23', 'E', 63, 0, 'E', '2014-07-24', 0, 'y', 30),
(2546, '2014-07-23', 'E', 64, 0, 'E', '2014-07-24', 0, 'y', 30),
(2547, '2014-07-23', 'E', 65, 0, 'E', '2014-07-24', 0, 'y', 30),
(2548, '2014-07-23', 'E', 54, 0, 'E', '2014-07-24', 0, 'y', 30),
(2549, '2014-07-23', 'E', 55, 2, 'E', '2014-07-24', 0, 'y', 30),
(2550, '2014-07-23', 'E', 52, 2, 'E', '2014-07-24', 0, 'y', 30),
(2551, '2014-07-23', 'E', 57, 0, 'E', '2014-07-24', 0, 'y', 30),
(2552, '2014-07-24', 'E', 11, 0, 'E', '2014-07-25', 0, 'y', 30),
(2553, '2014-07-24', 'E', 12, 6, 'E', '2014-07-25', 0, 'y', 30),
(2554, '2014-07-24', 'E', 13, 6, 'E', '2014-07-25', 0, 'y', 30),
(2555, '2014-07-24', 'E', 14, 2, 'E', '2014-07-25', 0, 'y', 30),
(2556, '2014-07-24', 'E', 15, 1, 'E', '2014-07-25', 0, 'y', 30),
(2557, '2014-07-24', 'E', 16, 0, 'E', '2014-07-25', 0, 'y', 30),
(2558, '2014-07-24', 'E', 17, 2, 'E', '2014-07-25', 0, 'y', 30),
(2559, '2014-07-24', 'M', 66, 3, 'M', '2014-07-25', 0, 'y', 30),
(2560, '2014-07-24', 'M', 67, 3, 'M', '2014-07-25', 0, 'y', 30),
(2561, '2014-07-24', 'M', 68, 1, 'M', '2014-07-25', 0, 'y', 30),
(2562, '2014-07-24', 'M', 69, 2, 'M', '2014-07-25', 0, 'y', 30),
(2563, '2014-07-24', 'M', 24, 3, 'M', '2014-07-25', 0, 'y', 30),
(2564, '2014-07-24', 'M', 71, 5, 'M', '2014-07-25', 0, 'y', 30),
(2565, '2014-07-24', 'M', 73, 4, 'M', '2014-07-25', 0, 'y', 30),
(2566, '2014-07-24', 'M', 74, 0, 'M', '2014-07-25', 0, 'y', 30),
(2567, '2014-07-24', 'M', 75, 2, 'M', '2014-07-25', 0, 'y', 30),
(2568, '2014-07-24', 'M', 76, 1, 'M', '2014-07-25', 0, 'y', 30),
(2569, '2014-07-24', 'M', 77, 1, 'M', '2014-07-25', 0, 'y', 30),
(2570, '2014-07-24', 'M', 78, 4, 'M', '2014-07-25', 0, 'y', 30),
(2571, '2014-07-24', 'M', 79, 1, 'M', '2014-07-25', 0, 'y', 30),
(2572, '2014-07-24', 'M', 80, 3, 'M', '2014-07-25', 0, 'y', 30),
(2573, '2014-07-24', 'M', 81, 3, 'M', '2014-07-25', 0, 'y', 30),
(2574, '2014-07-24', 'M', 82, 1, 'M', '2014-07-25', 0, 'y', 30),
(2575, '2014-07-24', 'M', 83, 0, 'M', '2014-07-25', 0, 'y', 30),
(2576, '2014-07-24', 'M', 84, 1, 'M', '2014-07-25', 0, 'y', 30),
(2577, '2014-07-24', 'M', 85, 0, 'M', '2014-07-25', 0, 'y', 30),
(2578, '2014-07-24', 'M', 86, 0, 'M', '2014-07-25', 0, 'y', 30),
(2579, '2014-07-24', 'M', 87, 0, 'M', '2014-07-25', 0, 'y', 30),
(2580, '2014-07-24', 'M', 88, 0, 'M', '2014-07-25', 0, 'y', 30),
(2581, '2014-07-24', 'M', 89, 0, 'M', '2014-07-25', 0, 'y', 30),
(2582, '2014-07-24', 'M', 90, 10, 'M', '2014-07-25', 0, 'y', 30),
(2583, '2014-07-24', 'M', 91, 0, 'M', '2014-07-25', 0, 'y', 30),
(2584, '2014-07-24', 'M', 92, 0, 'M', '2014-07-25', 0, 'y', 30),
(2585, '2014-07-24', 'M', 93, 0, 'M', '2014-07-25', 0, 'y', 30),
(2586, '2014-07-24', 'M', 94, 2, 'M', '2014-07-25', 0, 'y', 30),
(2587, '2014-07-24', 'M', 95, 1, 'M', '2014-07-25', 0, 'y', 30),
(2588, '2014-07-24', 'M', 96, 1, 'M', '2014-07-25', 0, 'y', 30),
(2589, '2014-07-24', 'M', 97, 3, 'M', '2014-07-25', 0, 'y', 30),
(2590, '2014-07-24', 'M', 98, 0, 'M', '2014-07-25', 0, 'y', 30),
(2591, '2014-07-24', 'M', 99, 0, 'M', '2014-07-25', 0, 'y', 30),
(2592, '2014-07-24', 'M', 100, 4, 'M', '2014-07-25', 0, 'y', 30),
(2593, '2014-07-24', 'M', 23, 0, 'M', '2014-07-25', 0, 'y', 30),
(2594, '2014-07-24', 'M', 72, 0, 'M', '2014-07-25', 0, 'y', 30),
(2595, '2014-07-24', 'M', 70, 0, 'M', '2014-07-25', 0, 'y', 30),
(2596, '2014-07-20', 'E', 11, 0, 'E', '2014-07-25', 0, 'y', 30),
(2597, '2014-07-20', 'E', 12, 6, 'E', '2014-07-25', 0, 'y', 30),
(2598, '2014-07-20', 'E', 13, 6, 'E', '2014-07-25', 0, 'y', 30),
(2599, '2014-07-20', 'E', 14, 2, 'E', '2014-07-25', 0, 'y', 30),
(2600, '2014-07-20', 'E', 15, 1, 'E', '2014-07-25', 0, 'y', 30),
(2601, '2014-07-20', 'E', 16, 1, 'E', '2014-07-25', 0, 'y', 30),
(2602, '2014-07-20', 'E', 17, 2, 'E', '2014-07-25', 0, 'y', 30),
(2603, '2014-07-21', 'E', 11, 0, 'E', '2014-07-25', 0, 'y', 30),
(2604, '2014-07-21', 'E', 12, 6, 'E', '2014-07-25', 0, 'y', 30),
(2605, '2014-07-21', 'E', 13, 6, 'E', '2014-07-25', 0, 'y', 30),
(2606, '2014-07-21', 'E', 14, 2, 'E', '2014-07-25', 0, 'y', 30),
(2607, '2014-07-21', 'E', 15, 1, 'E', '2014-07-25', 0, 'y', 30),
(2608, '2014-07-21', 'E', 16, 1, 'E', '2014-07-25', 0, 'y', 30),
(2609, '2014-07-21', 'E', 17, 2, 'E', '2014-07-25', 0, 'y', 30),
(2610, '2014-07-22', 'E', 11, 0, 'E', '2014-07-25', 0, 'y', 30),
(2611, '2014-07-22', 'E', 12, 6, 'E', '2014-07-25', 0, 'y', 30),
(2612, '2014-07-22', 'E', 13, 6, 'E', '2014-07-25', 0, 'y', 30),
(2613, '2014-07-22', 'E', 14, 2, 'E', '2014-07-25', 0, 'y', 30),
(2614, '2014-07-22', 'E', 15, 1, 'E', '2014-07-25', 0, 'y', 30),
(2615, '2014-07-22', 'E', 16, 0, 'E', '2014-07-25', 0, 'y', 30),
(2616, '2014-07-22', 'E', 17, 2, 'E', '2014-07-25', 0, 'y', 30),
(2617, '2014-07-28', 'E', 11, 0, 'E', '2014-07-29', 0, 'y', 30),
(2618, '2014-07-28', 'E', 12, 6, 'E', '2014-07-29', 0, 'y', 30),
(2619, '2014-07-28', 'E', 13, 6, 'E', '2014-07-29', 0, 'y', 30),
(2620, '2014-07-28', 'E', 14, 2, 'E', '2014-07-29', 0, 'y', 30),
(2621, '2014-07-28', 'E', 15, 1, 'E', '2014-07-29', 0, 'y', 30),
(2622, '2014-07-28', 'E', 16, 0, 'E', '2014-07-29', 0, 'y', 30),
(2623, '2014-07-28', 'E', 17, 2, 'E', '2014-07-29', 0, 'y', 30),
(2624, '2014-07-27', 'E', 11, 0, 'E', '2014-07-29', 0, 'y', 30),
(2625, '2014-07-27', 'E', 12, 6, 'E', '2014-07-29', 0, 'y', 30),
(2626, '2014-07-27', 'E', 13, 6, 'E', '2014-07-29', 0, 'y', 30),
(2627, '2014-07-27', 'E', 14, 2, 'E', '2014-07-29', 0, 'y', 30),
(2628, '2014-07-27', 'E', 15, 1, 'E', '2014-07-29', 0, 'y', 30),
(2629, '2014-07-27', 'E', 16, 0, 'E', '2014-07-29', 0, 'y', 30),
(2630, '2014-07-27', 'E', 17, 2, 'E', '2014-07-29', 0, 'y', 30),
(2631, '2014-07-25', 'E', 11, 0, 'E', '2014-07-29', 0, 'y', 30),
(2632, '2014-07-25', 'E', 12, 6, 'E', '2014-07-29', 0, 'y', 30),
(2633, '2014-07-25', 'E', 13, 6, 'E', '2014-07-29', 0, 'y', 30),
(2634, '2014-07-25', 'E', 14, 2, 'E', '2014-07-29', 0, 'y', 30),
(2635, '2014-07-25', 'E', 15, 1, 'E', '2014-07-29', 0, 'y', 30),
(2636, '2014-07-25', 'E', 16, 0, 'E', '2014-07-29', 0, 'y', 30),
(2637, '2014-07-25', 'E', 17, 2, 'E', '2014-07-29', 0, 'y', 30),
(2638, '2014-07-26', 'E', 11, 0, 'E', '2014-07-29', 0, 'y', 30),
(2639, '2014-07-26', 'E', 12, 6, 'E', '2014-07-29', 0, 'y', 30),
(2640, '2014-07-26', 'E', 13, 6, 'E', '2014-07-29', 0, 'y', 30),
(2641, '2014-07-26', 'E', 14, 2, 'E', '2014-07-29', 0, 'y', 30),
(2642, '2014-07-26', 'E', 15, 1, 'E', '2014-07-29', 0, 'y', 30),
(2643, '2014-07-26', 'E', 16, 0, 'E', '2014-07-29', 0, 'y', 30),
(2644, '2014-07-26', 'E', 17, 2, 'E', '2014-07-29', 0, 'y', 30),
(2645, '2014-07-28', 'M', 101, 0, 'M', '2014-07-29', 0, 'y', 30),
(2646, '2014-07-28', 'M', 22, 0, 'M', '2014-07-29', 0, 'y', 30),
(2647, '2014-07-28', 'M', 105, 0, 'M', '2014-07-29', 0, 'y', 30),
(2648, '2014-07-28', 'M', 106, 0, 'M', '2014-07-29', 0, 'y', 30),
(2649, '2014-07-28', 'M', 107, 0, 'M', '2014-07-29', 0, 'y', 30),
(2650, '2014-07-28', 'M', 108, 0, 'M', '2014-07-29', 0, 'y', 30),
(2651, '2014-07-28', 'M', 109, 0, 'M', '2014-07-29', 0, 'y', 30),
(2652, '2014-07-28', 'M', 110, 0, 'M', '2014-07-29', 0, 'y', 30),
(2653, '2014-07-28', 'M', 111, 0, 'M', '2014-07-29', 0, 'y', 30),
(2654, '2014-07-28', 'M', 112, 0, 'M', '2014-07-29', 0, 'y', 30),
(2655, '2014-07-28', 'M', 113, 0, 'M', '2014-07-29', 0, 'y', 30);

-- --------------------------------------------------------

--
-- Table structure for table `mothermaster`
--

CREATE TABLE IF NOT EXISTS `mothermaster` (
`motherMasterId` int(11) NOT NULL,
  `motherName` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `place`
--

CREATE TABLE IF NOT EXISTS `place` (
`placeId` int(11) NOT NULL,
  `place` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `place`
--

INSERT INTO `place` (`placeId`, `place`) VALUES
(1, 'Office'),
(2, 'Gaushala');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
`productId` int(11) NOT NULL,
  `productName` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE IF NOT EXISTS `register` (
`registerId` int(11) NOT NULL,
  `userTypeId` int(11) DEFAULT NULL,
  `fName` varchar(50) DEFAULT NULL,
  `mName` varchar(50) DEFAULT NULL,
  `lName` varchar(50) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `emailId` varchar(100) DEFAULT NULL,
  `mobileNo` varchar(20) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`registerId`, `userTypeId`, `fName`, `mName`, `lName`, `city`, `emailId`, `mobileNo`) VALUES
(1, NULL, 'a', 'b', 'b', '', '', ''),
(2, NULL, '1', '11', '111', '1', '1', '1'),
(6, 1, '1', '1', '1', '1', '1', '1'),
(7, 3, '1', '1', '1', '1', '1', '1'),
(8, 3, '10', '2', '2', '2', '2', '2'),
(9, 0, '', '', 'a', 'a', '', ''),
(10, 0, '', '', 'a', 'a', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
`staffId` int(11) NOT NULL,
  `staffTypeId` int(11) DEFAULT NULL,
  `placeId` int(11) DEFAULT NULL,
  `userName` varchar(50) DEFAULT NULL,
  `pass` varchar(50) DEFAULT NULL,
  `parentStaffId` int(11) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `phone` varchar(255) NOT NULL,
  `joinDate` date DEFAULT NULL,
  `commission` double DEFAULT NULL,
  `salary` double DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staffId`, `staffTypeId`, `placeId`, `userName`, `pass`, `parentStaffId`, `name`, `phone`, `joinDate`, `commission`, `salary`) VALUES
(1, 0, 1, 'om', 'om', 0, 'om', '', '2014-07-01', NULL, NULL),
(10, 1, 1, 'Lalo', 'lalo', 9, 'Lalo', '', '2014-01-01', 5, 0),
(11, 1, 1, 'Parag Vaghasiya', 'parag', 9, 'parag', '', '2014-01-01', 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `stafftype`
--

CREATE TABLE IF NOT EXISTS `stafftype` (
`staffTypeId` int(11) NOT NULL,
  `staffType` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `stafftype`
--

INSERT INTO `stafftype` (`staffTypeId`, `staffType`) VALUES
(1, 'Distributor'),
(2, 'Manager'),
(3, 'Govaal');

-- --------------------------------------------------------

--
-- Table structure for table `subpro`
--

CREATE TABLE IF NOT EXISTS `subpro` (
`subproId` int(11) NOT NULL,
  `productId` int(11) DEFAULT NULL,
  `subproductName` varchar(500) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `subpro`
--

INSERT INTO `subpro` (`subproId`, `productId`, `subproductName`) VALUES
(4, 1, '12'),
(5, 1, '12');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
`transactionId` int(11) NOT NULL,
  `transType` int(11) DEFAULT NULL,
  `customerIdCr` int(11) DEFAULT NULL,
  `staffIdCr` int(11) DEFAULT NULL,
  `accountIdCr` int(11) DEFAULT NULL,
  `customerIdDr` int(11) DEFAULT NULL,
  `staffIdDr` int(11) DEFAULT NULL,
  `accountIdDr` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `transDate` date DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `totalQty` double DEFAULT NULL,
  `sellItemId` double DEFAULT NULL,
  `cron_bill_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=138 ;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`transactionId`, `transType`, `customerIdCr`, `staffIdCr`, `accountIdCr`, `customerIdDr`, `staffIdDr`, `accountIdDr`, `amount`, `transDate`, `notes`, `rate`, `totalQty`, `sellItemId`, `cron_bill_id`) VALUES
(1, 0, NULL, NULL, 5, 11, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(2, 0, NULL, NULL, 5, 12, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 5, NULL, 0),
(3, 0, NULL, NULL, 5, 13, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 6, NULL, 0),
(4, 0, NULL, NULL, 5, 14, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 2, NULL, 0),
(5, 0, NULL, NULL, 5, 15, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 1, NULL, 0),
(6, 0, NULL, NULL, 5, 16, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 1, NULL, 0),
(7, 0, NULL, NULL, 5, 17, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 2, NULL, 0),
(8, 0, NULL, NULL, 5, 23, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(9, 0, NULL, NULL, 5, 24, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(10, 0, NULL, NULL, 5, 36, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(11, 0, NULL, NULL, 5, 66, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(12, 0, NULL, NULL, 5, 67, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(13, 0, NULL, NULL, 5, 68, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(14, 0, NULL, NULL, 5, 69, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(15, 0, NULL, NULL, 5, 70, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(16, 0, NULL, NULL, 5, 71, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(17, 0, NULL, NULL, 5, 72, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(18, 0, NULL, NULL, 5, 73, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(19, 0, NULL, NULL, 5, 74, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(20, 0, NULL, NULL, 5, 75, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(21, 0, NULL, NULL, 5, 76, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(22, 0, NULL, NULL, 5, 77, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(23, 0, NULL, NULL, 5, 78, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(24, 0, NULL, NULL, 5, 79, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(25, 0, NULL, NULL, 5, 80, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(26, 0, NULL, NULL, 5, 81, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(27, 0, NULL, NULL, 5, 82, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(28, 0, NULL, NULL, 5, 83, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(29, 0, NULL, NULL, 5, 84, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(30, 0, NULL, NULL, 5, 85, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(31, 0, NULL, NULL, 5, 86, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(32, 0, NULL, NULL, 5, 87, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(33, 0, NULL, NULL, 5, 88, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(34, 0, NULL, NULL, 5, 89, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(35, 0, NULL, NULL, 5, 90, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(36, 0, NULL, NULL, 5, 91, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(37, 0, NULL, NULL, 5, 92, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(38, 0, NULL, NULL, 5, 93, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(39, 0, NULL, NULL, 5, 94, NULL, NULL, 0, '2014-07-15', 'Auto bill created', 0, 0, NULL, 0),
(40, 0, NULL, NULL, 5, 11, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 7, NULL, 0),
(41, 0, NULL, NULL, 5, 12, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 188, NULL, 0),
(42, 0, NULL, NULL, 5, 13, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 192, NULL, 0),
(43, 0, NULL, NULL, 5, 14, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 70, NULL, 0),
(44, 0, NULL, NULL, 5, 15, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 35, NULL, 0),
(45, 0, NULL, NULL, 5, 16, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 28, NULL, 0),
(46, 0, NULL, NULL, 5, 17, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 70, NULL, 0),
(47, 0, NULL, NULL, 5, 22, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 41, NULL, 0),
(48, 0, NULL, NULL, 5, 23, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 0, NULL, 0),
(49, 0, NULL, NULL, 5, 24, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 84, NULL, 0),
(50, 0, NULL, NULL, 5, 26, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 70, NULL, 0),
(51, 0, NULL, NULL, 5, 27, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 34, NULL, 0),
(52, 0, NULL, NULL, 5, 28, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 64, NULL, 0),
(53, 0, NULL, NULL, 5, 29, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 91, NULL, 0),
(54, 0, NULL, NULL, 5, 30, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 80, NULL, 0),
(55, 0, NULL, NULL, 5, 31, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 82, NULL, 0),
(56, 0, NULL, NULL, 5, 33, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 41, NULL, 0),
(57, 0, NULL, NULL, 5, 34, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 43, NULL, 0),
(58, 0, NULL, NULL, 5, 35, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 29, NULL, 0),
(59, 0, NULL, NULL, 5, 36, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 44, NULL, 0),
(60, 0, NULL, NULL, 5, 37, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 64, NULL, 0),
(61, 0, NULL, NULL, 5, 38, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 63, NULL, 0),
(62, 0, NULL, NULL, 5, 39, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 39, NULL, 0),
(63, 0, NULL, NULL, 5, 40, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 11, NULL, 0),
(64, 0, NULL, NULL, 5, 41, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 6, NULL, 0),
(65, 0, NULL, NULL, 5, 42, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 51, NULL, 0),
(66, 0, NULL, NULL, 5, 43, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 12, NULL, 0),
(67, 0, NULL, NULL, 5, 44, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 16, NULL, 0),
(68, 0, NULL, NULL, 5, 45, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 21, NULL, 0),
(69, 0, NULL, NULL, 5, 46, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 30, NULL, 0),
(70, 0, NULL, NULL, 5, 47, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 0, NULL, 0),
(71, 0, NULL, NULL, 5, 48, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 29, NULL, 0),
(72, 0, NULL, NULL, 5, 49, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 42, NULL, 0),
(73, 0, NULL, NULL, 5, 50, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 46, NULL, 0),
(74, 0, NULL, NULL, 5, 51, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 20, NULL, 0),
(75, 0, NULL, NULL, 5, 52, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 43, NULL, 0),
(76, 0, NULL, NULL, 5, 53, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 46, NULL, 0),
(77, 0, NULL, NULL, 5, 54, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 34, NULL, 0),
(78, 0, NULL, NULL, 5, 55, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 22, NULL, 0),
(79, 0, NULL, NULL, 5, 57, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 0, NULL, 0),
(80, 0, NULL, NULL, 5, 58, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 4, NULL, 0),
(81, 0, NULL, NULL, 5, 59, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 21, NULL, 0),
(82, 0, NULL, NULL, 5, 60, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 42, NULL, 0),
(83, 0, NULL, NULL, 5, 61, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 44, NULL, 0),
(84, 0, NULL, NULL, 5, 62, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 94, NULL, 0),
(85, 0, NULL, NULL, 5, 63, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 82, NULL, 0),
(86, 0, NULL, NULL, 5, 64, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 45, NULL, 0),
(87, 0, NULL, NULL, 5, 65, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 43, NULL, 0),
(88, 0, NULL, NULL, 5, 66, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 72, NULL, 0),
(89, 0, NULL, NULL, 5, 67, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 79, NULL, 0),
(90, 0, NULL, NULL, 5, 68, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 28, NULL, 0),
(91, 0, NULL, NULL, 5, 69, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 56, NULL, 0),
(92, 0, NULL, NULL, 5, 70, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 0, NULL, 0),
(93, 0, NULL, NULL, 5, 71, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 137, NULL, 0),
(94, 0, NULL, NULL, 5, 72, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 0, NULL, 0),
(95, 0, NULL, NULL, 5, 73, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 105, NULL, 0),
(96, 0, NULL, NULL, 5, 74, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 12, NULL, 0),
(97, 0, NULL, NULL, 5, 75, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 54, NULL, 0),
(98, 0, NULL, NULL, 5, 76, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 28, NULL, 0),
(99, 0, NULL, NULL, 5, 77, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 51, NULL, 0),
(100, 0, NULL, NULL, 5, 78, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 108, NULL, 0),
(101, 0, NULL, NULL, 5, 79, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 37, NULL, 0),
(102, 0, NULL, NULL, 5, 80, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 70, NULL, 0),
(103, 0, NULL, NULL, 5, 81, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 75, NULL, 0),
(104, 0, NULL, NULL, 5, 82, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 25, NULL, 0),
(105, 0, NULL, NULL, 5, 83, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 29, NULL, 0),
(106, 0, NULL, NULL, 5, 84, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 27, NULL, 0),
(107, 0, NULL, NULL, 5, 85, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 47, NULL, 0),
(108, 0, NULL, NULL, 5, 86, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 20, NULL, 0),
(109, 0, NULL, NULL, 5, 87, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 49, NULL, 0),
(110, 0, NULL, NULL, 5, 88, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 0, NULL, 0),
(111, 0, NULL, NULL, 5, 89, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 11, NULL, 0),
(112, 0, NULL, NULL, 5, 90, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 246, NULL, 0),
(113, 0, NULL, NULL, 5, 91, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 34, NULL, 0),
(114, 0, NULL, NULL, 5, 92, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 38, NULL, 0),
(115, 0, NULL, NULL, 5, 93, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 10, NULL, 0),
(116, 0, NULL, NULL, 5, 94, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 55, NULL, 0),
(117, 0, NULL, NULL, 5, 95, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 27, NULL, 0),
(118, 0, NULL, NULL, 5, 96, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 25, NULL, 0),
(119, 0, NULL, NULL, 5, 97, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 78, NULL, 0),
(120, 0, NULL, NULL, 5, 98, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 64, NULL, 0),
(121, 0, NULL, NULL, 5, 99, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 32, NULL, 0),
(122, 0, NULL, NULL, 5, 100, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 109, NULL, 0),
(123, 0, NULL, NULL, 5, 101, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 42, NULL, 0),
(124, 0, NULL, NULL, 5, 105, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 40, NULL, 0),
(125, 0, NULL, NULL, 5, 106, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 40, NULL, 0),
(126, 0, NULL, NULL, 5, 107, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 24, NULL, 0),
(127, 0, NULL, NULL, 5, 108, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 39, NULL, 0),
(128, 0, NULL, NULL, 5, 109, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 74, NULL, 0),
(129, 0, NULL, NULL, 5, 110, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 20, NULL, 0),
(130, 0, NULL, NULL, 5, 111, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 18, NULL, 0),
(131, 0, NULL, NULL, 5, 112, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 54, NULL, 0),
(132, 0, NULL, NULL, 5, 113, NULL, NULL, 0, '2014-08-01', 'Auto bill created', 0, 104, NULL, 0),
(133, 0, NULL, NULL, 5, 114, NULL, NULL, 1500, '2014-08-01', 'Auto bill created', 60, 25, NULL, 0),
(134, 0, NULL, NULL, 5, 115, NULL, NULL, 3660, '2014-08-01', 'Auto bill created', 60, 61, NULL, 0),
(135, 0, NULL, NULL, 5, 116, NULL, NULL, 3180, '2014-08-01', 'Auto bill created', 60, 53, NULL, 0),
(136, 0, NULL, NULL, 5, 117, NULL, NULL, 2100, '2014-08-01', 'Auto bill created', 60, 35, NULL, 0),
(137, NULL, NULL, 10, NULL, NULL, 1, NULL, 1000, '2014-08-22', 'sddd', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE IF NOT EXISTS `unit` (
`unitId` int(11) NOT NULL,
  `unit` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`unitId`, `unit`) VALUES
(1, 'Litre'),
(2, 'Grams'),
(3, 'Kg'),
(4, 'Pieces');

-- --------------------------------------------------------

--
-- Table structure for table `usertype`
--

CREATE TABLE IF NOT EXISTS `usertype` (
`userTypeId` int(11) NOT NULL,
  `userType` varchar(50) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `usertype`
--

INSERT INTO `usertype` (`userTypeId`, `userType`) VALUES
(1, 'Admin'),
(2, 'User 1'),
(3, 'User 2');

-- --------------------------------------------------------

--
-- Table structure for table `vaccine`
--

CREATE TABLE IF NOT EXISTS `vaccine` (
`vaccineId` int(11) NOT NULL,
  `vaccineTypeId` int(11) DEFAULT NULL,
  `cowId` int(11) DEFAULT NULL,
  `reminderDate` date DEFAULT NULL,
  `vaccineDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vaccinetype`
--

CREATE TABLE IF NOT EXISTS `vaccinetype` (
`vaccineTypeId` int(11) NOT NULL,
  `vaccineName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
 ADD PRIMARY KEY (`accountId`);

--
-- Indexes for table `accounttype`
--
ALTER TABLE `accounttype`
 ADD PRIMARY KEY (`accountTypeId`);

--
-- Indexes for table `cow`
--
ALTER TABLE `cow`
 ADD PRIMARY KEY (`cowId`);

--
-- Indexes for table `cowtype`
--
ALTER TABLE `cowtype`
 ADD PRIMARY KEY (`cowTypeId`);

--
-- Indexes for table `cron_bill`
--
ALTER TABLE `cron_bill`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
 ADD PRIMARY KEY (`customerId`);

--
-- Indexes for table `customertype`
--
ALTER TABLE `customertype`
 ADD PRIMARY KEY (`customerTypeId`);

--
-- Indexes for table `fariya`
--
ALTER TABLE `fariya`
 ADD PRIMARY KEY (`fariyaId`);

--
-- Indexes for table `fathermaster`
--
ALTER TABLE `fathermaster`
 ADD PRIMARY KEY (`fatherMaster`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
 ADD PRIMARY KEY (`itemId`);

--
-- Indexes for table `itemcreate`
--
ALTER TABLE `itemcreate`
 ADD PRIMARY KEY (`itemCreateId`);

--
-- Indexes for table `itemdispatch`
--
ALTER TABLE `itemdispatch`
 ADD PRIMARY KEY (`itemDispatchId`);

--
-- Indexes for table `itemexpense`
--
ALTER TABLE `itemexpense`
 ADD PRIMARY KEY (`itemExpenseId`);

--
-- Indexes for table `itemsell`
--
ALTER TABLE `itemsell`
 ADD PRIMARY KEY (`itemSellId`);

--
-- Indexes for table `licence`
--
ALTER TABLE `licence`
 ADD PRIMARY KEY (`licenceId`);

--
-- Indexes for table `meeting`
--
ALTER TABLE `meeting`
 ADD PRIMARY KEY (`meetingId`);

--
-- Indexes for table `milkbycow`
--
ALTER TABLE `milkbycow`
 ADD PRIMARY KEY (`milkByCowId`);

--
-- Indexes for table `milksell`
--
ALTER TABLE `milksell`
 ADD PRIMARY KEY (`milkSellId`);

--
-- Indexes for table `mothermaster`
--
ALTER TABLE `mothermaster`
 ADD PRIMARY KEY (`motherMasterId`);

--
-- Indexes for table `place`
--
ALTER TABLE `place`
 ADD PRIMARY KEY (`placeId`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
 ADD PRIMARY KEY (`productId`);

--
-- Indexes for table `register`
--
ALTER TABLE `register`
 ADD PRIMARY KEY (`registerId`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
 ADD PRIMARY KEY (`staffId`);

--
-- Indexes for table `stafftype`
--
ALTER TABLE `stafftype`
 ADD PRIMARY KEY (`staffTypeId`);

--
-- Indexes for table `subpro`
--
ALTER TABLE `subpro`
 ADD PRIMARY KEY (`subproId`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
 ADD PRIMARY KEY (`transactionId`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
 ADD PRIMARY KEY (`unitId`);

--
-- Indexes for table `usertype`
--
ALTER TABLE `usertype`
 ADD PRIMARY KEY (`userTypeId`);

--
-- Indexes for table `vaccine`
--
ALTER TABLE `vaccine`
 ADD PRIMARY KEY (`vaccineId`);

--
-- Indexes for table `vaccinetype`
--
ALTER TABLE `vaccinetype`
 ADD PRIMARY KEY (`vaccineTypeId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
MODIFY `accountId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `accounttype`
--
ALTER TABLE `accounttype`
MODIFY `accountTypeId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cow`
--
ALTER TABLE `cow`
MODIFY `cowId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cowtype`
--
ALTER TABLE `cowtype`
MODIFY `cowTypeId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cron_bill`
--
ALTER TABLE `cron_bill`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
MODIFY `customerId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=118;
--
-- AUTO_INCREMENT for table `customertype`
--
ALTER TABLE `customertype`
MODIFY `customerTypeId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `fariya`
--
ALTER TABLE `fariya`
MODIFY `fariyaId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fathermaster`
--
ALTER TABLE `fathermaster`
MODIFY `fatherMaster` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
MODIFY `itemId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `itemcreate`
--
ALTER TABLE `itemcreate`
MODIFY `itemCreateId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `itemdispatch`
--
ALTER TABLE `itemdispatch`
MODIFY `itemDispatchId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `itemexpense`
--
ALTER TABLE `itemexpense`
MODIFY `itemExpenseId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `itemsell`
--
ALTER TABLE `itemsell`
MODIFY `itemSellId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `licence`
--
ALTER TABLE `licence`
MODIFY `licenceId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `meeting`
--
ALTER TABLE `meeting`
MODIFY `meetingId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `milkbycow`
--
ALTER TABLE `milkbycow`
MODIFY `milkByCowId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `milksell`
--
ALTER TABLE `milksell`
MODIFY `milkSellId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2656;
--
-- AUTO_INCREMENT for table `mothermaster`
--
ALTER TABLE `mothermaster`
MODIFY `motherMasterId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `place`
--
ALTER TABLE `place`
MODIFY `placeId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
MODIFY `productId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
MODIFY `registerId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
MODIFY `staffId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `stafftype`
--
ALTER TABLE `stafftype`
MODIFY `staffTypeId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `subpro`
--
ALTER TABLE `subpro`
MODIFY `subproId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
MODIFY `transactionId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
MODIFY `unitId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `usertype`
--
ALTER TABLE `usertype`
MODIFY `userTypeId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vaccine`
--
ALTER TABLE `vaccine`
MODIFY `vaccineId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vaccinetype`
--
ALTER TABLE `vaccinetype`
MODIFY `vaccineTypeId` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
