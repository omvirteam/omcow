<?php
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
	$milkTimeSelected = isset($_REQUEST['milkTime']) ? $_REQUEST['milkTime'] : 'M';
  if(isset($_POST['submitBtn']) && $_POST['cowId'] > 0)
  {
    $cowId     = isset($_POST['cowId']) ? $_POST['cowId'] : 0;
    $milkTime  = isset($_POST['milkTime']) ? $_POST['milkTime'] : 'M';
    $litre     = isset($_POST['litre']) && strlen($_POST['litre']) > 0 ? $_POST['litre'] : 0;
    $staffId   = isset($_POST['staffId']) ? $_POST['staffId'] : 0;
    $notes     = isset($_POST['notes']) ? $_POST['notes'] : "";
    $milkDate        = $_POST['milkDateYear']."-".$_POST['milkDateMonth']."-".$_POST['milkDateDay'];
  	
    $insertMilk  = "INSERT INTO milkbycow (cowId,milkDate,milkTime,litre,staffId,notes)
                    VALUES(".$cowId.",'".$milkDate."','".$milkTime."',".$litre.",".$staffId.",
                           '".$notes."')";
    $insertMilkRes = mysql_query($insertMilk);
    if(!$insertMilkRes)
    {
    	echo "Error in Inserting ".mysql_error();
    }
    else
    {
      header("Location:milkByCow.php?milkTime=".$milkTime);
    }
  }
  
  $i=0;
  $cowArray = array();
  $selCow = "SELECT cowId,name,grn
               FROM cow 
              WHERE cowtypeId = 1
           ORDER BY name";
  $selCowRes = mysql_query($selCow);
  while($cowRow = mysql_fetch_array($selCowRes))
  {
  	$cowArray['cowId'][$i] = $cowRow['cowId'];
  	$cowArray['name'][$i]  = $cowRow['name'] .":". $cowRow['grn'];
  	$i++;
  }

  $j=0;
  $staffArray = array();
  $selStaff = "SELECT staffId,name
                 FROM staff
                WHERE staffTypeId = 3
             ORDER BY name";
  $selStaffRes = mysql_query($selStaff);
  while($staffRow = mysql_fetch_array($selStaffRes))
  {
  	$staffArray['staffId'][$j]  = $staffRow['staffId'];
  	$staffArray['name'][$j]     = $staffRow['name'];
  	$j++;
  }
}
include("bottom.php");
$smarty->assign("cowArray",$cowArray);
$smarty->assign("staffArray",$staffArray);
$smarty->assign("milkTimeSelected",$milkTimeSelected);
$smarty->display("milkByCow.tpl");
?>