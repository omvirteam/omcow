<?php
/*
 *	Generate Bill search options are Distributor wise, customer wise, month and year.
 */   
include("include/conn.inc");
include("include/function.php");
session_start();	
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Om Cow</title>
    <!-- Core CSS - Include with every page -->
    <link type="text/css" href="./css/style.css" rel="stylesheet">
    <link type="text/css" href="./css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="./css/bootstrapValidator.min.css" rel="stylesheet">
    <link type="text/css" href="./font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link type="text/css" href="./css/sb-admin.css" rel="stylesheet">
    
	<!-- Core Scripts - Include with every page -->
    <script type="text/javascript" src="./js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js/bootstrapValidator.min.js"></script>
    <script type="text/javascript" src="./js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <!-- SB Admin Scripts - Include with every page -->
    <script type="text/javascript" src="./js/sb-admin.js"></script>

	<!-- Page-Level Plugin CSS - Tables -->
	<link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

	<!-- Page-Level Plugin Scripts - Tables -->
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$('#staffList').dataTable({
		"aoColumnDefs": [{ 'bSortable': false, 'aTargets': [ 4] }]
		//"aaSorting": [[ 1, "asc" ]]
		});
	});
	</script>
	<script type="text/javascript">
		function submitForm()
		{
			var currentIndex = $("#cmbDistributor option:selected").text();
			if(currentIndex === "All")
			{
				alert("You cannot show all the data because you are not admin");
			}
			else
			{
				form.submit();
			}
			
		}
	</script>
</head>
<body>
	
    <div id="wrapper">
	<?php include("include/header.php"); ?>
	
	<div id="page-wrapper">

	<!-- Display Records-->
	<div class="row">
		<div class="col-lg-10 col-md-offset-1">
		<div class="panel panel-primary">
			<div class="panel-heading">
			<i class="fa fa-list-ul fa-fw"></i> All Distributors
			</div>
			<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="staffList">
				<thead>
					<tr>
					<th align="center">Name</th>
					<th align="center">Join Date</th>
					<th align="center">Phone Number</th>
					<th align="center">User Name</th>
					<th align="center">Action</th> 
					</tr>
				</thead>
				<tbody>				
					<?php 
						$query = "select * from staff";
						$sql = mysql_query($query);
						while($row = mysql_fetch_array($sql))
						{
					?>
					<tr>
						<td><?php echo $row['name'];?></td>
						<td><?php echo $row['joinDate'];?></td>
						<td><?php echo $row['phone'];?></td>
						<td><?php echo $row['userName'];?></td>
						<td><a href="changePassword.php?id=<?php echo $row['staffId'];?>" target="_black">Change Password</a></td>
					</tr>
					<?php
						}
						?>
				</tbody>
				</table>
			</div>
			</div>
		</div>
		</div>
	</div>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
</body>
</html>