$(document).ready(function(){

$('#changeSubBtn').on('click',function(){

var oldpass = $("#oldPassword").val();
var newpass = $("#newPassword").val();
var confirmpass = $("#confirmNewPassword").val();

if(newpass != confirmpass)
{
	alert("Confirm password should be matched with New Password");
	$("#confirmNewPassword").focus();
}
else
{
	var pass= [];
	pass[0]= $("#oldPassword").val();
	pass[1]=  $("#newPassword").val();
							  
    $.ajax({
	  type: "POST",
	  url: 'changePassword.php',
	  data: {passarray: pass},
	  datatype : 'html',
	  async:false,
	  success:function(output){ 
			alert(output);
			if(output == "Old Password does not matched")
			{
				$("#oldPassword").focus();
			}
			else
			{
				$("#oldPassword").val("");
				$("#newPassword").val("");
				$("#confirmNewPassword").val("");
			}
		}
	});
}
});
});