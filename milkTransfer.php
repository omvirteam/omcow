<?php
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
  if(isset($_POST['submitBtn']) && $_POST['staffIdFrom'] > 0 && $_POST['staffIdTo'] > 0)
  {
    $staffIdFrom = isset($_POST['staffIdFrom']) ? $_POST['staffIdFrom'] : 0;
    $staffIdTo   = isset($_POST['staffIdTo']) ? $_POST['staffIdTo'] : 0;
    $milkDate    = $_POST['milkDateYear']."-".$_POST['milkDateMonth']."-".$_POST['milkDateDay'];
    $milkTime    = isset($_POST['milkTime']) ? $_POST['milkTime'] : 'M';
    $litre       = isset($_POST['litre']) && strlen($_POST['litre']) > 0 ? $_POST['litre'] : 0;
    $notes       = isset($_POST['notes']) ? $_POST['notes'] : "";
    
  	
    $insertMilkTrans  = "INSERT INTO milktransfer (staffIdFrom,staffIdTo,milkDate,milkTime,litre,notes)
                         VALUES(".$staffIdFrom.",".$staffIdTo.",'".$milkDate."','".$milkTime."',".$litre.",
                           '".$notes."')";
    $insertMilkTransRes = mysql_query($insertMilkTrans);
    if(!$insertMilkTransRes)
    {
    	echo "Error in Inserting ".mysql_error();
    }
    else
    {
      header("Location:milkTransfer.php");
    }
  }
  
  $j=0;
  $staffArray = array();
  $selStaff = "SELECT staffId,name
                 FROM staff
             ORDER BY name";
  $selStaffRes = mysql_query($selStaff);
  while($staffRow = mysql_fetch_array($selStaffRes))
  {
  	$staffArray['staffId'][$j]  = $staffRow['staffId'];
  	$staffArray['name'][$j]     = $staffRow['name'];
  	$j++;
  }
}
include("bottom.php");
$smarty->assign("staffArray",$staffArray);
$smarty->display("milkTransfer.tpl");
?>