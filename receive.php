<?php 
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
  $milkTime = isset($_REQUEST['milkTime']) ? $_REQUEST['milkTime'] : 'M';
  $distStaffIdId = isset($_REQUEST['staffId']) ? $_REQUEST['staffId'] : 0;
  $distStaffIdName = isset($_REQUEST['staffName']) ? $_REQUEST['staffName'] : "";
	if(isset($_REQUEST['submitBtn']))
	{
		$milkDate = $_POST['milkDateYear']."-".$_POST['milkDateMonth']."-".$_POST['milkDateDay'];
		$milktime = isset($_POST['milkTime']) ? $_POST['milkTime'] : '';
		$i = 0;
		while($i < count($_REQUEST['customerId']))
		{
			$customerId  = $_POST['customerId'][$i]; 
    	$currentAmt  = isset($_POST['currentAmt'][$i]) && $_POST['currentAmt'][$i] != '' ? $_POST['currentAmt'][$i] : 0;
    	$milkRec     = isset($_POST['milkRec'][$i]) && $_POST['milkRec'][$i] != '' ? $_POST['milkRec'][$i] : 0;
    	
    	$custUpdate = "UPDATE customer SET currentAmt = currentAmt + ".$milkRec." WHERE customerId = ".$customerId;
    	$custUpdateRes = mysql_query($custUpdate);
    	
    	$transQry = "INSERT INTO transaction (transType,customerIdCr,staffIdDr,amount,transDate)
    													 VALUE(0,".$customerId.",".$distStaffIdId.",".$milkRec.",".$milkDate.")";
    	$transQryRes = mysql_query($transQry);
    	
      $i++;
		}
		header("Location:staffList.php");
    exit();
	}
	else
	{
    $j=0;
    $custArrLoop = array();
    $grandTotLitre = 0;
    
    $selcustomer = "SELECT customerId,name,nickName,grnNo,rate,currentDrCr,currentAmt
                      FROM customer
                     WHERE (milkTime='".$milkTime."' OR milkTime='B')";
    if($distStaffIdId > 0)
    {
      $selStaffName = "SELECT name 
                         FROM staff 
                        WHERE staffId = ".$distStaffIdId;
      $selStaffNameRes = mysql_query($selStaffName);
      if($staffData = mysql_fetch_array($selStaffNameRes))
      {
        $distStaffIdName = $staffData['name'];
      }
      else
      {
        $distStaffIdName = "";
      }
      $selcustomer .= " AND distStaffIdId = ".$distStaffIdId; 
    }
    
    $selcustomer .= " ORDER BY distStaffIdId,name";
    $selcustomerRes = mysql_query($selcustomer);
    while($custRow = mysql_fetch_array($selcustomerRes))
    {
	    $i = 1;
	    $k = 0;
	    $totLitre = 0;
	    while($i <= date('d'))
	    {
		    $selMonthly = "SELECT customer.name,customer.nickName,
                              milksell.milkSellDate,milksell.milkGiven,milksell.customerMilkTime
		                     FROM milksell
		                LEFT JOIN customer ON customer.customerId = milksell.customerId
		                    WHERE milksell.milkSellDate = '".date('Y-m-'.$i.'')."'
		                      AND milksell.customerMilkTime='".$milkTime."'
		                      AND milksell.customerId = ".$custRow['customerId']."
		                 ORDER BY customer.name";
		    $selMonthlyRes = mysql_query($selMonthly);
		    if($dataRow = mysql_fetch_array($selMonthlyRes))
		    {
		    	$totLitre      += $dataRow['milkGiven'];
          $grandTotLitre += $dataRow['milkGiven'];
		    }	
		    $i++;  
		    $k++;
		  }

      $custArrLoop[$j]['totLitre']    = $totLitre;
      $custArrLoop[$j]['customerId']  = $custRow['customerId'];
      $custArrLoop[$j]['name']        = $custRow['name'] ." : ". $custRow['nickName'] ." : ".$custRow['grnNo'];
      $custArrLoop[$j]['currentAmt']  = $custRow['currentAmt'];
      $custArrLoop[$j]['currentDrCr'] = $custRow['currentDrCr'];
    	$j++;
    }	  

    include("./bottom.php");
		$smarty->assign('custArrLoop', $custArrLoop);
    $smarty->assign('distStaffIdId', $distStaffIdId);
    $smarty->assign('distStaffIdName', $distStaffIdName);
		$smarty->assign('milkTime', $milkTime);
		$smarty->assign('tomorrow', strtotime('+1 day'));
		$smarty->assign('grandTotLitre', $grandTotLitre);
		$smarty->display('receive.tpl');
	}
} 
?>