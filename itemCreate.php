<?php
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
    $_SESSION['item_create_success'] = "";
    $_SESSION['item_create_error'] = "";
	if(count($_POST) > 0)
	{
		$item =isset($_POST['item']) ? $_POST['item'] : "";
		$qty = isset($_POST['qty']) ? $_POST['qty'] : 0;
		$createDate= $_POST['createYear']."-".$_POST['createMonth']."-".$_POST['createDay'];
		$notes = isset($_POST['notes']) ? $_POST['notes'] : "";
		$insertItem  = "INSERT INTO itemcreate (itemId,createDate,qty,notes)
                                VALUES('".$item."','".$createDate."',".$qty.",'".addslashes($notes)."')";
		$insertItemRes = mysql_query($insertItem);
		
		//UPDATE ITEM
		$updateItem="UPDATE item SET qty=(qty+'".$qty."') WHERE itemId=".$item;
		mysql_query($updateItem);
		
		if(!$insertItemRes)
		{
		$_SESSION['item_create_error'] = "Unable to add details.";
		}
		else
		{
		$_SESSION['item_create_success'] = "Details successfully added.";
		}
	}
}

	$n=0;
	$itemArray=array();
	$item="SELECT itemId,name,unit FROM item where status='A' ORDER BY name";
	$itemRes=mysql_query($item);
	while($itemRow=mysql_fetch_array($itemRes))
	{
		$itemArray['itemId'][$n]=$itemRow['itemId'];
		$itemArray['name'][$n]=$itemRow['name'];
				
		//get first Item Unit
		if($n==0)
		{
			$unit=$itemRow['unit'];
		}
		$n++;
	}
	if(isset($unit))
	{
		$itemUnit="SELECT * FROM unit WHERE unitId=".$unit;
		$itemUnitRes=mysql_query($itemUnit);
		$itemUnitRow=mysql_fetch_array($itemUnitRes);
		$itemUnitVar=$itemUnitRow['unit'];
	}
	else
	{
		$itemUnitVar='';
	}
//GET ALL RECORDS TO DISPLAY IN DATATABLE
	$createArrayTable=array();
	$selectCreateItem="SELECT i.itemId,i.name,ic.* FROM itemcreate ic 
						LEFT JOIN item i ON ic.itemId=i.itemId 
						ORDER BY i.name";
	$selectCreateItemRes=mysql_query($selectCreateItem);
	
	if(mysql_num_rows($selectCreateItemRes)>0)
	{
		$t=0;
		while($selectCreateItemRow=mysql_fetch_array($selectCreateItemRes))
		{
		$createArrayTable[$t]['name']      = $selectCreateItemRow['name'];
		$createArrayTable[$t]['createDate']         = date('d-m-Y',strtotime($selectCreateItemRow['createDate']));
		$createArrayTable[$t]['qty']    = $selectCreateItemRow['qty'];
		$createArrayTable[$t]['notes']   = $selectCreateItemRow['notes'];
		$createArrayTable[$t]['itemCreateId']   = $selectCreateItemRow['itemCreateId'];
		$t++;             
		}
	
	}
	
include("bottom.php");
$smarty->assign("createArrayTable",$createArrayTable);
$smarty->assign("itemArray",$itemArray);
$smarty->assign("itemUnitVar",$itemUnitVar);
$smarty->assign("item_create_error",$_SESSION['item_create_error']);
$smarty->assign("item_create_success",$_SESSION['item_create_success']);

$smarty->display("itemCreate.tpl");
?>