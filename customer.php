<?php
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
	$edit = 0;
  $customer = array();
  $customer['milkTime'] = "M";
  $customer['currentLitre'] = 0;
  $customer['rate'] = 0;
  $customer['name'] = "";
  $customer['nickName'] = "";
  $customer['society'] = "";
  $customer['appartment'] = "";
  $customer['city'] = "";
  $customer['phone1'] = "";
  $customer['joinDate'] = "0000-00-00";
  $customer['currentAmt'] = 0;
  $customer['currentDrCr'] = "cr";
  $customer['collectionDate'] = "";
  $customer['notes'] = "";
  $customer['city'] = "";
  
  if(isset($_POST['submit']))
  {
    $customerTypeId = isset($_POST['customerTypeId']) ? $_POST['customerTypeId'] : 0;
  	$distStaffIdId  = isset($_POST['distStaffIdId']) ? $_POST['distStaffIdId'] : 0;
  	$milkTime       = isset($_POST['milkTime']) ? $_POST['milkTime'] : "";
  	$routeSequence  = isset($_POST['routeSequence']) ? $_POST['routeSequence'] : 0;
  	$currentLitre   = isset($_POST['currentLitre']) ? $_POST['currentLitre'] : 0;
    $rate           = (isset($_POST['rate']) && strlen($_POST['rate']) > 0) ? $_POST['rate'] : 0;
  	$name           = isset($_POST['name']) ? strtoupper($_POST['name']) : "";
  	$nickName       = isset($_POST['nickName']) ? strtoupper($_POST['nickName']) : "";
  	$society        = isset($_POST['society']) ? $_POST['society'] : "";
  	$appartment     = isset($_POST['appartment']) ? $_POST['appartment'] : "";
  	$city           = isset($_POST['city']) ? $_POST['city'] : "";
  	$phone1         = isset($_POST['phone1']) ? $_POST['phone1'] : "";
  	$phone2         = isset($_POST['phone2']) ? $_POST['phone2'] : "";
  	$currentAmt     = strlen($_POST['currentAmt']) > 0 ? $_POST['currentAmt'] : 0;
  	$currentDrCr    = isset($_POST['currentDrCr']) ? $_POST['currentDrCr'] : "";
  	$notes          = isset($_POST['notes']) ? $_POST['notes'] : "";
  	$joinDate       = $_POST['joinDateYear']."-".$_POST['joinDateMonth']."-".$_POST['joinDateDay'];
  	$collectionDate = $_POST['collectionDateYear']."-".$_POST['collectionDateMonth']."-".$_POST['collectionDateDay'];
  	
  	if(isset($_POST['custId'])) {
  		$custId = base64_decode($_POST['custId']);
  		$updateQry = "UPDATE customer SET customerTypeId = ".$customerTypeId.", distStaffIdId = ".$distStaffIdId.", 
  		                                  milkTime = '".$milkTime."', currentLitre = ".$currentLitre.",
  		                                  rate = ".$rate.", name = '".$name."', nickName = '".$nickName."', 
  		                                  society = '".$society."', appartment = '".$appartment."', city = '".$city."',
  		                                  phone1 = '".$phone1."', phone2 = '".$phone2."', joinDate = '".$joinDate."',
  		                                  currentAmt = ".$currentAmt.", currentDrCr = '".$currentDrCr."', 
  		                                  collectionDate = '".$collectionDate."', notes = '".$notes."'
  		               WHERE customerId = ".$custId;
  		$res = mysql_query($updateQry);
  		header("Location:custList.php");
      exit();
  	}else{
	  	$selGrnQry = "SELECT MAX(grnNo) + 1 AS grnNo 
	  	                FROM customer";
	  	$selGrnQryRes = mysql_query($selGrnQry);
	  	if($dataRow = mysql_fetch_array($selGrnQryRes))
	  	{
	      if($dataRow['grnNo'] > 0)
	      {
	        $grnNo = $dataRow['grnNo']; 
	      }
	      else
	      {
	        $grnNo = 1; 
	      }
	  	}
	  	else
	  	{
	  		$grnNo = 1;
	  	}
	    $insertCustomer  = "INSERT INTO customer (grnNo,customerTypeId,distStaffIdId,milkTime,morningSequence,eveningSequence,
	                                             currentLitre,rate,name,nickName,society,appartment,city,phone1,phone2,
	                                             joinDate,currentAmt,currentDrCr,collectionDate,notes)
	                                VALUES(".$grnNo.",".$customerTypeId.",".$distStaffIdId.",'".$milkTime."',0,0,
	                                       '".$currentLitre."','".$rate."','".$name."','".$nickName."','".$society."','".$appartment."',
	                                       '".$city."','".$phone1."','".$phone2."','".$joinDate."',".$currentAmt.",
	                                       '".$currentDrCr."','".$collectionDate."','".$notes."')";
	    $insertCustomerRes = mysql_query($insertCustomer);
	    if(!$insertCustomerRes)
	    {
	    	echo "Insert Fail";
	    }
	    else
	    {
	      header("Location:customer.php");
	    }
	  }
  }
  
  if(isset($_GET['custId'])){
  	$edit = 1;
  	$custId = base64_decode($_GET['custId']);
  	
  	$selQry = "SELECT * FROM customer WHERE customerId = ".$custId;
  	$res = mysql_query($selQry);
  	$customer = mysql_fetch_array($res);
  }
  
  
  
  $k=0;
  $customerArray = array();
  
  $customertype = "SELECT customerTypeId,customerType
                  FROM customertype";
  $customertypeRes = mysql_query($customertype);
  while($customertypeRow = mysql_fetch_array($customertypeRes))
  {
  	$customerArray['customerTypeId'][$k] = $customertypeRow['customerTypeId'];
  	$customerArray['customerType'][$k]   = $customertypeRow['customerType'];
  	$k++;
  }
  
  
  $o=0;
  $distArray = array();
  
  $distType = "SELECT staffId,staffTypeId,name
                 FROM staff
                WHERE staffTypeId = 1";
  $distTypeRes = mysql_query($distType);
  while($distTypeRow = mysql_fetch_array($distTypeRes))
  {
  	$distArray['staffId'][$o] = $distTypeRow['staffId'];
  	$distArray['name'][$o]        = $distTypeRow['name'];
  	$o++;
  }
  
  
}
include("bottom.php");
$smarty->assign("customerArray",$customerArray);
$smarty->assign("distArray",$distArray);
$smarty->assign("customer",$customer);
$smarty->assign("edit",$edit);
$smarty->display("customer.tpl");
?>