<?php
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
	$staffArray = array();
	$i = 0;
  $selectStaff = " SELECT master.staffId,master.name,DATE_FORMAT(master.joinDate,'%d-%m-%Y') AS joinDate,master.commission,
                          master.salary,place AS placeName,master.staffTypeId,staffType,parent.name AS parentName 
                     FROM staff AS master
                    LEFT JOIN place ON place.placeId = master.placeId
                    LEFT JOIN stafftype ON stafftype.staffTypeId = master.staffTypeId
                    LEFT JOIN staff AS parent ON parent.staffId = master.parentStaffId
                    ORDER BY name";
	$selectStaffRes = mysql_query($selectStaff);
	while($staffRow = mysql_fetch_array($selectStaffRes))
	{
		$staffArray[$i]['staffId']      = $staffRow['staffId'];
		$staffArray[$i]['staffTypeId']  = $staffRow['staffTypeId'];
		$staffArray[$i]['name']         = $staffRow['name'];
		$staffArray[$i]['placeName']    = $staffRow['placeName'];
		$staffArray[$i]['parentName']   = $staffRow['parentName'];
		$staffArray[$i]['staffType']    = $staffRow['staffType'];
		$staffArray[$i]['joinDate']     = $staffRow['joinDate'];
		$staffArray[$i]['commission']   = $staffRow['commission'];
		$staffArray[$i]['salary']       = $staffRow['salary'];
		$i++;                                      
	}
  
}
include("./bottom.php");
$smarty->assign("staffArray",$staffArray);
$smarty->display("staffList.tpl");
?>