<?php
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
	$custArray = array();
	$i = 0;
  $selectCust = " SELECT grnNo,customerId,customerType,staff.name AS distName,milkTime,morningSequence,eveningSequence,
                         currentLitre,rate,
                         customer.name,nickName,society,appartment,city,phone1,phone2,
                         DATE_FORMAT(customer.joinDate,'%d-%m-%Y') AS joinDate,currentAmt,currentDrCr,
                         DATE_FORMAT(collectionDate,'%d-%m-%Y') AS collectionDate,notes
                     FROM customer
                    LEFT JOIN customertype ON customertype.customerTypeId = customer.customerTypeId
                    LEFT JOIN staff ON staff.staffId = customer.distStaffIdId
                    ORDER BY customer.name";
	$selectCustRes = mysql_query($selectCust);
	while($custRow = mysql_fetch_array($selectCustRes))
	{
		$custArray[$i]['customerId']         = $custRow['customerId'];
		$custArray[$i]['grnNo']              = $custRow['grnNo'];
		$custArray[$i]['customerType']       = $custRow['customerType'];
		$custArray[$i]['distName']           = $custRow['distName'];
		$custArray[$i]['milkTime']           = $custRow['milkTime'];
		$custArray[$i]['morningSequence']    = $custRow['morningSequence'];
    $custArray[$i]['eveningSequence']    = $custRow['eveningSequence'];
		$custArray[$i]['currentLitre']       = $custRow['currentLitre'];
		$custArray[$i]['rate']               = $custRow['rate'];
		$custArray[$i]['name']               = $custRow['name'];
		$custArray[$i]['nickName']           = $custRow['nickName'];
		$custArray[$i]['society']            = $custRow['society'];
		$custArray[$i]['appartment']         = $custRow['appartment'];
		$custArray[$i]['city']               = $custRow['city'];
		$custArray[$i]['phone1']             = $custRow['phone1'];
		$custArray[$i]['phone2']             = $custRow['phone2'];
		$custArray[$i]['joinDate']           = $custRow['joinDate'];
		$custArray[$i]['currentAmt']         = $custRow['currentAmt'];
		$custArray[$i]['currentDrCr']        = $custRow['currentDrCr'];
		$custArray[$i]['collectionDate']     = $custRow['collectionDate'];
		$custArray[$i]['notes']              = $custRow['notes'];
		$i++;                                      
	}
  
}
include("./bottom.php");
$smarty->assign("custArray",$custArray);
$smarty->display("custList.tpl");
?>