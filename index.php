<?php
include("./include/config.inc.php");
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
    header("Location:checkLogin.php");
    exit;
}
else{
    include("./bottom.php");
    $smarty->display('index.tpl');
}
?>