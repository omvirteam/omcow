<?php
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
	$billArray = array();
	$i = 0;
	$selectBill = " SELECT customer.grnNo,customer.name,customer.nickName,
                         DATE_FORMAT(transaction.transDate,'%d-%m-%Y') AS transDate,
                         transaction.customerIdDr,transaction.transDate AS transDateYMD
                    FROM transaction
               LEFT JOIN customer ON customer.customerId = transaction.customerIdDr
                   WHERE accountIdCr = 5
                GROUP BY transaction.customerIdDr,transaction.transDate
                ORDER BY transDate";
	$selectBillRes = mysql_query($selectBill);
	while($billRow = mysql_fetch_array($selectBillRes))
	{
    $totalAmount = 0;
    $selTotalAmount = "SELECT amount 
                         FROM transaction
                        WHERE customerIdDr = ".$billRow['customerIdDr']."
                          AND transDate = '".$billRow['transDateYMD']."'";
    $selTotalAmountRes = mysql_query($selTotalAmount);
    while($amtRow = mysql_fetch_array($selTotalAmountRes))
    {
      $totalAmount += $amtRow['amount'];
    }
		$billArray[$i]['customerName'] = $billRow['name'] ." : ". $billRow['nickName'] ." : ".$billRow['grnNo'];
		$billArray[$i]['amount']       = $totalAmount;
		$billArray[$i]['transDate']    = $billRow['transDate'];
		$billArray[$i]['customerIdDr'] = $billRow['customerIdDr'];
		$billArray[$i]['transDateYMD'] = $billRow['transDateYMD'];
		$i++;                                      
	}
  
}
include("./bottom.php");
$smarty->assign("billArray",$billArray);
$smarty->display("billList.tpl");
?>