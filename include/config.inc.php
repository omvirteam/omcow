<?php
session_start();
include("conn.inc");
//Smarty Settings :Start
require('./include/smarty/libs/Smarty.class.php');  // put full path to Smarty.class.php
$smarty = new Smarty();

$smarty->setTemplateDir('./templates');
$smarty->setCompileDir ('./include/smarty/omTemplates_c');
$smarty->setCacheDir   ('./include/smarty/omCache');
$smarty->setConfigDir  ('./include/smarty/omConfigs');
$smarty->assign('sitemenu', true);
//Smarty Settings :End
date_default_timezone_set("Asia/Kolkata");
?>