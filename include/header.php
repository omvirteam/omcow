<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
	    <div class="container-fluid navbar-inner">
		
		<div class="navbar-header">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#siteMenu">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		    </button>
		    <a class="navbar-brand" href="index.php" rel=external><span>&#2384; Cow</span></a>
		</div>
		<!-- /.navbar-header -->

				<ul class="nav navbar-top-links navbar-right pull-right" id="siteMenu">
		<!--Account Menu : Start-->
                    <li class="dropdown">
  			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
  			    <i class="fa fa-edit fa-fw"></i>  <i class="fa fa-caret-down"></i>
  			</a>
  			<ul class="dropdown-menu">
  			    <li><a href="trans.php" rel=external><i class="fa fa-pencil fa-fw"></i>Transaction</a></li> 
  			    <li class="divider"></li>
  			    <li><a href="account.php" rel=external><i class="fa fa-pencil fa-fw"></i>Account Entry</a></li>
            <li class="divider"></li>
            <li><a href="createBill.php" rel=external><i class="fa fa-pencil fa-fw"></i>Create Bill</a></li>
  			</ul>
  			<!-- /.dropdown-messages -->
	</li>
	<!--This is the New Menu Ends-->

        		<!--This is the New Menu Begins-->
	<li class="dropdown">
  			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
  			    <i class="fa fa-edit fa-fw"></i>  <i class="fa fa-caret-down"></i>
  			</a>
  			<ul class="dropdown-menu">
  			    <li><a href="item.php" rel=external><i class="fa fa-pencil fa-fw"></i>Item Entry</a></li>
  			    <li class="divider"></li>
  			    <li><a href="itemCreate.php" rel=external><i class="fa fa-pencil fa-fw"></i>Item Create</a></li> 
  			    <li class="divider"></li>
  			    <li><a href="itemExpense.php" rel=external><i class="fa fa-pencil fa-fw"></i>Item Expense</a></li> 
  			    <li class="divider"></li>
  			    <li><a href="itemDispatch.php" rel=external><i class="fa fa-pencil fa-fw"></i>Item Dispatch</a></li>
				<li class="divider"></li>
  			    <li><a href="itemSell.php" rel=external><i class="fa fa-pencil fa-fw"></i>Item Sell</a></li>  			    
  			</ul>
  			<!-- /.dropdown-messages -->
	</li>
	<!--This is the New Menu Ends-->

      <li class="dropdown">
  			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
  			    <i class="fa fa-edit fa-fw"></i>  <i class="fa fa-caret-down"></i>
  			</a>
  			<ul class="dropdown-menu">
  			    <li><a href="milkByCow.php" rel=external><i class="fa fa-pencil fa-fw"></i>Milk By Cow</a></li>
  			    <li class="divider"></li>
  			    <li><a href="milkTransfer.php" rel=external><i class="fa fa-pencil fa-fw"></i> Milk Transfer</a></li> 
  			    <li class="divider"></li>
  			    <li><a href="milkSell.php" rel=external><i class="fa fa-pencil fa-fw"></i> Milk Sell</a></li> 
  			    <li class="divider"></li>
  			    <li><a href="milkBuy.php" rel=external><i class="fa fa-pencil fa-fw"></i>Milk buy</a></li> 
  			    
  			</ul>
  			<!-- /.dropdown-messages -->
	    </li>		  
		  
	    <li class="dropdown">
  			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
  			    <i class="fa fa-edit fa-fw"></i>  <i class="fa fa-caret-down"></i>
  			</a>
  			<ul class="dropdown-menu">
  			    <li><a href="cowEntry.php" rel=external><i class="fa fa-pencil fa-fw"></i> Cow Entry</a></li>
  			    <li class="divider"></li>
  			    <li><a href="staff.php" rel=external><i class="fa fa-pencil fa-fw"></i> Staff Entry</a></li>
  			    <li class="divider"></li>
  			    <li><a href="customer.php" rel=external><i class="fa fa-pencil fa-fw"></i> Customer Entry</a></li>
  			    <li class="divider"></li>
  			    <li><a href="licence.php" rel=external><i class="fa fa-pencil fa-fw"></i> Licence Entry</a></li>
						<li class="divider"></li>
  			    <li><a href="Vaccine.php" rel=external><i class="fa fa-pencil fa-fw"></i> Vaccine Entry</a></li>
  			</ul>
  			<!-- /.dropdown-messages -->
	    </li>
		  <li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			    <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
			    <li><a href="cowList.php" rel=external><i class="fa fa-table fa-fw"></i> Cow List</a></li>
			    <li class="divider"></li>
			    <li><a href="staffList.php" rel=external><i class="fa fa-table fa-fw"></i> Staff List</a></li>
			    <!--<li class="divider"></li>
			    <li><a href="itemList.php" rel=external><i class="fa fa-table fa-fw"></i> Item List</a></li>-->
			    <li class="divider"></li>
			    <li><a href="custList.php" rel=external><i class="fa fa-table fa-fw"></i> Customer List</a></li>
          <li class="divider"></li>
			    <li><a href="billList.php" rel=external><i class="fa fa-table fa-fw"></i> Bill List</a></li>
			</ul>
		    </li>
		    <li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			    <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
			    <li><a href="commonRoute.php?time=M" rel=external><i class="fa fa-table fa-fw"></i>Morning Route</a></li>
          <li><a href="commonRoute.php?time=E" rel=external><i class="fa fa-table fa-fw"></i>Evening Route</a></li>
			</ul>
		    </li>
		    <li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			    <i class="fa fa-user fa-fw"></i>&nbsp;om&nbsp;&nbsp;<i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
			    <li><a href="register.php" rel=external><i class="fa fa-plus fa-fw"></i> Register</a></li>
			    <li class="divider"></li>
			    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Change Password</a></li>
			    <li class="divider"></li>
			    <li><a href="logout.php" rel=external><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
			</ul>
		    </li>
		</ul>
		<!-- /.navbar-top-links -->
		
	    </div>

        </nav>