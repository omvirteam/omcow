<?php
function getFieldValue($tableName,$fieldName,$whereClause)
{
	$qrySel = "SELECT ".$fieldName." FROM ".$tableName." WHERE ".$whereClause;
	$resSel = mysql_query($qrySel);
	$qFetch = mysql_fetch_array($resSel);
	return $qFetch[$fieldName];
}

function getItemSellAmountTotal($whereClause)
{
	$qrySelItemSell = "SELECT SUM(amount) as totalAmount FROM itemsell WHERE billCreated='y' AND ".$whereClause." GROUP BY customerId ASC";
	$resSelItemSell = mysql_query($qrySelItemSell);
	$qFetchItemSell = mysql_fetch_array($resSelItemSell);
	return $qFetchItemSell["totalAmount"];
}
?>