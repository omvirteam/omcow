{include file="./headStart.tpl"}

<!-- Page-Level Plugin CSS - Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

<!-- Page-Level Plugin Scripts - Tables -->
<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

<script type="text/javascript">
$(document).ready(function() {


	$('#itemExp').dataTable({
	"aoColumnDefs": [{ 'bSortable': false, 'aTargets': [ 0,-3 ] }],
	"aaSorting": [[ 1, "asc" ]]
    });
});
</script>
{include file="./headEnd.tpl"}
<div class="row">
    <div class="col-lg-6 col-md-offset-3">
		{if $item_expense_error != ""}
	    <div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		{$item_expense_error}
	    </div>
	{/if}

	{if $item_expense_success != ""}
	    <div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		{$item_expense_success}
	    </div>
	{/if}
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-edit fa-fw"></i> Item Expense	
			</div>
			<div class="panel-body">
				<div class="row col-lg-13">
					<form id="itemCreateForm" method="POST" action="itemExpense.php" class="formBox form-horizontal" role="form">
						<div class="form-group">
							<label class="control-label col-sm-4">Select Item</label>
							<div class="col-sm-5">
								<select name="item" id="item" class="form-control input-sm">
									{html_options values=$itemArray.itemId output=$itemArray.name}
								</select>
							</div>							
						</div>
						
						<div class="form-group">
								<label class="control-label col-sm-4">Expense Date</label>
								<div class="col-sm-8">
									{html_select_date prefix="expense" start_year="-1" end_year="+0" field_order="DMY" day_value_format="%02d" all_extra="class='form-control date-controls pull-left input-sm'"}
								</div>
						</div>						
						
						<div class="form-group">
							<label class="control-label col-sm-4">Amount</label>
							<div class="col-sm-3">
								<input type="text" value="" name="amount" class="form-control input-sm" />
								
							</div>						
						</div>					
						
						<div class="form-group">
							<label class="control-label col-sm-4">Notes</label>
							<div class="col-sm-7">
								<textarea name="notes" rows="10" cols="40" class="form-control"></textarea>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-sm-4"></label>
								<div class="col-sm-7">
								<button class="btn btn-success btn-lg" type="submit">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
	    <!-- /.panel-body -->
		</div>
	<!-- /.panel -->
	</div>
    <!-- /.col-lg-12 -->
</div>
<!-- Display Records-->
<div class="row">
    <div class="col-lg-10 col-md-offset-1">
	<div class="panel panel-primary">
	    <div class="panel-heading">
		<i class="fa fa-list-ul fa-fw"></i> Expense Item List
	    </div>
	    <div class="panel-body">
		<div class="table-responsive">
		    <table class="table table-striped table-bordered table-hover" id="itemExp">
			<thead>
			    <tr>
				<th align="center">&nbsp;</th>
				<th align="center">Item name</th>
				<th align="center">Expense Date</th>
				<th align="center">Amount</th>
				<th align="center">Notes</th> 
				
			    </tr>
			</thead>
			<tbody>
			    {section name="sec" loop=$expArrayTable}
			    <tr>
				<td align="center">
				    <a href="expItemDelete.php?expItemId={$expArrayTable[sec].itemExpenseId}" onclick="return confirm('Are You Sure To Delete??');"  class="link">
					<img src="./images/deleteIcon.png" />
				    </a>
				</td>
				<td align="center">{$expArrayTable[sec].name}</td>
				<td align="center">{$expArrayTable[sec].expDate}</td>
				<td align="center">{$expArrayTable[sec].amount}</td>
				<td align="center">{$expArrayTable[sec].notes}</td>
	
				
			    </tr>
			    {/section}
			</tbody>
		    </table>
		</div>
	    </div>
	</div>
    </div>
</div>
{include file="./footer.tpl"}