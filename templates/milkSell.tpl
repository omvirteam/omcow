{include file="./headStart.tpl"}
<script>
var _gaq = _gaq || [];
$(document).ready(function(e) {
  $('.milkGiven').change(function(){
  	var totMilkGiven = 0;
		$(".milkGiven").each(function( i ) {
			if($(this).val() != '' && $(this).val() > 0)
			{
		  	totMilkGiven += parseFloat($(this).val()); 
			}
		});
		
		$('#totMilkGiven').val(totMilkGiven);
  });

  $('.nextLitre').change(function(){
  	var totNextLiter = 0;
		$(".nextLitre").each(function( i ) {
			if($(this).val() != '' && $(this).val() > 0)
			{
		  	totNextLiter += parseFloat($(this).val()); 
			}
		});
		
		$('#totNextLiter').val(totNextLiter);
  });
});
</script>
{include file="./headEnd.tpl"}

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
    <div class="panel-heading">Milk Sell {if $distStaffIdId > 0} <b>For : {$distStaffIdName}</b >{/if}</div><!-- /.panel-heading -->
      <div class="panel-body">
     
        <form method="POST" id="milkTimeForm" name="form1" action="{$smarty.server.PHP_SELF}" class="formBox form-horizontal" role="form">
          <input type="hidden" name="staffId" value="{$distStaffIdId}" />
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th style="text-align:center;" >
                    Milk Time : 
          			    	<select style="width:120px;" name="milkTime" onchange="this.form.submit();" id="milkTime" class="input-sm" >
              				  <option value="M" {if $milkTime == 'M'}SELECTED{/if}>Morning</option>
              				  <option value="E" {if $milkTime == 'E'}SELECTED{/if}>Evening</option>
              				</select>
                  </th>                  
                  <th style="text-align:center;"  nowrap>
                    Milk Date : 
{html_select_date prefix="milkDate" start_year="-1" end_year="+0" field_order="DMY" day_value_format="%02d" 
 time=$smarty.request.milkDate all_extra="style='width:120px;' class='input-sm'" }
                  <input type="hidden" name="topFormSubmitted" value="1">
                  <input type="submit" data-theme="b" class="btn btn-success" name="go" id="go" value="Go">
                  </th>
                </tr>
              </thead>
            </table>
          </div>
     </form>
        <!-- /.table-responsive -->          
          <div class="table-responsive">
          <form method="POST" id="milkTimeForm" name="form1" action="{$smarty.server.PHP_SELF}" class="formBox form-horizontal" role="form">
            <table border="1" class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th style="text-align:right;" colspan="2"><b>Date For:</b></th>
                  <th style="text-align:center;" >{$smarty.request.milkDate}</th>
                  <th colspan="2">&nbsp;</th>
                </tr>
              </thead>
              <thead>
                <tr>
                  <th style="text-align:center;"  nowrap>Name</th>
                  <th style="text-align:center;"  nowrap>Rate</th>
                  <th style="text-align:center;" >Milk Given </th>
                  <th style="text-align:left;" colspan="2">Next Time</th>
                </tr>
              </thead>
              <tbody>
                {section name="sec" loop=$custArrLoop}
          			  <tr>
          				  <td align="right">
          				    <input type="hidden" size="2" value="{$custArrLoop[sec]['customerId']}" name="customerId[]" />
          				    {$custArrLoop[sec]['name']}
          				  </td>
          				  <td align="right">
          				    {$custArrLoop[sec]['rate']}
          				  </td>
          				  <td align="center">
            			    <input type="text" size="3" style="width:60px;" name="milkGiven[]" 
                             tabindex="{$smarty.section.sec.iteration}" class="milkGiven form-control input-sm"  
                             {if $smarty.section.sec.iteration == 1} autofocus{/if} 
                             value="{if $custArrLoop[sec]['milkGiven'] != 0}{$custArrLoop[sec]['milkGiven']}{/if}"/>
                      <input type="hidden" size="3" style="width:60px;" name="rate[]" value="{$custArrLoop[sec]['rate']}"/>
          				  </td>
          				  <td align="center" NOWRAP>
                      <input type="text" size="3" style="width:60px;" name="nextLitre[]" tabindex="{math equation="x + y" x=$smarty.section.sec.iteration y=1000}" class="nextLitre form-control input-sm" /> 
          				  </td>
          				  <td align="left">
              				<select name="customerMilkTime[]" style="width:120px;" class="input-sm">
              				  <option value="M" {if $milkTime == 'M'}SELECTED{/if}>Morning</option>
              				  <option value="E" {if $milkTime == 'E'}SELECTED{/if}>Evening</option>
              				</select>
          				      {html_select_date field_array="nextDate[]" prefix="" start_year="-1" end_year="+0" field_order="DMY" day_value_format="%02d" all_extra="style='width:120px;' class='input-sm'" time="$tomorrow"}
          				  </td>          				  
         			    </tr>
         			  {sectionelse}
          			  <tr>
          				  <td align="center" colspan="4">No Record Found</td>
         			    </tr>   			  
                {/section}
          			  <tr>
          				  <th style="text-align:right" colspan="2">
          				  	Total :
          				  </th>
          				  <td align="center">
            			    <input type="text" size="3" name="totMilkGiven" id="totMilkGiven" DISABLED class="form-control" />
          				  </td>
          				  <td align="left" NOWRAP>
          				  	<input type="text" size="3" name="totNextLiter" id="totNextLiter" DISABLED class="form-control" />
          				  </td>
          				  <td align="left" NOWRAP>
          				  	&nbsp;
          				  </td>
         			    </tr>
              </tbody>
            </table>
          </div>
          <div class="form-group">
      		<label class="control-label col-sm-5"></label>
      			<div class="col-sm-7">
                <input type="hidden" name="milkTime" value="{$smarty.request.milkTime}" />
                <input type="hidden" name="staffId" value="{$distStaffIdId}" />
                <input type="hidden" name="milkDate" value="{$smarty.request.milkDate}" />
      			    <input type="submit" data-theme="b" class="btn btn-success btn-lg" name="submitBtn" id="submitBtn" value="Submit">
      			</div>
          </div>          
        <!-- /.table-responsive -->
      </form>
      </div>
      <!-- /.panel-body -->
      <div class="panel-body">
        <div class="table-responsive">
          <table border="1" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th style="text-align:center;"  nowrap>Name</th>
                <th style="text-align:center;"  nowrap>Total</th>
                {section name="secDateHeading" loop=$dateArray}
                	<th style="text-align:right;" >{$smarty.section.secDateHeading.iteration}</th>
                {/section}

              </tr>
            </thead>
            <tbody>
            	{section name="sec1" loop=$custArrLoop}
              <tr>
              	<td align="right">
        				  	{$custArrLoop[sec1].name}
        				</td>	
        				<td align="right">
        				  	{$custArrLoop[sec1].totLitre}
        				</td>	
        				{section name="sec2" loop=$custMonthlyArr[sec1]}
        			    <td align="right" NOWRAP>
        				  	{$custMonthlyArr[sec1][sec2].milkGiven} 
        				  </td>
        				{/section}  
       			  </tr>
       			  {sectionelse}
        			  <tr>
        				  <td align="center" colspan="5">No Record Found</td>
       			    </tr>   			  
              {/section}
            </tbody>
            <tr>
              <th style="text-align:center;">&nbsp;</th>
              <th style="text-align:right;" >{$grandTotLitre}</th>
              {section name="secDateTotal" loop=$dateArray}
                <th style="text-align:right;" >{$dateArray[secDateTotal]}</th>
              {/section}

            </tr>
          </table>
          </table>
	        </div>
        <!-- /.table-responsive -->
      </div>
      <!-- /.panel-body -->      
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-6 -->
</div>
{include file="./footer.tpl"}