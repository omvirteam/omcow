{include file="./headStart.tpl"}
<!-- Page-Level Plugin CSS - Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

<!-- Page-Level Plugin Scripts - Tables -->
<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
</script>

{include file="./headEnd.tpl"}

<div class="row">
<div class="col-lg-12">
	<div class="panel panel-primary">
	  <div class="panel-heading">
		  <i class="fa fa-edit fa-fw"></i>Milk Sell
	  </div>
	  <div class="panel-body">
	  <form method="POST" action="{$smarty.server.PHP_SELF}" class="formBox form-horizontal" role="form">
		  <div class="row col-lg-6">
		    <div class="form-group">
			    <label class="control-label col-sm-4">Milk Date</label>
			    <div class="col-sm-8">
				    {html_select_date prefix="milkDate" start_year="-1" end_year="+0" field_order="DMY" day_value_format="%02d" all_extra="class='form-control date-controls pull-left input-sm'"}
			    </div>
			  </div>
		  </div><!-- /.row col-lg-6 -->
      <div class="col-lg-6">
			  <div class="form-group">
			    <label class="control-label col-sm-4">Milk Time</label>
  			  <div class="col-sm-5">
    				<select name="milkTime" class="form-control input-sm">
    				  <option value="M">Morning</option>
    				  <option value="E">Evening</option>
    				</select>
  			  </div>
			  </div>        
      </div><!-- /.row col-lg-6 --> 
 
		<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead>
      <tr>
        <th align="center" nowrap>
    			    Name
        				<select name="customerName" class="form-control input-sm">
        				  <option value="M">Morning</option>
        				  <option value="E">Evening</option>
        				</select>
    			
        </th>
        <th align="center">
    			    Milk Given
    			    <div class="col-sm-3">
    				    <input type="text" value="" name="milkGiven" id="rate" REQUIRED class="form-control input-sm" />
    				  </div  
        </th>
        <th align="center">
    			    Next Date
        				<select name="customerName" class="form-control input-sm">
        				  <option value="M">Morning</option>
        				  <option value="E">Evening</option>
        				</select>
        </th>
        <th align="center">
    			    Next Litre
    				    <input type="text" value="" name="nextLitre" id="rate" REQUIRED class="form-control input-sm" />
        </th>
      </tr>
			</thead>
    </table>
		</div>      
            
    </form>       		  
	  </div><!-- /.panel-body -->
	</div><!-- /.panel -->
</div><!-- /.col-lg-12 -->
</div><!-- /.row -->

{include file="./footer.tpl"}