{include file="./headStart.tpl"}
{include file="./headEnd.tpl"}
<div class="row">
<div class="col-lg-6 col-md-offset-3">	
<div class="panel panel-primary">
	<div class="panel-heading">
	    <i class="fa fa-edit fa-fw"></i> Licence Entry
	</div>
<div class="panel-body">
  <div class="row col-lg-12">

    <form id="check-user" class="formBox form-horizontal" role="form" data-ajax="false" method="POST" action="licence.php">

	  <div class="form-group">
		<label class="control-label col-sm-4">Name:</label>
			<div class="col-sm-5">
        <input type="licenceName" class="form-control input-sm" name="licenceName" required >
			</div>
	  </div>

	  <div class="form-group">
		<label class="control-label col-sm-4">Due Date :</label>
			<div class="col-sm-7">
        {html_select_date prefix="renewDate" start_year="-1" end_year="+0" field_order="DMY" day_value_format="%02d" all_extra="class='form-control date-controls pull-left input-sm'"}
			</div>
	  </div>

    <div class="form-group">
		<label class="control-label col-sm-4"></label>
			<div class="col-sm-7">
			    <input type="submit" data-theme="b" class="btn btn-success btn-lg" value="Submit" name="submit">
			</div>
    </div>
	</form>
	</div><!-- /.col-lg-12 -->
</div><!-- /.panel-body -->
</div><!-- /.panel panel-primary-->
</div><!-- /.col-lg-6 col-md-offset-3-->
</div><!-- /.row -->

<div class="row">
<div class="col-lg-6 col-md-offset-3">
	<div class="panel panel-primary">
	  <div class="panel-heading">
			<i class="fa fa-list-ul fa-fw"></i> Licence List
	  </div>
	  <div class="panel-body">
		<div class="table-responsive">
		  <table class="table table-striped table-bordered table-hover" id="cowList">
			<thead>
			  <tr>
      		<th>&nbsp;</td>
      		<th >Name</th>
      		<th >Date</th>
			  </tr>
			</thead>
			<tbody>
       	{section name="sec" loop=$lienceArray}
       	<tr>
       		<td align="center">
       			<a href="licenceDelete.php?licenceId={$lienceArray[sec].licenceId}" onclick="return confirm('Are You Sure To Delete??');" style="text-decoration:none;">Delete</a></td>
       		<td NOWRAP>{$lienceArray[sec].licenceName}</td>
       		<td NOWRAP>{$lienceArray[sec].renewDate}</td>
       		
       	</tr>
       	{/section}
			</tbody>
		  </table>
		</div><!-- .table-responsive -->
	  </div><!-- .panel-body -->
	</div><!-- .panel panel-primary -->
</div><!-- .col-lg-12 -->
</div><!-- .row -->
{include file="./footer.tpl"}