{include file="./headStart.tpl"}
{include file="./headEnd.tpl"}
<div class="row">
<div class="col-lg-6 col-md-offset-3">
	<div class="panel panel-primary">
	  <div class="panel-heading">
		<i class="fa fa-edit fa-fw"></i>Register
	  </div>
	  <div class="panel-body">
            <div class="row col-lg-13">
<div data-role="page" id="login" data-theme="b">


  
    <form id="check-user" data-ajax="false" method="POST" action="register.php"  class="formBox form-horizontal" role="form">
    	<div data-role="fieldcontain" class="form-group">
    		<label for="select-choice-1" value="0" class="control-label col-sm-4">Select User Type</label>
    	   <div class="col-sm-5">	
			<select name="userTypeId" data-native-menu="false" class="form-control input-sm">
    		  {html_options values=$userArray.userTypeId output=$userArray.userType}
    		</select>
		   </div>
    	</div>
      <fieldset>
          <div data-role="fieldcontain" class="form-group">
              <label for="fName" class="control-label col-sm-4">First Name:</label>
            <div class="col-sm-3">
			  <input type="text" value="" name="fName" id="fName" class="form-control input-sm" required autofocus data-mini="true" />
            </div>
		  </div>                                  
          <div data-role="fieldcontain" class="form-group">                                      
              <label for="mName" class="control-label col-sm-4">Middle Name:</label>
			<div class="col-sm-3">
              <input type="text" value="" name="mName" id="mName" class="form-control input-sm"/> 
			</div>
          </div>
          <div data-role="fieldcontain" class="form-group">                                      
              <label for="lName" class="control-label col-sm-4">Last Name:</label>
             <div class="col-sm-3"> 
			  <input type="text" value="" name="lName" id="lName" class="form-control input-sm"/> 
              </div>
		  </div>
          <div data-role="fieldcontain" class="form-group">                                      
              <label for="city" class="control-label col-sm-4">City:</label>
            <div class="col-sm-3">
			  <input type="text" value="" name="city" id="city" class="form-control input-sm"/> 
            </div>
		  </div>
          <div data-role="fieldcontain" class="form-group">                                      
              <label for="emailId" class="control-label col-sm-4">Email Id:</label>
             <div class="col-sm-3">
			  <input type="text" value="" name="emailId" id="emailId" class="form-control input-sm"/> 
			 </div>
		  </div>
          <div data-role="fieldcontain" class="form-group">                                      
              <label for="mobileNo" class="control-label col-sm-4">Mobile No:</label>
             <div class="col-sm-3">
			  <input type="text" value="" name="mobileNo" id="mobileNo" class="form-control input-sm"/> 
			 </div>
          </div>
		  
		  <div class="form-group">
                    <label class="control-label col-sm-4"></label>
                    <div class="col-sm-7">
                            <input type="submit" data-theme="b" name="submit" id="submit" value="Submit" class="btn btn-success btn-lg">
                    </div>
                  </div>
          
      </fieldset>
    </form>
  
  <div data-theme="a" data-role="footer" data-position="fixed"></div>
</div>
<div data-role="page" id="second">
  <div data-theme="a" data-role="header">
    <h3></h3>
  </div>
<div data-role="content"></div>
</div>
</div><!-- /.row col-lg-13 -->
	  </div><!-- /.panel-body -->
	</div><!-- /.panel -->
</div><!-- /.col-lg-12 -->
</div><!-- /.row -->

{include file="./footer.tpl"}