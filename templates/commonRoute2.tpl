{include file="./headStart.tpl"}
  <link rel="stylesheet" href="./css/jquery.mobile-1.4.0-alpha.2.min.css" />
  <script src="./js/jquery.mobile-1.4.0-alpha.2.min.js"></script>
  <script src="./js/jquery-ui.js"></script>
  <script src="./js/jquery.ui.touch-punch.min.js"></script>
  <script>
  var _gaq = _gaq || [];
  $(document).ready(function(e) {
  	$.mobile.page.prototype.options.domCache = false;
    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + 
                  '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  }); 

  $(document).on("pageshow", function(event, ui) {
    try {
      _gaq.push(['_setAccount', 'UA-28940436-2']);    
      if ($.mobile.activePage.attr("data-url")) {
            _gaq.push(['_trackPageview', $.mobile.activePage.attr("data-url")]);
        } else {
            _gaq.push(['_trackPageview']);
        }
    } catch(err) {}
  });
  
  $(document).bind('pageinit', function() {
    $( "#sortable" ).sortable({
       items: "li:not(.ui-li-divider)"
    });
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
    $( "#sortable" ).bind( "sortstop", function(event, ui) {
      $('#sortable').listview('refresh');
    } );
  } );
  
  </script>  
  <style type="text/css">
	.div_arrange
	{
		float:left; 
		width:150px;
		border-radius:5px;
		border:1px gray solid;
		text-align:center;
		height:30px;
	}
  </style>
{include file="./headEnd.tpl"}
<form type="POST" action="{$smarty.server.PHP_SELF}" data-ajax="false" name="form1" >
<input type="hidden" name="time" value="{$routeTime}" />  
<div>
  <div data-role="content" data-theme="c" >
    <ul data-role="listview" data-inset="true" data-theme="d" id="sortable">
      <li data-role="list-divider">{$routeText} List</li>
      {section name="sec" loop=$res1}
      <li><input type="hidden" name="customerId[]" value="{$res1[sec].customerId}" />
<!--        <table width="100%" cellpadding="3" cellspacing="15">
          <tr>
            <th>
             {$res1[sec].nickName} 
            </th>
            <th>
             {$res1[sec].name} 
            </th>
            <th>
             {$res1[sec].grnNo} 
            </th>
          </tr>
        </table>
        -->
        <div class="div_arrange">{$res1[sec].nickName}</div>
        <div class="div_arrange">{$res1[sec].name}</div>
        <div class="div_arrange">{$res1[sec].grnNo}</div>
      </li>
      {sectionelse}
        <li>No Record Found</li>
      {/section}  	
    </ul>
    <input type="submit" value="submit" name="submitBtn">
  </div>
</div>
</form>
{include file="./footer.tpl"}