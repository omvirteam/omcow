{include file="./headStart.tpl"}
{include file="./headEnd.tpl"}
<div class="row">
<div class="col-lg-6 col-md-offset-3">
	<div class="panel panel-primary">
	  <div class="panel-heading">
		<i class="fa fa-edit fa-fw"></i>Account Entry
	  </div>
	  <div class="panel-body">
            <div class="row col-lg-13">
                <form method="POST" action="{$smarty.server.PHP_SELF}" class="formBox form-horizontal" role="form">

                  <div class="form-group">
                    <label class="control-label col-sm-4">Account Type</label>
                    <div class="col-sm-5">
                          <select name="accountTypeId" class="form-control input-sm" REQUIRED >
                            <option value="">Select Type</option>
                            {html_options values=$accountTypeArr.accountTypeId output=$accountTypeArr.accountType}
                          </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-sm-4">Account Name</label>
                    <div class="col-sm-8">
                      <input type="text" value="" name="accountName" REQUIRED class="form-control input-sm" />
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-sm-4">Place</label>
                    <div class="col-sm-5">
                          <select name="placeId" class="form-control input-sm">
                            <option value="0">Select Place</option>
                            {html_options values=$placeArr.placeId output=$placeArr.place}
                          </select>
                    </div>
                  </div>

                    <div class="form-group">
                      <label class="control-label col-sm-4">Contact No</label>
                      <div class="col-sm-3">
                          <input type="text" value="" name="contactNo" class="form-control input-sm" />
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-sm-4">Notes</label>
                      <div class="col-sm-7">
                        <textarea name="notes" rows="5" cols="25" class="form-control"></textarea>
                      </div>
                    </div>

                  <div class="form-group">
                    <label class="control-label col-sm-4"></label>
                    <div class="col-sm-7">
                            <button class="btn btn-success btn-lg" name="submitBtn" type="submit">Submit</button>
                    </div>
                  </div>

                </form>
            </div><!-- /.row col-lg-13 -->
	  </div><!-- /.panel-body -->
	</div><!-- /.panel -->
</div><!-- /.col-lg-12 -->
</div><!-- /.row -->

{include file="./footer.tpl"}