{include file="./headStart.tpl"}
<script>
var _gaq = _gaq || [];
$(document).ready(function(e) {
  $('.milkGiven').change(function(){
  	var totMilkGiven = 0;
		$(".milkGiven").each(function( i ) {
			if($(this).val() != '' && $(this).val() > 0)
			{
		  	totMilkGiven += parseFloat($(this).val()); 
			}
		});
		
		$('#totMilkGiven').val(totMilkGiven);
  });
  
});
</script>
{include file="./headEnd.tpl"}

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
    <div class="panel-heading">Milk Receive {if $distStaffIdId > 0} <b>For : {$distStaffIdName}</b >{/if}</div><!-- /.panel-heading -->
      <div class="panel-body">
        <form method="POST" id="milkTimeForm" name="form1" action="{$smarty.server.PHP_SELF}" class="formBox form-horizontal" role="form">
          <input type="hidden" name="staffId" value="{$distStaffIdId}" />
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th style="text-align:center;" >
                    Milk Time : 
          			    	<select style="width:120px;" name="milkTime" onchange="this.form.submit();" id="milkTime" class="input-sm" >
              				  <option value="M" {if $milkTime == 'M'}SELECTED{/if}>Morning</option>
              				  <option value="E" {if $milkTime == 'E'}SELECTED{/if}>Evening</option>
              				</select>
                  </th>                  
                  <th style="text-align:center;"  nowrap>
                    Milk Date : 
{html_select_date prefix="milkDate" start_year="-1" end_year="+0" field_order="DMY" day_value_format="%02d" all_extra="style='width:120px;' class='input-sm'"}                    
                  </th>
                </tr>
              </thead>
            </table>
          </div>
        <!-- /.table-responsive -->          
          <div class="table-responsive">
            <table border="1" class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th style="text-align:center;"  nowrap>Name</th>
                  <th style="text-align:center;"  nowrap>Current</th>
                  <th style="text-align:center;" >Receive </th>
                </tr>
              </thead>
              <tbody>
                {section name="sec" loop=$custArrLoop}
          			  <tr>
          				  <td align="right">
          				    <input type="hidden" size="2" value="{$custArrLoop[sec]['customerId']}" name="customerId[]" />
          				    {$custArrLoop[sec]['name']}
          				  </td>
          				  <td align="right">
          				    {$custArrLoop[sec]['currentAmt']} {$custArrLoop[sec]['currentDrCr']}
          				  </td>
          				  <td align="center">
            			    <input type="text" size="3" style="width:60px;" name="milkRec[]" 
                      		tabindex="{$smarty.section.sec.iteration}" class="milkGiven form-control input-sm" 
                        	{if $smarty.section.sec.iteration == 1} autofocus{/if}/>
                      <input type="hidden" size="3" style="width:60px;" name="currentAmt[]" value="{$custArrLoop[sec]['currentAmt']}"/>
          				  </td>
         			    </tr>
         			  {sectionelse}
          			  <tr>
          				  <td align="center" colspan="4">No Record Found</td>
         			    </tr>   			  
                {/section}
          			  <tr>
          				  <th style="text-align:right" colspan="2">
          				  	Total :
          				  </th>
          				  <td align="center">
            			    <input type="text" size="3" name="totMilkGiven" id="totMilkGiven" DISABLED class="form-control" />
          				  </td>
          				  <td align="left" NOWRAP>
          				  	&nbsp;
          				  </td>
         			    </tr>
              </tbody>
            </table>
          </div>
          <div class="form-group">
      		<label class="control-label col-sm-5"></label>
      			<div class="col-sm-7">
      			    <input type="submit" data-theme="b" class="btn btn-success btn-lg" name="submitBtn" id="submitBtn" value="Submit">
      			</div>
          </div>          
        <!-- /.table-responsive -->
      </form>
      </div>
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-6 -->
</div>
{include file="./footer.tpl"}