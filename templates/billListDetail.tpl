{include file="./headStart.tpl"}

<!-- Page-Level Plugin CSS - Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

<!-- Page-Level Plugin Scripts - Tables -->
<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

<script type="text/javascript">
</script>
{include file="./headEnd.tpl"}
<div class="row">
  <div class="col-lg-7 col-md-offset-2">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <i class="fa fa-list-ul fa-fw"></i> Bill
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover" id="billList">
            <thead>
              <tr>
                <th align="center" colspan="2">Name : {$billArray[0].customerName}</th>
                <th align="center" >GRN : {$billArray[0].grn}</th>
                <th align="center" >Date : {$billArray[0].transDate}</th>
              </tr>
            </thead>
            <thead>
              <tr>
                <th align="center">Product</th>
                <th align="center">Rate</th>
                <th align="center">Quantity</th>
                <th align="center">Amount</th> 
              </tr>
            </thead>
            <tbody>
              {section name="sec" loop=$billArray}
                <tr>
                  <td align="center">{$billArray[sec].productName}</td>
                  <td align="right">{$billArray[sec].rate}</td>
                  <td align="right">{$billArray[sec].totalQty}</td>
                  <td align="right">{$billArray[sec].amount}</td>
                </tr>
              {/section}
            </tbody>
            <tbody>
              <tr>
                <td align="right" colspan="3"><b>Total</b></td>
                <td align="right" ><b>{$totAmount}</b></td>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <td align="center" colspan="2">Route : {$billArray[0].milkTime}</td>
                <td align="center" colspan="2">Distributor : {$distName}</td>
              </tr>
            </tbody>
            <tr>
              <td align="center" colspan="4">Gir gangaa helpline 94096 92692, 0281 2561198</td>
            </tr>
            </table>
              </div>
            </div>
          </div>
        </div>
      </div>
{include file="./footer.tpl"}               
