{include file="./headStart.tpl"}

<!-- Page-Level Plugin CSS - Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

<!-- Page-Level Plugin Scripts - Tables -->
<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#staffList').dataTable({
      "aoColumnDefs": [{ 'bSortable': false, 'aTargets': [0, -3] }],
      "aaSorting": [[1, "asc"]]
    });
  });
</script>

{include file="./headEnd.tpl"}

<div class="row">
  <div class="col-lg-10 col-md-offset-1">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <i class="fa fa-list-ul fa-fw"></i> Staff List
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover" id="staffList">
            <thead>
              <tr>
                <th align="center">&nbsp;</th>
                <th align="center">&nbsp;</th>
                <th align="center">Staff Type</th>
                <th align="center">Place</th>
                <th align="center">Parent Staff Name</th>
                <th align="center">Name</th> 
                <th align="center">Joining Date</th>
                <th align="center">Commission</th>
                <th align="center">Salary</th>
              </tr>
            </thead>
            <tbody>
              {section name="sec" loop=$staffArray}
                <tr>
                  <td align="center">
                    <a href="staffDelete.php?staffId={$staffArray[sec].staffId}" onclick="return confirm('Are You Sure To Delete??');"  class="link">
                      <img src="./images/deleteIcon.png" />
                    </a>
                  </td>
                  <td align="center">
                    {if $staffArray[sec].staffTypeId eq 1} {* 1 = Distributor *}
                      <a href="commonRoute.php?time=M&staffId={$staffArray[sec].staffId}">M</a> 
                      | 
                      <a href="commonRoute.php?time=E&staffId={$staffArray[sec].staffId}">E</a>
                      | 
                      <a href="milkSell.php?milkTime=M&staffId={$staffArray[sec].staffId}&milkDate={$smarty.now|date_format:'%Y-%m-%d'}">MSell</a>
                      | 
                      <a href="milkSell.php?milkTime=E&staffId={$staffArray[sec].staffId}&milkDate={$smarty.now|date_format:'%Y-%m-%d'}">ESell</a>
                      | 
                      <a href="receive.php?staffId={$staffArray[sec].staffId}">Receive</a>
                      |
                      <a href="staffDetailTrans.php?staffId={$staffArray[sec].staffId}">Ledger</a>
                    {else}
                      &nbsp;
                    {/if}</td>
                    <td align="center">{$staffArray[sec].staffType}</td>
                    <td align="center">{$staffArray[sec].placeName}</td>
                    <td align="center">{$staffArray[sec].parentName}</td>
                    <td align="center">{$staffArray[sec].name}</td>
                    <td align="center">{$staffArray[sec].joinDate}</td>
                    <td align="center">{$staffArray[sec].commission}</td>
                    <td align="center">{$staffArray[sec].salary}</td>
                  </tr>
                  {/section}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      {include file="./footer.tpl"}
