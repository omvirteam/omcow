{include file="./headStart.tpl"}

<script type="text/javascript">
$(document).ready(function() {
    $('#staffEntryForm').bootstrapValidator({
	message: 'This value is not valid',
	feedbackIcons: {
	    valid: 'glyphicon glyphicon-ok',
	    invalid: 'glyphicon glyphicon-remove',
	    validating: 'glyphicon glyphicon-refresh'
	},
	submitButtons: 'button[type="submit"]',
	submitHandler: function(validator, form, submitButton) {
	    validator.defaultSubmit();
	},
	fields: {
	    userName: { validators: { notEmpty: { message: 'The is required' } } },
	    pass: { validators: { notEmpty: { message: 'The is required' }, identical: { field: 'repass', message: 'The password and its retype password are not the same' } } },
	    repass: { validators: { notEmpty: { message: 'The is required' }, identical: { field: 'pass', message: 'The password and its retype password are not the same' } } },
	    name: { validators: { notEmpty: { message: 'The is required' } } }
	}
    });
});
</script>

{include file="./headEnd.tpl"}

<div class="row">
    <div class="col-lg-6 col-md-offset-3">
	    
	{if $staff_entry_error != ""}
	    <div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		{$staff_entry_error}
	    </div>
	{/if}

	{if $staff_entry_success != ""}
	    <div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		{$staff_entry_success}
	    </div>
	{/if}
	    
	<div class="panel panel-primary">
	    <div class="panel-heading">
		<i class="fa fa-edit fa-fw"></i> Staff Entry
	    </div>
	    <div class="panel-body">
		<div class="row col-lg-13">
		    <form id="staffEntryForm" method="POST" action="staff.php" class="formBox form-horizontal" role="form">
			
			<div class="form-group">
			    <label class="control-label col-sm-4">Select User Type</label>
			    <div class="col-sm-5">
				<select name="staffTypeId" class="form-control input-sm">
				  {html_options values=$staffArray.staffTypeId output=$staffArray.staffType}
				</select>
			    </div>
			</div>
				
			<div class="form-group">
			    <label class="control-label col-sm-4">Select Place</label>
			    <div class="col-sm-5">
				<select name="placeId" class="form-control input-sm">
				  {html_options values=$placeArray.placeId output=$placeArray.place}
				</select>
			    </div>
			</div>
				
			<div class="form-group">
			    <label class="control-label col-sm-4">Select Parent Staff</label>
			    <div class="col-sm-5">
				<select name="parentStaffId" class="form-control input-sm">
				  {html_options values=$parentStaff.staffId output=$parentStaff.name}
				</select>
			    </div>
			</div>
				
			<div class="form-group">
			    <label class="control-label col-sm-4">User Name <sup>*</sup></label>
			    <div class="col-sm-7">
				<input type="text" value="" name="userName" id="userName" class="form-control input-sm" />
			    </div>
			</div>
				
			<div class="form-group">
			    <label class="control-label col-sm-4">Password <sup>*</sup></label>
			    <div class="col-sm-7">
				<input type="password" value="" name="pass" id="pass" class="form-control input-sm" />
			    </div>
			</div>
				
			<div class="form-group">
			    <label class="control-label col-sm-4">Retype Password <sup>*</sup></label>
			    <div class="col-sm-7">
				<input type="password" value="" name="repass" id="repass" class="form-control input-sm" />
			    </div>
			</div>
				
			<div class="form-group">
			    <label class="control-label col-sm-4">Name <sup>*</sup></label>
			    <div class="col-sm-7">
				<input type="text" value="" name="name" id="name" class="inputUpperCase form-control input-sm" />
			    </div>
			</div>
			
			<div class="form-group">
			    <label class="control-label col-sm-4">Phone No.</label>
			    <div class="col-sm-7">
				<input type="text" value="" name="phone" id="phone" class="inputUpperCase form-control input-sm" />
			    </div>
			</div>
				
			<div class="form-group">
			    <label class="control-label col-sm-4">Joining Date</label>
			    <div class="col-sm-8">
				{html_select_date prefix="joinDate" start_year="-1" end_year="+0" field_order="DMY" day_value_format="%02d" all_extra="class='form-control date-controls pull-left input-sm'"}
			    </div>
			</div>
			    
			<div class="form-group">
			    <label class="control-label col-sm-4">Commission</label>
			    <div class="col-sm-3">
				<input type="text" value="" name="commission" id="commission" class="form-control input-sm" />
			    </div>
			</div>
			    
			<div class="form-group">
			    <label class="control-label col-sm-4">Salary</label>
			    <div class="col-sm-3">
				<input type="text" value="" name="salary" id="salary" class="form-control input-sm" />
			    </div>
			</div>
			    
			<div class="form-group">
			    <label class="control-label col-sm-4"></label>
			    <div class="col-sm-7">
				<button class="btn btn-success btn-lg" type="submit">Submit</button>
			    </div>
			</div>
			
		    </form>
		</div>
	    </div>
	    <!-- /.panel-body -->
	</div>
	<!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

{include file="./footer.tpl"}