{include file="./headStart.tpl"}
<script type="text/javascript">
$(document).ready(function(){
	$("#cowId").change(function(){
		$.ajax({
		    url: 'getStallAj.php',
		    type: 'GET',
		    data: "cowId="+$('#cowId').val(), 
		    success: function (data) {
		        $('#staffId').val(data);
		    }
		});		
	});
	
  $('#litre').change(function() {
		var litre = $('#litre').val();   	
  	$('#litre').val(parseFloat(litre).toFixed(3));
  });
});

</script>

{include file="./headEnd.tpl"}

<div class="row">
<div class="col-lg-6 col-md-offset-3">
	<div class="panel panel-primary">
	  <div class="panel-heading">
		  <i class="fa fa-edit fa-fw"></i>Milk Entry
	  </div>
	  <div class="panel-body">
		  <div class="row col-lg-13">
		    <form id="staffEntryForm" method="POST" action="{$smarty.server.PHP_SELF}" class="formBox form-horizontal" role="form">
			
  			<div class="form-group">
  			  <label class="control-label col-sm-4">Select Cow</label>
  			  <div class="col-sm-5">
    				<select name="cowId" id="cowId" class="form-control input-sm" REQUIRED >
    				  <option value="">Select Cow</option>
    				  {html_options values=$cowArray.cowId output=$cowArray.name}
    				</select>
  			  </div>
  			</div>
				
			  <div class="form-group">
			    <label class="control-label col-sm-4">Milk Date</label>
			    <div class="col-sm-8">
				    {html_select_date prefix="milkDate" start_year="-1" end_year="+0" field_order="DMY" day_value_format="%02d" all_extra="class='form-control date-controls pull-left input-sm'"}
			    </div>
			  </div>

  			<div class="form-group">
  			  <label class="control-label col-sm-4">Milk Time</label>
  			  <div class="col-sm-5">
    				<select name="milkTime" class="form-control input-sm">
    				  <option value="M" {if $milkTimeSelected == 'M'}SELECTED{/if}>Morning</option>
    				  <option value="E" {if $milkTimeSelected == 'E'}SELECTED{/if}>Evening</option>
    				</select>
  			  </div>
  			</div>
			    
			  <div class="form-group">
			    <label class="control-label col-sm-4">Litre</label>
			    <div class="col-sm-3">
				    <input type="text" name="litre" id="litre" REQUIRED class="form-control input-sm" />
			    </div>
			  </div>

  			<div class="form-group">
  			  <label class="control-label col-sm-4">Select Staff</label>
  			  <div class="col-sm-5">
    				<select name="staffId" id="staffId" class="form-control input-sm" REQUIRED>
    				  <option value="">Select Staff</option>
    				  {html_options values=$staffArray.staffId output=$staffArray.name}
    				</select>
  			  </div>
  			</div>
				
			  <div class="form-group">
				  <label class="control-label col-sm-4">Notes</label>
				  <div class="col-sm-7">
				    <textarea name="notes" rows="5" cols="25" class="form-control"></textarea>
				  </div>
			  </div>
			    
  			<div class="form-group">
  			  <label class="control-label col-sm-4"></label>
  			  <div class="col-sm-7">
  				  <button class="btn btn-success btn-lg" name="submitBtn" type="submit">Submit</button>
  			  </div>
  			</div>
			
		    </form>
		  </div><!-- /.row col-lg-13 -->
	  </div><!-- /.panel-body -->
	</div><!-- /.panel -->
</div><!-- /.col-lg-12 -->
</div><!-- /.row -->

{include file="./footer.tpl"}