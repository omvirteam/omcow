{include file="./headStart.tpl"}

<script type="text/javascript">
$(document).ready(function() {
    $('#cowEntryForm').bootstrapValidator({
	message: 'This value is not valid',
	feedbackIcons: {
	    valid: 'glyphicon glyphicon-ok',
	    invalid: 'glyphicon glyphicon-remove',
	    validating: 'glyphicon glyphicon-refresh'
	},
	submitButtons: 'button[type="submit"]',
	submitHandler: function(validator, form, submitButton) {
	    validator.defaultSubmit();
	},
	fields: {
	    name: { validators: { notEmpty: { message: 'The is required' } } },
	    grn: { validators: { notEmpty: { message: 'The is required' } } }
	}
    });
});
</script>

{include file="./headEnd.tpl"}

    <div class="row">
	<div class="col-lg-6 col-md-offset-3">
	    
	    {if $cow_entry_error != ""}
		<div class="alert alert-danger alert-dismissable">
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		    {$cow_entry_error}
		</div>
	    {/if}
	    
	    {if $cow_entry_success != ""}
		<div class="alert alert-success alert-dismissable">
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		    {$cow_entry_success}
		</div>
	    {/if}
	    
    <div class="panel panel-primary">
		<div class="panel-heading">
		    <i class="fa fa-edit fa-fw"></i> Add Cow Details
		</div>
		<div class="panel-body">
	    <div class="row col-lg-12">
			<form id="cowEntryForm" method="POST" action="cowEntry.php" class="formBox form-horizontal" role="form">

			  <div class="form-group">
				<label class="control-label col-sm-4">Name <sup>*</sup></label>
					<div class="col-sm-7">
					    <input type="text" value="" name="name" id="name" class="inputUpperCase form-control input-sm" />
					</div>
			  </div>

			    <div class="form-group">
				<label class="control-label col-sm-4">G.R.N. <sup>*</sup></label>
				<div class="col-sm-7">
				    <input type="text" value="" name="grn" id="grn" class="form-control input-sm" />
				</div>
			    </div>

			    <div class="form-group">
				<label class="control-label col-sm-4">Cow Type</label>
				<div class="col-sm-5">
				    <select name="cowTypeId" class="form-control input-sm">
					{html_options values=$cowArray.cowTypeId output=$cowArray.type}
				    </select>
				</div>
			    </div>

			    <div class="form-group">
				<label class="control-label col-sm-4">Birth Date</label>
				<div class="col-sm-8">
				    {html_select_date prefix="birthDate" start_year="-15" end_year="+3" field_order="DMY" day_value_format="%02d" all_extra="class='form-control date-controls pull-left input-sm'"}
				</div>
			    </div>
				
			    <div class="form-group">
				<label class="control-label col-sm-4">Mother Name</label>
				<div class="col-sm-5">
				    <select name="motherId" class="form-control input-sm">
					{html_options values=$motherArray.cowId output=$motherArray.name}
				    </select>
				</div>
			    </div>
				    
			    <div class="form-group">
				<label class="control-label col-sm-4">Father Name</label>
				<div class="col-sm-5">
				    <select name="fatherId" class="form-control input-sm">
					{html_options values=$fatherArray.cowId output=$fatherArray.name}
				    </select>
				</div>
			    </div>
				    
			    <div class="form-group">
				<label class="control-label col-sm-4">Dhankhut Name</label>
				<div class="col-sm-5">
				    <select name="dhankhutId" class="form-control input-sm">
					{html_options values=$fatherArray.cowId output=$fatherArray.name}
				    </select>
				</div>
			    </div>
				    
			    <div class="form-group">
				<label class="control-label col-sm-4">Milk Per Day</label>
				<div class="col-sm-3">
				    <input type="text" value="" name="milkPerDay" id="milkPerDay" class="form-control input-sm" />
				</div>
			    </div>
				    
			    <div class="form-group">
				<label class="control-label col-sm-4">Purchase Rate</label>
				<div class="col-sm-3">
				    <input type="text" value="" name="purchaseRate" id="purchaseRate" class="form-control input-sm" />
				</div>
			    </div>
				    
			    <div class="form-group">
				<label class="control-label col-sm-4">Purchased From</label>
				<div class="col-sm-7">
				    <input type="text" value="" name="purchasedFrom" id="purchasedFrom" class="form-control input-sm" />
				</div>
			    </div>
				    
			    <div class="form-group">
				<label class="control-label col-sm-4">Purchase Date</label>
				<div class="col-sm-8">
				    {html_select_date prefix="purchaseDate" start_year="-15" end_year="+3" field_order="DMY" day_value_format="%02d" day_empty="Day" month_empty="Month" year_empty="Year" time=null all_extra="class='form-control date-controls pull-left input-sm'"}
				</div>
			    </div>
				
			    <div class="form-group">
				<label class="control-label col-sm-4">Dwarning Date</label>
				<div class="col-sm-8">
				    {html_select_date prefix="dwarningDate" start_year="-15" end_year="+3" field_order="DMY" day_value_format="%02d" day_empty="Day" month_empty="Month" year_empty="Year" time=null all_extra="class='form-control date-controls pull-left input-sm'"}
				</div>
			    </div>
				
			    <div class="form-group">
				<label class="control-label col-sm-4">Milk Close Date</label>
				<div class="col-sm-8">
				    {html_select_date prefix="milkCloseDate" start_year="-15" end_year="+3" field_order="DMY" day_value_format="%02d" day_empty="Day" month_empty="Month" year_empty="Year" time=null all_extra="class='form-control date-controls pull-left input-sm'"}
				</div>
			    </div>
				
			    <div class="form-group">
				<label class="control-label col-sm-4">Staff Name</label>
				<div class="col-sm-5">
				    <select name="staffId" class="form-control input-sm">
					{html_options values=$staffArray.staffId output=$staffArray.name}
				    </select>
				</div>
			    </div>
				    
			    <div class="form-group">
				<label class="control-label col-sm-4">Is Died?</label>
				<div class="col-sm-3">
				    <select name="isDied" class="form-control input-sm">
					{html_options values=$isDiedValues output=$isDiedOutput}
				    </select>
				</div>
			    </div>
				    
			    <div class="form-group">
				<label class="control-label col-sm-4">Notes</label>
				<div class="col-sm-7">
				    <textarea name="notes" rows="5" cols="25" class="form-control"></textarea>
				</div>
			    </div>
				    
		    <div class="form-group">
				<label class="control-label col-sm-4"></label>
				<div class="col-sm-7">
				    <button class="btn btn-success btn-lg" type="submit">Submit</button>
				</div>
		    </div>
				    
			</form>
		    </div>
		    <!-- /.row (nested) -->
		    
		</div>
		<!-- /.panel-body -->
	    </div>
	    <!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

{include file="./footer.tpl"}