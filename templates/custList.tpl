{include file="./headStart.tpl"}

<!-- Page-Level Plugin CSS - Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

<!-- Page-Level Plugin Scripts - Tables -->
<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#cowList').dataTable({
	"aoColumnDefs": [{ 'bSortable': false, 'aTargets': [ 0,1,2,-1 ] }],
	"aaSorting": [[ 3, "asc" ]]
    });
});
</script>

{include file="./headEnd.tpl"}

<div class="row">
    <div class="col-lg-12" style="width:auto">
	  <div class="panel panel-primary">
	    <div class="panel-heading">
		    <i class="fa fa-list-ul fa-fw"></i> Customer List
	    </div>
	  <div class="panel-body">
  		<div class="table-responsive">
  		<table class="table table-striped table-bordered table-hover" id="cowList">
  			<thead>
  			  <tr>
						<th class="table1">&nbsp;</th>
						<th class="table1">&nbsp;</th>
						<th class="table1">&nbsp;</th>
						<th class="table1">Grn No</th>
						<th class="table1">Customer Type</th>
						<th class="table1">Dist Name</th>
						<th class="table1">Milk Time</th>
						<th class="table1">Morning Sequence</th>
            <th class="table1">Evening Sequence</th>
						<th class="table1">Current Litre</th>
						<th class="table1">Rate</th>
						<th class="table1">Name</th>
						<th class="table1">Nick Name</th>
						<th class="table1">Phone 1</th>
						<th class="table1">Phone 2</th>
						<th class="table1">Join Date</th>
						<th class="table1">Current Amount</th>
						<th class="table1">Collection Date</th>
						<th class="table1">Notes</th>
  				</tr>
  			</thead>
  			<tbody>
				  {section name="sec" loop=$custArray}
				  <tr>
				  	<td class="table1" align="center"><a href="custDelete.php?customerId={$custArray[sec].customerId}" onclick="return confirm('Are You Sure To Delete??');"  class="link"><img src="./images/deleteIcon.png"></a></td>
				  	<td class="table1" align="center"><a href="customer.php?custId={$custArray[sec].customerId|base64_encode}"  class="link"><img src="./images/edit.png"></a></td>
				  	<td class="table1" align="center"><a href="custDetailTrans.php?cid={$custArray[sec].customerId}">Ledger</a></td>
				  	<td class="table1" align="center">{$custArray[sec].grnNo}</td>
				  	<td class="table1" align="center">{$custArray[sec].customerType}</td>
				  	<td class="table1" align="center">{$custArray[sec].distName}</td>
				  	<td class="table1" align="center">{$custArray[sec].milkTime}</td>
				  	<td class="table1" align="center">{$custArray[sec].morningSequence}</td>
            <td class="table1" align="center">{$custArray[sec].eveningSequence}</td>
				  	<td class="table1" align="center">{$custArray[sec].currentLitre}</td>
				  	<td class="table1" align="center">{$custArray[sec].rate}</td>
				  	<td class="table1" align="center">{$custArray[sec].name}</td>
				  	<td class="table1" align="center">{$custArray[sec].nickName}</td>
				  	<td class="table1" align="center">{$custArray[sec].phone1}</td>
				  	<td class="table1" align="center">{$custArray[sec].phone2}</td>
				  	<td class="table1" align="center" NOWRAP>{$custArray[sec].joinDate}</td>
				  	<td class="table1" align="center">{$custArray[sec].currentAmt} {$custArray[sec].currentDrCr}</td>
				  	<td class="table1" align="center" NOWRAP>{$custArray[sec].collectionDate}</td>
				  	<td class="table1" align="center">{$custArray[sec].notes}</td>
				  </tr>
				  {/section}
  			</tbody>
  		</table>
  		</div>
	  </div>
	</div>
    </div>
</div>

{include file="./footer.tpl"}