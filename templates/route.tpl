{include file="./headStart.tpl"}
  <link rel="stylesheet" href="./css/jquery.mobile-1.4.0-alpha.2.min.css" />
  <script src="./js/jquery.mobile-1.4.0-alpha.2.min.js"></script>
  <script src="./js/jquery-ui.js"></script>
  <script src="./js/jquery.ui.touch-punch.min.js"></script>
  <script>
  var _gaq = _gaq || [];
  $(document).ready(function(e) {
    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + 
                  '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  }); 

  $(document).on("pageshow", function(event, ui) {
    try {
      _gaq.push(['_setAccount', 'UA-28940436-2']);    
      if ($.mobile.activePage.attr("data-url")) {
            _gaq.push(['_trackPageview', $.mobile.activePage.attr("data-url")]);
        } else {
            _gaq.push(['_trackPageview']);
        }
    } catch(err) {}
  });
  
  $(document).bind('pageinit', function() {
    $( "#sortable" ).sortable({
       items: "li:not(.ui-li-divider)"
    });
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
    $( "#sortable" ).bind( "sortstop", function(event, ui) {
      $('#sortable').listview('refresh');
    } );
  } );
  </script>  
{include file="./headEnd.tpl"}
<div>
  <div data-role="content" data-theme="c">
  
    <ul data-role="listview" data-inset="true" data-theme="d" id="sortable">
      <li data-role="list-divider">Morning List</li>
      <li>Item 1</li>
      <li>Item 2</li>
      <li>Item 3</li>
      <li>Item 4</li>
      <li>Item 5</li>
    </ul>
  </div>
</div>
{include file="./footer.tpl"}