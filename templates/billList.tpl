{include file="./headStart.tpl"}

<!-- Page-Level Plugin CSS - Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

<!-- Page-Level Plugin Scripts - Tables -->
<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#billList').dataTable({
      "aoColumnDefs": [{ 'bSortable': false, 'aTargets': [0, -3] }],
      "aaSorting": [[1, "asc"]]
    });
  });
</script>

{include file="./headEnd.tpl"}

<div class="row">
  <div class="col-lg-10 col-md-offset-1">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <i class="fa fa-list-ul fa-fw"></i> Bill List
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover" id="billList">
            <thead>
              <tr>
                <th align="center">&nbsp;</th>
                <th align="center">Name</th>
                <th align="center">Amount</th>
                <th align="center">Date</th>
              </tr>
            </thead>
            <tbody>
              {section name="sec" loop=$billArray}
                <tr>
                  <td align="center">
                    <img src="./images/deleteIcon.png" />
                  </td>
                  <td align="right">
                      <a href="billListDetail.php?customerIdDr={$billArray[sec].customerIdDr}&transDate={$billArray[sec].transDateYMD}" >
                        {$billArray[sec].customerName}
                      </a>
                  </td>
                  <td align="right">{$billArray[sec].amount}</td>
                  <td align="center">{$billArray[sec].transDate}</td>
                </tr>
                {/section}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      {include file="./footer.tpl"}
