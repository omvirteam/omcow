{include file="./headStart.tpl"}

<link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
<script>
$(document).ready(function(){
	$("#staffs").change(function(){
		var id = $(this).val();
		window.location = "staffDetailTrans.php?staffId="+id;
	});
});
</script>
{include file="./headEnd.tpl"}
<div class="row">
  <div class="col-lg-12">
	  <div class="panel panel-primary">
	    <div class="panel-heading">
		    <i class="fa fa-list-ul fa-fw"></i> Transection Detail
	    </div>
	  <div class="panel-body">
  		<div class="table-responsive">
  			<select name="staffs" id="staffs">
  				{section name="sec1" loop=$staffs}
  					<option value={$staffs[sec1].id} {if $staffId == $staffs[sec1].id}selected="selected"{/if}>{$staffs[sec1].name}</option>
  				{/section}
  			</select>
  			<table class="table table-striped table-bordered table-hover" id="cowList">
	  			<thead>
	  				<tr>
	  					<th>Date</th>
	  					<th>Account</th>
                        <th>Amount</th>
	  					<th>Credit/Debit</th>
	  				</tr>
	  			</thead>
	  			<tbody>
	  			{if $staffArr|@count > 0}
	  				{section name="sec" loop=$staffArr}
		  				<tr>
		  					<td>{$staffArr[sec].transDate}</td>
                            <td>{$staffArr[sec].oppName}</td>
		  					<td align="right">{$staffArr[sec].amount}</td>
		  					<td>{if $staffArr[sec].staffIdCr == $staffId}Credit{else}Debit{/if}</td>
		  				</tr>
	  				{/section}
	  			{else}
	  				<tr><td colspan=3 align="center">Record not found</td></tr>
	  			{/if}
	  			</tbody>
  			<table>
  		</div>
	  </div>
{include file="./footer.tpl"}