{include file="./headStart.tpl"}

{include file="./headEnd.tpl"}
<div class="row">
    <div class="col-lg-6 col-md-offset-3">
		{if $item_vaccination_error != ""}
	    <div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		{$item_vaccination_error}
	    </div>
	{/if}

	{if $item_vaccination_success != ""}
	    <div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		{$item_vaccination_success}
	    </div>
	{/if}
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-edit fa-fw"></i> Cow Vaccination	
			</div>
			<div class="panel-body">
				<div class="row col-lg-13">
					<form id="itemCreateForm" method="POST" action="{$smarty.server.PHP_SELF}" class="formBox form-horizontal" role="form">
						<div class="form-group">
							<label class="control-label col-sm-4">Select vaccineType</label>
							<div class="col-sm-5">
								<select name="vaccine" id="vaccine" class="form-control input-sm">
									{html_options values=$vaccineArray.vaccineTypeId output=$vaccineArray.vaccineName}
								</select>
							</div>							
						</div>
						
						<div class="form-group">
							<label class="control-label col-sm-4">Select Cow</label>
							<div class="col-sm-5">
								<select name="cowId" id="cowId" class="form-control input-sm">
									{html_options values=$cowArray.cowId output=$cowArray.name selected=$selectedCow}
								</select>
							</div>							
						</div>
						
						<div class="form-group">
								<label class="control-label col-sm-4">Vaccine Date</label>
								<div class="col-sm-8">
									{html_select_date prefix="vacc" start_year="-1" end_year="+0" field_order="DMY" day_value_format="%02d" all_extra="class='form-control date-controls pull-left input-sm'"}
								</div>
						</div>						
						
						<div class="form-group">
							<label class="control-label col-sm-4"></label>
								<div class="col-sm-7">
								<button class="btn btn-success btn-lg" type="submit">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
	    <!-- /.panel-body -->
		</div>
	<!-- /.panel -->
	</div>
    <!-- /.col-lg-12 -->
</div>
