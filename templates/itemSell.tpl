{include file="./headStart.tpl"}

<!-- Page-Level Plugin CSS - Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

<!-- Page-Level Plugin Scripts - Tables -->
<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$(document).change(function(){
		var a=$("#item").val();
		$.ajax(
		{
			type:"GET",
			url:"getdata.php",
			data:"itemid="+a,
			success:function(result){
				$("#getUnit").html(result);
			}
		});
	});
	$('#sellList').dataTable({
	"aoColumnDefs": [{ 'bSortable': false, 'aTargets': [ 0,-3 ] }],
	"aaSorting": [[ 1, "asc" ]]
    });
	
	$("#rate").focusout(function(){
		//alert();
		var rate=$("#rate").val();
		var qty=$("#qty").val();
		var amount=rate*qty;
		$("#amt").val(amount);
	});
});
</script>
{include file="./headEnd.tpl"}
<div class="row">
    <div class="col-lg-6 col-md-offset-3">
		{if $item_sell_error != ""}
	    <div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		{$item_sell_error}
	    </div>
	{/if}

	{if $item_sell_success != ""}
	    <div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		{$item_sell_success}
	    </div>
	{/if}
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-edit fa-fw"></i>Item Sell
			</div>
			<div class="panel-body">
				<div class="row col-lg-13">
					<form id="itemDispatchForm" method="POST" action="itemSell.php" class="formBox form-horizontal" role="form">
						<div class="form-group">
							<label class="control-label col-sm-4">Select Item</label>
							<div class="col-sm-5">
								<select name="item" id="item" class="form-control input-sm">
									{html_options values=$itemArray.itemId output=$itemArray.name}
								</select>
							</div>							
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4">Select Customer</label>
							<div class="col-sm-5">
								<select name="customer" id="customer" class="form-control input-sm">
									{html_options values=$customerArray.customerId output=$customerArray.name}
								</select>
							</div>							
						</div>						
						<div class="form-group">
								<label class="control-label col-sm-4">Sell Date</label>
								<div class="col-sm-8">
									{html_select_date prefix="sell" start_year="-1" end_year="+0" field_order="DMY" day_value_format="%02d" all_extra="class='form-control date-controls pull-left input-sm'"}
								</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-sm-4">Quantity</label>
							<div class="col-sm-3">
								<input type="text" value="" name="qty" id="qty" class="form-control input-sm" />								
							</div>
							<label class="" id="getUnit">{$itemUnitVar}</label>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4">Rate</label>
							<div class="col-sm-3">
								<input type="text" value="" name="rate" id="rate" class="form-control input-sm" />								
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4">Amount</label>
							<div class="col-sm-3">
								<input type="text" value="" name="amt" id="amt" class="form-control input-sm" readonly="readonly" />								
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-sm-4">Notes</label>
							<div class="col-sm-7">
								<textarea name="notes" rows="10" cols="40" class="form-control"></textarea>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-sm-4"></label>
								<div class="col-sm-7">
								<button class="btn btn-success btn-lg" type="submit">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
	    <!-- /.panel-body -->
		</div>
	<!-- /.panel -->
	</div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<!-- Display Records-->
<div class="row">
    <div class="col-lg-10 col-md-offset-1">
	<div class="panel panel-primary">
	    <div class="panel-heading">
		<i class="fa fa-list-ul fa-fw"></i> Sell Item List
	    </div>
	    <div class="panel-body">
		<div class="table-responsive">
		    <table class="table table-striped table-bordered table-hover" id="sellList">
			<thead>
			    <tr>
				<th align="center">&nbsp;</th>
				<th align="center">Customer name</th>
				<th align="center">Item name</th>				
				<th align="center">Quantity</th>
				<th align="center">Rate</th>
				<th align="center">Amount</th>
				<th align="center">Sell Date</th>
				<th align="center">Notes</th> 
				
			    </tr>
			</thead>
			<tbody>
			    {section name="sec" loop=$sellArrayTable}
			    <tr>
				<td align="center">
				    <a href="itemSellDelete.php?sellItemId={$sellArrayTable[sec].itemSellId}" onclick="return confirm('Are You Sure To Delete??');"  class="link">
					<img src="./images/deleteIcon.png" />
				    </a>
				</td>
				<td align="center">{$sellArrayTable[sec].customerName}</td>
				<td align="center">{$sellArrayTable[sec].itemName}</td>
				<td align="center">{$sellArrayTable[sec].qty}</td>
				<td align="center">{$sellArrayTable[sec].rate}</td>
				<td align="center">{$sellArrayTable[sec].amount}</td>
				<td align="center">{$sellArrayTable[sec].sellDate}</td>				
				<td align="center">{$sellArrayTable[sec].notes}</td>
	
				
			    </tr>
			    {/section}
			</tbody>
		    </table>
		</div>
	    </div>
	</div>
    </div>
</div>
{include file="./footer.tpl"}