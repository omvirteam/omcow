{include file="./headStart.tpl"}

<link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
<script>
$(document).ready(function(){
	$("#customer").change(function(){
		var id = $(this).val();
		window.location = "custDetailTrans.php?cid="+id;
	});
});
</script>
{include file="./headEnd.tpl"}
<div class="row">
    <div class="col-lg-12">
	  <div class="panel panel-primary">
	    <div class="panel-heading">
		    <i class="fa fa-list-ul fa-fw"></i> Transection Detail
	    </div>
	  <div class="panel-body">
  		<div class="table-responsive">
  			<select name="customer" id="customer">
  				{section name="sec1" loop=$customers}
  					<option value={$customers[sec1].id} {if $custId == $customers[sec1].id}selected="selected"{/if}>{$customers[sec1].name}</option>
  				{/section}
  			</select>
  			<table class="table table-striped table-bordered table-hover" id="cowList">
	  			<thead>
	  				<tr>
	  					<th>Date</th>
	  					<th>Amount</th>
	  					<th>Credit/Debit</th>
                        <th>Notes</th>
	  				</tr>
	  			</thead>
	  			<tbody>
	  			{if $custArr|@count > 0}
	  				{section name="sec" loop=$custArr}
	  				<tr>
	  					<td>{$custArr[sec].transDate}</td>
	  					<td align="right">{$custArr[sec].amount}</td>
	  					<td>{if $custArr[sec].customerIdCr == $custId}Credit{else}Debit{/if}</td>
	  					<td>{$custArr[sec].customerNotes}</td>
	  				</tr>
	  				{/section}
                    <tr><td></td><td><strong>Balance</strong><div style="float:right; display:inline">{$balance}</div></td><td></td><td></td></tr>
	  			{else}
	  				<tr><td colspan=3 align="center">Record not found</td></tr>
	  			{/if}
	  			</tbody>
  			<table>

{include file="./footer.tpl"}