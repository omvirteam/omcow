{include file="./headStart.tpl"}

<!-- Page-Level Plugin CSS - Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

<!-- Page-Level Plugin Scripts - Tables -->
<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#cowList').dataTable({
	"aoColumnDefs": [{ 'bSortable': false, 'aTargets': [ 0,1,2,-1 ] }],
	"aaSorting": [[ 3, "asc" ]]
    });
});
</script>

{include file="./headEnd.tpl"}

<div class="row">
    <div class="col-lg-12">
	  <div class="panel panel-primary">
	    <div class="panel-heading">
		    <i class="fa fa-list-ul fa-fw"></i> Cow List
	    </div>
	  <div class="panel-body">
  		<div class="table-responsive">
  		    <table class="table table-striped table-bordered table-hover" id="cowList">
  			<thead>
  			    <tr>
  				<th align="center">&nbsp;</th>
  				<th align="center">&nbsp;</th>
  				<th align="center">&nbsp;</th>
  				<th align="center">Name</th>
  				<th align="center">GRN</th>
  				<th align="center">Cow Type</th>
  				<th align="center">Mother</th> 
  				<th align="center">Father</th>
  				<th align="center">Dhankhut</th>
  				<th align="center">Milk Per Day</th>
  				<th align="center">Purchase Rate</th>
  				<th align="center">Purchased From</th>
  				<th align="center">Is Died</th>
  				<th align="center">Notes</th>
  			    </tr>
  			</thead>
  			<tbody>
  			    {section name="sec" loop=$cowArray}
  			    <tr>
  				  <td align="center">
  				      <a href="cowDelete.php?cowId={$cowArray[sec].cowId}" onclick="return confirm('Are You Sure To Delete??');"  class="link">
  					  <img src="./images/deleteIcon.png" />
  				      </a>
  				  </td>
  				  {if $cowArray[sec].cowTypeId == 1}
    				  <td align="center"><a href="fariya.php?cowId={$cowArray[sec].cowId}" style="text-decoration:none;">Fari</a></td>
  	  			  <td align="center"><a href="meeting.php?cowTypeId={$cowArray[sec].cowTypeId}&cowId={$cowArray[sec].cowId}" style="text-decoration:none;">Meeting</a> | 
						<a href="vaccination.php?cowId={$cowArray[sec].cowId}" style="text-decoration:none;">Vaccine</a></td>
  				  {else if $cowArray[sec].cowTypeId == 2}
  		  		  <td align="center"></td>
  			  	  <td align="center"><a href="meeting.php?cowTypeId={$cowArray[sec].cowTypeId}&cowId={$cowArray[sec].cowId}" style="text-decoration:none;">Meeting</a> | 
						<a href="vaccination.php?cowId={$cowArray[sec].cowId}" style="text-decoration:none;">Vaccine</a></td>
  				  {else}
  				  <td align="center"></td>
  				  <td align="center"> 
						<a href="vaccination.php?cowId={$cowArray[sec].cowId}" style="text-decoration:none;">Vaccine</a></td>
  				  {/if}
  				  <td align="center">{$cowArray[sec].name}</td>
  				  <td align="center">{$cowArray[sec].grn}</td>
  				  <td align="center">{$cowArray[sec].type}</td>
  				  <td align="center">{$cowArray[sec].motherName}</td> 
  				  <td align="center">{$cowArray[sec].fatherName}</td>
  				  <td align="center">{$cowArray[sec].dhankhutName}</td>
  				  <td align="center">{$cowArray[sec].milkPerDay}</td>
  				  <td align="center">{$cowArray[sec].purchaseRate}</td>
  				  <td align="center">{$cowArray[sec].purchasedFrom}</td>
  				  {if $cowArray[sec].isDied == 0}
  				    <td align="center">No</td>
  				  {else}
  				    <td align="center">No</td>
  				  {/if}
  				  <td align="center">{$cowArray[sec].notes}</td>
  			    </tr>
  			    {/section}
  			</tbody>
  		    </table>
  		</div>
	  </div>
	</div>
    </div>
</div>

{include file="./footer.tpl"}