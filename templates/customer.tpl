{include file="./headStart.tpl"}
{include file="./headEnd.tpl"}
<div class="row">
<div class="col-lg-6 col-md-offset-3">	
<div class="panel panel-primary">
	<div class="panel-heading">
		
	   <i class="fa fa-edit fa-fw"></i>{if $edit == 1} Customer Edit {else} Customer Entry {/if}
	</div>
<div class="panel-body">
  <div class="row col-lg-12">
	<form id="check-user" class="formBox form-horizontal" role="form" data-ajax="false" method="POST" action="customer.php">	
		{if $edit == 1}
			<input type="hidden" name="custId" id="custId" value="{$customer.customerId|base64_encode}" />
		{/if}
	  <div class="form-group">
		<label class="control-label col-sm-4">Select Customer Type:</label>
			<div class="col-sm-5">
        <select name="customerTypeId" class="form-control input-sm" data-native-menu="false">
    	  {html_options values=$customerArray.customerTypeId output=$customerArray.customerType selected=$customer.customerTypeId}
    	  </select>
			</div>
	  </div>	

	  <div class="form-group">
		<label class="control-label col-sm-4">Select Distributor:</label>
			<div class="col-sm-5">
    		<select name="distStaffIdId" class="form-control input-sm" data-native-menu="false">
    		  {html_options values=$distArray.staffId output=$distArray.name selected=$customer.distStaffIdId}
    		</select>
			</div>
	  </div>	

	  <div class="form-group">
		<label class="control-label col-sm-4">Milk Time:</label>
			<div class="col-sm-5">
	    <select name="milkTime" class="form-control input-sm" id="milkTime">
				<option {if $customer.milkTime == 'M'}selected{/if} value="M">Morning</option>
				<option {if $customer.milkTime == 'E'}selected{/if} value="E">Evening</option>
				<option {if $customer.milkTime == 'B'}selected{/if} value="B">Both</option>
	    </select>
			</div>
	  </div>	

	  <div class="form-group">
		<label class="control-label col-sm-4">Current Litre:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control input-sm" value="{$customer.currentLitre}" name="currentLitre" id="currentLitre"/>
			</div>
	  </div>	

	  <div class="form-group">
		<label class="control-label col-sm-4">Rate:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control input-sm" name="rate" id="rate" value="{$customer.rate}"/>
			</div>
	  </div>	

	  <div class="form-group">
		<label class="control-label col-sm-4">Name:</label>
			<div class="col-sm-7">
				<input type="text" class="inputUpperCase form-control input-sm" value="{$customer.name}" name="name" id="name"/> 
			</div>
	  </div>	

	  <div class="form-group">
		<label class="control-label col-sm-4">Nick Name:</label>
			<div class="col-sm-7">
				<input type="text" class="inputUpperCase form-control input-sm" value="{$customer.nickName}" name="nickName" id="nickName"/> 
			</div>
	  </div>	

	  <div class="form-group">
		<label class="control-label col-sm-4">Society:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control input-sm" value="{$customer.society}" name="society" id="society"/>
			</div>
	  </div>	

	  <div class="form-group">
		<label class="control-label col-sm-4">Appartment:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control input-sm" value="{$customer.appartment}" name="appartment" id="appartment"/> 
			</div>
	  </div>	

	  <div class="form-group">
		<label class="control-label col-sm-4">City:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control input-sm" value="{$customer.city}" name="city" id="city"/>
			</div>
	  </div>	

	  <div class="form-group">
		<label class="control-label col-sm-4">Phone 1 :</label>
			<div class="col-sm-7">
				<input type="text" class="form-control input-sm" value="{$customer.phone1}" name="phone1" id="phone1"/>
			</div>
	  </div>	

	  <div class="form-group">
		<label class="control-label col-sm-4">Join Date :</label>
			<div class="col-sm-7">
				{html_select_date prefix="joinDate" start_year="-1" end_year="+0" field_order="DMY" day_value_format="%02d" all_extra="class='form-control date-controls pull-left input-sm'" time=$customer.joinDate}
			</div>
	  </div>	

	  <div class="form-group">
		<label class="control-label col-sm-4">Current Amount:</label>
			<div class="col-sm-7">
				<input type="text" value="{$customer.currentAmt}" class="form-control input-sm" name="currentAmt" id="currentAmt"/>
			</div>
	  </div>	

	  <div class="form-group">
		<label class="control-label col-sm-4">Current DrCr</label>
			<div class="col-sm-7">
	    <select name="currentDrCr" class="form-control input-sm" id="currentDrCr">
				<option {if $customer.currentDrCr == 'dr'}selected {/if} value="dr">Debit</option>
				<option {if $customer.currentDrCr == 'cr'}selected {/if} value="cr">Credit</option>
	    </select>
			</div>
	  </div>	

	  <div class="form-group">
		<label class="control-label col-sm-4">Collection Date :</label>
			<div class="col-sm-7">
	    {html_select_date prefix="collectionDate" start_year="-1" end_year="+0" field_order="DMY" day_value_format="%02d" all_extra="class='form-control date-controls pull-left input-sm'" time=$customer.collectionDate}
			</div>
	  </div>	

	  <div class="form-group">
		<label class="control-label col-sm-4">Notes</label>
			<div class="col-sm-7">
	    <textarea name="notes" rows="10" cols="40" class="form-control">{$customer.notes}</textarea>
			</div>
	  </div>	

    <div class="form-group">
		<label class="control-label col-sm-4"></label>
			<div class="col-sm-7">
				{if $edit == 1}
			    <input type="submit" data-theme="b" class="btn btn-success btn-lg" name="submit" id="submit" value="Edit">
			  {else}
			    <input type="submit" data-theme="b" class="btn btn-success btn-lg" name="submit" id="submit" value="Save">
			  {/if}
			</div>
    </div>

	</form>
	</div><!-- /.col-lg-12 -->
</div><!-- /.panel-body -->
</div><!-- /.panel panel-primary-->
</div><!-- /.col-lg-6 col-md-offset-3-->
</div><!-- /.row -->
{include file="./footer.tpl"}