<!DOCTYPE html>
<html>
<head>
<style type="text/css">
.ui-input-text
{
width: 175px !important;
}
</style>
	  <title>Om Cow</title>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="./css/jquery.mobile-1.2.0.min.css" />
     <style>
        #login-button {
            margin-top: 30px;
        }        
    </style> 
    <script src="./js/jquery-1.8.2.min.js"></script>    
    <script src="./js/jquery.mobile-1.2.0.min.js"></script>
    <script src="js/index.js"></script>
</head>
<body>
<div data-role="page" id="login" data-theme="b">
  <div data-role="header" data-theme="a">
    <h3>Register Page</h3>
  </div>
 <br></br>
  <div data-role="content">
    <a href="logout.php"  style="text-decoration: none;">Logout</a> | 
    <a href="cowEntry.php" style="text-decoration: none;">Cow Entry</a> | 
    <a href="cowList.php" style="text-decoration: none;">Cow List</a> | 
    <a href="staff.php" style="text-decoration: none;">Staff Entry</a> |
    <a href="staffList.php" style="text-decoration: none;">Staff List</a> |
    <a href="item.php" style="text-decoration: none;">Item Entry</a> |
    <a href="itemList.php" style="text-decoration: none;">Item List</a> |
    <a href="customer.php" style="text-decoration: none;">Customer Entry</a> |
    <a href="custList.php" style="text-decoration: none;">Customer List</a> |
    <a href="licence.php" style="text-decoration: none;">Licence Entry</a>
    <form id="check-user" class="" data-ajax="false" method="POST" action="register.php">
    	<div data-role="fieldcontain">
    		<label for="select-choice-1" value="0">Select User Type</label>
    		<select name="userTypeId" data-native-menu="false">
    		  {html_options values=$userArray.userTypeId output=$userArray.userType}
    		</select>
    	</div>
      <fieldset>
          <div data-role="fieldcontain">
              <label for="fName">First Name:</label>
              <input type="text" value="" name="fName" id="fName" required autofocus data-mini="true" />
          </div>                                  
          <div data-role="fieldcontain">                                      
              <label for="mName">Middle Name:</label>
              <input type="text" value="" name="mName" id="mName"/> 
          </div>
          <div data-role="fieldcontain">                                      
              <label for="lName">Last Name:</label>
              <input type="text" value="" name="lName" id="lName"/> 
          </div>
          <div data-role="fieldcontain">                                      
              <label for="city">City:</label>
              <input type="text" value="" name="city" id="city"/> 
          </div>
          <div data-role="fieldcontain">                                      
              <label for="emailId">Email Id:</label>
              <input type="text" value="" name="emailId" id="emailId"/> 
          </div>
          <div data-role="fieldcontain">                                      
              <label for="mobileNo">Mobile No:</label>
              <input type="text" value="" name="mobileNo" id="mobileNo"/> 
          </div>
          <input type="submit" data-theme="b" name="submit" id="submit" value="Submit">
      </fieldset>
    </form>
  </div>
  <div data-theme="a" data-role="footer" data-position="fixed"></div>
</div>
<div data-role="page" id="second">
  <div data-theme="a" data-role="header">
    <h3></h3>
  </div>
<div data-role="content"></div>
</div>
</body>
</html>