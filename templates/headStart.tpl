<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Om Cow</title>

    <!-- Core CSS - Include with every page -->
    <link type="text/css" href="./css/style.css" rel="stylesheet">
    <link type="text/css" href="./css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="./css/bootstrapValidator.min.css" rel="stylesheet">
    <link type="text/css" href="./font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link type="text/css" href="./css/sb-admin.css" rel="stylesheet">
    
    <!-- Core Scripts - Include with every page -->
    <script type="text/javascript" src="./js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js/bootstrapValidator.min.js"></script>
    <script type="text/javascript" src="./js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script type="text/javascript" src="./js/sb-admin.js"></script>
    
    <script type="text/javascript">
	$(document).ready(function(){
	    $('form,input,select,textarea').attr("autocomplete", "off");
	    
	    var filename = window.location.pathname.match(/.*\/(.*)$/)[1];
	    
	    $('#siteMenu a[href*="' + filename + '"]').addClass('active');
	});
    </script>
    