{include file="./headStart.tpl"}

{include file="./headEnd.tpl"}
<div class="row">
    <div class="col-lg-6 col-md-offset-3">
		{if $item_vaccine_error != ""}
	    <div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		{$item_vaccine_error}
	    </div>
	{/if}

	{if $item_vaccine_success != ""}
	    <div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		{$item_vaccine_success}
	    </div>
	{/if}
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-edit fa-fw"></i> Vaccine Entry	
			</div>
			<div class="panel-body">
				<div class="row col-lg-13">
					<form id="itemCreateForm" method="POST" action="vaccine.php" class="formBox form-horizontal" role="form">
												
						<div class="form-group">
							<label class="control-label col-sm-4">Vaccine Type</label>
							<div class="col-sm-3">
								<input type="text" value="" name="vaccine" class="form-control input-sm" />								
							</div>						
						</div>		
						
						<div class="form-group">
							<label class="control-label col-sm-4"></label>
								<div class="col-sm-7">
								<button class="btn btn-success btn-lg" type="submit">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
	    <!-- /.panel-body -->
		</div>
	<!-- /.panel -->
	</div>
    <!-- /.col-lg-12 -->
</div>
<!-- Display Records-->
<div class="row">
    <div class="col-lg-10 col-md-offset-1">
	<div class="panel panel-primary">
	    <div class="panel-heading">
		<i class="fa fa-list-ul fa-fw"></i> Vaccine List
	    </div>
	    <div class="panel-body">
		<div class="table-responsive">
		    <table class="table table-striped table-bordered table-hover">
			<thead>
			    <tr>
				<th>&nbsp;</th>
				<th>Vaccine Type</th>			
			    </tr>
			</thead>
			<tbody>
			    {section name="sec" loop=$vaccineArrayTable}
			    <tr>
				<td align="center">
				    <a href="vaccineDelete.php?vaccineId={$vaccineArrayTable[sec].vaccineTypeId}" onclick="return confirm('Are You Sure To Delete??');"  class="link">
					<img src="./images/deleteIcon.png" />
				    </a>
				</td>
				<td NOWRAP>{$vaccineArrayTable[sec].vaccineName}</td>			
			    </tr>
			    {/section}
			</tbody>
		    </table>
		</div>
	    </div>
	</div>
    </div>
</div>
{include file="./footer.tpl"}