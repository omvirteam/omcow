{include file="./headStart.tpl"}
{include file="./headEnd.tpl"}
<div class="row">
<div class="col-lg-6 col-md-offset-3">	
<div class="panel panel-primary">
	<div class="panel-heading">
	    <i class="fa fa-edit fa-fw"></i> Fariya Entry
	</div>
<div class="panel-body">
  <div class="row col-lg-12">

    <form id="check-user" class="formBox form-horizontal" role="form" data-ajax="false" method="POST" action="fariya.php">

	  <div class="form-group">
			<label class="control-label col-sm-4">Cow:</label>
			<div class="col-sm-5">
      	<select name="cowId" class="form-control input-sm">
      		{html_options values=$cowArray.cowId output=$cowArray.name selected=$cowId}
        </select>        
			</div>
	  </div>

	  <div class="form-group">
			<label class="control-label col-sm-4">Fariya Date :</label>
			<div class="col-sm-7">
      	{html_select_date prefix="fariyaDate" start_year="-1" end_year="+0" field_order="DMY" day_value_format="%02d" all_extra="class='form-control date-controls pull-left input-sm'"}        
			</div>
	  </div>

	  <div class="form-group">
			<label class="control-label col-sm-4"></label>
			<div class="col-sm-5">
      	<input type="submit" data-theme="b" class="btn btn-success btn-lg" value="Submit" name="submit">
			</div>
	  </div>

    </form>
	</div><!-- /.col-lg-12 -->
</div><!-- /.panel-body -->
</div><!-- /.panel panel-primary-->
</div><!-- /.col-lg-6 col-md-offset-3-->
</div><!-- /.row -->
{include file="./footer.tpl"}