{include file="./headStart.tpl"}
{include file="./headEnd.tpl"}

<script src="js/changePassword.js"></script>
<div class="container">
    <div class="row">
	<div class="col-md-4 col-md-offset-4">
	    <div class="login-panel panel panel-primary">
		<div class="panel-heading">
		    <h3 class="panel-title">Change Password</h3>
		</div>
		<div class="panel-body">
		    <div class="row-fluid col-lg-12">
			<form id="check-user" method="POST" role="form" class="form-horizontal">
			    <div class="form-group">
				<label class="col-lg-4 control-label">Old Password <sup>*</sup></label>
				<div class="col-lg-7">
				    <input class="form-control" placeholder="Old Password" name="oldPassword" id="oldPassword" type="password" autofocus />
				</div>
			    </div>
			    <div class="form-group">
				<label class="col-lg-4 control-label">New Password <sup>*</sup></label>
				<div class="col-lg-7">
				    <input class="form-control" placeholder="New Password" name="newPassword" id="newPassword" type="password" value="">
				</div>
			    </div>
				
				<div class="form-group">
				<label class="col-lg-4 control-label">Confirm Password <sup>*</sup></label>
				<div class="col-lg-7">
				    <input class="form-control" placeholder="Confirm Password" name="confirmNewPassword" id="confirmNewPassword" type="password" value="">
				</div>
			    </div>
			 
			    <button type="button" id="changeSubBtn" class="btn btn-lg btn-success btn-block">Submit</button>
			</form>
		    </div>
		</div>
	    </div>
	</div>
    </div>
</div>

{include file="./footer.tpl"}
