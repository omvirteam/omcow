{include file="./headStart.tpl"}

<script type="text/javascript">
$(document).ready(function() {
    $('#check-user').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
	    submitButtons: 'button[type="submit"]',
            submitHandler: function(validator, form, submitButton) {
                validator.defaultSubmit();
            },
            fields: {
		userName: { validators: { notEmpty: { message: 'The username is required' } } },
		pass: { validators: { notEmpty: { message: 'The password is required' } } }
            }
        });
});
</script>

{include file="./headEnd.tpl"}

<div class="container">
    <div class="row">
	<div class="col-md-4 col-md-offset-4">
	    <div class="login-panel panel panel-primary">
		<div class="panel-heading">
		    <h3 class="panel-title">Login</h3>
		</div>
		<div class="panel-body">
		    <div class="row-fluid col-lg-12">
			<form id="check-user" method="POST" role="form" class="form-horizontal">
			    <div class="form-group">
				<label class="col-lg-4 control-label">Username <sup>*</sup></label>
				<div class="col-lg-7">
				    <input type="text" class="form-control" placeholder="Username" name="userName" id="userName" autofocus />
				</div>
			    </div>
			    <div class="form-group">
				<label class="col-lg-4 control-label">Password <sup>*</sup></label>
				<div class="col-lg-7">
				    <input class="form-control" placeholder="Password" name="pass" id="password" type="password" value="">
				</div>
			    </div>
			    <!-- Change this to a button or input when using this as a form -->
			    <button type="submit" class="btn btn-lg btn-success btn-block">Login</button>
			</form>
		    </div>
		</div>
	    </div>
	</div>
    </div>
</div>

{include file="./footer.tpl"}
