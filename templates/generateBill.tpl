{include file="./headStart.tpl"}

<script type="text/javascript">
$(document).ready(function(e) {
    $("#cmbDistributor").change(function() {
		var curUrl = window.location.pathname;
		var  formData = "name=ravi&age=31"; 
		var completeData = $(this).serialize();
		alert(formData);
		$.ajax({
    		type: "POST",
    		data : $("form#cmbDistributor").serialize(),
    		success: function(data, textStatus, jqXHR)
    		{
        		
    		},
    		error: function (jqXHR, textStatus, errorThrown)
    		{
 				
    		}
		});
	});
});
</script>
{include file="./headEnd.tpl"}
<p class="message"><?php echo $_POST['name']; ?></p>
<div class="row">
<div class="col-lg-6 col-md-offset-3">
	<div class="panel panel-primary">
	  <div class="panel-heading">
		<i class="fa fa-edit fa-fw"></i>Generate Bill
	  </div>
	  <div class="panel-body">
            <div class="row col-lg-13">
                <form method="POST" action="{$smarty.server.PHP_SELF}" class="formBox form-horizontal" role="form">

                  <div class="form-group">
                    <label class="control-label col-sm-4">Distributer</label>
                    <div class="col-sm-5">
                         <select name="cmbDistributor" id="cmbDistributor" class="form-control input-sm">
                            <option value="">Select Type</option>
                         	{html_options values=$staffArray.staffId output=$staffArray.name}
                         </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-sm-4">Account Name</label>
                    <div class="col-sm-8">
                      <input type="text" value="" name="accountName" REQUIRED class="form-control input-sm" />
                    </div>
                  </div>

                  <div class="form-group">
							<label class="control-label col-sm-4">Customer</label>
							<div class="col-sm-5">
								<select name="cmbCustomer" id="cmbCustomer" class="form-control input-sm">
									<option value="All">All</option>
                   				</select>
						</div>
					</div>
                    

                    <div class="form-group">
								<label class="control-label col-sm-4">Month</label>
								<div class="col-sm-8">
									<select class="form-control date-controls pull-left input-sm" name="cmbFromMonth">
                    				</select>
								</div>
					</div>				

                    <div class="form-group">
								<label class="control-label col-sm-4">Year</label>
								<div class="col-sm-8">
									<select class="form-control date-controls pull-left input-sm" name="cmbFromYear">
         
									</select>
								</div>
					</div>	

                 <div class="form-group">
							<label class="control-label col-sm-4"></label>
								<div class="col-sm-7">
								<button name="btnGenerateBill" class="btn btn-success btn-lg" type="submit">Submit</button>
							</div>
						</div>

                </form>
            </div><!-- /.row col-lg-13 -->
	  </div><!-- /.panel-body -->
	</div><!-- /.panel -->
</div><!-- /.col-lg-12 -->
</div><!-- /.row -->

{include file="./footer.tpl"}