{include file="./headStart.tpl"}

<!-- Page-Level Plugin CSS - Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

<!-- Page-Level Plugin Scripts - Tables -->
<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#itemList').dataTable({
	"aoColumnDefs": [{ 'bSortable': false, 'aTargets': [ 0 ] }],
	"aaSorting": [[ 1, "asc" ]]
    });
});
</script>

{include file="./headEnd.tpl"}

<div class="row">
    <div class="col-lg-6 col-md-offset-3">
	<div class="panel panel-primary">
	    <div class="panel-heading">
		<i class="fa fa-list-ul fa-fw"></i> Item List
	    </div>
	    <div class="panel-body">
		<div class="table-responsive">
		    <table class="table table-striped table-bordered table-hover" id="itemList">
			<thead>
			    <tr>
				<th align="center">&nbsp;</th>
				<th align="center">Name</th> 
				<th align="center">Rate</th>
				<th align="center">Quantity</th>
			    </tr>
			</thead>
			<tbody>
			    {section name="sec" loop=$itemArray}
				<tr>
				      <td align="center">
					  <a href="itemDelete.php?itemId={$itemArray[sec].itemId}" onclick="return confirm('Are You Sure To Delete??');"  class="link">
					      <img src="./images/deleteIcon.png" />
					  </a>
				      </td>
				      <td align="center">{$itemArray[sec].name}</td>
				      <td align="center">{$itemArray[sec].rate}</td>
				      <td align="center">{$itemArray[sec].qty}</td>
				</tr>
			    {/section}
			</tbody>
		    </table>
		</div>
	    </div>
	</div>
    </div>
</div>

{include file="./footer.tpl"}