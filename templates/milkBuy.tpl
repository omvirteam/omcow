{include file="./headStart.tpl"}

<script type="text/javascript">
</script>

{include file="./headEnd.tpl"}

<div class="row">
<div class="col-lg-6 col-md-offset-3">
	<div class="panel panel-primary">
	  <div class="panel-heading">
		  <i class="fa fa-edit fa-fw"></i>Milk Buy
	  </div>
	  <div class="panel-body">
		  <div class="row col-lg-13">
		    <form method="POST" action="{$smarty.server.PHP_SELF}" class="formBox form-horizontal" role="form">
			
  			<div class="form-group">
  			  <label class="control-label col-sm-4">Select Customer</label>
  			  <div class="col-sm-5">
    				<select name="cowId" class="form-control input-sm" REQUIRED >
    				  <option value="">Select Customer</option>
    				  {html_options values=$customerArray.customerId output=$customerArray.name}
    				</select>
  			  </div>
  			</div>
				
  			<div class="form-group">
  			  <label class="control-label col-sm-4">Select Staff</label>
  			  <div class="col-sm-5">
    				<select name="staffId" class="form-control input-sm" REQUIRED>
    				  <option value="">Select Staff</option>
    				  {html_options values=$staffArray.staffId output=$staffArray.name}
    				</select>
  			  </div>
  			</div>
  			
			  <div class="form-group">
			    <label class="control-label col-sm-4">Buy Date</label>
			    <div class="col-sm-8">
				    {html_select_date prefix="buyDate" start_year="-1" end_year="+0" field_order="DMY" day_value_format="%02d" all_extra="class='form-control date-controls pull-left input-sm'"}
			    </div>
			  </div>

			  <div class="form-group">
			    <label class="control-label col-sm-4">Litre</label>
			    <div class="col-sm-3">
				    <input type="text" value="" name="litre" id="litre" REQUIRED class="form-control input-sm" />
			    </div>
			  </div>
				
			  <div class="form-group">
			    <label class="control-label col-sm-4">Rate</label>
			    <div class="col-sm-3">
				    <input type="text" value="" name="rate" id="rate" REQUIRED class="form-control input-sm" />
			    </div>
			  </div>
				
			  <div class="form-group">
			    <label class="control-label col-sm-4">Fat</label>
			    <div class="col-sm-3">
				    <input type="text" value="" name="fat" id="rate" REQUIRED class="form-control input-sm" />
			    </div>
			  </div>
				
			  <div class="form-group">
				  <label class="control-label col-sm-4">Notes</label>
				  <div class="col-sm-7">
				    <textarea name="notes" rows="5" cols="25" class="form-control"></textarea>
				  </div>
			  </div>
			    
  			<div class="form-group">
  			  <label class="control-label col-sm-4"></label>
  			  <div class="col-sm-7">
  				  <button class="btn btn-success btn-lg" name="submitBtn" type="submit">Submit</button>
  			  </div>
  			</div>
			
		    </form>
		  </div><!-- /.row col-lg-13 -->
	  </div><!-- /.panel-body -->
	</div><!-- /.panel -->
</div><!-- /.col-lg-12 -->
</div><!-- /.row -->

{include file="./footer.tpl"}