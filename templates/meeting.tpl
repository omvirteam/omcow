{include file="./headStart.tpl"}
{include file="./headEnd.tpl"}
<div class="row">
<div class="col-lg-6 col-md-offset-3">	
<div class="panel panel-primary">
	<div class="panel-heading">
	    <i class="fa fa-edit fa-fw"></i> Meeting Entry
	</div>
<div class="panel-body">
  <div class="row col-lg-12">

    <form id="check-user" class="formBox form-horizontal" role="form" data-ajax="false" method="POST" action="{$smarty.server.PHP_SELF}">

	  <div class="form-group">
			<label class="control-label col-sm-4">Cow:</label>
			<div class="col-sm-5">
      	<select name="cowId" class="form-control input-sm">
      		<option value="">Select Cow</option>
      		{html_options values=$cowArray.cowId output=$cowArray.name selected=$selectedCow}
        </select>        
			</div>
	  </div>

	  <div class="form-group">
			<label class="control-label col-sm-4">Dhankhut:</label>
			<div class="col-sm-5">
      	<select name="dhankhutId" class="form-control input-sm">
      		<option value="">Select Dhankhut</option>
      		{html_options values=$dhankhutArray.cowId output=$dhankhutArray.name selected=$selectedDhankhut}
        </select>        
			</div>
	  </div>

	  <div class="form-group">
			<label class="control-label col-sm-4">Meeting Date :</label>
			<div class="col-sm-7">
      	{html_select_date prefix="meetingDate" start_year="-1" end_year="+0" field_order="DMY" day_value_format="%02d" all_extra="class='form-control date-controls pull-left input-sm'"}        
			</div>
	  </div>

	  <div class="form-group">
			<label class="control-label col-sm-4"></label>
			<div class="col-sm-5">
      	<input type="submit" data-theme="b" class="btn btn-success btn-lg" value="Submit" name="submit">
			</div>
	  </div>

    </form>
	</div><!-- /.col-lg-12 -->
</div><!-- /.panel-body -->
</div><!-- /.panel panel-primary-->
</div><!-- /.col-lg-6 col-md-offset-3-->
</div><!-- /.row -->
{include file="./footer.tpl"}