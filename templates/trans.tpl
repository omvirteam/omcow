{include file="./headStart.tpl"} 
<script type="text/javascript">
  $(document).ready(function() {
    $('#litre').change(function() {
      var litre = $('#litre').val();
      $('#litre').val(parseFloat(litre).toFixed(3));
    });
	
	//CREDIT
	$('#customerIdCr').change(function(){
		$('#staffIdCr').val('');
		$('#accountIdCr').val('');
	});
	
	//CREDIT
	$('#accountIdCr').change(function(){
		$('#staffIdCr').val('');
		$('#customerIdCr').val('');
	});
	
	//CREDIT
	$('#staffIdCr').change(function(){
		$('#accountIdCr').val('');
		$('#customerIdCr').val('');
	});
	
	//DEBIT
	$('#customerIdCr').change(function(){
		$('#staffIdDr').val('');
		$('#accountIdDr').val('');
	});
	
	//DEBIT
	$('#accountIdDr').change(function(){
		$('#staffIdDr').val('');
		$('#customerIdDr').val('');
	});
	
	//DEBIT
	$('#staffIdDr').change(function(){
		$('#accountIdDr').val('');
		$('#customerIdDr').val('');
	});
	
	$('#submitBtn').click(function(){
		var reg = /(\d+(.\d+)?)/;
		var amt = $.trim($('#amount').val());
		
		if($.trim($('#customerIdCr').val()) == '' && $.trim($('#accountIdCr').val()) == '' && $.trim($('#staffIdCr').val()) == ''){
			alert('Please select customer or staff or account from credit side');
			return false;
		} 
		else if($.trim($('#customerIdDr').val()) == '' && $.trim($('#accountIdDr').val()) == '' && $.trim($('#staffIdDr').val()) == ''){
			alert('Please select customer or staff or account from debit side');
			return false;
		} 
		else if(amt == ''){
			alert('Please enter amount');
			$('#amount')
			return false;
		}
		else if(!reg.test(amt)){
			alert('Please enter valid amount');
			$('#amount')
			return false;
		}
	});
  });
</script> 
{include file="./headEnd.tpl"}
<div class="row">
  <div class="col-lg-13 col-md-offset-0">
    <div class="panel panel-primary">
      <div class="panel-heading"> <i class="fa fa-edit fa-fw"></i>Transaction Entry </div>
      <div class="panel-body">
        <div class="row col-lg-10">
          <form id="staffEntryForm" method="POST" action="{$smarty.server.PHP_SELF}" class="formBox form-horizontal" role="form">
            <table border="0" width="100%">
              <tr>
                <th> <table>
                    <tr>
                      <td>
                      <label class="control-label col-sm-1"></label>
                      <label class="control-label col-sm-6">
                      <span style="color:#C00; font-weight:bold;">{$message}</span></label></td>
                    </tr>
                    <tr>
                      <td><label class="control-label col-sm-6">Credit</label>
                        <div class="form-group">
                          <label class="control-label col-sm-4">Customer</label>
                          <div class="col-sm-5">
                            <select id="customerIdCr" name="customerIdCr" class="form-control input-sm"  >
                              <option value="">Select Customer</option>
                              
	                          {html_options values=$customerArray.customerId output=$customerArray.name}
                            
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4">Staff</label>
                          <div class="col-sm-5">
                            <select id="staffIdCr" name="staffIdCr" class="form-control input-sm"  >
                              <option value="">Select Staff</option>
                              
	                          {html_options values=$staffArray.staffId output=$staffArray.name}
                            
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4">Account</label>
                          <div class="col-sm-5">
                            <select id="accountIdCr" name="accountIdCr" class="form-control input-sm"  >
                              <option value="">Select Account</option>
                              
	                          {html_options values=$accountArray.accountId output=$accountArray.name}
                            
                            </select>
                          </div>
                        </div></td>
                    </tr>
                  </table>
                </th>
                <th> <table>
                    <tr>
                      <td><label class="control-label col-sm-6">Debit</label>
                        <div class="form-group">
                          <label class="control-label col-sm-4">Customer</label>
                          <div class="col-sm-5">
                            <select id="customerIdDr" name="customerIdDr" class="form-control input-sm"  >
                              <option value="">Select Customer</option>
                              
                              {html_options values=$customerArray.customerId output=$customerArray.name}
                            
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4">Staff</label>
                          <div class="col-sm-5">
                            <select id="staffIdDr" name="staffIdDr" class="form-control input-sm"  >
                              <option value="">Select Staff</option>
                              
                              {html_options values=$staffArray.staffId output=$staffArray.name}
                            
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4">Account</label>
                          <div class="col-sm-5">
                            <select id="accountIdDr" name="accountIdDr" class="form-control input-sm"  >
                              <option value="">Select Account</option>
                              
                              {html_options values=$accountArray.accountId output=$accountArray.name}                        
                            
                            </select>
                          </div>
                        </div></td>
                    </tr>
                  </table>
                </th>
              </tr>
              <tr style="text-align:left">
                <td style="text-align:left"><div class="form-group">
                    <label class="control-label col-sm-4">Amount</label>
                    <div class="col-sm-3">
                      <input type="text" name="amount" id="amount"class="form-control input-sm" />
                    </div>
                  </div></td>
              </tr>
              <tr>
                <th > <div class="form-group">
                    <label class="control-label col-sm-4">Date</label>
                    <div class="col-sm-8"> {html_select_date prefix="milkDate" start_year="-1" end_year="+0" field_order="DMY" day_value_format="%02d" all_extra="class='form-control date-controls pull-left input-sm'"} </div>
                  </div>
                </th>
              </tr>
              <tr>
                <th > <div class="form-group">
                    <label class="control-label col-sm-4">Notes</label>
                    <div class="col-sm-7">
                      <textarea id="notes" name="notes" rows="5" cols="25" class="form-control"></textarea>
                    </div>
                  </div>
                </th>
              </tr>
              <tr>
                <th colspan="2"> <div class="form-group">
                    <label class="control-label col-sm-4"></label>
                    <div class="col-sm-7">
                      <button class="btn btn-success btn-lg" id="submitBtn" name="submitBtn" type="submit">Submit</button>
                    </div>
                  </div>
                </th>
              </tr>
            </table>
          </form>
        </div>
        <!-- /.row col-lg-13 --> 
      </div>
      <!-- /.panel-body --> 
    </div>
    <!-- /.panel --> 
  </div>
  <!-- /.col-lg-12 --> 
</div>
<!-- /.row --> 
{include file="./footer.tpl"}