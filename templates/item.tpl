{include file="./headStart.tpl"}
<!-- Page-Level Plugin CSS - Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

<!-- Page-Level Plugin Scripts - Tables -->
<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>


<script type="text/javascript">
$(document).ready(function() {
    $('#itemEntryForm').bootstrapValidator({
	message: 'This value is not valid',
	feedbackIcons: {
	    valid: 'glyphicon glyphicon-ok',
	    invalid: 'glyphicon glyphicon-remove',
	    validating: 'glyphicon glyphicon-refresh'
	},
	submitButtons: 'button[type="submit"]',
	submitHandler: function(validator, form, submitButton) {
	    validator.defaultSubmit();
	},
	fields: {
	    name: { validators: { notEmpty: { message: 'The is required' } } },
	    rate: { validators: { notEmpty: { message: 'The is required' } } },
	    qty: { validators: { notEmpty: { message: 'The is required' } } }
	}
    });
	
	
	$('#itemList').dataTable({
	"aoColumnDefs": [{ 'bSortable': false, 'aTargets': [ 0 ] }],
	"aaSorting": [[ 1, "asc" ]]
    });
});
</script>

{include file="./headEnd.tpl"}

<div class="row">
    <div class="col-lg-6 col-md-offset-3">
	    
	{if $item_entry_error != ""}
	    <div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		{$item_entry_error}
	    </div>
	{/if}

	{if $item_entry_success != ""}
	    <div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		{$item_entry_success}
	    </div>
	{/if}
	    
	<div class="panel panel-primary">
	    <div class="panel-heading">
		<i class="fa fa-edit fa-fw"></i> Item Entry
	    </div>
	    <div class="panel-body">
		<div class="row col-lg-13">
		    <form id="itemEntryForm" method="POST" action="item.php" class="formBox form-horizontal" role="form">
			
			<div class="form-group">
			    <label class="control-label col-sm-4">Name <sup>*</sup></label>
			    <div class="col-sm-5">
				<input type="text" value="" name="name" id="name" class="form-control input-sm" />
			    </div>
			</div>
			
			<div class="form-group">
			    <label class="control-label col-sm-4">Rate <sup>*</sup></label>
			    <div class="col-sm-3">
				<input type="text" value="" name="rate" id="rate" class="form-control input-sm" />
			    </div>
			</div>
			
			<div class="form-group">
			    <label class="control-label col-sm-4">Quantity <sup>*</sup></label>
			    <div class="col-sm-3">
				<input type="text" value="" name="qty" id="qty" class="form-control input-sm" />
			    </div>
			</div>
			
			<div class="form-group">
			    <label class="control-label col-sm-4">Select Unit</label>
			    <div class="col-sm-5">
				<select name="itemunit" class="form-control input-sm">
				  {html_options values=$itemunitArray.unitId output=$itemunitArray.unit}
				</select>
			    </div>
			</div>
			
			<div class="form-group">
			    <label class="control-label col-sm-4"></label>
			    <div class="col-sm-7">
				<button class="btn btn-success btn-lg" type="submit">Submit</button>
			    </div>
			</div>
			
		    </form>
		</div>
	    </div>
	    <!-- /.panel-body -->
	</div>
	<!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<!-- Display Datatable-->
<div class="row">
    <div class="col-lg-6 col-md-offset-3">
	<div class="panel panel-primary">
	    <div class="panel-heading">
		<i class="fa fa-list-ul fa-fw"></i> Item List
	    </div>
	    <div class="panel-body">
		<div class="table-responsive">
		    <table class="table table-striped table-bordered table-hover" id="itemList">
			<thead>
			    <tr>
				<th align="center">&nbsp;</th>
				<th align="center">Name</th> 
				<th align="center">Rate</th>
				<th align="center">Quantity</th>
			    </tr>
			</thead>
			<tbody>
			    {section name="sec" loop=$itemArray}
				<tr>
				      <td align="center">
					  <a href="itemDelete.php?itemId={$itemArray[sec].itemId}" onclick="return confirm('Are You Sure To Delete??');"  class="link">
					      <img src="./images/deleteIcon.png" />
					  </a>
				      </td>
				      <td align="center">{$itemArray[sec].name}</td>
				      <td align="center">{$itemArray[sec].rate}</td>
				      <td align="center">{$itemArray[sec].qty}</td>
				</tr>
			    {/section}
			</tbody>
		    </table>
		</div>
	    </div>
	</div>
    </div>
</div>


{include file="./footer.tpl"}