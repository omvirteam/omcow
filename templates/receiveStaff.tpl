{include file="./headStart.tpl"}
<!-- Page-Level Plugin CSS - Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

<!-- Page-Level Plugin Scripts - Tables -->
<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>


{include file="./headEnd.tpl"}

<div class="row">
    <div class="col-lg-6 col-md-offset-3">
	    
	<div class="panel panel-primary">
	    <div class="panel-heading">
		<i class="fa fa-edit fa-fw"></i> Money Transfer
	    </div>
	    <div class="panel-body">
		<div class="row col-lg-13">
		    <form id="staffForm" method="POST" action="" class="formBox form-horizontal" role="form">
			
			<div class="form-group">
			    <label class="control-label col-sm-4">Staff From<sup>*</sup></label>
			    <div class="col-sm-5">
					<select name="staffFrom" class="form-control input-sm">
				  {html_options values=$staffArr.staffId output=$staffArr.name}
					</select>
			  
			    </div>
			</div>
			
			<div class="form-group">
			    <label class="control-label col-sm-4">Staff To<sup>*</sup></label>
			    <div class="col-sm-3">
				<select name="staffTo" class="form-control input-sm">
				  {html_options values=$staffArr.staffId output=$staffArr.name}
					</select>
			    </div>
			</div>
			
			<div class="form-group">
			    <label class="control-label col-sm-4">Amount <sup>*</sup></label>
			    <div class="col-sm-3">
				<input type="text" value="" name="amount" id="amount" class="form-control input-sm" />
			    </div>
			</div>
			
			<div class="form-group">
			    <label class="control-label col-sm-4">Note</label>
			    <div class="col-sm-5">
					<textarea name="note" id="note"></textarea>
			    </div>
			</div>
			
			<div class="form-group">
			    <label class="control-label col-sm-4"></label>
			    <div class="col-sm-7">
				<button class="btn btn-success btn-lg" id="submitBtn" name="submitBtn" type="submit">Submit</button>
			    </div>
			</div>
			
		    </form>
		</div>
	    </div>
	    <!-- /.panel-body -->
	</div>
	<!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

{include file="./footer.tpl"}