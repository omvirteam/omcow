<?php
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
	$billArray = array();
	$i = 0;
	$totAmount = 0;
  $customerIdDr = isset($_GET['customerIdDr']) ? $_GET['customerIdDr'] : 0;
  $transDate    = isset($_GET['transDate']) ? $_GET['transDate'] : date("Y-m-d");
  $selectBill = " SELECT customer.grnNo,customer.name AS custName,customer.nickName,transaction.amount,
                         DATE_FORMAT(transaction.transDate,'%d-%m-%Y') AS transDate,transaction.notes,
                         transaction.rate,transaction.totalQty,transaction.sellItemId,item.name,
                         customer.milkTime,customer.distStaffIdId
                    FROM transaction
               LEFT JOIN customer ON customer.customerId = transaction.customerIdDr
               LEFT JOIN itemsell ON itemsell.itemSellId = transaction.sellItemId
               LEFT JOIN item     ON item.itemId         = itemsell.itemId
                   WHERE accountIdCr = 5
                     AND customerIdDr = ".$customerIdDr."
                     AND transDate = '".$transDate."'
                ORDER BY transactionId";
	$selectBillRes = mysql_query($selectBill);
	while($billRow = mysql_fetch_array($selectBillRes))
	{
		$billArray[$i]['customerName'] = $billRow['custName'] ." - ". $billRow['nickName'];
    if($billRow['grnNo'] == 'M')
    {
      $milkTime = "Morning";
    }
    elseif($billRow['grnNo'] == 'E')
    {
      $milkTime = "Evening";
    }
    else
    {
      $milkTime = "Both";
    }
    $selDistQry = "SELECT name
                     FROM staff
                    WHERE staffId = ".$billRow['distStaffIdId'];
    $selDistQryRes = mysql_query($selDistQry);
    if($distRow = mysql_fetch_array($selDistQryRes))
    {
      $distName = $distRow['name'];
    }
    else
    {
      $distName = "";
    }
    $totAmount += $billRow['amount'];
    $billArray[$i]['milkTime'] =     $milkTime;
		$billArray[$i]['grn']          = $billRow['grnNo'];
		$billArray[$i]['productName']  = $billRow['name'] != NULL ? $billRow['name'] : 'Milk';
		$billArray[$i]['rate']         = $billRow['rate'];
		$billArray[$i]['totalQty']     = $billRow['totalQty'];
		$billArray[$i]['amount']       = $billRow['amount'];
		$billArray[$i]['transDate']    = $billRow['transDate'];
		$billArray[$i]['notes']        = $billRow['notes'];
		$i++;                                      
	}
  
}
include("./bottom.php");
$smarty->assign("billArray",$billArray);
$smarty->assign("distName",$distName);
$smarty->assign("totAmount",$totAmount);
$smarty->display("billListDetail.tpl");
?>