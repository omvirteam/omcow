<?php
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
    $_SESSION['item_expense_success'] = "";
    $_SESSION['item_expense_error'] = "";
	if(count($_POST) > 0)
	{
		$item =isset($_POST['item']) ? $_POST['item'] : "";
		$amount = isset($_POST['amount']) ? $_POST['amount'] : 0;
		$expenseDate= $_POST['expenseYear']."-".$_POST['expenseMonth']."-".$_POST['expenseDay'];
		$notes = isset($_POST['notes']) ? $_POST['notes'] : "";
		$insertItem  = "INSERT INTO itemexpense (itemId,expDate,amount,notes)
                                VALUES('".$item."','".$expenseDate."',".$amount.",'".addslashes($notes)."')";
		$insertItemRes = mysql_query($insertItem);
		if(!$insertItemRes)
		{
		$_SESSION['item_expense_error'] = "Unable to add details.";
		}
		else
		{
		$_SESSION['item_expense_success'] = "Details successfully added.";
		}
	}
}
$n=0;
	$itemArray=array();
	$item="SELECT itemId,name FROM item where status='A' ORDER BY name";
	$itemRes=mysql_query($item);
	while($itemRow=mysql_fetch_array($itemRes))
	{
		$itemArray['itemId'][$n]=$itemRow['itemId'];
		$itemArray['name'][$n]=$itemRow['name'];
		$n++;
	}

	//GET ALL RECORDS TO DISPLAY IN DATATABLE
	$expArrayTable=array();
	$selectExpItem="SELECT i.itemId,i.name,ex.* FROM itemexpense ex 
						LEFT JOIN item i ON ex.itemId=i.itemId 
						ORDER BY i.name";
	$selectExpItemRes=mysql_query($selectExpItem);
	
	if(mysql_num_rows($selectExpItemRes)>0)
	{
		$t=0;
		while($selectExpItemRow=mysql_fetch_array($selectExpItemRes))
		{
		$expArrayTable[$t]['name']      = $selectExpItemRow['name'];
		$expArrayTable[$t]['expDate']         = date('d-m-Y',strtotime($selectExpItemRow['expDate']));
		$expArrayTable[$t]['amount']    = $selectExpItemRow['amount'];
		$expArrayTable[$t]['notes']   = $selectExpItemRow['notes'];
		$expArrayTable[$t]['itemExpenseId']   = $selectExpItemRow['itemExpenseId'];
		$t++;             
		}
	
	}
include("bottom.php");
$smarty->assign("expArrayTable",$expArrayTable);
$smarty->assign("itemArray",$itemArray);
$smarty->assign("item_expense_error",$_SESSION['item_expense_error']);
$smarty->assign("item_expense_success",$_SESSION['item_expense_success']);

$smarty->display("itemExpense.tpl");
?>