<?php
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
    $_SESSION['item_dispatch_success'] = "";
    $_SESSION['item_dispatch_error'] = "";
	if(count($_POST) > 0)
	{
		$staff =isset($_POST['staff']) ? $_POST['staff'] : "";
		$item =isset($_POST['item']) ? $_POST['item'] : "";
		$qty = isset($_POST['qty']) ? $_POST['qty'] : 0;
		$dispatchDate= $_POST['dispatchYear']."-".$_POST['dispatchMonth']."-".$_POST['dispatchDay'];
		$notes = isset($_POST['notes']) ? $_POST['notes'] : "";
		$insertItem  = "INSERT INTO itemdispatch (staffId,itemId,qty,dispDate,notes)
                                VALUES(".$staff.",".$item.",".$qty.",'".$dispatchDate."','".addslashes($notes)."')";
		$insertItemRes = mysql_query($insertItem);
		if(!$insertItemRes)
		{
		$_SESSION['item_dispatch_error'] = "Unable to add details.";
		}
		else
		{
		$_SESSION['item_dispatch_success'] = "Details successfully added.";
		}
	}
}

	$n=0;
	$itemArray=array();
	$item="SELECT itemId,name,unit FROM item where status='A' ORDER BY name";
	$itemRes=mysql_query($item);
	while($itemRow=mysql_fetch_array($itemRes))
	{
		$itemArray['itemId'][$n]=$itemRow['itemId'];
		$itemArray['name'][$n]=$itemRow['name'];
				
		//get first Item Unit
		if($n==0)
		{
			$unit=$itemRow['unit'];
		}
		$n++;
	}
	if(isset($unit))
	{
		$itemUnit="SELECT * FROM unit WHERE unitId=".$unit;
		$itemUnitRes=mysql_query($itemUnit);
		$itemUnitRow=mysql_fetch_array($itemUnitRes);
		$itemUnitVar=$itemUnitRow['unit'];
	}
	else
	{
		$itemUnitVar='';
	}
	
	$m=0;
	$staffArray=array();
	$staff="SELECT staffId,name FROM staff ORDER BY name";
	$staffRes=mysql_query($staff);
	while($staffRow=mysql_fetch_array($staffRes))
	{
		$staffArray['staffId'][$m]=$staffRow['staffId'];
		$staffArray['name'][$m]=$staffRow['name'];
		$m++;
	}
	
//GET ALL RECORDS TO DISPLAY IN DATATABLE
	$dispatchArrayTable=array();
	$selectDispatchItem="SELECT i.itemId,i.name as itemName,d.*,s.staffId,s.name as staffName FROM itemdispatch d 
						LEFT JOIN item i ON d.itemId=i.itemId
						LEFT JOIN staff s ON d.staffId=s.staffId
						ORDER BY i.name";
	$selectDispatchItemRes=mysql_query($selectDispatchItem);
	
	if(mysql_num_rows($selectDispatchItemRes)>0)
	{
		$t=0;
		while($selectDispatchItemRow=mysql_fetch_array($selectDispatchItemRes))
		{
		$dispatchArrayTable[$t]['staffName']      = $selectDispatchItemRow['staffName'];
		$dispatchArrayTable[$t]['itemName']      = $selectDispatchItemRow['itemName'];
		$dispatchArrayTable[$t]['dispDate']         = date('d-m-Y',strtotime($selectDispatchItemRow['dispDate']));
		$dispatchArrayTable[$t]['qty']    = $selectDispatchItemRow['qty'];
		$dispatchArrayTable[$t]['notes']   = $selectDispatchItemRow['notes'];
		$dispatchArrayTable[$t]['itemDispatchId']   = $selectDispatchItemRow['itemDispatchId'];
		$t++;             
		}
	
	}
	
include("bottom.php");
$smarty->assign("dispatchArrayTable",$dispatchArrayTable);
$smarty->assign("staffArray",$staffArray);
$smarty->assign("itemArray",$itemArray);
$smarty->assign("itemUnitVar",$itemUnitVar);
$smarty->assign("item_dispatch_error",$_SESSION['item_dispatch_error']);
$smarty->assign("item_dispatch_success",$_SESSION['item_dispatch_success']);

$smarty->display("itemDispatch.tpl");
?>