<?php
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
  if(isset($_POST['submitBtn']) && $_POST['accountTypeId'] > 0)
  {
    $accountTypeId = isset($_POST['accountTypeId']) ? $_POST['accountTypeId'] : 0;
    $accountName   = isset($_POST['accountName']) && $_POST['accountName'] != '' ? $_POST['accountName'] : '';
    $contactNo     = isset($_POST['contactNo']) && strlen($_POST['contactNo']) > 0 ? $_POST['contactNo'] : '';
    $placeId       = isset($_POST['placeId']) ? $_POST['placeId'] : 0;
    $notes         = isset($_POST['notes']) && strlen($_POST['notes']) > 0? $_POST['notes'] : "";
  	
    $insertAccount  = "INSERT INTO account (accountTypeId,name,placeId,contactNo,notes)
                    VALUES(".$accountTypeId.",'".$accountName."',".$placeId.",'".$contactNo."',
                           '".$notes."')";
    $insertAccountRes = mysql_query($insertAccount);
    if(!$insertAccountRes)
    {
    	echo "Error in Inserting ".mysql_error();
    }
    else
    {
      header("Location:account.php");
    }
  }
  
  $i=0;
  $accountTypeArr = array();
  $selAccType = "SELECT accountTypeId,accountType
                   FROM accounttype 
               ORDER BY accountType";
  $selAccTypeRes = mysql_query($selAccType);
  while($accRow = mysql_fetch_array($selAccTypeRes))
  {
    $accountTypeArr['accountTypeId'][$i] = $accRow['accountTypeId'];
    $accountTypeArr['accountType'][$i]  = $accRow['accountType'];
    $i++;
  }

  $j=0;
  $placeArr = array();
  $selPlace = "SELECT placeId,place
                 FROM place
             ORDER BY place";
  $selPlaceRes = mysql_query($selPlace);
  while($placeRow = mysql_fetch_array($selPlaceRes))
  {
  	$placeArr['placeId'][$j]  = $placeRow['placeId'];
  	$placeArr['place'][$j]    = $placeRow['place'];
  	$j++;
  }

}
include("bottom.php");
$smarty->assign("accountTypeArr",$accountTypeArr);
$smarty->assign("placeArr",$placeArr);
$smarty->display("account.tpl");
?>