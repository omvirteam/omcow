<?php
include("./include/config.inc.php");
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
    header("Location:checkLogin.php");
    exit;
}
if(!isset($_GET['staff_id'])){
	echo "<h2>404<br/>page not found";
	exit;
}
include("./include/fpdf/fpdf.php");

//fetch staff data
$qry = "SELECT name, userName FROM staff WHERE staffId = ".mysql_real_escape_string($_GET['staff_id'])." LIMIT 1";
$res = mysql_query($qry);
if(mysql_num_rows($res) != 1){
	echo "<h2>404<br/>page not found";
	exit;
}
$result = mysql_fetch_array($res);

//create a FPDF object
$pdf=new FPDF();

//set document properties
$pdf->SetTitle('BILL');

$pdf->SetFont('Helvetica','B',20);
$pdf->SetTextColor(50,60,100);

//set up a page
$pdf->AddPage('P'); 
$pdf->SetDisplayMode('real','default');

$pdf->SetXY(50,20);
$pdf->SetDrawColor(50,60,100);
$pdf->Cell(100,10,'BILL',1,0,'C',0);

$pdf->SetXY (10,50);
$pdf->SetFontSize(10);
$pdf->Write(5,'Name:');

$pdf->SetXY (45,50);
$pdf->SetFontSize(10);
$pdf->Write(5,$result['name']);

$pdf->SetXY (10,55);
$pdf->SetFontSize(10);
$pdf->Write(5,'Nick Name:');

$pdf->SetXY (45,55);
$pdf->SetFontSize(10);
$pdf->Write(5,$result['userName']);

$pdf->SetXY (10,60);
$pdf->SetFontSize(10);
$pdf->Write(5,'Bill generate Date:');

$pdf->SetXY (45,60);
$pdf->SetFontSize(10);
$pdf->Write(5,date('d-m-Y'));

$pdf->Output('example1.pdf','I'); 
?>