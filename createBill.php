<?php
include('include/config.inc.php');
if(!isset($_SESSION['s_activId']) && !isset($_SESSION['s_userType']))
{
  header("Location:checkLogin.php");
  exit;
}
else
{
  $selectMilkSell = "SELECT customerId,rate
                         FROM milksell
                        WHERE billCreated = 'n'
                     GROUP BY customerId,rate";
  $selectMilkSellRes = mysql_query($selectMilkSell);
  $flag = 0;
  while($dataRow = mysql_fetch_array($selectMilkSellRes))
  {
    $totAmount = 0;
    $rate  = $dataRow['rate'];
    $totQty = 0;
    $selectMilkSell2 = "SELECT milkSellId,milkGiven,rate
                            FROM milksell
                           WHERE customerId = ".$dataRow['customerId']."
                             AND rate = ".$dataRow['rate'];
    $selectMilkSellRes2 = mysql_query($selectMilkSell2);
    while($amtRow = mysql_fetch_array($selectMilkSellRes2))
    {
      $totAmount += $amtRow['milkGiven'] * $amtRow['rate'];
      $totQty    += $amtRow['milkGiven'];
      $updateBillStatus = "UPDATE milksell 
                                SET billCreated = 'y'
                              WHERE milkSellId = ".$amtRow['milkSellId'];
      $updateBillStatusRes = mysql_query($updateBillStatus);
    }

    $accountIdBill = 5;
    $insTransQry = "INSERT INTO transaction (transType,accountIdCr,customerIdDr,amount,transDate,notes,rate,totalQty,	cron_bill_id)
                      VALUES (0,".$accountIdBill.",".$dataRow['customerId'].",".$totAmount.",'".date("Y-m-d")."',
                             'Auto bill created',".$rate.",".$totQty.",".$cronBillId.")";
    $insTransQryRes = mysql_query($insTransQry);

    if($flag != $dataRow['customerId'])
    {
      $cronBillSql = "INSERT INTO cron_bill (cron_month,cron_year,created_date,customer_id,bill_amount)"
        . "VALUES(".date('m').",".date('Y').",'".date("Y-m-d")."',".$dataRow['customerId'].",".$totAmount.")";
      
      $result = mysql_query($cronBillSql);
      $cronBillId = mysql_insert_id();
      $flag = $dataRow['customerId'];
    }
    else
    {
      $cronBillSql = "UPDATE cron_bill SET bill_amount = bill_amount + ".$totAmount." 
      	                 WHERE customer_id = ".$flag;
      $result = mysql_query($cronBillSql);
    }
  }

  $selItemSell = "SELECT itemSellId,customerId,qty,rate,amount
                      FROM itemsell
                     WHERE billCreated = 'n'";
  $selItemSellRes = mysql_query($selItemSell);
  while($itemRow = mysql_fetch_array($selItemSellRes))
  {
    $updateItemStatus = "UPDATE itemsell 
                              SET billCreated = 'y'
                              WHERE itemSellId = ".$itemRow['itemSellId'];
    $updateItemStatusRes = mysql_query($updateItemStatus);

    $accountIdBill = 5;
    $insTransQry = "INSERT INTO transaction (transType,accountIdCr,customerIdDr,amount,transDate,notes,rate,totalQty,sellItemId)
                      VALUES (0,".$accountIdBill.",".$itemRow['customerId'].",".$itemRow['amount'].",'".date("Y-m-d")."',
                             'Item bill created',".$itemRow['rate'].",".$itemRow['qty'].",".$itemRow['itemSellId'].")";
    $insTransQryRes = mysql_query($insTransQry);

    $cronBillSql = "SELECT * FROM cron_bill 
      									WHERE cron_month = '".date('m')."'
      									  AND cron_year = '".date('Y')."'
      									  AND customer_id = ".$itemRow['customerId'];
    $result = mysql_query($cronBillSql);
    if(mysql_num_rows($result) > 0)
    {
      $cronBillSql = "UPDATE cron_bill SET bill_amount = bill_amount + ".$itemRow['amount']." 
      	                 WHERE customer_id = ".$itemRow['customerId'];
      $result = mysql_query($cronBillSql);
    }
    else
    {
      $cronBillSql = "INSERT INTO cron_bill (cron_month,cron_year,created_date,customer_id,bill_amount)"
        . "VALUES(".date('m').",".date('Y').",'".date("Y-m-d")."',".$itemRow['customerId'].",".$itemRow['amount'].")";
      $result = mysql_query($cronBillSql);
    }
  }
}
?>