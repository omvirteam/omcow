<?php
/************************************************
 *		Cron Job Script For Create Bill
 *		::: LOGIC :::
 *		Cron_bill table --- When cronBill.php file run, the previous month entry will be inserted
 *							into this table.
 *		
 *	1). Check that previous month and year billl is created for the specific customer into the cron_bill table or not.
 *	2). if the previous month and year bill is not generated then generate it.
 *	3). and check that in makeSell table, is previous month and year bill is created or not (using billCreated='n')
 *	4). If the billCreated == 'n' in milkSell table, then update it. that means make it 'y' instead of 'n'.
 *  5). and also check that in itemSell table, customer has purchased extra item or not 
 *  6). If customer has purchased extra item, then check their billCreated status.
 *	6). If the billCreated == 'n' in itemsell table, then update it. and make it 'y'.
 *
 *************************************************/
include("include/conn.inc");
ini_set("max_execution_time","3000000");
date_default_timezone_set("Asia/Kolkata");

$getPreviousMonthYear = date("Y-m",strtotime("-1 Months"));

$explodeMonthYear = explode("-",$getPreviousMonthYear);

$getMonth = $explodeMonthYear[1];
$getYear = $explodeMonthYear[0];

$qrySelCustomer = "SELECT * FROM customer ORDER BY customerId ASC";
$resSelCustomer = mysql_query($qrySelCustomer);
$numOfCustomer = mysql_num_rows($resSelCustomer);

if($numOfCustomer>0)
{
	while($qFetchCustomer = mysql_fetch_array($resSelCustomer))
	{
		$customerId = $qFetchCustomer["customerId"];
		
		$qrySelCheckCron = "SELECT * FROM cron_bill WHERE cron_month='".$getMonth."' AND cron_year='".$getYear."' AND customer_id='".$customerId."'";
		$resSelCheckCron = mysql_query($qrySelCheckCron);
		$numOfRowCheckCron = mysql_num_rows($resSelCheckCron);

		if($numOfRowCheckCron==0)
		{
			$qryInsCron = "INSERT INTO cron_bill (cron_month,cron_year,created_date,customer_id) VALUES ('".$getMonth."','".$getYear."','".date('Y-m-d H:i:s')."','".$customerId."')";
			$resInsCron = mysql_query($qryInsCron);
			$cronBillId = mysql_insert_id();

			$qrySelMilksell = "SELECT * FROM milksell WHERE MONTH(milkSellDate)='".$getMonth."' AND YEAR(milkSellDate)='".$getYear."' AND customerId='".$customerId."' AND billCreated='n'";
			$resSelMilksell = mysql_query($qrySelMilksell);
			$numOfRows = mysql_num_rows($resSelMilksell);

			if($numOfRows>0)
			{
				//ADDED ON 10-OCT-2015
				$totAmount = 0;
				$totQty = 0;
				$rate = 0;
				
				while($qFetchMilkSell = mysql_fetch_array($resSelMilksell))
				{
					//ADDED ON 10-OCT-2015
					$rate = $qFetchMilkSell['rate'];
					$totAmount += $qFetchMilkSell['milkGiven'] * $qFetchMilkSell['rate'];
      				$totQty    += $qFetchMilkSell['milkGiven'];
	  
					$millSellId = $qFetchMilkSell["milkSellId"];
					
					$qryUpdMilkSell = "UPDATE milksell SET billCreated='y' WHERE milkSellId='".$millSellId."'";
					mysql_query($qryUpdMilkSell);
				}
				
				//ADDED ON 10-OCT-2015
				$accountIdBill = 5;
   				$insTransQry = "INSERT INTO transaction (transType,accountIdCr,customerIdDr,amount,transDate,notes,rate,totalQty,	cron_bill_id) VALUES (0,".$accountIdBill.",".$customerId.",".$totAmount.",'".date("Y-m-d")."', 'Auto bill created',".$rate.",".$totQty.",".$cronBillId.")";
    			$insTransQryRes = mysql_query($insTransQry);
				
			}
			
			$qrySelItemSell = "SELECT * FROM itemsell WHERE MONTH(sellDate)='".$getMonth."' AND YEAR(sellDate)='".$getYear."' AND customerId='".$customerId."' AND billCreated='n'";
			$resSelItemSell = mysql_query($qrySelItemSell);
			if(mysql_num_rows($resSelItemSell)>0)
			{
				//ADDED ON 10-OCT-2015
				$totAmount = 0;
				$totQty = 0;
				$rate = 0;
				
				while($qFetchItemSell = mysql_fetch_array($resSelItemSell))
				{
					//ADDED ON 10-OCT-2015
					$rate = $qFetchItemSell['rate'];
					$totAmount += $qFetchItemSell['qty'] * $qFetchItemSell['rate'];
      				$totQty    += $qFetchItemSell['qty'];
					
					$itemSellID = $qFetchItemSell["itemSellId"];
					$qryUpdItemSell = "UPDATE itemsell SET billCreated='y' WHERE itemSellId='".$itemSellID."'";
					mysql_query($qryUpdItemSell);
				}
				
				//ADDED ON 10-OCT-2015
				$accountIdBill = 5;
   				$insTransQry = "INSERT INTO transaction (transType,accountIdCr,customerIdDr,amount,transDate,notes,rate,totalQty,	cron_bill_id) VALUES (0,".$accountIdBill.",".$customerId.",".$totAmount.",'".date("Y-m-d")."', 'Auto bill created',".$rate.",".$totQty.",".$cronBillId.")";
    			$insTransQryRes = mysql_query($insTransQry);
			}
			
		}
	}
}
?>